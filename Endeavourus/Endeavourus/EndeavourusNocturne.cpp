#include "stdafx.h"
#include "EndeavourusNocturne.h"
#include "SmiteManager.h"
#include "SpellShieldManager.h"
#include "BaseSettings.h"
#include "SmiteDrawingManager.h"
#include "NocturneDrawingManager.h"
#include "NocturneMenuManager.h"
#include "NocturneLogicManager.h"
#include "EndeavourusActivator.h"

void EndeavourusNocturne::Init()
{
	SmiteManager::Init();
	SpellShieldManager::Init();
	NocturneMenuManager::Init();
	NocturneLogicManager::Init();
	EndeavourusActivator::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, OnCastStart);
}

/*
 * This gets called 30 times per second -> Logic goes here
*/
void __cdecl EndeavourusNocturne::OnUpdate(void * UserData)
{
	if (!Game::IsAvailable())
	{
		return;
	}


	SmiteManager::PerformSmiteLogic();

	if (BaseSettings::isChampEnabled()) {
		NocturneLogicManager::PerformLogic();
	}
}

/*
 * This gets called X times per second, where X is your league fps. -> Important stuff only
*/
void __cdecl EndeavourusNocturne::OnDraw(void * UserData)
{
	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();
	NocturneDrawingManager::Draw();
}

/*
 * Menu gets drawn here
*/
void __cdecl EndeavourusNocturne::OnDrawMenu(void * UserData)
{
	NocturneMenuManager::DrawMenu();
}

void __cdecl EndeavourusNocturne::OnCastStart(void* heroCasting, PSDK_SPELL_CAST SpellCast, void* UserData)
{
	GameObject* target = pSDK->EntityManager->GetObjectFromPTR(SpellCast->TargetObject);
	GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(heroCasting);

	if (target != nullptr && hero != nullptr && hero->GetName() != Player.GetName() && target->GetName() == Player.GetName())
	{
		if (SpellShieldManager::CanShieldSpell(SpellCast->Spell.Name) && NocturneLogicManager::WCast.IsReady() && Player.GetManaPercent() >= 5) {
			NocturneLogicManager::NOC_SHOULD_CAST_SHIELD = true;
		}
	}
}

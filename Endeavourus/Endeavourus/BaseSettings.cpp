#include "stdafx.h"
#include "BaseSettings.h"
#include "EndeavourusMiscUtils.h"
#include "LogicConstants.h"
#include "DrawConstants.h"
#include "JungleConstants.h"
#include "BaseMenuManager.h"
#include "GeneralConstants.h"
#include "EndeavourusMenu.h"

//Champ Settings
bool BaseSettings::isQEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_COMBO, BaseMenuManager::GetComboResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_COMBO, BaseMenuManager::GetComboResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_COMBO, BaseMenuManager::GetComboResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_COMBO, BaseMenuManager::GetComboResourceManagement().isRDefaultValEnabled());
}

bool BaseSettings::isQPredictionEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_COMBO_PREDICTION_ENABLED, BaseMenuManager::GetComboResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_COMBO_PREDICTION_ENABLED, BaseMenuManager::GetComboResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_COMBO_PREDICTION_ENABLED, BaseMenuManager::GetComboResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledCombo()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_COMBO_PREDICTION_ENABLED, BaseMenuManager::GetComboResourceManagement().isRPredictionEnabled());
}

HitChance getHitChance(int hitChanceVal)
{
	switch (hitChanceVal)
	{
	case 0:
		return HitChance::Low;

	case 1:
		return HitChance::Medium;

	case 2:
		return HitChance::High;

	case 3:
		return HitChance::VeryHigh;

	case 4:
		return HitChance::Immobile;

	default:
		return HitChance::Medium;

	}
}

HitChance BaseSettings::getQPredictionCombo()
{
	int dropDownSelection = EndeavourusMiscUtils::GetDropDownSetting(Q_COMBO_PREDICTION_LEVEL, BaseMenuManager::GetComboResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionCombo()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_COMBO_PREDICTION_LEVEL, BaseMenuManager::GetComboResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionCombo()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_COMBO_PREDICTION_LEVEL, BaseMenuManager::GetComboResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionCombo()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_COMBO_PREDICTION_LEVEL, BaseMenuManager::GetComboResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

int BaseSettings::getComboQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_COMBO_RESOURCE, defaultVal);
}

int BaseSettings::getComboWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_COMBO_RESOURCE, defaultVal);
}

int BaseSettings::getComboEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_COMBO_RESOURCE, defaultVal);
}

int BaseSettings::getComboRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_COMBO_RESOURCE, defaultVal);
}

bool BaseSettings::isQEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_HARASS, BaseMenuManager::GetHarassResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_HARASS, BaseMenuManager::GetHarassResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_HARASS, BaseMenuManager::GetHarassResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_HARASS, BaseMenuManager::GetHarassResourceManagement().isRDefaultValEnabled());
}

bool BaseSettings::isQPredictionEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_HARASS_PREDICTION_ENABLED, BaseMenuManager::GetHarassResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_HARASS_PREDICTION_ENABLED, BaseMenuManager::GetHarassResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_HARASS_PREDICTION_ENABLED, BaseMenuManager::GetHarassResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledHarass()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_HARASS_PREDICTION_ENABLED, BaseMenuManager::GetHarassResourceManagement().isRPredictionEnabled());
}

HitChance BaseSettings::getQPredictionHarass()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(Q_HARASS_PREDICTION_LEVEL, BaseMenuManager::GetHarassResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionHarass()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_HARASS_PREDICTION_LEVEL, BaseMenuManager::GetHarassResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionHarass()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_HARASS_PREDICTION_LEVEL, BaseMenuManager::GetHarassResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionHarass()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_HARASS_PREDICTION_LEVEL, BaseMenuManager::GetHarassResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

int BaseSettings::getHarassQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_HARASS_RESOURCE, defaultVal);
}

int BaseSettings::getHarassWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_HARASS_RESOURCE, defaultVal);
}

int BaseSettings::getHarassEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_HARASS_RESOURCE, defaultVal);
}

int BaseSettings::getHarassRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_HARASS_RESOURCE, defaultVal);
}

bool BaseSettings::isQEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_CLEAR, BaseMenuManager::GetClearResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_CLEAR, BaseMenuManager::GetClearResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_CLEAR, BaseMenuManager::GetClearResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_CLEAR, BaseMenuManager::GetClearResourceManagement().isRDefaultValEnabled());
}

bool BaseSettings::isQPredictionEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_CLEAR_PREDICTION_ENABLED, BaseMenuManager::GetClearResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_CLEAR_PREDICTION_ENABLED, BaseMenuManager::GetClearResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_CLEAR_PREDICTION_ENABLED, BaseMenuManager::GetClearResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledClear()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_CLEAR_PREDICTION_ENABLED, BaseMenuManager::GetClearResourceManagement().isRPredictionEnabled());
}

HitChance BaseSettings::getQPredictionClear()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(Q_CLEAR_PREDICTION_LEVEL, BaseMenuManager::GetClearResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionClear()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_CLEAR_PREDICTION_LEVEL, BaseMenuManager::GetClearResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionClear()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_CLEAR_PREDICTION_LEVEL, BaseMenuManager::GetClearResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionClear()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_CLEAR_PREDICTION_LEVEL, BaseMenuManager::GetClearResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

int BaseSettings::getClearQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_CLEAR_RESOURCE, defaultVal);
}

int BaseSettings::getClearWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_CLEAR_RESOURCE, defaultVal);
}

int BaseSettings::getClearEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_CLEAR_RESOURCE, defaultVal);
}

int BaseSettings::getClearRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_CLEAR_RESOURCE, defaultVal);
}

bool BaseSettings::isQEnabledLH()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_LH, BaseMenuManager::GetLastHitResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledLH()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_LH, BaseMenuManager::GetLastHitResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledLH()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_LH, BaseMenuManager::GetLastHitResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledLH()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_LH, BaseMenuManager::GetLastHitResourceManagement().isRDefaultValEnabled());
}

bool BaseSettings::isQPredictionEnabledLh()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_LH_PREDICTION_ENABLED, BaseMenuManager::GetLastHitResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledLh()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_LH_PREDICTION_ENABLED, BaseMenuManager::GetLastHitResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledLh()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_LH_PREDICTION_ENABLED, BaseMenuManager::GetLastHitResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledLh()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_LH_PREDICTION_ENABLED, BaseMenuManager::GetLastHitResourceManagement().isRPredictionEnabled());
}

HitChance BaseSettings::getQPredictionLh()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(Q_LH_PREDICTION_LEVEL, BaseMenuManager::GetLastHitResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionLh()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_LH_PREDICTION_LEVEL, BaseMenuManager::GetLastHitResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionLh()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_LH_PREDICTION_LEVEL, BaseMenuManager::GetLastHitResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionLh()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_LH_PREDICTION_LEVEL, BaseMenuManager::GetLastHitResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

int BaseSettings::getLHQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_LH_RESOURCE, defaultVal);
}

int BaseSettings::getLHWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_LH_RESOURCE, defaultVal);
}

int BaseSettings::getLHEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_LH_RESOURCE, defaultVal);
}

int BaseSettings::getLHRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_LH_RESOURCE, defaultVal);
}

bool BaseSettings::isQEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_ESCAPE, BaseMenuManager::GetEscapeResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_ESCAPE, BaseMenuManager::GetEscapeResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_ESCAPE, BaseMenuManager::GetEscapeResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_ESCAPE, BaseMenuManager::GetEscapeResourceManagement().isRDefaultValEnabled());
}

HitChance BaseSettings::getQPredictionEscape()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(Q_ESCAPE_PREDICTION_LEVEL, BaseMenuManager::GetEscapeResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionEscape()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_ESCAPE_PREDICTION_LEVEL, BaseMenuManager::GetEscapeResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionEscape()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_ESCAPE_PREDICTION_LEVEL, BaseMenuManager::GetEscapeResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionEscape()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_ESCAPE_PREDICTION_LEVEL, BaseMenuManager::GetEscapeResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

bool BaseSettings::isQPredictionEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_ESCAPE_PREDICTION_ENABLED, BaseMenuManager::GetEscapeResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_ESCAPE_PREDICTION_ENABLED, BaseMenuManager::GetEscapeResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_ESCAPE_PREDICTION_ENABLED, BaseMenuManager::GetEscapeResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledEscape()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_ESCAPE_PREDICTION_ENABLED, BaseMenuManager::GetEscapeResourceManagement().isRPredictionEnabled());
}

int BaseSettings::getEscapeQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_ESCAPE_RESOURCE, defaultVal);
}

int BaseSettings::getEscapeWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_ESCAPE_RESOURCE, defaultVal);
}

int BaseSettings::getEscapeEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_ESCAPE_RESOURCE, defaultVal);
}

int BaseSettings::getEscapeRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_ESCAPE_RESOURCE, defaultVal);
}

bool BaseSettings::isQEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_Q_KS, BaseMenuManager::GetKsResourceManagement().isQDefaultValEnabled());
}

bool BaseSettings::isWEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_W_KS, BaseMenuManager::GetKsResourceManagement().isWDefaultValEnabled());
}

bool BaseSettings::isEEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_E_KS, BaseMenuManager::GetKsResourceManagement().isEDefaultValEnabled());
}

bool BaseSettings::isREnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_R_KS, BaseMenuManager::GetKsResourceManagement().isRDefaultValEnabled());
}

int BaseSettings::getKsQResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_Q_KS_RESOURCE, defaultVal);
}

int BaseSettings::getKsWResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_W_KS_RESOURCE, defaultVal);
}

int BaseSettings::getKsEResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_E_KS_RESOURCE, defaultVal);
}

int BaseSettings::getKsRResourceLimit(int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_R_KS_RESOURCE, defaultVal);
}

bool BaseSettings::isQPredictionEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_KS_PREDICTION_ENABLED, BaseMenuManager::GetKsResourceManagement().isQPredictionEnabled());
}

bool BaseSettings::isWPredictionEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_KS_PREDICTION_ENABLED, BaseMenuManager::GetKsResourceManagement().isWPredictionEnabled());
}

bool BaseSettings::isEPredictionEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_KS_PREDICTION_ENABLED, BaseMenuManager::GetKsResourceManagement().isEPredictionEnabled());
}

bool BaseSettings::isRPredictionEnabledKs()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_KS_PREDICTION_ENABLED, BaseMenuManager::GetKsResourceManagement().isRPredictionEnabled());
}

HitChance BaseSettings::getQPredictionKs()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(Q_KS_PREDICTION_LEVEL, BaseMenuManager::GetKsResourceManagement().getQPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getWPredictionKs()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(W_KS_PREDICTION_LEVEL, BaseMenuManager::GetKsResourceManagement().getWPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getEPredictionKs()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(E_KS_PREDICTION_LEVEL, BaseMenuManager::GetKsResourceManagement().getEPredictionMode());
	return getHitChance(dropDownSelection);
}

HitChance BaseSettings::getRPredictionKs()
{
	int dropDownSelection = EndeavourusMiscUtils::GetBoolSetting(R_KS_PREDICTION_LEVEL, BaseMenuManager::GetKsResourceManagement().getRPredictionMode());
	return getHitChance(dropDownSelection);
}

bool BaseSettings::isQBlockedOnChampionCombo(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_COMBO + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionCombo(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_COMBO + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionCombo(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_COMBO + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionCombo(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_COMBO + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedOnChampionHarass(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_HARASS + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionHarass(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_HARASS + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionHarass(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_HARASS + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionHarass(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_HARASS + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedOnChampionClear(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_CLEAR + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionClear(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_CLEAR + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionClear(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_CLEAR + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionClear(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_CLEAR + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedOnChampionLh(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_LH + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionLh(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_LH + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionLh(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_LH + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionLh(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_LH + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedOnChampionKs(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_KS + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionKs(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_KS + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionKs(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_KS + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionKs(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_KS + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedOnChampionEscape(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_Q_ESCAPE + "_" + enemyName, false);
}

bool BaseSettings::isWBlockedOnChampionEscape(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_W_ESCAPE + "_" + enemyName, false);
}

bool BaseSettings::isEBlockedOnChampionEscape(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_E_ESCAPE + "_" + enemyName, false);
}

bool BaseSettings::isRBlockedOnChampionEscape(std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_CHAMPION_BLOCK_R_ESCAPE + "_" + enemyName, false);
}

bool BaseSettings::isQBlockedSpellComboGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_COMBO_GROUP, false);
}

bool BaseSettings::isWBlockedSpellComboGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_COMBO_GROUP, false);
}

bool BaseSettings::isEBlockedSpellComboGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_COMBO_GROUP, false);
}

bool BaseSettings::isRBlockedSpellComboGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_COMBO_GROUP, false);
}

bool BaseSettings::isQBlockedSpellHarassGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_HARASS_GROUP, false);
}

bool BaseSettings::isWBlockedSpellHarassGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_HARASS_GROUP, false);
}

bool BaseSettings::isEBlockedSpellHarassGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_HARASS_GROUP, false);
}

bool BaseSettings::isRBlockedSpellHarassGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_HARASS_GROUP, false);
}

bool BaseSettings::isQBlockedSpellClearGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_CLEAR_GROUP, false);
}

bool BaseSettings::isWBlockedSpellClearGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_CLEAR_GROUP, false);
}

bool BaseSettings::isEBlockedSpellClearGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_CLEAR_GROUP, false);
}

bool BaseSettings::isRBlockedSpellClearGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_CLEAR_GROUP, false);
}

bool BaseSettings::isQBlockedSpellLhGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_LH_GROUP, false);
}

bool BaseSettings::isWBlockedSpellLhGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_LH_GROUP, false);
}

bool BaseSettings::isEBlockedSpellLhGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_LH_GROUP, false);
}

bool BaseSettings::isRBlockedSpellLhGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_LH_GROUP, false);
}

bool BaseSettings::isQBlockedSpellEscapeGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_ESCAPE_GROUP, false);
}

bool BaseSettings::isWBlockedSpellEscapeGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_ESCAPE_GROUP, false);
}

bool BaseSettings::isEBlockedSpellEscapeGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_ESCAPE_GROUP, false);
}

bool BaseSettings::isRBlockedSpellEscapeGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_ESCAPE_GROUP, false);
}

bool BaseSettings::isQBlockedSpellKsGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_Q_KS_GROUP, false);
}

bool BaseSettings::isWBlockedSpellKsGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_W_KS_GROUP, false);
}

bool BaseSettings::isEBlockedSpellKsGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_E_KS_GROUP, false);
}

bool BaseSettings::isRBlockedSpellKsGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMPION_BLOCK_R_KS_GROUP, false);
}

bool BaseSettings::isSmiteGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_GROUP);
}

bool BaseSettings::isSmiteEnabled()
{
	return EndeavourusMenu::Get<EndeavourusKeyToggle>(EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_SMITE)).Toggle;
}

int BaseSettings::getSmiteMode()
{
	return EndeavourusMiscUtils::GetDropDownSetting(SMITE_MODE);
}

bool BaseSettings::isSmiteAssistKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SMITE_ASSIST_KEY)).Active;
}

bool BaseSettings::isSmiteAllMonsterGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_ALL_MONSTER_GROUP);
}

bool BaseSettings::isSmitePlayerGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_PLAYER_GROUP);
}

bool BaseSettings::isSmiteBuffGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_BUFF_MONSTER_GROUP);
}

bool BaseSettings::isSmiteEpicGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_GROUP);
}

bool BaseSettings::isSmitePlayedRedEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_PLAYER_RED_ON_COMBO, true);
}

bool BaseSettings::isSmitePlayerBlueEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_PLAYER_BLUE_ON_COMBO, true);
}

bool BaseSettings::isSmitePlayerKsEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_PLAYER_KS_BLUE, true);
}

bool BaseSettings::isSmiteCreepGroupOpen()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_CREEP_MONSTER_GROUP);
}

bool BaseSettings::isSmiteWolfEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_MONSTER_WOLF, true);
}

bool BaseSettings::isSmiteRaptorEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_MONSTER_RAPTOR, true);
}

bool BaseSettings::isSmiteKrugEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_MONSTER_KRUG, true);
}

bool BaseSettings::isSmiteGrompEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_MONSTER_GROMP, true);
}

bool BaseSettings::isSmiteCrabEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_MONSTER_CRAB, true);
}

bool BaseSettings::isSmiteBlueEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_BUFF_MONSTER_BLUE, true);
}

bool BaseSettings::isSmiteRedEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_BUFF_MONSTER_RED, true);
}

bool BaseSettings::isSmiteBaronEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_BARON, true);
}

bool BaseSettings::isSmiteRiftEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_RIFT, true);
}

bool BaseSettings::isSmiteElderEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_ELDER, true);
}

bool BaseSettings::isSmiteInfernalEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_INFERNAL, true);
}

bool BaseSettings::isSmiteMountainEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_MOUNTAIN, true);
}

bool BaseSettings::isSmiteCloudEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_CLOUD, true);
}

bool BaseSettings::isSmiteOceanEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(SMITE_EPIC_MONSTER_OCEAN, true);
}

bool BaseSettings::isSmiteDrawEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(DRAW_SMITE_RANGE_ENABLED, true);
}

bool BaseSettings::getSmiteDrawGroup()
{
	return EndeavourusMiscUtils::GetBoolSetting(DRAW_SMITE_GROUP, false);
}

SDKCOLOR BaseSettings::getSmiteRangeColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(DRAW_SMITE_RANGE_COLOUR));
}

bool BaseSettings::isDrawQRangeEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(Q_RANGE_DRAWING_ENABLED, true);
}

bool BaseSettings::isDrawWRangeEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(W_RANGE_DRAWING_ENABLED, true);
}

bool BaseSettings::isDrawERangeEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(E_RANGE_DRAWING_ENABLED, true);
}

bool BaseSettings::isDrawRRangeEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(R_RANGE_DRAWING_ENABLED, true);
}

SDKCOLOR BaseSettings::getQRangeColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(Q_RANGE_DRAWING_COLOUR));
}

SDKCOLOR BaseSettings::getWRangeColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(W_RANGE_DRAWING_COLOUR));
}

SDKCOLOR BaseSettings::getERangeColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(E_RANGE_DRAWING_COLOUR));
}

SDKCOLOR BaseSettings::getRRangeColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(R_RANGE_DRAWING_COLOUR));
}

SDKCOLOR BaseSettings::getSelectedChampColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(SELECTED_CHAMP_DRAWING_COLOUR));
}

// Smite settings
bool BaseSettings::isEnableSmite()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_SMITE, true);
}

bool BaseSettings::isEnableDrawings()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_DRAWINGS);
}

bool BaseSettings::isEnableActivator()
{
	return NULL;
	//return EndeavourusMiscUtils::GetBoolSetting(ENABLE_ACTIVATOR);
}

bool BaseSettings::isChampEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(CHAMP_ENABLED, true);
}

bool BaseSettings::isComboKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(COMBO_KEY)).Active;
}

bool BaseSettings::isMixedKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(Mixed_KEY)).Active;
}

bool BaseSettings::isClearKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(Clear_KEY)).Active;
}

bool BaseSettings::isLasthitKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(Lasthit_KEY)).Active;
}

bool BaseSettings::isEscapeKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(Escape_KEY)).Active;
}
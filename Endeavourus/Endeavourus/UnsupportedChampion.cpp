#include "stdafx.h"
#include "UnsupportedChampion.h"
#include "SmiteManager.h"
#include "SmiteDrawingManager.h"
#include "UnsupportedMenuManager.h"
#include "EndeavourusActivator.h"

void UnsupportedChampion::Init()
{
	SmiteManager::Init();
	UnsupportedMenuManager::Init();
	SmiteManager::Init();
	EndeavourusActivator::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
}

/*
 * This gets called 30 times per second -> Logic goes here
*/
void __cdecl UnsupportedChampion::OnUpdate(void * UserData)
{
	if (!Game::IsAvailable())
	{
		return;
	}

	SmiteManager::PerformSmiteLogic();
}

/*
 * This gets called X times per second, where X is your league fps. -> Important stuff only
*/
void __cdecl UnsupportedChampion::OnDraw(void * UserData)
{
	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();
}

/*
 * Menu gets drawn here
*/
void __cdecl UnsupportedChampion::OnDrawMenu(void * UserData)
{
	UnsupportedMenuManager::DrawMenu();
}
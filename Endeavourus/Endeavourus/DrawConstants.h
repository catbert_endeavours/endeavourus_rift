#pragma once
#include <string>

const std::string DRAWING_GROUP = "_DRAWINGS_GROUP";

// Drawing Consts
const std::string ENABLE_DRAWINGS = "_DRAW_ENABLED";

// Drawing Consts
const std::string Q_RANGE_DRAWING_ENABLED = "_Q_RANGE_DRAWING_ENABLED";
const std::string W_RANGE_DRAWING_ENABLED = "_W_RANGE_DRAWING_ENABLED";
const std::string E_RANGE_DRAWING_ENABLED = "_E_RANGE_DRAWING_ENABLED";
const std::string R_RANGE_DRAWING_ENABLED = "_R_RANGE_DRAWING_ENABLED";

// Drawing Consts
const std::string Q_RANGE_DRAWING_COLOUR = "_Q_RANGE_DRAWING_COLOUR";
const std::string W_RANGE_DRAWING_COLOUR = "_W_RANGE_DRAWING_COLOUR";
const std::string E_RANGE_DRAWING_COLOUR = "_E_RANGE_DRAWING_COLOUR";
const std::string R_RANGE_DRAWING_COLOUR = "_R_RANGE_DRAWING_COLOUR";

const std::string SELECTED_CHAMP_DRAWING_COLOUR = "_SELECTED_CHAMP_DRAWING_COLOUR";
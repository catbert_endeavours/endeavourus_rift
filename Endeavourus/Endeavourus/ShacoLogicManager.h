#pragma once
#include "SDK Extensions.h"

class ShacoLogicManager
{
private:
	static Spell::Targeted QCast;
	static Spell::Targeted WCast;
	static Spell::Targeted ECast;
	static Spell::Active RCast;

	static void RefreshSpells();

	static void CastQ(OrbwalkingMode orbwalkingMode);
	static void CastR(OrbwalkingMode orbwalkingMode);

	static void KillSteal();

	static float GetEDamage(AIBaseClient * enemy);

	static Vector3 GetPositionAroundMinion(std::pair<int, AIMinionClient*> enemy, float distance);

	static Vector3 GetPositionAroundHero(std::pair<int, AIHeroClient*> enemy, float distance);

	static void UpdateDeceiveTimer();

	static std::vector<float> EDamage;
	static std::vector<float> EAdRatio;
	static float EAPRatio;

	static float QCastOffset;
	static float WCastOffset;
	static float ECastOffset;
	static float RCastOffset;

public:
	static void Init();
	static void __cdecl OnUpdate(void * UserData);
	static void PerformLogic();
	static void PerformCombo(OrbwalkingMode orbwalkingMode);
	static void PerformHarass(OrbwalkingMode orbwalkingMode);
	static void PerformClear(OrbwalkingMode orbwalkingMode);
	static void PerformLastHit(OrbwalkingMode orbwalkingMode);
	static void PerformEscape(OrbwalkingMode orbwalkingMode);

	static void CastE(OrbwalkingMode orbwalkingMode);
	static void CastW(OrbwalkingMode orbwalkingMode);


	static bool SHACO_SHOULD_CAST_CLONE;

	static float DECEIVE_TIMER;

	static Vector3 GetWPosition(AIHeroClient * hero);
	static void AutoWImmobile();
	static bool CanCastW(OrbwalkingMode orbwalkingMode);
	static bool CanCastE(OrbwalkingMode orbwalkingMode);
	static void PerformSmoothMundoCombo();
	static void PerformSmoothRCombo();

	static bool PERFORM_SMOOTH_R_COMBO;

	static float EOutOfRangeTimer;
};


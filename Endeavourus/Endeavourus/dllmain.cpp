// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "Endeavourus.h"
#include "GeneralConstants.h"

PLUGIN_SETUP(MODULE_NAME.c_str(), OnLoad);

void __cdecl OnLoad(void* UserData) {
	LOAD_ENVIRONMENT();

	if (Player.PTR() && pSDK && pCore) {
		Endeavourus::Init();
	}
}
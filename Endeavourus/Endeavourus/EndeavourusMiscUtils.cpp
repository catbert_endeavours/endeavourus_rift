#include "stdafx.h"
#include "GeneralConstants.h"
#include "BaseSettings.h"
#include "JungleConstants.h"
#include <cmath>
#include "EndeavourusMiscUtils.h"
#include "Resource.h"
#include "ResourceMenuManager.h"
#include <iterator>

std::unordered_map<std::string, bool> CACHED_BOOL_SETTINGS{};
std::unordered_map<std::string, float> CACHED_FLOAT_SETTINGS{};
std::unordered_map<std::string, int> CACHED_INT_SETTINGS{};

bool EndeavourusMiscUtils::GetBoolSetting(std::string settingName, const bool defaultVal)
{
	std::string sName = GetUniqueSettingName(settingName);
	auto result = CACHED_BOOL_SETTINGS.find(sName);
	if (result != CACHED_BOOL_SETTINGS.end())
	{
		return result->second;
	}
	else
	{
		bool setting{false};
		SdkGetSettingBool(sName.c_str(), &setting, defaultVal);
		CACHED_BOOL_SETTINGS.emplace(sName, setting);
		return setting;
	}
}

float EndeavourusMiscUtils::GetFloatSetting(std::string settingName, float defaultVal)
{
	std::string sName = GetUniqueSettingName(settingName);
	auto result = CACHED_FLOAT_SETTINGS.find(sName);
	if (result != CACHED_FLOAT_SETTINGS.end())
	{
		return result->second;
	}
	else
	{
		float setting{ 0.0f };
		SdkGetSettingFloat(sName.c_str(), &setting, defaultVal);
		CACHED_FLOAT_SETTINGS.emplace(sName, setting);
		return setting;
	}
}

int EndeavourusMiscUtils::GetDropDownSetting(std::string settingName, int defaultVal)
{
	return EndeavourusMiscUtils::GetIntSetting(settingName, defaultVal);
}

int EndeavourusMiscUtils::GetIntSetting(std::string settingName, const int defaultVal)
{
	std::string sName = GetUniqueSettingName(settingName);
	auto result = CACHED_INT_SETTINGS.find(sName);
	if (result != CACHED_INT_SETTINGS.end())
	{
		return result->second;
	}
	else
	{
		int setting{ 0 };
		SdkGetSettingNumber(sName.c_str(), &setting, defaultVal);
		CACHED_INT_SETTINGS.emplace(sName, setting);
		return setting;
	}
}

void EndeavourusMiscUtils::UpdateBoolSetting(std::string settingName, bool value)
{
	auto result = CACHED_BOOL_SETTINGS.find(settingName);
	if (result != CACHED_BOOL_SETTINGS.end())
	{
		result->second = value;
	}
}

void EndeavourusMiscUtils::UpdateFloatSetting(std::string settingName, float value)
{
	auto result = CACHED_FLOAT_SETTINGS.find(settingName);
	if (result != CACHED_FLOAT_SETTINGS.end())
	{
		result->second = value;
	}
}

void EndeavourusMiscUtils::UpdateIntSetting(std::string settingName, int value)
{
	auto result = CACHED_INT_SETTINGS.find(settingName);
	if (result != CACHED_INT_SETTINGS.end())
	{
		result->second = value;
	}
}

const std::string EndeavourusMiscUtils::GetUniqueSettingName(std::string settingName)
{
	return (Player.GetCharName() + MODULE_NAME + settingName).c_str();
}


bool EndeavourusMiscUtils::IsBlueTeam()
{
	return Player.GetTeamID() == TEAM_TYPE_ORDER;
}

bool EndeavourusMiscUtils::IsSummonersRift() {
	unsigned int MapID{};
	SdkGetGameData(&MapID, nullptr, nullptr);
	if (MapID == 11) {
		return true;
	}

	return false;
}

bool EndeavourusMiscUtils::PlayerHasSmite() {
	return std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find(SMITE) != std::string::npos ||
		std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find(SMITE) != std::string::npos;
}

bool EndeavourusMiscUtils::PlayerHasUnsealedSpellbook() {
	for (auto buffs : Player.GetBuffs())
	{
		if (buffs.Name.find("UnsealedSpellbook") != std::string::npos)
		{
			return true;
		}
	}
	return false;
}

OrbwalkingMode EndeavourusMiscUtils::GetOrbwalkingMode()
{
	bool comboActive = BaseSettings::isComboKeyEnabled();
	bool fleeActive = BaseSettings::isEscapeKeyEnabled();
	bool freezeActive = BaseSettings::isLasthitKeyEnabled();
	bool jungleClearActive = BaseSettings::isClearKeyEnabled();
	bool lastHitActive = BaseSettings::isLasthitKeyEnabled();
	bool laneClearActive = BaseSettings::isClearKeyEnabled();
	bool mixedActive = BaseSettings::isMixedKeyEnabled();

	if (comboActive)
	{
		return OrbwalkingMode::Combo;
	}
	else if (fleeActive)
	{
		return OrbwalkingMode::Flee;
	}
	else if (freezeActive)
	{
		return OrbwalkingMode::Freeze;
	}
	else if (jungleClearActive)
	{
		return OrbwalkingMode::JungleClear;
	}
	else if (lastHitActive)
	{
		return OrbwalkingMode::LastHit;
	}
	else if (laneClearActive)
	{
		return OrbwalkingMode::LaneClear;
	}
	else if (mixedActive)
	{
		return OrbwalkingMode::Mixed;
	}
	else
	{
		return OrbwalkingMode::None;
	}
}

bool CanCastComboSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{ 0 };
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledCombo() && BaseSettings::getComboQResourceLimit(ResourceMenuManager::comboResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledCombo() && BaseSettings::getComboWResourceLimit(ResourceMenuManager::comboResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledCombo() && BaseSettings::getComboEResourceLimit(ResourceMenuManager::comboResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledCombo() && BaseSettings::getComboRResourceLimit(ResourceMenuManager::comboResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool CanCastHarassSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{ 0 };
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledHarass() && BaseSettings::getHarassQResourceLimit(ResourceMenuManager::harassResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledHarass() && BaseSettings::getHarassWResourceLimit(ResourceMenuManager::harassResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledHarass() && BaseSettings::getHarassEResourceLimit(ResourceMenuManager::harassResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledHarass() && BaseSettings::getHarassRResourceLimit(ResourceMenuManager::harassResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool CanCastClearSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{ 0 };
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledClear() && BaseSettings::getClearQResourceLimit(ResourceMenuManager::clearResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledClear() && BaseSettings::getClearWResourceLimit(ResourceMenuManager::clearResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledClear() && BaseSettings::getClearEResourceLimit(ResourceMenuManager::clearResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledClear() && BaseSettings::getClearRResourceLimit(ResourceMenuManager::clearResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool CanCastLastHitSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{ 0 };
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledLH() && BaseSettings::getLHQResourceLimit(ResourceMenuManager::lastHitResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledLH() && BaseSettings::getLHWResourceLimit(ResourceMenuManager::lastHitResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledLH() && BaseSettings::getLHEResourceLimit(ResourceMenuManager::lastHitResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledLH() && BaseSettings::getLHRResourceLimit(ResourceMenuManager::lastHitResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool CanCastEscapeSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{};
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledEscape() && BaseSettings::getEscapeQResourceLimit(ResourceMenuManager::escapeResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledEscape() && BaseSettings::getEscapeWResourceLimit(ResourceMenuManager::escapeResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledEscape() && BaseSettings::getEscapeEResourceLimit(ResourceMenuManager::escapeResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledEscape() && BaseSettings::getEscapeRResourceLimit(ResourceMenuManager::escapeResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool CanCastKsSpell(SpellSlot spellSlot, Resource::ResourceType resourceType)
{
	float playerResourcePercent{};
	if (resourceType == Resource::ResourceType::Mana) {
		playerResourcePercent = Player.GetManaPercent();
	}
	else if (resourceType == Resource::ResourceType::Health)
	{
		playerResourcePercent = Player.GetHealthPercent();
	}

	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledKs() && BaseSettings::getKsQResourceLimit(ResourceMenuManager::ksResourceSettings.getDefaultQMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledKs() && BaseSettings::getKsWResourceLimit(ResourceMenuManager::ksResourceSettings.getDefaultWMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledKs() && BaseSettings::getKsEResourceLimit(ResourceMenuManager::ksResourceSettings.getDefaultEMinResourceValue()) < playerResourcePercent;
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledKs() && BaseSettings::getKsRResourceLimit(ResourceMenuManager::ksResourceSettings.getDefaultRMinResourceValue()) < playerResourcePercent;
	}

	return false;
}

bool EndeavourusMiscUtils::CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode, Resource::ResourceType resourceType)
{
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		return CanCastComboSpell(spellSlot, resourceType);

	case OrbwalkingMode::Flee:
		return CanCastEscapeSpell(spellSlot, resourceType);

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		return CanCastClearSpell(spellSlot, resourceType);

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		return CanCastLastHitSpell(spellSlot, resourceType);

	case OrbwalkingMode::Mixed:
		return CanCastHarassSpell(spellSlot, resourceType);

	case OrbwalkingMode::Custom:
		return CanCastKsSpell(spellSlot, resourceType);

	case OrbwalkingMode::None:
		return false;

	default:
		return false;
	}
}


bool EndeavourusMiscUtils::CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode)
{
	return CanCastSpell(spellSlot, orbwalkingMode, Resource::ResourceType::Mana);
}

bool EndeavourusMiscUtils::IsValidToCastSpell(AIBaseClient* enemy)
{
	return Player.IsAlive() && Player.IsValid() && !Player.IsZombie() && enemy != nullptr && enemy->IsValid() && enemy->IsAlive() && enemy->IsVisible() && !enemy->IsZombie() && !enemy->HasBuff("KarthusDeathDefied", true);
}

bool EndeavourusMiscUtils::CanCastSpellSmoothInCombo(AIBaseClient* enemy)
{
	//return !Player.IsAttacking() || Player.Distance(&enemy->GetServerPosition()) > Player.GetAttackRange();
	return true;
}

bool hasCollision(AIBaseClient* target, Spell::Skillshot skillshot)
{
	SDKVECTOR playerVector{ Player.GetServerPosition() };
	SDKVECTOR targetVector{ target->GetServerPosition() };

	bool collided{ false };
	std::shared_ptr<ICollision::Output> d = pSDK->Collision->GetCollisions(playerVector, targetVector, skillshot.Width, skillshot.Delay, skillshot.Speed);
	for (auto minion : d.get()->Units)
	{
		if (d.get()->Units.size() == 1 && minion->GetNetworkID() == target->GetNetworkID())
		{
			break;
		}

		if (minion->GetNetworkID() != target->GetNetworkID())
		{
			collided = true;
			break;
		}
	}

	return collided;
}

AIBaseClient* EndeavourusMiscUtils::GetJungleMinionTarget(float range, bool lastHitMode, bool getLowestHealth)
{
	AIBaseClient* target{};
	float distance{ FLT_MAX };
	float health{ FLT_MAX };
	for (auto creep : pSDK->EntityManager->GetJungleMonsters(range)) {
		for (auto creepName : JUNGLE_CREEPS)
		{
			if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
				std::string creepString = std::string(creep.second->GetName());
				if (creepString.find(creepName) != std::string::npos && creepString.find("Mini") == std::string::npos)
				{
					if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
						continue;

					if (!getLowestHealth) {
						float d = Player.Distance(creep.second);
						if (d < distance)
						{
							distance = d;
							target = creep.second;
						}
					}
					else
					{
						if (creep.second->GetHealth().Current < health)
						{
							target = creep.second;
							health = creep.second->GetHealth().Current;
						}
					}
				}
			}
		}
	}

	if (!lastHitMode) {
		if (target == nullptr) {
			for (auto creep : pSDK->EntityManager->GetEnemyMinions(range)) {
				if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
					if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
						continue;

					if (getLowestHealth) {
						if (creep.second->GetHealth().Current < health)
						{
							target = creep.second;
							health = creep.second->GetHealth().Current;
						}
					}
					else
					{
						float d = Player.Distance(creep.second);
						if (d < distance)
						{
							distance = d;
							target = creep.second;
						}
					}
				}
			}
		}
	}
	else
	{
		for (auto creep : pSDK->EntityManager->GetEnemyMinions(range)) {
			if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
				if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
					continue;

				if (creep.second->GetHealth().Current < health)
				{
					target = creep.second;
					health = creep.second->GetHealth().Current;
				}
			}
		}
	}

	return target;
}

AIBaseClient* EndeavourusMiscUtils::GetJungleMinionTarget(float range, bool lastHitMode, bool getLowestHealth, bool collisionCheck, Spell::Skillshot* skillshot)
{
	AIBaseClient* target{};
	float distance{ FLT_MAX };
	float health{ FLT_MAX };
	if (!lastHitMode) {
		for (auto creep : pSDK->EntityManager->GetJungleMonsters(range)) {
			for (auto creepName : JUNGLE_CREEPS)
			{
				if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
					std::string creepString = std::string(creep.second->GetName());
					if (creepString.find(creepName) != std::string::npos && creepString.find("Mini") == std::string::npos)
					{
						if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
							continue;

						if (collisionCheck && hasCollision(creep.second, *skillshot))
						{
							break;
						}

						if (!hasCollision(creep.second, *skillshot)) {
							if (!getLowestHealth) {
								float d = Player.Distance(creep.second);
								if (d < distance)
								{
									distance = d;
									target = creep.second;
								}
							}
							else
							{
								if (creep.second->GetHealth().Current < health)
								{
									target = creep.second;
									health = creep.second->GetHealth().Current;
								}
							}
						}
					}
				}
			}
		}
	}

	if (!lastHitMode) {
		if (target == nullptr) {
			for (auto creep : pSDK->EntityManager->GetEnemyMinions(range)) {
				if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
					if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
						continue;

					if (collisionCheck && hasCollision(creep.second, *skillshot))
					{
						continue;
					}

					if (getLowestHealth) {
						if (creep.second->GetHealth().Current < health)
						{
							target = creep.second;
							health = creep.second->GetHealth().Current;
						}
					}
					else
					{
						float d = Player.Distance(creep.second);
						if (d < distance)
						{
							distance = d;
							target = creep.second;
						}
					}
				}
			}
		}
	}
	else
	{
		for (auto creep : pSDK->EntityManager->GetEnemyMinions(range)) {
			if (creep.second != nullptr && creep.second->IsValid() && creep.second->IsAlive() && creep.second->IsVisible()) {
				if (std::string{ creep.second->GetName() }.find("Turret") != std::string::npos || std::string{ creep.second->GetName() }.find("Trap") != std::string::npos)
					continue;

				if (collisionCheck && hasCollision(creep.second, *skillshot))
				{
					continue;
				}

				if (creep.second->GetHealth().Current < health)
				{
					target = creep.second;
					health = creep.second->GetHealth().Current;
				}
			}
		}
	}

	return target;
}

/**
 * Credit - Unclear
 */
EndeavourusMiscUtils::PointProjection EndeavourusMiscUtils::VectorPointProjectionOnLineSegment(Vector2 v1, Vector2 v2, Vector2 v3)
{
	float cx = v3.x;
	float cy = v3.y;
	float ax = v1.x;
	float ay = v1.y;
	float bx = v2.x;
	float by = v2.y;
	float rL = ((cx - ax) * (bx - ax) + (cy - ay) * (by - ay)) /
		(static_cast<float>(pow(bx - ax, 2)) + static_cast<float>(pow(by - ay, 2)));
	Vector2 pointLine = new Vector2(ax + rL * (bx - ax), ay + rL * (by - ay));
	float rS;
	if (rL < 0)
	{
		rS = 0;
	}
	else if (rL > 1) {
		rS = 1;
	}
	else {
		rS = rL;
	}

	bool isOnSegment = rS == rL; // May need to change this
	Vector2 pointSegment = isOnSegment ? pointLine : new Vector2(ax + rS * (bx - ax), ay + rS * (by - ay));

	return PointProjection{ pointSegment, pointLine, isOnSegment };
}
#pragma once
#include "BaseSettings.h"
class TeemoSettings :
	public BaseSettings
{
public:
	// Draw
	static bool isEnableShroomDraw();
	static float getEnableShroomDrawDistance();

	static bool isAutoRImmobileChampion();
	static bool isAutoRShroomPlacement();

	static bool isBlockQOnChampion(std::string champName);

	static SDKCOLOR getShroomPlacementColour();

	static bool isSnapRKeyPressed();

};


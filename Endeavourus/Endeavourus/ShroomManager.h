#pragma once
#include "SDK Extensions.h"
#include "TeemoConstants.h"
#include "ShroomLocations.h"

class ShroomManager
{
public:
	static void SnapRToClosestShroomLoc();
	static void PerformShroomAutomation();

};
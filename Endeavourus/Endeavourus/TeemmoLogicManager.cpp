#include "stdafx.h"
#include "TeemoLogicManager.h"
#include "ShroomManager.h"
#include "BaseLogicManager.h"

Spell::Targeted TeemoLogicManager::QCast{ SpellSlot::Q, 0 };
Spell::Active TeemoLogicManager::WCast{ SpellSlot::W };
Spell::Skillshot TeemoLogicManager::RCast{ SpellSlot::R, 0, SkillshotType::Line };

void TeemoLogicManager::Init()
{
	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	QCast = Spell::Targeted{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, DamageType::Magical };
	WCast = Spell::Active{ SpellSlot::W };

	//RCast = Spell::Skillshot{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, SkillshotType::Line, 1, BaseLogicManager::R_Spell.MissileSpeed, BaseLogicManager::R_Spell.LineWidth, DamageType::Magical, false, CollisionFlags::Nothing };
	RCast = Spell::Skillshot{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, SkillshotType::Line, 0.25, BaseLogicManager::R_Spell.MissileSpeed, BaseLogicManager::R_Spell.LineWidth, DamageType::Magical, false, CollisionFlags::Nothing };
	RCast.MinimumHitChance = HitChance::High;
}

void TeemoLogicManager::PerformLogic()
{
	bool comboActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Combo);
	bool customActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Custom);
	bool fleeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Flee);
	bool freezeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Freeze);
	bool jungleClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::JungleClear);
	bool lastHitActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LastHit);
	bool laneClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LaneClear);
	bool mixedActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Mixed);

	if (comboActive)
	{
		AIHeroClient* enemy = pCore->TS->GetTarget(RCast.Range, DamageType::Magical);
		if (enemy != nullptr && RCast.IsReady())
		{
			pCore->TS->ForceTarget(enemy);
			AIHeroClient* enemyHero = pCore->Orbwalker->GetTarget()->AsAIHeroClient();
			Game::PrintChat("Chucking R");
			RCast.Cast(enemyHero);
		}
		else if (enemy == nullptr)
		{
			Game::PrintChat("Test");
		}
	}
	else if (customActive)
	{
	}
	else if (fleeActive)
	{
	}
	else if (freezeActive)
	{
	}
	else if (jungleClearActive)
	{
	}
	else if (lastHitActive)
	{
	}
	else if (laneClearActive)
	{
	}
	else if (mixedActive)
	{
	}
	else
	{
		//None
		//ShroomManager::PerformShroomAutomation();
	}
}


void TeemoLogic::RefreshRRange()
{
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
	RCast = Spell::Skillshot{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, SkillshotType::Line, 0.25, BaseLogicManager::R_Spell.MissileSpeed, BaseLogicManager::R_Spell.LineWidth, DamageType::Magical, false, CollisionFlags::Nothing };
}
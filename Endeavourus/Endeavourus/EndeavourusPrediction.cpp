#include "stdafx.h"
#include "EndeavourusPrediction.h"
#include "EndeavourusPathTracker.h"
#include "Geometry.hpp"

EndeavourusPrediction::DefaultValues_t EndeavourusPrediction::DefaultValues;

EndeavourusPrediction::EndeavourusPrediction() {
	EndeavourusPathTracker::Init();
}

bool IsImmobileTarget(AIBaseClient *target)
{
	if (target->HasBuffType(BUFF_TYPE_SNARE)) {
		return true;
	}
	else if (target->HasBuffType(BUFF_TYPE_STUN)) {
		return true;
	}
	else if (target->HasBuffType(BUFF_TYPE_CHARM)) {
		return true;
	}
	else if (target->HasBuffType(BUFF_TYPE_KNOCKUP)) {
		return true;
	}
	else if (target->HasBuffType(BUFF_TYPE_SUPPRESSION)) {
		return true;
	}

	return false;
}

//distanceSet - distance target can walk before being hit, if left 0 will get calculated
Vector2 EndeavourusPrediction::GetFastPrediction(AIBaseClient* target, float delay, float missileSpeed, Vector2* from, float distanceSet) {
	Game::SendChat("True1");

	auto path = target->GetWaypoints();

	if (from == nullptr) {
		from = &Player.GetServerPosition().To2D();
	}

	if (path.size() <= 1 || (target->IsHero() && target->AsAIHeroClient()->IsChannelingImportantSpell()) || IsImmobileTarget(target)) {
		return target->GetServerPosition().To2D();
	}

	auto nav{ target->NavInfo() };
	if (nav.IsDashing) {
		return nav.EndPos;
	}

	float distance = distanceSet;

	if (distance == 0)
	{
		Vector2 ser{ target->GetServerPosition() };
		float targetDistance = from->Distance(ser);
		float flyTime = targetDistance / missileSpeed;

		if (missileSpeed != 0 && path.size() == 2)
		{
			Vector2 Vt = (path[1] - path[0]).Normalized() * target->GetMovementSpeed();
			Vector2 Vs = (target->GetServerPosition().To2D() - from).Normalized() * missileSpeed;
			Vector2 Vr = Vt - Vs;

			flyTime = targetDistance / Vr.Length();
		}

		float t = flyTime + delay + Game::Ping() / 2000.0f;
		distance = t * target->GetMovementSpeed();
	}

	for (int i = 0; i < path.size() - 1; i++)
	{
		float d = path[i + 1].Distance(path[i]);
		if (distance == d) {
			return path[i + 1];
		}
		else if (distance < d)
		{
			return path[i] + distance * (path[i + 1] - path[i]).Normalized();
		}
		else {
			distance -= d;
		}
	}

	return path.back();
}

Vector2 EndeavourusPrediction::GetFastPrediction(AIBaseClient* target, std::vector<Vector2> path, float delay, float missileSpeed, Vector2* from, float moveSpeed, float distanceSet) {
	Game::SendChat("True2");

	if (from == nullptr) { //TODO
		from = &target->GetServerPosition().To2D();
	}

	if (moveSpeed == 0) {
		moveSpeed = target->GetMovementSpeed();
	}

	if (path.size() <= 1 || (target->IsHero() && target->AsAIHeroClient()->IsChannelingImportantSpell()) || IsImmobileTarget(target)) {
		return target->GetServerPosition().To2D();
	}

	auto nav{ target->NavInfo() };
	if (target->IsDashing()) {
		return nav.EndPos;
	}

	float distance = distanceSet;

	if (distance == 0)
	{
		Vector2 ser{ target->GetServerPosition() };

		float targetDistance = from->Distance(ser);
		float flyTime = 0.0f;

		if (missileSpeed != 0) //skillshot with a missile
		{
			Vector2 Vt = (path[path.size() - 1] - path[0]).Normalized() * moveSpeed;
			Vector2 Vs = (target->GetServerPosition().To2D() - from).Normalized() * missileSpeed;
			Vector2 Vr = Vs - Vt;

			flyTime = targetDistance / Vr.Length();

			if (path.size() > 5) //complicated movement
			{
				flyTime = targetDistance / missileSpeed;
			}
		}

		float t = flyTime + delay + Game::Ping() / 2000.0f + 0 / 1000.0f; // TODO float t = flyTime + delay + Game::Ping() / 2000.0f + ConfigMenu.SpellDelay / 1000f;
		distance = t * moveSpeed;
	}

	for (int i = 0; i < path.size() - 1; i++)
	{
		float d = path[i + 1].Distance(path[i]);
		if (distance == d) {
			return path[i + 1];
		}
		else if (distance < d)
		{
			return path[i] + distance * (path[i + 1] - path[i]).Normalized();
		}
		else {
			distance -= d;
		}
	}

	return path.back();
}

/// <returns></returns>
float GetArrivalTimeE(float distance, float delay, float missileSpeed = 0)
{
	if (missileSpeed != 0) {
		return distance / missileSpeed + delay;
	}

	return delay;
}

float LeftImmobileTime(AIBaseClient *target)
{
	float maxTime{ 0.0f };
	for (auto b : target->GetBuffs()) {
		if (b.Type == BUFF_TYPE_SNARE || b.Type == BUFF_TYPE_STUN || b.Type == BUFF_TYPE_CHARM || b.Type == BUFF_TYPE_KNOCKUP || b.Type == BUFF_TYPE_SUPPRESSION) {
			if (b.EndTime > maxTime) {
				maxTime = b.EndTime;
			}
		}
	}

	return maxTime;
}

HitChance GetHitChanceE(float t, float avgt, float movt, float avgp, float anglediff)
{
	if (avgp > 400)
	{
		if (movt > 50)
		{
			if (avgt >= t * 1.25f)
			{
				if (anglediff < 30) {
					return HitChance::VeryHigh;
				}
				else {
					return HitChance::High;
				}
			}
			else if (avgt - movt >= t)
			{
				return HitChance::Medium;
			}
			else
			{
				return HitChance::Low;
			}
		}
		else
		{
			return HitChance::VeryHigh;
		}
	}
	else
	{
		return HitChance::High;
	}
}

std::shared_ptr<IPrediction::Output> EndeavourusPrediction::GetPrediction(AIHeroClient* target, std::shared_ptr<IPrediction::Input> input) {
	std::shared_ptr<EndeavourusPrediction::Output> result{ std::make_shared<EndeavourusPrediction::Output>(input, target) };

	Game::SendChat("True");
	Vector3 t{ target->GetPosition() };
	Game::SendPing(t, PingType::Back, true);

	result->Unit = target;

	auto currentPath{ target->GetWaypoints2D() };

	EnemyData enemyData = EndeavourusPathTracker::GetData(target);

	//TODO
	if (input->SkillType == SkillshotType::Circle) {
		input->Range += input->Width;
	}

	if (target->IsHero()) {
		if (target->AsAIHeroClient()->IsChannelingImportantSpell()) {
			result->Hitchance = HitChance::VeryHigh;
			result->CastPosition = target->GetServerPosition().To2D();
			result->UnitPosition = result->CastPosition;
			result->Lock();
		}
	}

	if (Game::Time() - enemyData.LastAATick < 300) {//TODO 
		if (target->GetAttackCastDelay() * 1000 + enemyData.AvgOrbwalkTime + enemyData.AvgMovChangeTime() - input->Width / 2.0f / target->GetMovementSpeed() >= GetArrivalTime(target->GetServerPosition().To2D().Distance(input->From), input->Delay, input->MissileSpeed))
		{
			result->Hitchance = HitChance::High;
			result->CastPosition = target->GetServerPosition().To2D();
			result->UnitPosition = result->CastPosition;
			result->Lock();
		}
	}

	//to do: hook logic ? by storing average movement direction etc
	if (currentPath.size() <= 1 && enemyData.AvgMovChangeTime() > 100 && (Game::Time() - enemyData.LastAATick > 300)) //if target is not moving, easy to hit (and not aaing) //todo || !ConfigMenu::getCheckAAWindUp()
	{
		result->Hitchance = HitChance::VeryHigh;
		result->CastPosition = target->GetServerPosition().To2D();
		result->UnitPosition = result->CastPosition;
		result->Lock();

		return result;
	}

	float l = EndeavourusPathTracker::GetPathLength(currentPath);

	//to do: find a fuking logic
	if (enemyData.AvgPathLenght < 400 && enemyData.LastMovChangeTime() < 100 && l <= enemyData.AvgPathLenght)
	{
		Vector2 val{};
		if (!currentPath.empty())
		{
			val = currentPath.back();
		}

		result->Hitchance = HitChance::High;
		result->CastPosition = val;
		result->UnitPosition = result->CastPosition;
		result->Lock();

		return result;
	}

	if (target->IsDashing()) {
		//Target Dashing/Blinking
		auto dashData{ pSDK->GapcloserManager->GetGapcloseDataInst(target->GetNetworkID()) };
		auto navInfo{ target->NavInfo() };
		if (dashData.Data.IsValid()) {
			auto curPos{ target->GetPosition().To2D() };
			auto endPos{ dashData.Spell.EndPosition.To2D() };

			if (dashData.Data.IsBlink) {
				result->Hitchance = HitChance::Immobile;
				result->CastPosition = endPos;
				return result;
			}

			result->CastPosition = GetFastPrediction(target->AsAIBaseClient(), input->Delay, input->MissileSpeed, &input->From.To2D());
			result->Hitchance = HitChance::Dashing;

			result->Lock(false);
		}
	}

	if (IsImmobileTarget(target)) {
		result->CastPosition = target->GetServerPosition().To2D();
		result->UnitPosition = result->CastPosition;

		//calculate spell arrival time
		float t = input->Delay + Game::Ping() / 2000.0f;
		if (input->MissileSpeed != 0) {
			Vector3 s{ target->GetServerPosition() };
			t += input->From.Distance(s) / input->MissileSpeed;
		}

		if (input->SkillType == SkillshotType::Circle) {
			t += input->Width / target->GetMovementSpeed() / 2.0f;
		}

		if (t >= LeftImmobileTime(target))
		{
			result->Hitchance = HitChance::Immobile;
			result->Lock();

			return result;
		}

		if (target->IsHero()) {
			result->Hitchance = GetHitChanceE(t - LeftImmobileTime(target), enemyData.AvgMovChangeTime(), 0, 0, 0);
		}
		else {
			result->Hitchance = HitChance::High;
		}

		result->Lock();

		return result;
	}

	float moveSpeed = target->GetMovementSpeed();
	float flyTimeMax = 0.0f;

	if (input->MissileSpeed != 0) {//skillshot with a missile
		flyTimeMax = input->Range / input->MissileSpeed;
	}

	float tMin = input->Delay + Game::Ping() / 2000.0f + 0 / 1000.0f; //TODO+ ConfigMenu.SpellDelay / 1000f;
	float tMax = flyTimeMax + input->Delay + Game::Ping() / 1000.0f + 0 / 1000.0f; //+ConfigMenu.SpellDelay / 1000f;
	float pathTime = 0.0f;
	int pathBounds[2] = { -1, -1 };

	//find bounds
	for (int i = 0; i < currentPath.size() - 1; i++)
	{
		float t = currentPath[i + 1].Distance(currentPath[i]) / moveSpeed;

		if (pathTime <= tMin && pathTime + t >= tMin)
		{
			pathBounds[0] = i;
		}

		if (pathTime <= tMax && pathTime + t >= tMax) {
			pathBounds[1] = i;
		}

		if (pathBounds[0] != -1 && pathBounds[1] != -1) {
			break;
		}

		pathTime += t;
	}

	//calculate cast & unit position
	if (pathBounds[0] != -1 && pathBounds[1] != -1)
	{
		for (int k = pathBounds[0]; k <= pathBounds[1]; k++)
		{
			Vector2 direction = (currentPath[k + 1] - currentPath[k]).Normalized();
			float distance = input->Width;
			float extender = target->GetBoundingRadius();

			if (input->SkillType == SkillshotType::Line) {
				extender = input->Width;
			}

			int steps = static_cast<int>(std::floor(currentPath[k].Distance(currentPath[k + 1]) / distance));
			//split & anlyse current path
			for (int i = 1; i < steps - 1; i++)
			{
				Vector2 pCenter = currentPath[k] + (direction * distance * i);
				Vector2 pA = pCenter - (direction * extender);
				Vector2 pB = pCenter + (direction * extender);

				float flytime = input->MissileSpeed != 0 ? input->From.Distance(pCenter) / input->MissileSpeed : 0.0f;
				float t = flytime + input->Delay + Game::Ping() / 2000.0f + 0 / 1000.0f; //TODO2000f + ConfigMenu.SpellDelay / 1000.0f;

				Vector2 currentPosition = target->GetServerPosition().To2D();

				float arriveTimeA = currentPosition.Distance(pA) / moveSpeed;
				float arriveTimeB = currentPosition.Distance(pB) / moveSpeed;

				if (min(arriveTimeA, arriveTimeB) <= t && max(arriveTimeA, arriveTimeB) >= t)
				{
					result->Hitchance = GetHitChanceE(t, enemyData.AvgMovChangeTime(), enemyData.LastMovChangeTime(), enemyData.AvgPathLenght, enemyData.LastAngleDiff);
					result->CastPosition = pCenter;
					result->UnitPosition = pCenter; //+ (direction * (t - Math.Min(arriveTimeA, arriveTimeB)) * moveSpeed);

					Vector2 ss{ target->GetServerPosition().To2D() };
					float d = result->CastPosition.Distance(ss);
					if (d >= (enemyData.AvgMovChangeTime() - enemyData.LastMovChangeTime()) * target->GetMovementSpeed() && d >= enemyData.AvgPathLenght) {
						result->Hitchance = HitChance::Medium;
					}

					return result;
				}
			}
		}
	}

	result->Hitchance = HitChance::Immobile;
	result->CastPosition = target->GetServerPosition().To2D();

	Vector2 ss{ target->GetServerPosition().To2D() };
	float d = result->CastPosition.Distance(ss);
	if (d >= (enemyData.AvgMovChangeTime() - enemyData.LastMovChangeTime()) * target->GetMovementSpeed() && d >= enemyData.AvgPathLenght) {
		result->Hitchance = HitChance::Medium;
	}

	return result;
}
std::shared_ptr<IPrediction::Output> EndeavourusPrediction::GetPrediction(AIHeroClient* target, float width, float delay, float missileSpeed, float range, SkillshotType type, bool collisionable, CollisionFlags flags, Vector3 from, Vector3 rangeCheckFrom) {
	std::shared_ptr<IPrediction::Input> input{ std::make_shared<IPrediction::Input>(type, range, delay, missileSpeed, width, collisionable, flags, from) };
	input->RangeCheckFrom = rangeCheckFrom;
	return GetPrediction(target, input);
}

#pragma region Internal Methods

std::shared_ptr<EndeavourusPrediction::Output> EndeavourusPrediction::WaypointAnalysis(AIHeroClient * target, std::shared_ptr<IPrediction::Input> input, std::vector<Vector2>& path, Vector2 & from) {
	auto moveSpeed{ target->GetMovementSpeed() };
	if (moveSpeed < EPSILON) { moveSpeed = 350.0f; } //This is only in case the Sdk method fails

	auto result{ std::make_shared<EndeavourusPrediction::Output>(input, target) };

	float extraSpellDelay{ DefaultValues.SpellDelay };
	float extraPing{ Game::Ping() / 2000.f };
	float flyTimeMax = input->MissileSpeed != 0 ? input->Range / input->MissileSpeed : 0.f;

	float tMin = input->Delay + extraPing + extraSpellDelay;
	float tMax = flyTimeMax + input->Delay + 2.0f*extraPing + extraSpellDelay;
	float pathTime = 0.f;
	int pathBounds[]{ -1, -1 };

	//find bounds
	for (size_t i = 1; i < path.size(); i++) {
		float t = path[i].Distance(path[i - 1]) / moveSpeed;

		if (pathTime <= tMin && pathTime + t >= tMin)
			pathBounds[0] = (int)i - 1;
		if (pathTime <= tMax && pathTime + t >= tMax)
			pathBounds[1] = (int)i - 1;

		if (pathBounds[0] != -1 && pathBounds[1] != -1)
			break;

		pathTime += t;
	}

	//calculate cast & unit position
	if (pathBounds[0] != -1 && pathBounds[1] != -1) {
		auto width{ std::max<float>(input->Width, 30.0f) };
		for (size_t k = (size_t)pathBounds[0]; k <= (size_t)pathBounds[1]; k++) {
			float extender = target->GetBoundingRadius();

			if (input->SkillType == SkillshotType::Line)
				extender += width;

			Vector2 direction = (path[k + 1] - path[k]).Normalized();
			int steps = (int)std::floor(path[k].Distance(path[k + 1]) / width);
			//split & analyze current path
			for (int i = 1; i < (steps - 1); i++) {
				Vector2 pCenter = path[k] + (direction * width * (float)i);
				Vector2 pA = pCenter - (direction * extender);
				Vector2 pB = pCenter + (direction * extender);

				float flytime = input->MissileSpeed != 0 ? from.Distance(pCenter) / input->MissileSpeed : 0.f;
				float t = flytime + input->Delay + extraPing + extraSpellDelay;

				Vector2 currentPosition = target->GetPosition().To2D();

				float arriveTimeA = currentPosition.Distance(pA) / moveSpeed;
				float arriveTimeB = currentPosition.Distance(pB) / moveSpeed;

				if (min(arriveTimeA, arriveTimeB) <= t && max(arriveTimeA, arriveTimeB) >= t) {
					result->Hitchance = GetHitChance(target, t);
					result->CastPosition = pCenter;
					result->UnitPosition = pCenter; //+ (direction * (t - min(arriveTimeA, arriveTimeB)) * moveSpeed);
					return result;
				}
			}
		}
	}

	result->Hitchance = HitChance::Impossible;
	result->CastPosition = target->GetPosition();

	return result;
}
Vector3 EndeavourusPrediction::PositionAfter(AIBaseClient* Unit, float t, float speed) {
	auto distance = t * speed;
	std::vector<Vector3> Path{ Unit->GetWaypoints() };

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			return a + distance * (b - a).Normalized();
		}
	}
	return Path.size() > 0 ? Path[Path.size() - 1] : Vector3();
}
Vector3 EndeavourusPrediction::PositionAfter(std::vector<Vector3>& Path, float t, float speed) {
	auto distance = t * speed;

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			return a + distance * (b - a).Normalized();
		}
	}
	return Path.size() > 0 ? Path[Path.size() - 1] : Vector3();
}
Vector3 EndeavourusPrediction::PositionAfter(std::vector<Vector2>& Path, float t, float speed) {
	auto distance = t * speed;

	for (size_t i = 1; i < Path.size(); i++) {
		auto a = Path[i - 1];
		auto b = Path[i];
		auto d = a.Distance(b);

		if (d < distance) {
			distance -= d;
		}
		else {
			auto pos{ a + distance * (b - a).Normalized() };
			return pos.To3D(pos.GetTerrainHeight());
		}
	}

	return Path.empty() ? Vector3() : Path.back().To3D(Path.back().GetTerrainHeight());
}
float   EndeavourusPrediction::GetArrivalTime(float distance, float delay, float missileSpeed) {
	float Time{ delay + Game::Ping() / 2000.0f };
	if (missileSpeed > EPSILON)
		Time += distance / missileSpeed;
	return Time;
}
HitChance EndeavourusPrediction::GetHitChance(AIHeroClient* Unit, float t) {
	auto &Data{ EndeavourusPathTracker::GetData(Unit) };
	if (Data.AvgPathLenght > 400.0f) {
		if (Data.LastMovChangeTime() > 50) {
			if (Data.AvgReactionTime() >= t) {
				if (Data.LastAngleDiff < 30.0f) {
					return HitChance::VeryHigh;
				}
				else {
					return HitChance::High;
				}
			}
			else if (Data.AvgReactionTime() - Data.LastMovChangeTime() >= t) {
				return HitChance::Medium;
			}

			else {
				return HitChance::Low;
			}
		}
		else {
			return HitChance::VeryHigh;
		}
	}
	else {
		return HitChance::High;
	}
}

inline Vector3 EndeavourusPrediction::GetFrom(std::shared_ptr<IPrediction::Input> input) {
	return input->From.IsValid() ? input->From : Player.GetPosition();
}
inline Vector3 EndeavourusPrediction::GetRangeCheckFrom(std::shared_ptr<IPrediction::Input> input) {
	return input->RangeCheckFrom.IsValid() ? input->RangeCheckFrom : GetFrom(input);
}
#pragma endregion


#pragma region Output Internal Methods
inline void EndeavourusPrediction::Output::Lock(bool checkDodge) {
	if (this->Input->Collision) {
		auto fromPos{ GetFrom(this->Input) };
		this->CollisionResult = pSDK->Collision->GetCollisions(fromPos, this->CastPosition, this->Input->Width, this->Input->Delay, this->Input->MissileSpeed, false, this->Input->Flags);
		this->CheckCollisions();
	}
	this->CheckOutofRange(checkDodge);
}
inline void EndeavourusPrediction::Output::CheckCollisions() {
	if (this->CollisionResult && this->Input->Flags & this->CollisionResult->Objects)
		this->Hitchance = HitChance::Collision;
}
inline void EndeavourusPrediction::Output::CheckOutofRange(bool checkDodge) {
	auto rangeIgnoreMod{ (100.0f - DefaultValues.MaxRangeIgnore) / 100.f };
	auto dist{ GetRangeCheckFrom(this->Input).Distance(this->CastPosition) - (this->Input->SkillType == SkillshotType::Circle ? this->Input->Width : 0.0f) };

	if (dist > this->Input->Range - (checkDodge ? GetArrivalTime(dist, this->Input->Delay, this->Input->MissileSpeed) * this->Unit->GetMovementSpeed() * rangeIgnoreMod : 0.0f))
		this->Hitchance = HitChance::OutOfRange;
}
#pragma endregion
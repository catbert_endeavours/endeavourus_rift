#pragma once
#include "SDK Extensions.h"
#include "SmiteMenuManager.h"
#include "ResourceMenuManager.h"

class BaseMenuManager : public ResourceMenuManager, SmiteMenuManager
{
private:
	static bool unsupportedChampionLoaded;

public:
	static void DrawMenu();
	static void DrawKeySettings();
	static void DrawActivatorSettings();

	static void *specificChampLogic;

	static Resource GetComboResourceManagement();
	static Resource GetHarassResourceManagement();
	static Resource GetClearResourceManagement();
	static Resource GetLastHitResourceManagement();
	static Resource GetEscapeResourceManagement();
	static Resource GetKsResourceManagement();

	static bool GetUnsupportedChampionLoaded();
	static void SetUnsupportedChampionLoaded(const bool unsupportedChampionLoaded);

	static void DrawQRange();
	static void DrawWRange();
	static void DrawERange();
	static void DrawRRange();
	static void DrawSelectedTarget();
	static void SelectedTarget();
};

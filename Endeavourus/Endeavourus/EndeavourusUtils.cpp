#include "stdafx.h"
#include "EndeavourusMiscUtils.h"
#include "GeneralConstants.h"
#include "BaseSettings.h"
#include "JungleConstants.h"
#include <cmath>

struct PointProjection
{
	Vector2 pointSegment;
	Vector2 pointLine{};
	bool isOnSeg{};
};

bool EndeavourusMiscUtils::GetBoolSetting(std::string settingName, const bool defaultVal)
{
	bool setting{ false };
	SdkGetSettingBool(GetUniqueSettingName(settingName).c_str(), &setting, defaultVal);
	return setting;
}

float EndeavourusMiscUtils::GetFloatSetting(std::string settingName, float defaultVal)
{
	float settingVal{ false };
	SdkGetSettingFloat(GetUniqueSettingName(settingName).c_str(), &settingVal, defaultVal);
	return settingVal;
}

int EndeavourusMiscUtils::GetDropDownSetting(std::string settingName, int defaultVal)
{
	int settingVal{ false };
	SdkGetSettingNumber(GetUniqueSettingName(settingName).c_str(), &settingVal, defaultVal);
	return settingVal;
}

const std::string EndeavourusMiscUtils::GetUniqueSettingName(std::string settingName)
{
	return (Player.GetCharName() + MODULE_NAME + settingName).c_str();
}

bool EndeavourusMiscUtils::PlayerHasSmite() {
	return std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find(SMITE) != std::string::npos ||
		std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find(SMITE) != std::string::npos;
}

bool EndeavourusMiscUtils::PlayerHasUnsealedSpellbook() {
	for (auto buffs : Player.GetBuffs())
	{
		if (buffs.Name.find("UnsealedSpellbook") != std::string::npos)
		{
			return true;
		}
	}
	return false;
}


OrbwalkingMode EndeavourusMiscUtils::GetOrbwalkingMode()
{
	bool comboActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Combo);
	bool customActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Custom);
	bool fleeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Flee);
	bool freezeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Freeze);
	bool jungleClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::JungleClear);
	bool lastHitActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LastHit);
	bool laneClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LaneClear);
	bool mixedActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Mixed);

	if (comboActive)
	{
		return OrbwalkingMode::Combo;
	}
	else if (customActive)
	{
		return OrbwalkingMode::Custom;
	}
	else if (fleeActive)
	{
		return OrbwalkingMode::Flee;
	}
	else if (freezeActive)
	{
		return OrbwalkingMode::Freeze;
	}
	else if (jungleClearActive)
	{
		return OrbwalkingMode::JungleClear;
	}
	else if (lastHitActive)
	{
		return OrbwalkingMode::LastHit;
	}
	else if (laneClearActive)
	{
		return OrbwalkingMode::LaneClear;
	}
	else if (mixedActive)
	{
		return OrbwalkingMode::Mixed;
	}
	else
	{
		return OrbwalkingMode::None;
	}
}

bool CanCastComboSpell(SpellSlot spellSlot)
{
	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledCombo() && BaseSettings::getComboQResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledCombo() && BaseSettings::getComboWResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledCombo() && BaseSettings::getComboEResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledCombo() && BaseSettings::getComboRResourceLimit() < Player.GetManaPercent();
	}

	return false;
}

bool CanCastHarassSpell(SpellSlot spellSlot)
{
	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledHarass() && BaseSettings::getHarassQResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledHarass() && BaseSettings::getHarassWResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledHarass() && BaseSettings::getHarassEResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledHarass() && BaseSettings::getHarassRResourceLimit() < Player.GetManaPercent();
	}

	return false;
}

bool CanCastClearSpell(SpellSlot spellSlot)
{
	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledClear() && BaseSettings::getClearQResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledClear() && BaseSettings::getClearWResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledClear() && BaseSettings::getClearEResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledClear() && BaseSettings::getClearRResourceLimit() < Player.GetManaPercent();
	}

	return false;
}

bool CanCastLastHitSpell(SpellSlot spellSlot)
{
	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledLH() && BaseSettings::getLHQResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledLH() && BaseSettings::getLHWResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledLH() && BaseSettings::getLHEResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledLH() && BaseSettings::getLHRResourceLimit() < Player.GetManaPercent();
	}

	return false;
}

bool CanCastEscapeSpell(SpellSlot spellSlot)
{
	if (spellSlot == SpellSlot::Q)
	{
		return BaseSettings::isQEnabledEscape() && BaseSettings::getEscapeQResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::W)
	{
		return BaseSettings::isWEnabledEscape() && BaseSettings::getEscapeWResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::E)
	{
		return BaseSettings::isEEnabledEscape() && BaseSettings::getEscapeEResourceLimit() < Player.GetManaPercent();
	}
	else if (spellSlot == SpellSlot::R)
	{
		return BaseSettings::isREnabledEscape() && BaseSettings::getEscapeRResourceLimit() < Player.GetManaPercent();
	}

	return false;
}

bool EndeavourusMiscUtils::CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		return CanCastComboSpell(spellSlot);

	case OrbwalkingMode::Flee:
		return CanCastEscapeSpell(spellSlot);

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		return CanCastClearSpell(spellSlot);

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		return CanCastLastHitSpell(spellSlot);

	case OrbwalkingMode::Mixed:
		return CanCastHarassSpell(spellSlot);

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		return false;

	default:
		return false;
	}
}

bool EndeavourusMiscUtils::IsValidToCastSpell(AIBaseClient* enemy)
{

	return Player.IsAlive() && Player.IsValid() && enemy != nullptr && enemy->IsValid() && enemy->IsAlive();
}

/**
 * Credit - Unclear
 */
static PointProjection VectorPointProjectionOnLineSegment(Vector2 v1, Vector2 v2, Vector2 v3)
{
	float cx = v3.x;
	float cy = v3.y;
	float ax = v1.x;
	float ay = v1.y;
	float bx = v2.x;
	float by = v2.y;
	float rL = ((cx - ax) * (bx - ax) + (cy - ay) * (by - ay)) /
		(static_cast<float>(pow(bx - ax, 2)) + static_cast<float>(pow(by - ay, 2)));
	Vector2 pointLine = new Vector2(ax + rL * (bx - ax), ay + rL * (by - ay));
	float rS;
	if (rL < 0)
	{
		rS = 0;
	}
	else if (rL > 1) {
		rS = 1;
	}
	else {
		rS = rL;
	}

	bool isOnSegment = rS == rL; // May need to change this
	Vector2 pointSegment = isOnSegment ? pointLine : new Vector2(ax + rS * (bx - ax), ay + rS * (by - ay));

	return PointProjection {pointSegment, pointLine, isOnSegment};
}
#pragma once
#include "SDK Extensions.h"
class ShacoDodgeSpellLogic
{
public:
	static void Init();
	static void PerformAntiBlitz();
	static void PerformLogic();
	static void PerformSkillshotLogic();
	static void __cdecl OnCastStart(void * heroCasting, PSDK_SPELL_CAST SpellCast, void * UserData);
	static void OnCastStartCheckSkillshot(void * heroCasting, PSDK_SPELL_CAST SpellCast);
	static void OnCastStartCheckCircle(void * heroCasting, PSDK_SPELL_CAST SpellCast);
	static void OnCastStartCheckTargeted(void * heroCasting, PSDK_SPELL_CAST SpellCast);

	static float shacoWCastOffset;
};


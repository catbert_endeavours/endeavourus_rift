#include "stdafx.h"
#include "ShacoDeceiveBackStabLogicManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "ShacoConstants.h"
#include "BaseSettings.h"
#include "ItemLogicManager.h"
#include "ShacoLogicManager.h"
#include "ShacoCloneLogicManager.h"

AttackableUnit* ShacoDeceiveBackStabLogicManager::PLAYER_TARGET{};
float ShacoDeceiveBackStabLogicManager::TARGET_LAST_UPDATE{ 0 };

AIHeroClient* ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET{};

void ShacoDeceiveBackStabLogicManager::Init() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::PreAttack, OnPreAttack);
}

void __cdecl ShacoDeceiveBackStabLogicManager::OnPreAttack(bool* Process, AttackableUnit** Target) {

	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
	
	if (orbwalkingMode == OrbwalkingMode::Combo) {
		if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 3 || ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 2) {
			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Targeted RCast = { SpellSlot::R, R.CastRange };

			if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
				ShacoLogicManager::PERFORM_SMOOTH_R_COMBO = true;
			}

			if (ShacoLogicManager::PERFORM_SMOOTH_R_COMBO) {
				*Process = false;
			}

			if (ItemLogicManager::ShouldCastShacoEBeforeAA || ItemLogicManager::ShouldCastShacoWBeforeAA || ItemLogicManager::ShouldCastShacoTiamatBeforeAA) {
				*Process = false;
			}
		}
	}

	if (BaseSettings::isChampEnabled()) {
		PLAYER_TARGET = *Target;
		TARGET_LAST_UPDATE = Game::Time() + 2;

		if (PLAYER_TARGET->IsValid() && PLAYER_TARGET->IsAlive() && PLAYER_TARGET->IsVisible() && !PLAYER_TARGET->IsZombie() && PLAYER_TARGET->IsHero()) {
			PLAYER_HERO_TARGET = PLAYER_TARGET->AsAIHeroClient();
		}
		else {
			PLAYER_HERO_TARGET = nullptr;
		}

		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_DECEIVE_BLOCK_AA_IN_DECEIVE_UNLESS_BACKSTAB, false)) {
			if (PLAYER_TARGET->IsHero()) {
				if (Player.HasBuff("Deceive")) {
					if (PLAYER_TARGET->AsAIHeroClient()->IsFacing(&Player)) {
						*Process = false;
					}
				}
			}
		}
	}
}
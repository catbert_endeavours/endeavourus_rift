#pragma once
#include <string>
#include <vector>
#include <SDK Extensions.h>

class SpellShieldManager
{
private:
	static bool isSpellShieldEnabledForSpell(std::string option);
	static bool isSpellShieldEnabled();
	static void LoadCurrentSupportedInGameSpells();
public:
	static std::vector<std::string> TARGETED_SPELLS;
	static std::vector<std::string> SPELL_SHIELD_MENU_OPTIONS;

	static void Init();
	static bool CanShieldSpell(std::string spellName);
};


#include "stdafx.h"
#include "SmiteDrawingManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "BaseSettings.h"
#include "SmiteManager.h"
#include "ChampionBase.h"
#include "JungleConstants.h"

void SmiteDrawingManager::DrawSmite()
{
	// For spellbook purposes
	if (SmiteManager::GetUnsealedSpellBookEnabled())
	{
		const bool smiteActive = EndeavourusMiscUtils::PlayerHasSmite();
		if (SmiteManager::GetPlayerHasSmite() != smiteActive)
		{
			ChampionBase::SetForceSettingsRefresh(true);
			SmiteManager::SetPlayerHasSmite(smiteActive);
		}
	}

	if (SmiteManager::GetPlayerHasSmite()) {
		DrawSmiteEnabledOnScreen();
		DrawSmiteRange();
	}
}

void SmiteDrawingManager::DrawSmiteRange()
{
	if (SmiteManager::GetPlayerHasSmite()) {
		if (BaseSettings::isSmiteDrawEnabled()) {
			PSDKCOLOR psdkcolor{ &BaseSettings::getSmiteRangeColour() };
			SDKVECTOR playerPosition{ Player.GetPosition() };

			Draw::Circle(&playerPosition, 500.0f, psdkcolor);
		}
	}
}

void SmiteDrawingManager::DrawSmiteEnabledOnScreen()
{
	const bool smiteEnabled = BaseSettings::isSmiteEnabled();
	SDKVECTOR playerPosition{ Player.GetPosition() };

	if (playerPosition.IsValid() && playerPosition.IsOnScreen()) {
		if (EndeavourusMiscUtils::GetBoolSetting(SMITE_DRAW_TEXT_ENABLED, true)) {
			playerPosition.y -= EndeavourusMiscUtils::GetFloatSetting(SMITE_DRAW_TEXT_Y_OFFSET, 75);
			playerPosition.x -= EndeavourusMiscUtils::GetFloatSetting(SMITE_DRAW_TEXT_X_OFFSET, 80);
			Draw::Text(&playerPosition, nullptr, smiteEnabled ? "Smite Enabled" : "Smite Disabled", "Arial", smiteEnabled ? &Color::Green : &Color::Red, 24, 8, 2);
			Draw::Text(&playerPosition, nullptr, smiteEnabled ? "Smite Enabled" : "Smite Disabled", "Arial", smiteEnabled ? &Color::Green : &Color::Red, 24, 8, 2);
		}
	}
}

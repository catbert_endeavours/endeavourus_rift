#pragma once
#include "BaseMenuManager.h"

class NocturneMenuManager : public BaseMenuManager
{
private:
	static void NocturneSpecificSpellLogic();
	static void SpecificChampMenuLogic();
	static void NocturneDrawingLogic();
public:
	static void Init();
	static void DrawMenu();
};


#include "stdafx.h"
#include "EndeavourusMundo.h"
#include "SmiteManager.h"
#include "MundoMenuManager.h"
#include "MundoLogicManager.h"
#include "BaseSettings.h"
#include "SmiteDrawingManager.h"
#include "MundoDrawingManager.h"
#include "EndeavourusMiscUtils.h"
#include "EndeavourusActivator.h"
#include "ActivatorMenuManager.h"

void EndeavourusMundo::Init()
{
	SmiteManager::Init();
	MundoMenuManager::Init();
	MundoLogicManager::Init();
	EndeavourusActivator::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::PostAttack, OnPostAttack);
}

void __cdecl EndeavourusMundo::OnUpdate(void * UserData)
{
	if (!Game::IsAvailable())
	{
		return;
	}

	SmiteManager::PerformSmiteLogic();

	if (BaseSettings::isChampEnabled()) {
		MundoLogicManager::PerformLogic();
	}
}

void __cdecl EndeavourusMundo::OnDraw(void * UserData)
{
	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();
	MundoDrawingManager::Draw();
}

void __cdecl EndeavourusMundo::OnDrawMenu(void * UserData)
{
	MundoMenuManager::DrawMenu();
}

void __cdecl EndeavourusMundo::OnPostAttack(void * UserData)
{
	if (BaseSettings::isChampEnabled()) {
		OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
		MundoLogicManager::CastE(orbwalkingMode);
	}
}

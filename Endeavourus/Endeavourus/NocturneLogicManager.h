#pragma once
#include <Spell.hpp>

class NocturneLogicManager
{
private:
	static Spell::Skillshot QCast;
	static Spell::Targeted ECast;
	static Spell::Targeted RCast;

	static void RefreshSpells();

	static void CastQ(OrbwalkingMode orbwalkingMode);
	static void CastE(OrbwalkingMode orbwalkingMode);
	static void CastR(OrbwalkingMode orbwalkingMode);

	static void KillSteal();

	static float GetQDamage(AIBaseClient * enemy);
	static float GetEDamage(AIBaseClient * enemy);
	static float GetRDamage(AIBaseClient * enemy);
	static void GetPrediction(OrbwalkingMode orbwalkingMode);
public:
	static void Init();
	static void PerformLogic();
	static void PerformCombo(OrbwalkingMode orbwalkingMode);
	static void PerformHarass(OrbwalkingMode orbwalkingMode);
	static void PerformClear(OrbwalkingMode orbwalkingMode);
	static void PerformLastHit(OrbwalkingMode orbwalkingMode);
	static void PerformEscape(OrbwalkingMode orbwalkingMode);
	static void RefreshRSpell();

	static void CastW(OrbwalkingMode orbwalkingMode);

	static std::vector<int> QDamage;
	static float QRatio;

	static std::vector<int> EDamage;
	static float ERatio;

	static std::vector<int> RDamage;
	static float RRatio;

	static Spell::Active WCast;

	static bool NOC_SHOULD_CAST_SHIELD;
};

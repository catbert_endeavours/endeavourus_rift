#include "stdafx.h"
#include "ShacoCloneLogicManager.h"
#include "SDK Extensions.h"
#include "EndeavourusLogicUtils.h"
#include <vector>
#include "EndeavourusMenu.h"
#include "ShacoConstants.h"
#include "ShacoDeceiveBackStabLogicManager.h"
#include "BaseSettings.h"

int ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX{ 0 };
std::vector<std::string> ShacoCloneLogicManager::SHACO_CLONE_OPTIONS{ "Attack Shaco's Target", "Attack Selected Enemy Hero", "Attack Closest Enemy",
"Mouse Position", "Ally Nexus (Follows at 1000 range)", "Farm", "Manual" };

int ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX{ 0 };
std::vector<std::string> ShacoCloneLogicManager::SHACO_COMBO_OPTIONS{ "(Standard) AA - Tiamat - E - AA", "(Standard + W) AA - Tiamat - E - W - AA",
"(Burst) R - AA - Tiamat - E - AA", "(Burst + W) R - AA - Tiamat - E - W - AA", "AP Mode - Spam E, W off Cooldown" };

float shacoCloneMoveCooldown{ 0 };
float moveLeft{ false };
Vector3 clonePos{ };

bool shacoChangeCloneModeButtonPressed{};
bool shacoChangeComboModeButtonPressed{};

bool ShacoCloneLogicManager::SHACO_CLONE_ENABLED{ false };
AIMinionClient* shacoMinion{};
void ShacoCloneLogicManager::PerformLogic() {
	UpdateCloneStatus();
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_CLONE_LOGIN_ENABLED, true)) {
		if (SHACO_CLONE_ENABLED == true) {
			if (shacoMinion != nullptr && shacoMinion->IsAlive() && shacoMinion->IsValid() && shacoMinion->IsVisible()) {

				if (SHACO_CLONE_OPTION_INDEX == 0) {
					AttackableUnit* unit = ShacoCloneLogicManager::GetShacoTarget();
					if (unit != nullptr) {
						pSDK->Control->Attack(unit, true);
					}
					else {
						pSDK->Control->Move(&Player.GetPosition(), true);
					}
				}
				else if (SHACO_CLONE_OPTION_INDEX == 1) {
					AIBaseClient* unit = ShacoCloneLogicManager::GetSelectedTarget();
					if (unit != nullptr) {
						pSDK->Control->Attack(unit, true);
					}
					else {
						pSDK->Control->Move(&Player.GetPosition(), true);
					}
				}
				else if (SHACO_CLONE_OPTION_INDEX == 2) {
					float distance{ FLT_MAX };
					AIHeroClient* hero{};
					for (auto e : pSDK->EntityManager->GetEnemyHeroes(1125.0f)) {
						if (e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
							float d{ Player.Distance(e.second) };
							if (d < distance) {
								distance = d;
								hero = e.second;
							}
						}

						if (hero != nullptr) {
							pSDK->Control->Attack(hero, true);
						}
						else {
							pSDK->Control->Move(&Player.GetPosition(), true);
						}
					}
				}
				else if (SHACO_CLONE_OPTION_INDEX == 3) {
					Vector3 m{ MousePos() };
					pSDK->Control->Move(&m, true);
				}
				else if (SHACO_CLONE_OPTION_INDEX == 4) {
					Vector3 t{ GetAllyNexusPosition() };

					float distance{ Player.Distance(shacoMinion) };
					if (distance < 1000 || distance > 1100.0f) {
						Vector3 clonePos{ Player.GetPosition().Extended(t, 1100.0f) };
						pSDK->Control->Move(&clonePos, true);
						/**
						else {
							if (Game::Time() > shacoCloneMoveCooldown) {
								shacoCloneMoveCooldown = Game::Time() + 1.0f;
								Vector3 clonePos{ shacoMinion->GetPosition() };

								if (moveLeft) {
									moveLeft = false;
									clonePos = shacoMinion->GetPosition();

									int distToMove = rand() % 200 + 100;
									Game::SendChat(std::to_string(distToMove));
									clonePos.x -= distToMove;

									pSDK->Control->Move(&clonePos, true);
								}
								else {
									moveLeft = true;
									clonePos = shacoMinion->GetPosition();

									int distToMove = rand() % 400 + 100;
									Game::SendChat(std::to_string(distToMove));
									clonePos.x += distToMove;

									pSDK->Control->Move(&clonePos, true);
								}
							}
							else if (shacoMinion->Distance(clonePos) <= 1) {
								shacoCloneMoveCooldown = 0.0f;
							}
						}
						*/
					}
				}
			}
			else if (SHACO_CLONE_OPTION_INDEX == 5) {
				AIBaseClient* minion{ EndeavourusMiscUtils::GetJungleMinionTarget(1125.0f, false) };

				if (minion != nullptr) {
					pSDK->Control->Attack(minion, true);
				}
				else {
					pSDK->Control->Move(&Player.GetPosition(), true);
				}
			}
			else {
				//Manual
			}
		}
	}
}

SDKVECTOR ShacoCloneLogicManager::GetAllyNexusPosition() {
	SDKVECTOR nexusPos{};
	for (auto n : pSDK->EntityManager->GetNexus()) {
		if (n.second->IsAlly()) {
			nexusPos = n.second->GetPosition();
		}
	}

	return nexusPos;
}

AITurretClient* ShacoCloneLogicManager::GetClosestAliveAllyTower() {
	float distance{ FLT_MAX };
	AITurretClient* turret{};
	for (auto t : pSDK->EntityManager->GetAllyTurrets()) {

		if (t.second->IsAlive() && t.second->IsAlly() && t.second->IsValid()) {
			if (Player.Distance(t.second) < distance) {
				turret = t.second;
			}
		}
	}

	return turret;
}

AttackableUnit* ShacoCloneLogicManager::GetEnemyTarget() {
	AttackableUnit* target = pCore->Orbwalker->GetTarget();

	if (target == nullptr) {
		target = pCore->TS->GetTarget(1125.0f);
	}

	if (target != nullptr && target->IsAlive() && target->IsValid() && !target->IsZombie()) {
		return target;
	}

	return nullptr;
}

AttackableUnit* ShacoCloneLogicManager::GetShacoTarget() {
	if (Game::Time() < ShacoDeceiveBackStabLogicManager::TARGET_LAST_UPDATE) {

		if (ShacoDeceiveBackStabLogicManager::PLAYER_TARGET != nullptr && ShacoDeceiveBackStabLogicManager::PLAYER_TARGET->IsValid() &&
			ShacoDeceiveBackStabLogicManager::PLAYER_TARGET->IsVisible() && ShacoDeceiveBackStabLogicManager::PLAYER_TARGET->IsAlive()) {

			return ShacoDeceiveBackStabLogicManager::PLAYER_TARGET;
		}
		else {
			ShacoDeceiveBackStabLogicManager::PLAYER_TARGET = nullptr;
		}
	}

	if (Game::Time() < PLAYER_POST_ATTACK_TARGET_TIMER) {

		if (PLAYER_POST_ATTACK_TARGET != nullptr && PLAYER_POST_ATTACK_TARGET->IsValid() &&
			PLAYER_POST_ATTACK_TARGET->IsVisible() && PLAYER_POST_ATTACK_TARGET->IsAlive()) {

			return PLAYER_POST_ATTACK_TARGET;
		}
		else {
			PLAYER_POST_ATTACK_TARGET = nullptr;
		}
	}


	return nullptr;
}



AIBaseClient* ShacoCloneLogicManager::GetSelectedTarget() {
	return EndeavourusLogicUtils::SELECTED_ENEMY;
}

Vector3 ShacoCloneLogicManager::MousePos() {
	return Renderer::MousePos();
}

void ShacoCloneLogicManager::ShacoCloneModeOptionSelection() {
	if (Game::IsAvailable()) {
		if (isCloneModeChangeKeyEnabled() && !shacoChangeCloneModeButtonPressed) {
			shacoChangeCloneModeButtonPressed = true;

			ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX++;
			if (ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX > 6) {
				ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX = 0;
			}
		}
		else if (!isCloneModeChangeKeyEnabled()) {
			shacoChangeCloneModeButtonPressed = false;
		}
	}
}

void ShacoCloneLogicManager::ShacoComboModeOptionSelection() {
	if (Game::IsAvailable()) {
		if (isComboModeChangeKeyEnabled() && !shacoChangeComboModeButtonPressed) {
			shacoChangeComboModeButtonPressed = true;

			ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX++;
			if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX > 4) {
				ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX = 0;
			}
		}
		else if (!isComboModeChangeKeyEnabled()) {
			shacoChangeComboModeButtonPressed = false;
		}
	}
}

bool ShacoCloneLogicManager::isComboModeChangeKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CYCLE_COMBO_MODE_KEY)).Active;
}

bool ShacoCloneLogicManager::isCloneModeChangeKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CYCLE_CLONSE_MODE_KEY)).Active;
}

bool ShacoCloneLogicManager::isPlayerFreezingAttackAndMoveKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_FREEZE_ATTACK_MOVE_KEY)).Active;
}

void ShacoCloneLogicManager::UpdateCloneStatus()
{
	bool found{ false };
	for (auto m : pSDK->EntityManager->GetAllyMinions(1500.0f)) {
		if (strcmp(m.second->GetName(), Player.GetName()) == 0) {
			if (m.second->GetNetworkID() != Player.GetNetworkID()) {
				if (m.second->IsAlive() && m.second->IsValid() && m.second->IsVisible()) {
					found = true;
					SHACO_CLONE_ENABLED = true;
					shacoMinion = m.second;
					break;
				}
			}
		}
	}

	if (!found) {
		SHACO_CLONE_ENABLED = false;
	}
}

float ShacoCloneLogicManager::PLAYER_POST_ATTACK_TARGET_TIMER{ 0.0F };
AIBaseClient* ShacoCloneLogicManager::PLAYER_POST_ATTACK_TARGET{};
void __cdecl ShacoCloneLogicManager::OnAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData) {

	GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(AI);
	GameObject* target = pSDK->EntityManager->GetObjectFromPTR(TargetObject);

	if (BaseSettings::isChampEnabled()) {
		if (hero != nullptr && target != nullptr) {
			if (hero->GetNetworkID() == Player.GetNetworkID()) {
				PLAYER_POST_ATTACK_TARGET_TIMER = Game::Time() + 2;
				PLAYER_POST_ATTACK_TARGET = target->AsAIBaseClient();
			}
		}
	}
}
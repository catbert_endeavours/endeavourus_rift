#pragma once
class SmiteDrawingManager
{
private:
	static void DrawSmiteRange();
	static void DrawSmiteEnabledOnScreen();
public:
	static void DrawSmite();
};


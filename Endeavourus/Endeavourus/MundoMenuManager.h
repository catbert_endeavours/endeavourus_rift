#pragma once
#include "BaseMenuManager.h"

class MundoMenuManager : public BaseMenuManager
{
private:
	static void MundoSpecificSpellLogic();
	static void SpecificChampMenuLogic();
	static void MundoDrawingLogic();
public:
	static void Init();
	static void DrawMenu();
};


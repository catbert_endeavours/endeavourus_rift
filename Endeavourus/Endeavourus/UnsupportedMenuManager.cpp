#include "stdafx.h"
#include "UnsupportedMenuManager.h"
#include "ActivatorMenuManager.h"

void UnsupportedMenuManager::Init()
{
	SetUnsupportedChampionLoaded(true);
}

void UnsupportedMenuManager::DrawMenu()
{
	BaseMenuManager::DrawMenu();
}
#include "stdafx.h"
#include "ActivatorMenuManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMenu.h"
#include "EndeavourusMiscUtils.h"
#include "ActivatorConstants.h"
#include "SmiteMenuManager.h"
#include "JungleConstants.h"

std::vector<std::string> ActivatorMenuManager::ENEMY_NAMES{};
std::vector<std::string> ActivatorMenuManager::ALLY_NAMES{};

std::string itemToPopulate{};

void ActivatorMenuManager::Init() {
	for (auto enemy : pSDK->EntityManager->GetEnemyHeroes())
	{
		if (enemy.second != nullptr)
		{
			ENEMY_NAMES.emplace_back(enemy.second->GetCharName());
		}
	}

	for (auto ally : pSDK->EntityManager->GetAllyHeroes())
	{
		if (ally.second != nullptr)
		{
			ALLY_NAMES.emplace_back(ally.second->GetCharName());
		}
	}
}

void ActivatorMenuManager::HextechProtobeltGLP800() {
	EndeavourusMenu::Checkbox("Hextech Enabled", EndeavourusMiscUtils::GetUniqueSettingName(HextechProtobelt_ENABLED), true);

	std::vector<std::string> ProtoBeltOptions{ "On Target Health", "Always" };
	EndeavourusMenu::DropList("Hextech Mode", EndeavourusMiscUtils::GetUniqueSettingName(HEXTECH_PRO_GLP_MODE), ProtoBeltOptions, 0);

	int mode = EndeavourusMiscUtils::GetIntSetting(HEXTECH_PRO_GLP_MODE);
	if (mode == 0) {
		EndeavourusMenu::SliderFloat("Target Health %", EndeavourusMiscUtils::GetUniqueSettingName(HEXTECH_PRO_GLP_HEALTH), 75.0f, 0, 100);
	}
}

void ActivatorMenuManager::HextechGunblade() {
	EndeavourusMenu::Checkbox("Gunblade Enabled", EndeavourusMiscUtils::GetUniqueSettingName(HextechGunblade_ENABLED), true);

	std::vector<std::string> GunBladeOptions{ "On Target Health", "Always" };
	EndeavourusMenu::DropList("Gunblade Mode", EndeavourusMiscUtils::GetUniqueSettingName(HextechGunblade_MODE), GunBladeOptions, 0);

	int mode = EndeavourusMiscUtils::GetIntSetting(HextechGunblade_MODE);
	if (mode == 0) {
		EndeavourusMenu::SliderFloat("Target Health %", EndeavourusMiscUtils::GetUniqueSettingName(HextechGunblade_HEALTH), 75.0f, 0, 100);
	}
}

void ActivatorMenuManager::Spellbinder() {
	EndeavourusMenu::Checkbox("SpellBinder Enabled", EndeavourusMiscUtils::GetUniqueSettingName(Spellbinder_ENABLED), true);
	EndeavourusMenu::SliderFloat("Enemy Search Radius", EndeavourusMiscUtils::GetUniqueSettingName(Spellbinder_ENEMY_RANGE), 750.0f, 0, 3000);

	std::vector<std::string> SpellBinderOptions{ "On Target Health", "Always" };
	EndeavourusMenu::DropList("SpellBinder Mode", EndeavourusMiscUtils::GetUniqueSettingName(Spellbinder_MODE), SpellBinderOptions, 0);

	int mode = EndeavourusMiscUtils::GetIntSetting(Spellbinder_MODE);
	if (mode == 0) {
		EndeavourusMenu::SliderFloat("Target Health %", EndeavourusMiscUtils::GetUniqueSettingName(Spellbinder_HEALTH), 75.0f, 0, 100);
	}
}

void ActivatorMenuManager::EdgeOfNight() {
	EndeavourusMenu::Checkbox("Edge of Night Enabled", EndeavourusMiscUtils::GetUniqueSettingName(EDGE_OF_NIGHT_ENABLED), true);
	EndeavourusMenu::SliderFloat("If Closest Target Distance >", EndeavourusMiscUtils::GetUniqueSettingName(EDGE_OF_NIGHT_CLOSEST_TARGET), 1000.0f, 0, 10000);
}

void ActivatorMenuManager::TwinShadows() {
	EndeavourusMenu::Checkbox("Twin Shadows Enabled", EndeavourusMiscUtils::GetUniqueSettingName(TwinShadows_ENABLED), true);
	EndeavourusMenu::SliderFloat("If Closest Target Distance >", EndeavourusMiscUtils::GetUniqueSettingName(TwinShadows_CLOSEST_TARGET), 2500.0f, 0, 10000);
}

void ActivatorMenuManager::Tiamat() {
	EndeavourusMenu::Checkbox("Tiamat/Hydra Reset Enabled", EndeavourusMiscUtils::GetUniqueSettingName(TiamatENABLED), true);
	EndeavourusMenu::Checkbox("Tiamat/Hydra Reset Combo", EndeavourusMiscUtils::GetUniqueSettingName(TiamatENABLED_COMBO), true);
	EndeavourusMenu::Checkbox("Tiamat/Hydra Reset Clear/Jungle", EndeavourusMiscUtils::GetUniqueSettingName(TiamatENABLED_CLEAR), true);

}

void ActivatorMenuManager::BilgewaterCutlassBOTRK() {
	EndeavourusMenu::Checkbox("Bilgewater Cutlass/BOTRK Enabled", EndeavourusMiscUtils::GetUniqueSettingName(BilgewaterCutlassBOTRK_ENABLED), true);

	std::vector<std::string> GunBladeOptions{ "On Target Health", "Always" };
	EndeavourusMenu::DropList("Bilgewater Cutlass/BOTRK Mode", EndeavourusMiscUtils::GetUniqueSettingName(BilgewaterCutlassBOTRKMODE), GunBladeOptions, 0);

	int mode = EndeavourusMiscUtils::GetIntSetting(BilgewaterCutlassBOTRKMODE);
	if (mode == 0) {
		EndeavourusMenu::SliderFloat("Target Health %", EndeavourusMiscUtils::GetUniqueSettingName(BilgewaterCutlassBOTRKHEALTH), 75.0f, 0, 100);
	}
}

void ActivatorMenuManager::YoumuusGhostblade() {
	EndeavourusMenu::Checkbox("Youmuus Ghostblade Enabled", EndeavourusMiscUtils::GetUniqueSettingName(YoumuusGhostbladeENABLED), true);
	EndeavourusMenu::SliderFloat("If an Enemy is within Distance", EndeavourusMiscUtils::GetUniqueSettingName(YoumuusGhostbladeECLOSEST_TARGET), 700.0f, 0, 5000);
}

void ActivatorMenuManager::ArchangelsStaff() {
	EndeavourusMenu::Checkbox("Archangels Staff Enabled", EndeavourusMiscUtils::GetUniqueSettingName(ArchangelsStaffENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(ArchangelsStaff_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Cast at Health %", EndeavourusMiscUtils::GetUniqueSettingName(ArchangelsStaff_PERCENT), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Cast if Enemy is withing Range", EndeavourusMiscUtils::GetUniqueSettingName(ArchangelsStaff_ENEMY_DISTANCE), 700.0f, 0, 3000);
}

void ActivatorMenuManager::ZhonyasHourglass() {
	EndeavourusMenu::Checkbox("Zhonyas Hourglass Enabled", EndeavourusMiscUtils::GetUniqueSettingName(ZhonyasHourglassENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(ZhonyasHourglass_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Cast at Health %", EndeavourusMiscUtils::GetUniqueSettingName(ZhonyasHourglassPERCENT), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Cast if Enemy is withing Range", EndeavourusMiscUtils::GetUniqueSettingName(ZhonyasHourglass_ENEMY_DISTANCE), 700.0f, 0, 3000);
}

void ActivatorMenuManager::ShurelyasReverie() {
	EndeavourusMenu::Checkbox("Shurelyas Reverie Enabled", EndeavourusMiscUtils::GetUniqueSettingName(ShurelyasReverieENABLED), true);
	EndeavourusMenu::SliderInt("Enemies within Below Range", EndeavourusMiscUtils::GetUniqueSettingName(ShurelyasReverie_MIN_ENEMIES), 2, 0, 5);
	EndeavourusMenu::SliderFloat("Enemy Range", EndeavourusMiscUtils::GetUniqueSettingName(ShurelyasReverie_ENEMIES_DISTANCE), 1000, 0, 3000);
}

void ActivatorMenuManager::QuicksilverSashMercurialScimitar() {
	EndeavourusMenu::Checkbox("QuicksilverSash/Mercurial Scimitar Enabled", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_ENABLED), true);
	EndeavourusMenu::Checkbox("Enable on Charm", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_CHARM), true);
	EndeavourusMenu::Checkbox("Enable on Blind", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_BLIND), true);
	EndeavourusMenu::Checkbox("Enable on Disarm", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_DISARM), true);
	EndeavourusMenu::Checkbox("Enable on Fear", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_FEAR), true);
	EndeavourusMenu::Checkbox("Enable on Flee", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_FLEE), true);
	EndeavourusMenu::Checkbox("Enable on Polymorph", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_POLY), true);
	EndeavourusMenu::Checkbox("Enable on Snare", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_SNARE), true);
	EndeavourusMenu::Checkbox("Enable on Taunt", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_TAUNT), true);
	EndeavourusMenu::Checkbox("Enable on Exhaust", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_EXHAUST), true);
	EndeavourusMenu::Checkbox("Enable on Stun", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_STUN), true);
	EndeavourusMenu::Checkbox("Enable on Suppression", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarBUFF_TYPE_SUPRESSION), true);
}

void ActivatorMenuManager::MikaelsCrucible() {
	EndeavourusMenu::Checkbox("Mikaels Crucible Enabled", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_ENABLED), true);
	EndeavourusMenu::Checkbox("Enable on Charm", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_CHARM), true);
	EndeavourusMenu::Checkbox("Enable on Blind", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_BLIND), true);
	EndeavourusMenu::Checkbox("Enable on Disarm", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_DISARM), true);
	EndeavourusMenu::Checkbox("Enable on Fear", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_FEAR), true);
	EndeavourusMenu::Checkbox("Enable on Flee", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_FLEE), true);
	EndeavourusMenu::Checkbox("Enable on Polymorph", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_POLY), true);
	EndeavourusMenu::Checkbox("Enable on Snare", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_SNARE), true);
	EndeavourusMenu::Checkbox("Enable on Taunt", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_TAUNT), true);
	EndeavourusMenu::Checkbox("Enable on Exhaust", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_EXHAUST), true);
	EndeavourusMenu::Checkbox("Enable on Stun", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_STUN), true);
	EndeavourusMenu::Checkbox("Enable on Suppression", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBUFF_TYPE_SUPRESSION), true);

	itemToPopulate = MikaelsCrucibleBLACKLIST_ENABLED;
	EndeavourusMenu::Tree("Mikaels Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleBLACKLIST), false, AllyBlacklistSettings);
}

void ActivatorMenuManager::LocketoftheIronSolari() {
	EndeavourusMenu::Checkbox("Locket Enabled", EndeavourusMiscUtils::GetUniqueSettingName(LocketoftheIronSolari_ENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(LocketoftheIronSolari_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Ally/Player Health % to cast", EndeavourusMiscUtils::GetUniqueSettingName(LocketoftheIronSolari_ALLY_HEALTH), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Enemy within Range", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplatee_ENEMIES_WITHIN_RANGE), 750, 0, 3000);
}

void ActivatorMenuManager::GargoyleStoneplate() {
	EndeavourusMenu::Checkbox("Gargoyle Stoneplate Enabled", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplateeENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplatee_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Player Health %", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplatee_HEALTH), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Enemy within Range", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplatee_ENEMIES_WITHIN_RANGE), 750.0f, 0, 3000);
}

void ActivatorMenuManager::Redemption() {
	EndeavourusMenu::Checkbox("Redemption Ally Enabled", EndeavourusMiscUtils::GetUniqueSettingName(REDEMTION_ALLY_ENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(REDEMTION_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Ally Health to Cast", EndeavourusMiscUtils::GetUniqueSettingName(REDEMTION_ALLY_PERCENT), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Enemy within Range of ally", EndeavourusMiscUtils::GetUniqueSettingName(REDEMTION_ENEMY_RANGE), 750.0f, 0, 3000);
}


void ActivatorMenuManager::Ohmwrecker() {
	EndeavourusMenu::Checkbox("Ohmwrecker Enabled", EndeavourusMiscUtils::GetUniqueSettingName(OhmwreckerENABLED), true);
}

void ActivatorMenuManager::RighteousGlory() {
	EndeavourusMenu::Checkbox("Righteous Glory Enabled", EndeavourusMiscUtils::GetUniqueSettingName(RighteousGloryENABLED), true);
	EndeavourusMenu::SliderInt("Enemies within Below Range", EndeavourusMiscUtils::GetUniqueSettingName(RighteousGlory_MIN_ENEMIES), 2, 0, 5);
	EndeavourusMenu::SliderFloat("Enemy Range", EndeavourusMiscUtils::GetUniqueSettingName(RighteousGlory_ENEMIES_DISTANCE), 1000, 0, 3000);
}

void ActivatorMenuManager::DefensiveItems()
{
	EndeavourusMenu::Tree("Archangels Staff", EndeavourusMiscUtils::GetUniqueSettingName(ArchangelsStaffTREE), false, ArchangelsStaff);
	EndeavourusMenu::Tree("Zhonyas Hourglass", EndeavourusMiscUtils::GetUniqueSettingName(ZhonyasHourglassTREE), false, ZhonyasHourglass);
	EndeavourusMenu::Tree("Shurelyas Reverie", EndeavourusMiscUtils::GetUniqueSettingName(ShurelyasReverieTREE), false, ShurelyasReverie);
	EndeavourusMenu::Tree("Quicksilver/Mercurial Scimitar", EndeavourusMiscUtils::GetUniqueSettingName(QuicksilverSashMercurialScimitarTREE), false, QuicksilverSashMercurialScimitar);
	EndeavourusMenu::Tree("Mikaels Crucible", EndeavourusMiscUtils::GetUniqueSettingName(MikaelsCrucibleTREE), false, MikaelsCrucible);
	EndeavourusMenu::Tree("Locket of the Iron Solari", EndeavourusMiscUtils::GetUniqueSettingName(LocketoftheIronSolariTREE), false, LocketoftheIronSolari);
	EndeavourusMenu::Tree("Gargoyle Stoneplate", EndeavourusMiscUtils::GetUniqueSettingName(GargoyleStoneplateTREE), false, GargoyleStoneplate);
	EndeavourusMenu::Tree("Redemption", EndeavourusMiscUtils::GetUniqueSettingName(RedemptionTREE), false, Redemption);
	EndeavourusMenu::Tree("Ohmwrecker", EndeavourusMiscUtils::GetUniqueSettingName(OhmwreckerTREE), false, Ohmwrecker);
	EndeavourusMenu::Tree("Righteous Glory", EndeavourusMiscUtils::GetUniqueSettingName(RighteousGloryTREE), false, RighteousGlory);
	EndeavourusMenu::Tree("Edge of Night", EndeavourusMiscUtils::GetUniqueSettingName(EDGE_OF_NIGH_TREE), false, EdgeOfNight);
}

void ActivatorMenuManager::OffensiveItems()
{
	EndeavourusMenu::Tree("HextechProtobelt/GLP800", EndeavourusMiscUtils::GetUniqueSettingName(HextechProtobeltGLP800TREE), false, HextechProtobeltGLP800);
	EndeavourusMenu::Tree("Hextech Gunblade", EndeavourusMiscUtils::GetUniqueSettingName(HextechGunbladeTREE), false, HextechGunblade);
	EndeavourusMenu::Tree("Spellbinder", EndeavourusMiscUtils::GetUniqueSettingName(SpellbinderTREE), false, Spellbinder);
	EndeavourusMenu::Tree("TwinShadows", EndeavourusMiscUtils::GetUniqueSettingName(TwinShadowsTREE), false, TwinShadows);
	EndeavourusMenu::Tree("Tiamat/TitanicHydra/RavenousHydra", EndeavourusMiscUtils::GetUniqueSettingName(TiamatTREE), false, Tiamat);
	EndeavourusMenu::Tree("BilgewaterCutlass/BOTRK", EndeavourusMiscUtils::GetUniqueSettingName(BilgewaterCutlassBOTRKTREE), false, BilgewaterCutlassBOTRK);
	EndeavourusMenu::Tree("Youmuus Ghostblade", EndeavourusMiscUtils::GetUniqueSettingName(YoumuusGhostbladeTREE), false, YoumuusGhostblade);
}

void ActivatorMenuManager::Items()
{
	EndeavourusMenu::Tree("Offensive Items", EndeavourusMiscUtils::GetUniqueSettingName(OFFENSIVE_ITEMS_TREE), false, OffensiveItems);
	EndeavourusMenu::Tree("Defensive Items", EndeavourusMiscUtils::GetUniqueSettingName(DEFENSIVE_ITEMS_TREE), false, DefensiveItems);
}

void ActivatorMenuManager::Ignite()
{
	EndeavourusMenu::Checkbox("Cast Ignite if killable", EndeavourusMiscUtils::GetUniqueSettingName(IGNITE_ENABLED), true);

	itemToPopulate = IGNITE_BLACKLIST_ENABLED;
	EndeavourusMenu::Tree("Ignite Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(IGNITE_BLACKLIST), false, BlacklistSettings);
}

void ActivatorMenuManager::Exhaust()
{
	EndeavourusMenu::Checkbox("Enable Exhaust", EndeavourusMiscUtils::GetUniqueSettingName(EXHAUST_ENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(EXHAUST_ICOMING_DAMAGE_ENABLED), true);

	EndeavourusMenu::SliderFloat("Player/Ally Health %", EndeavourusMiscUtils::GetUniqueSettingName(EXHAUST_PLAYER_ALLY_HEALTH), 25, 0, 100);

	itemToPopulate = EXHAUST_BLACKLIST_ENABLED;
	EndeavourusMenu::Tree("Exhaust Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(EXHAUST_BLACKLIST), false, BlacklistSettings);
}

void ActivatorMenuManager::Cleanse()
{
	EndeavourusMenu::Checkbox("Cleanse Enabled", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_ENABLED), true);
	EndeavourusMenu::Checkbox("Enable on Charm", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_CHARM), true);
	EndeavourusMenu::Checkbox("Enable on Blind", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_BLIND), true);
	EndeavourusMenu::Checkbox("Enable on Disarm", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_DISARM), true);
	EndeavourusMenu::Checkbox("Enable on Fear", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_FEAR), true);
	EndeavourusMenu::Checkbox("Enable on Flee", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_FLEE), true);
	EndeavourusMenu::Checkbox("Enable on Polymorph", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_POLY), true);
	EndeavourusMenu::Checkbox("Enable on Snare", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_SNARE), true);
	EndeavourusMenu::Checkbox("Enable on Taunt", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_TAUNT), true);
	EndeavourusMenu::Checkbox("Enable on Exhaust", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_EXHAUST), true);
	EndeavourusMenu::Checkbox("Enable on Stun", EndeavourusMiscUtils::GetUniqueSettingName(CleanseBUFF_TYPE_STUN), true);
}

void ActivatorMenuManager::Heal()
{
	EndeavourusMenu::Checkbox("Enable Heal", EndeavourusMiscUtils::GetUniqueSettingName(HEAL_ENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(HEAL_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Player/Ally Health %", EndeavourusMiscUtils::GetUniqueSettingName(HEAL_PERCENT), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Enemy within Range of ally", EndeavourusMiscUtils::GetUniqueSettingName(HEAL_ENEMY_RANGE), 750.0f, 0, 3000);

	itemToPopulate = Heal_BLACKLIST_ENABLED;
	EndeavourusMenu::Tree("Heal Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(Heal_BLACKLIST), false, AllyBlacklistSettings);
}

void ActivatorMenuManager::Barrier()
{
	EndeavourusMenu::Checkbox("Enable Barrier", EndeavourusMiscUtils::GetUniqueSettingName(BARRIER_ENABLED), true);
	EndeavourusMenu::Checkbox("Incoming damage checks (Excludes skillshots) before casting", EndeavourusMiscUtils::GetUniqueSettingName(BARRIER_ICOMING_DAMAGE_ENABLED), true);
	EndeavourusMenu::SliderFloat("Player Health %", EndeavourusMiscUtils::GetUniqueSettingName(BARRIER_PERCENT), 25, 0, 100);
	EndeavourusMenu::SliderFloat("Enemy within Range of ally", EndeavourusMiscUtils::GetUniqueSettingName(BARRIER_ENEMY_RANGE), 750.0f, 0, 3000);
}

void ActivatorMenuManager::SummonerSpells()
{
	EndeavourusMenu::Tree("Smite Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_GROUP), false, SmiteMenuManager::DrawSmiteSettings);
	EndeavourusMenu::Tree("Heal Settings", EndeavourusMiscUtils::GetUniqueSettingName(HEAL_TREE), false, ActivatorMenuManager::Heal);
	EndeavourusMenu::Tree("Barrier Settings", EndeavourusMiscUtils::GetUniqueSettingName(BARRIER_TREE), false, ActivatorMenuManager::Barrier);
	EndeavourusMenu::Tree("Ignite Settings", EndeavourusMiscUtils::GetUniqueSettingName(IGNITE_TREE), false, ActivatorMenuManager::Ignite);
	EndeavourusMenu::Tree("Exhaust Settings", EndeavourusMiscUtils::GetUniqueSettingName(CLEANSE_TREE), false, ActivatorMenuManager::Exhaust);
	EndeavourusMenu::Tree("Cleanse Settings", EndeavourusMiscUtils::GetUniqueSettingName(EXHAUST_TREE), false, ActivatorMenuManager::Cleanse);
}

void ActivatorMenuManager::ActivatorSettings()
{
	EndeavourusMenu::Checkbox("Enable Activator", EndeavourusMiscUtils::GetUniqueSettingName(ACTIVATOR_ENABLED), true);
	SdkUiText("-----------Enabled by using Combo Key-----------");
	EndeavourusMenu::Tree("Summoner Spells", EndeavourusMiscUtils::GetUniqueSettingName(SUMMONER_SPELLS_TREE), false, SummonerSpells);
	EndeavourusMenu::Tree("Items", EndeavourusMiscUtils::GetUniqueSettingName(ITEMS_TREE), false, Items);
}

void ActivatorMenuManager::DrawActivatorSettings()
{
	EndeavourusMenu::Tree("Activator Settings", EndeavourusMiscUtils::GetUniqueSettingName(ACTIVATOR_TREE), false, ActivatorSettings);
}

void ActivatorMenuManager::BlacklistSettings()
{
	for (auto enemyName : ENEMY_NAMES)
	{
		EndeavourusMenu::Checkbox(("Block on " + enemyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(itemToPopulate + "_" + enemyName), false);
	}
}

void ActivatorMenuManager::AllyBlacklistSettings()
{
	for (auto allyName : ALLY_NAMES)
	{
		EndeavourusMenu::Checkbox(("Block on " + allyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(itemToPopulate + "_" + allyName), false);
	}
}
#include "stdafx.h"
#include "SmiteManager.h"
#include "BaseSettings.h"
#include "JungleConstants.h"
#include "EndeavourusMiscUtils.h"

bool SmiteManager::playerHasSmite{ false };
bool SmiteManager::unsealedSpellBookEnabled{ false };

void SmiteManager::Init()
{
	SetUnsealedSpellbookEnabled(EndeavourusMiscUtils::PlayerHasUnsealedSpellbook());
	SetPlayerHasSmite(EndeavourusMiscUtils::PlayerHasSmite());
}

void SmiteManager::PerformSmiteLogic()
{
	if (GetPlayerHasSmite() && BaseSettings::isSmiteEnabled()) {
		SDK_SPELL smiteSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find(SMITE) != std::string::npos)
		{
			smiteSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find(SMITE) != std::string::npos)
		{
			smiteSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (smiteSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Targeted smite = Spell::Targeted{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2, smiteSpell.CastRange, DamageType::True };
			int smiteMode = BaseSettings::getSmiteMode();
			CastSmiteAtMonster(smite, smiteMode);
			CastSmiteKS(smite, smiteMode);
			CastSmiteCombo(smite, smiteMode);
		}
	}
}

void SmiteManager::CastSmiteAtMonster(Spell::Targeted smite, const int smiteMode)
{
	AIBaseClient* creep = SmiteManager::GetSmiteCreepTarget(smite.Range);

	if (creep != nullptr && smite.IsReady()) {
		std::string creepName(creep->GetName());
		if (isEnabledMonster(creepName)) {
			if (smiteMode == 0) {
				smite.Cast(creep);
			}
			else if (smiteMode == 1) {
				// Save one charge
				if (smite.AmmoQuantity() > 1 || BaseSettings::isSmiteAssistKeyEnabled())
				{
					smite.Cast(creep);
				}
				else if (creepName.find(BARON) != std::string::npos || creepName.find(DRAGON) != std::string::npos || creepName.find(RIFT) != std::string::npos)
				{
					smite.Cast(creep);
				}
			}
			else if (smiteMode == 2) {
				// Only on assist key press
				if (BaseSettings::isSmiteAssistKeyEnabled())
				{
					smite.Cast(creep);
				}
			}
		}
	}
}

AIBaseClient* SmiteManager::GetSmiteCreepTarget(const float smiteRange) {
	for (auto creep : pSDK->EntityManager->GetJungleMonsters(smiteRange))
	{
		for (auto creepName : JUNGLE_CREEPS)
		{
			if (creep.second != nullptr) {
				std::string creepString = std::string(creep.second->GetName());
				if (creepString.find(creepName) != std::string::npos && creepString.find("Mini") == std::string::npos)
				{
					AIBaseClient* c = creep.second;
					int damage = SMITE_M_DAMAGE[Player.GetLevel() - 1];

					if (creep.second->GetHealth().Current <= damage)
					{
						return creep.second;
					}
				}
			}
		}
	}

	return nullptr;
}

bool SmiteManager::isEnabledMonster(std::string creepName)
{
	if (creepName.find(BARON) != std::string::npos)
	{
		return BaseSettings::isSmiteBaronEnabled();
	}
	else if (creepName.find(DRAGON) != std::string::npos)
	{
		if (creepName.find(DRAGON_OCEAN) != std::string::npos)
		{
			return BaseSettings::isSmiteOceanEnabled();
		}
		else if (creepName.find(DRAGON_MOUNTAIN) != std::string::npos)
		{
			return BaseSettings::isSmiteMountainEnabled();
		}
		else if (creepName.find(DRAGON_INFERNAL) != std::string::npos)
		{
			return BaseSettings::isSmiteInfernalEnabled();
		}
		else if (creepName.find(DRAGON_CLOUD) != std::string::npos)
		{
			return BaseSettings::isSmiteCloudEnabled();
		}
		else if (creepName.find(DRAGON_ELDER) != std::string::npos)
		{
			return BaseSettings::isSmiteElderEnabled();
		}
	}
	else if (creepName.find(RIFT) != std::string::npos)
	{
		return BaseSettings::isSmiteRiftEnabled();
	}
	else if (creepName.find(RED_BUFF) != std::string::npos)
	{
		return BaseSettings::isSmiteRedEnabled();
	}
	else if (creepName.find(BLUE_BUFF) != std::string::npos)
	{
		return BaseSettings::isSmiteBlueEnabled();
	}
	else if (creepName.find(WOLF) != std::string::npos)
	{
		return BaseSettings::isSmiteWolfEnabled();
	}
	else if (creepName.find(RAPTOR) != std::string::npos)
	{
		return BaseSettings::isSmiteRaptorEnabled();
	}
	else if (creepName.find(KRUGS) != std::string::npos)
	{
		return BaseSettings::isSmiteKrugEnabled();
	}
	else if (creepName.find(GROMP) != std::string::npos)
	{
		return BaseSettings::isSmiteGrompEnabled();
	}
	else if (creepName.find(CRAB) != std::string::npos)
	{
		return BaseSettings::isSmiteCrabEnabled();
	}

	return false;
}

void SmiteManager::CastSmiteKS(Spell::Targeted smite, const int smiteMode)
{
	// IF we are saving a smite charge we do not want to waste it
	if ((smite.AmmoQuantity() == 1 || smite.AmmoQuantity() == 0) && smiteMode == 1)
	{
		return;
	}

	if (BaseSettings::isSmitePlayerKsEnabled() && smite.Name().find(SMITE_PLAYER_GANK) != std::string::npos)
	{
		if (smite.AmmoQuantity() >= 1 && !smite.IsOnCooldown()) {
			for (auto enemy : pSDK->EntityManager->GetEnemyHeroes(smite.Range))
			{
				if (enemy.second != nullptr) {
					if (enemy.second->GetHealth().Current <= SMITE_P_DAMAGE[Player.GetLevel() - 1])
					{
						AIHeroClient* target = enemy.second;
						if (target != nullptr && target->IsAlive() && target->IsValid())
						{
							smite.Cast(target);
						}
					}
				}
			}
		}
	}
}

void SmiteManager::CastSmiteCombo(Spell::Targeted smite, const int smiteMode)
{
	// IF we are saving a smite charge we do not want to waste it
	if ((smite.AmmoQuantity() == 1 || smite.AmmoQuantity() == 0) && smiteMode == 1)
	{
		return;
	}

	bool attackPlayer{ false };
	const bool comboActive = BaseSettings::isComboKeyEnabled();
	if (comboActive) {

		if (BaseSettings::isSmitePlayerBlueEnabled() && smite.Name().find(SMITE_PLAYER_GANK) != std::string::npos) {
			attackPlayer = true;
		}
		else if (BaseSettings::isSmitePlayedRedEnabled() && smite.Name().find(SMITE_PLAYER_DUEL) != std::string::npos) {
			attackPlayer = true;
		}

		if (attackPlayer)
		{
			if (smite.AmmoQuantity() >= 1 && !smite.IsOnCooldown()) {
				AIHeroClient* target = pCore->TS->GetTarget(smite.Range);
				if (target != nullptr && target->IsAlive() && target->IsValid())
				{
					smite.Cast(target);
				}
			}
		}
	}
}

bool SmiteManager::GetPlayerHasSmite()
{
	return playerHasSmite;
}

void SmiteManager::SetPlayerHasSmite(const bool playerHasSmite)
{
	SmiteManager::playerHasSmite = playerHasSmite;
}

bool SmiteManager::GetUnsealedSpellBookEnabled()
{
	return unsealedSpellBookEnabled;
}

void SmiteManager::SetUnsealedSpellbookEnabled(const bool unsealedSpellBookEnabled)
{
	SmiteManager::unsealedSpellBookEnabled = unsealedSpellBookEnabled;
}

#pragma once
#include "BaseMenuManager.h"
#include <vector>
#include <string>
class ShacoMenuManager : public BaseMenuManager
{
public:
	static void Init();
	static void SpecificChampMenuLogic();
	static void DrawQTimer();
	static void DrawCloneText();
	static void DrawComboText();
	static void DrawDeceiveSpots();
	static void DrawWSpellDodge();
	static void AntiBlitzcrank();
	static void DrawAntiBlitzcrank();
	static void ShacoWSkillshot();
	static void ShacoRSkillshot();
	static void ShacoRCircle();
	static void ShacoRTargeted();
	static void ShacoRDodgeSettings();
	static void ShacoSpellDodgeSettings();
	static void DrawShacoSpellDodge();
	static void DeceiveSpots();
	static void DrawQText();
	static void DrawCloneModeText();
	static void DrawComboModeText();
	static void ShacoDrawingLogic();
	static void DrawMenu();
	static void ShacoAutoJungleSettings();
	static void ShacoComboSettings();
	static void ShacoAPComboSettings();
	static void ShacoHarassSettings();
	static void ShacoRSettings();
	static void ShacoWBlockSettings();
	static void ShacoClearAndJungleSettings();
	static void ShacoSpecificSpellLogic();

	static void ShacoDeceiveSettings();

	static void ShacoDeceiveAssistant();

	static int TOP_BOTTOM_DROPDOWN_INDEX;
};


#pragma once
#include <string>

const std::string TEEMO_TRAP = "Noxious Trap";

const std::string ENABLE_SHROOM_GAME = "_DRAW_SHROOM_GAME";
const std::string ENABLE_SHROOM_MAP = "_DRAW_SHROOM_MAP";
const std::string ENABLE_SHROOM_DISTANCE = "_DRAW_SHROOM_DISTANCE";

const std::string ENABLE_SHROOM_DRAWINGS = "_SHROOM_DRAWING";
const std::string ENABLE_SHROOM_DRAWINGS_DISTANCE = "_SHROOM_DRAWING_DISTANCE";

const std::string TEEMO_LOGIC_SETTING_GROUP = "TEEMO_LOGIC_SETTING_GROUP";
const std::string ENABLE_AUTO_R_SHROOM_SPOTS = "_ENABLE_AUTO_R_SHROOM_SPOTS";
const std::string ENABLE_AUTO_R_IMMOBILE = "_AUTO_R_IMMOBILE";

const std::string ENABLE_SHROOM_DRAWINGS_MASTER = "ENABLE_SHROOM_DRAWINGS_MASTER";
const std::string SHROOM_DISTANCE_DRAWINGS = "_SHROOM_DISTANCE_DRAWINGS";

const std::string R_PLACEMENT_CIRCLE_COLOUR = "_R_PLACEMENT_CIRCLE_COLOUR";
const std::string SHROOM_LOCATION_COLOUR_GROUP = "SHROOM_LOCATION_COLOUR_GROUP";

const std::string TEEMO_R_SNAP_KEY_ENABLED = "_TEEMO_R_SNAP_KEY_ENABLED";

// Caters for our placed objects not being directly on the position we set
const float TEEMO_R_PLACEMENT_POS_OFFSET = 150.0f;
const float TEEMO_R_PLACEMENT_TIME_OFFSET = 0.80f;

const float TEEMO_R_PLACEMENT_AUTO_SHROOM_TIME_OFFSET = 1.5f;

#include "stdafx.h"
#include "EndeavourusLogicUtils.h"
#include "BaseSettings.h"
#include "BaseMenuManager.h"

AIBaseClient* EndeavourusLogicUtils::SELECTED_ENEMY{ nullptr };

bool EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode orbwalkingMode, SpellSlot spellSlot, std::string enemyName, bool ksCheck)
{
	switch (spellSlot)
	{
	case SpellSlot::Q:
		return !isQSpellBlockedOnChampion(orbwalkingMode, enemyName, ksCheck);

	case SpellSlot::W:
		return !isWSpellBlockedOnChampion(orbwalkingMode, enemyName, ksCheck);

	case SpellSlot::E:
		return !isESpellBlockedOnChampion(orbwalkingMode, enemyName, ksCheck);

	case SpellSlot::R:
		return !isRSpellBlockedOnChampion(orbwalkingMode, enemyName, ksCheck);

	default:
		return true;
	}
}

bool EndeavourusLogicUtils::isQSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck)
{
	if (orbwalkingMode == OrbwalkingMode::Combo)
	{
		return BaseMenuManager::GetComboResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionCombo(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Mixed)
	{
		return BaseMenuManager::GetHarassResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionHarass(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LaneClear || orbwalkingMode == OrbwalkingMode::JungleClear)
	{
		return BaseMenuManager::GetClearResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionClear(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LastHit || orbwalkingMode == OrbwalkingMode::Freeze)
	{
		return BaseMenuManager::GetLastHitResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionLh(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Flee)
	{
		return BaseMenuManager::GetEscapeResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionEscape(enemyName) : false;
	}
	else if (ksCheck)
	{
		return BaseMenuManager::GetKsResourceManagement().isQBlockerEnabled() ? BaseSettings::isQBlockedOnChampionKs(enemyName) : false;
	}

	return true;
}

bool EndeavourusLogicUtils::isWSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck)
{
	if (orbwalkingMode == OrbwalkingMode::Combo)
	{
		return BaseMenuManager::GetComboResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionCombo(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Mixed)
	{
		return BaseMenuManager::GetHarassResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionHarass(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LaneClear || orbwalkingMode == OrbwalkingMode::JungleClear)
	{
		return BaseMenuManager::GetClearResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionClear(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LastHit || orbwalkingMode == OrbwalkingMode::Freeze)
	{
		return BaseMenuManager::GetLastHitResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionLh(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Flee)
	{
		return BaseMenuManager::GetEscapeResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionEscape(enemyName) : false;
	}
	else if (ksCheck)
	{
		return BaseMenuManager::GetKsResourceManagement().isWBlockerEnabled() ? BaseSettings::isWBlockedOnChampionKs(enemyName) : false;
	}

	return true;
}

bool EndeavourusLogicUtils::isESpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck)
{
	if (orbwalkingMode == OrbwalkingMode::Combo)
	{
		return BaseMenuManager::GetComboResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionCombo(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Mixed)
	{
		return BaseMenuManager::GetHarassResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionHarass(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LaneClear || orbwalkingMode == OrbwalkingMode::JungleClear)
	{
		return BaseMenuManager::GetClearResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionClear(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LastHit || orbwalkingMode == OrbwalkingMode::Freeze)
	{
		return BaseMenuManager::GetLastHitResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionLh(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Flee)
	{
		return BaseMenuManager::GetEscapeResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionEscape(enemyName) : false;
	}
	else if (ksCheck)
	{
		return BaseMenuManager::GetKsResourceManagement().isEBlockerEnabled() ? BaseSettings::isEBlockedOnChampionKs(enemyName) : false;
	}

	return true;
}

bool EndeavourusLogicUtils::isRSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck)
{
	if (orbwalkingMode == OrbwalkingMode::Combo)
	{
		return BaseMenuManager::GetComboResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionCombo(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Mixed)
	{
		return BaseMenuManager::GetHarassResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionHarass(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LaneClear || orbwalkingMode == OrbwalkingMode::JungleClear)
	{
		return BaseMenuManager::GetClearResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionClear(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::LastHit || orbwalkingMode == OrbwalkingMode::Freeze)
	{
		return BaseMenuManager::GetLastHitResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionLh(enemyName) : false;
	}
	else if (orbwalkingMode == OrbwalkingMode::Flee)
	{
		return BaseMenuManager::GetEscapeResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionEscape(enemyName) : false;
	}
	else if (ksCheck)
	{
		return BaseMenuManager::GetKsResourceManagement().isRBlockerEnabled() ? BaseSettings::isRBlockedOnChampionKs(enemyName) : false;
	}

	return true;
}

void EndeavourusLogicUtils::UpdateSelectedChamp() {
	if (SELECTED_ENEMY != nullptr) {
		if (SELECTED_ENEMY->IsValid() && SELECTED_ENEMY->IsAlive() && SELECTED_ENEMY->IsVisible() && !SELECTED_ENEMY->IsZombie()) {

		}
		else {
			SELECTED_ENEMY = nullptr;
		}
	}
	if (Game::IsKeyPressed(KeyCode::LButton)) {
		bool foundEnemy{ false };
		if (Player.IsValid() && Player.IsAlive() && !Player.IsZombie() && Player.IsVisible()) {
			for (auto e : pSDK->EntityManager->GetEnemyHeroes()) {
				if (e.second != nullptr && e.second->IsValid() && e.second->IsAlive() && e.second->IsVisible() && !e.second->IsZombie()) {
					if (e.second->IsMouseOver()) {
						SELECTED_ENEMY = e.second->AsAIBaseClient();
						//Game::PrintChat(e.second->GetCharName());
						foundEnemy = true;
					}
				}
			}

			if (!foundEnemy) {
				SELECTED_ENEMY = nullptr;
				//Game::PrintChat("Removing target");
			}
		}
	}
}

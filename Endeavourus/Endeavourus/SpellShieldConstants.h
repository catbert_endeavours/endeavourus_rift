#pragma once
#include <string>

const std::string SPELL_SHIELD_PARENT_GROUP = "_SPELL_SHIELD_PARENT_GROUP";
const std::string SPELL_SHIELD_GROUP = "_SPELL_SHIELD_GROUP";
const std::string ENABLE_SPELL_SHIELD = "_ENABLE_SPELL_SHIELD";
const std::string SPELL_SHIELD_SPELL = "_SPELL_SHIELD_SPELL";
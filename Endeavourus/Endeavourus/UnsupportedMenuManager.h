#pragma once
#include "BaseMenuManager.h"

class UnsupportedMenuManager : public BaseMenuManager
{
public:
	static void Init();
	static void DrawMenu();
};


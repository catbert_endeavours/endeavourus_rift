#pragma once
#include <SDK Extensions.h>

class SmiteManager
{
private:
	static bool playerHasSmite;
	static bool unsealedSpellBookEnabled;
	static AIBaseClient * GetSmiteCreepTarget(const float smiteRange);
	static bool isEnabledMonster(const std::string creepName);
	static void CastSmiteAtMonster(Spell::Targeted smite, const int smiteMode);
	static void CastSmiteKS(const Spell::Targeted smite, const int smiteMode);
	static void CastSmiteCombo(const Spell::Targeted smite, const int smiteMode);
public:
	static void Init();

	static void PerformSmiteLogic();
	static bool GetPlayerHasSmite();
	static void SetPlayerHasSmite(const bool playerHasSmite);

	static bool GetUnsealedSpellBookEnabled();
	static void SetUnsealedSpellbookEnabled(const bool unsealedSpellBookEnabled);
};


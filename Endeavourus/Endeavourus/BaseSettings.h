#pragma once
#include "SDK Extensions.h"
class BaseSettings
{
public:
	static bool isEnableDrawings();
	static bool isEnableSmite();
	static bool isEnableActivator();

	static bool isChampEnabled();

	static bool isComboKeyEnabled();
	static bool isMixedKeyEnabled();
	static bool isClearKeyEnabled();
	static bool isLasthitKeyEnabled();
	static bool isEscapeKeyEnabled();


	// Combo
	static bool isQEnabledCombo();
	static bool isWEnabledCombo();
	static bool isEEnabledCombo();
	static bool isREnabledCombo();

	static bool isQPredictionEnabledCombo();
	static bool isWPredictionEnabledCombo();
	static bool isEPredictionEnabledCombo();
	static bool isRPredictionEnabledCombo();

	static HitChance getQPredictionCombo();
	static HitChance getWPredictionCombo();
	static HitChance getEPredictionCombo();
	static HitChance getRPredictionCombo();

	static int getComboQResourceLimit(int defaultVal = 0);
	static int getComboWResourceLimit(int defaultVal = 0);
	static int getComboEResourceLimit(int defaultVal = 0);
	static int getComboRResourceLimit(int defaultVal = 0);

	// Harass
	static bool isQEnabledHarass();
	static bool isWEnabledHarass();
	static bool isEEnabledHarass();
	static bool isREnabledHarass();

	static bool isQPredictionEnabledHarass();
	static bool isWPredictionEnabledHarass();
	static bool isEPredictionEnabledHarass();
	static bool isRPredictionEnabledHarass();

	static int getHarassQResourceLimit(int defaultVal = 40);
	static int getHarassWResourceLimit(int defaultVal = 40);
	static int getHarassEResourceLimit(int defaultVal = 40);
	static int getHarassRResourceLimit(int defaultVal = 40);

	static HitChance getQPredictionHarass();
	static HitChance getWPredictionHarass();
	static HitChance getEPredictionHarass();
	static HitChance getRPredictionHarass();

	// Lane clear
	static bool isQEnabledClear();
	static bool isWEnabledClear();
	static bool isEEnabledClear();
	static bool isREnabledClear();

	static int getClearQResourceLimit(int defaultVal = 40);
	static int getClearWResourceLimit(int defaultVal = 40);
	static int getClearEResourceLimit(int defaultVal = 40);
	static int getClearRResourceLimit(int defaultVal = 40);

	static bool isQPredictionEnabledClear();
	static bool isWPredictionEnabledClear();
	static bool isEPredictionEnabledClear();
	static bool isRPredictionEnabledClear();

	static HitChance getQPredictionClear();
	static HitChance getWPredictionClear();
	static HitChance getEPredictionClear();
	static HitChance getRPredictionClear();

	// Last hit
	static bool isQEnabledLH();
	static bool isWEnabledLH();
	static bool isEEnabledLH();
	static bool isREnabledLH();

	static int getLHQResourceLimit(int defaultVal = 40);
	static int getLHWResourceLimit(int defaultVal = 40);
	static int getLHEResourceLimit(int defaultVal = 40);
	static int getLHRResourceLimit(int defaultVal = 40);

	static bool isQPredictionEnabledLh();
	static bool isWPredictionEnabledLh();
	static bool isEPredictionEnabledLh();
	static bool isRPredictionEnabledLh();

	static HitChance getQPredictionLh();
	static HitChance getWPredictionLh();
	static HitChance getEPredictionLh();
	static HitChance getRPredictionLh();

	// Escape
	static bool isQEnabledEscape();
	static bool isWEnabledEscape();
	static bool isEEnabledEscape();
	static bool isREnabledEscape();

	static int getEscapeQResourceLimit(int defaultVal = 0);
	static int getEscapeWResourceLimit(int defaultVal = 0);
	static int getEscapeEResourceLimit(int defaultVal = 0);
	static int getEscapeRResourceLimit(int defaultVal = 0);

	static bool isQPredictionEnabledEscape();
	static bool isWPredictionEnabledEscape();
	static bool isEPredictionEnabledEscape();
	static bool isRPredictionEnabledEscape();

	static HitChance getQPredictionEscape();
	static HitChance getWPredictionEscape();
	static HitChance getEPredictionEscape();
	static HitChance getRPredictionEscape();

	// Ks
	static bool isQEnabledKs();
	static bool isWEnabledKs();
	static bool isEEnabledKs();
	static bool isREnabledKs();

	static int getKsQResourceLimit(int defaultVal = 0);
	static int getKsWResourceLimit(int defaultVal = 0);
	static int getKsEResourceLimit(int defaultVal = 0);
	static int getKsRResourceLimit(int defaultVal = 0);

	static bool isQPredictionEnabledKs();
	static bool isWPredictionEnabledKs();
	static bool isEPredictionEnabledKs();
	static bool isRPredictionEnabledKs();

	static HitChance getQPredictionKs();
	static HitChance getWPredictionKs();
	static HitChance getEPredictionKs();
	static HitChance getRPredictionKs();

	// Spell blocking
	static bool isQBlockedOnChampionCombo(std::string enemyName);
	static bool isWBlockedOnChampionCombo(std::string enemyName);
	static bool isEBlockedOnChampionCombo(std::string enemyName);
	static bool isRBlockedOnChampionCombo(std::string enemyName);

	static bool isQBlockedOnChampionHarass(std::string enemyName);
	static bool isWBlockedOnChampionHarass(std::string enemyName);
	static bool isEBlockedOnChampionHarass(std::string enemyName);
	static bool isRBlockedOnChampionHarass(std::string enemyName);

	static bool isQBlockedOnChampionClear(std::string enemyName);
	static bool isWBlockedOnChampionClear(std::string enemyName);
	static bool isEBlockedOnChampionClear(std::string enemyName);
	static bool isRBlockedOnChampionClear(std::string enemyName);

	static bool isQBlockedOnChampionLh(std::string enemyName);
	static bool isWBlockedOnChampionLh(std::string enemyName);
	static bool isEBlockedOnChampionLh(std::string enemyName);
	static bool isRBlockedOnChampionLh(std::string enemyName);

	static bool isQBlockedOnChampionKs(std::string enemyName);
	static bool isWBlockedOnChampionKs(std::string enemyName);
	static bool isEBlockedOnChampionKs(std::string enemyName);
	static bool isRBlockedOnChampionKs(std::string enemyName);

	static bool isQBlockedOnChampionEscape(std::string enemyName);
	static bool isWBlockedOnChampionEscape(std::string enemyName);
	static bool isEBlockedOnChampionEscape(std::string enemyName);
	static bool isRBlockedOnChampionEscape(std::string enemyName);

	static bool isQBlockedSpellComboGroupOpen();
	static bool isWBlockedSpellComboGroupOpen();
	static bool isEBlockedSpellComboGroupOpen();
	static bool isRBlockedSpellComboGroupOpen();

	static bool isQBlockedSpellHarassGroupOpen();
	static bool isWBlockedSpellHarassGroupOpen();
	static bool isEBlockedSpellHarassGroupOpen();
	static bool isRBlockedSpellHarassGroupOpen();

	static bool isQBlockedSpellClearGroupOpen();
	static bool isWBlockedSpellClearGroupOpen();
	static bool isEBlockedSpellClearGroupOpen();
	static bool isRBlockedSpellClearGroupOpen();

	static bool isQBlockedSpellLhGroupOpen();
	static bool isWBlockedSpellLhGroupOpen();
	static bool isEBlockedSpellLhGroupOpen();
	static bool isRBlockedSpellLhGroupOpen();

	static bool isQBlockedSpellEscapeGroupOpen();
	static bool isWBlockedSpellEscapeGroupOpen();
	static bool isEBlockedSpellEscapeGroupOpen();
	static bool isRBlockedSpellEscapeGroupOpen();

	static bool isQBlockedSpellKsGroupOpen();
	static bool isWBlockedSpellKsGroupOpen();
	static bool isEBlockedSpellKsGroupOpen();
	static bool isRBlockedSpellKsGroupOpen();

	//Smite
	static bool isSmiteGroupOpen();
	static bool isSmiteEnabled();

	static int getSmiteMode();

	static bool isSmiteAssistKeyEnabled();

	static bool isSmiteAllMonsterGroupOpen();
	static bool isSmitePlayerGroupOpen();
	static bool isSmiteBuffGroupOpen();
	static bool isSmiteEpicGroupOpen();

	static bool isSmitePlayedRedEnabled();
	static bool isSmitePlayerBlueEnabled();
	static bool isSmitePlayerKsEnabled();

	static bool isSmiteCreepGroupOpen();
	static bool isSmiteWolfEnabled();
	static bool isSmiteRaptorEnabled();
	static bool isSmiteKrugEnabled();
	static bool isSmiteGrompEnabled();
	static bool isSmiteCrabEnabled();

	static bool isSmiteBlueEnabled();
	static bool isSmiteRedEnabled();

	static bool isSmiteBaronEnabled();
	static bool isSmiteRiftEnabled();

	static bool isSmiteElderEnabled();
	static bool isSmiteInfernalEnabled();
	static bool isSmiteMountainEnabled();
	static bool isSmiteCloudEnabled();
	static bool isSmiteOceanEnabled();

	static bool isSmiteDrawEnabled();
	static bool getSmiteDrawGroup();

	static SDKCOLOR getSmiteRangeColour();

	static bool isDrawQRangeEnabled();
	static bool isDrawWRangeEnabled();
	static bool isDrawERangeEnabled();
	static bool isDrawRRangeEnabled();

	static SDKCOLOR getQRangeColour();
	static SDKCOLOR getWRangeColour();
	static SDKCOLOR getERangeColour();
	static SDKCOLOR getRRangeColour();
	static SDKCOLOR getSelectedChampColour();
};


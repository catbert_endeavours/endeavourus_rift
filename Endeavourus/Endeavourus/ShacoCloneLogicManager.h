#pragma once
#include "SDK Extensions.h"
#include <vector>
class ShacoCloneLogicManager
{
public:

	static bool SHACO_CLONE_ENABLED;

	static void PerformLogic();

	static AttackableUnit* GetEnemyTarget();
	static AttackableUnit * GetShacoTarget();
	static AIBaseClient * GetSelectedTarget();
	static Vector3 MousePos();
	static void ShacoCloneModeOptionSelection();
	static void ShacoComboModeOptionSelection();
	static SDKVECTOR GetAllyNexusPosition();

	static AITurretClient * GetClosestAliveAllyTower();
	static bool isComboModeChangeKeyEnabled();
	static bool isCloneModeChangeKeyEnabled();
	static bool isPlayerFreezingAttackAndMoveKeyEnabled();
	static int SHACO_CLONE_OPTION_INDEX;

	static void UpdateCloneStatus();

	static void __cdecl OnAttack(void* AI, void* TargetObject, bool StartAttack, bool StopAttack, void* UserData);

	static std::vector<std::string> SHACO_CLONE_OPTIONS;
	static std::vector<std::string> SHACO_COMBO_OPTIONS;

	static int SHACO_COMBO_OPTION_INDEX;

	static float PLAYER_POST_ATTACK_TARGET_TIMER;
	static AIBaseClient* PLAYER_POST_ATTACK_TARGET;
};


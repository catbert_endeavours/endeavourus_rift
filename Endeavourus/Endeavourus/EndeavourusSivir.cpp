#include "stdafx.h"
#include "EndeavourusSivir.h"
#include "SivirLogicManager.h"
#include "SpellShieldManager.h"
#include "SmiteManager.h"
#include "SivirMenuManager.h"
#include "BaseSettings.h"
#include "SmiteDrawingManager.h"
#include "SivirDrawingManager.h"
#include "EndeavourusMiscUtils.h"
#include "EndeavourusActivator.h"

void EndeavourusSivir::Init()
{
	SmiteManager::Init();
	SpellShieldManager::Init();
	SivirMenuManager::Init();
	SivirLogicManager::Init();
	EndeavourusActivator::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::PostAttack, OnPostAttack);

	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, OnCastStart);
}

void __cdecl EndeavourusSivir::OnUpdate(void * UserData)
{
	if (!Game::IsAvailable())
	{
		return;
	}


	SmiteManager::PerformSmiteLogic();

	if (BaseSettings::isChampEnabled()) {
		SivirLogicManager::PerformLogic();
	}
}

void __cdecl EndeavourusSivir::OnDraw(void * UserData)
{
	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();
	SivirDrawingManager::Draw();
}

void __cdecl EndeavourusSivir::OnDrawMenu(void * UserData)
{
	SivirMenuManager::DrawMenu();
}

void __cdecl EndeavourusSivir::OnPostAttack(void* UserData)
{
	if (BaseSettings::isChampEnabled()) {
		OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
		AttackableUnit* target = pCore->TS->GetTarget(Player.GetAttackRange());

		if (target != nullptr && target->IsValid() && (Player.Distance(&target->GetServerPosition()) < Player.GetAttackRange()) && orbwalkingMode == OrbwalkingMode::Combo)
		{
			SivirLogicManager::PerformMultiSpellCombo(orbwalkingMode);
		}
		else
		{
			SivirLogicManager::CastW(orbwalkingMode);
		}
	}
}

void __cdecl EndeavourusSivir::OnCastStart(void * heroCasting, PSDK_SPELL_CAST SpellCast, void * UserData)
{
	GameObject* target = pSDK->EntityManager->GetObjectFromPTR(SpellCast->TargetObject);
	GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(heroCasting);

	if (target != nullptr && hero != nullptr && hero->GetName() != Player.GetName() && target->GetName() == Player.GetName())
	{
		if (SpellShieldManager::CanShieldSpell(SpellCast->Spell.Name) && SivirLogicManager::ECast.IsReady()) {
			SivirLogicManager::SIVIR_SHOULD_CAST_SHIELD = true;
		}
	}
}

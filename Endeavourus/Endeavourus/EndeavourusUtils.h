#pragma once
#include "SDK Extensions.h"

class EndeavourusMiscUtils
{
public:
	static bool GetBoolSetting(std::string settingName, const bool defaultVal = false);
	static float GetFloatSetting(std::string settingName, const float defaultVal = 0);
	static int GetDropDownSetting(std::string settingName, const int defaultVal = 0);
	static int GetIntSetting(std::string settingName, const int defaultVal = 0);

	const static std::string GetUniqueSettingName(std::string settingName);
	static bool PlayerHasSmite();
	static bool PlayerHasUnsealedSpellbook();

	static OrbwalkingMode GetOrbwalkingMode();

	static bool CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode);
	static bool IsValidToCastSpell(AIBaseClient* enemy);
};


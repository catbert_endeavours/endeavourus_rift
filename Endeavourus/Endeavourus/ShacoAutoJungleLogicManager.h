#pragma once
class ShacoAutoJungleLogicManager
{
public:
	static void Init();
	static void PerformLogic();
	static void PerformBlueSideSetup();
	static void PerformRedSideSetup();
	static bool IsEnabled();
	static int PATH_SELECTION;
};


#include "stdafx.h"
#include "ShacoDeceiveHelper.h"
#include "SDK Extensions.h"
#include "EndeavourusMenu.h"
#include "ShacoConstants.h"
#include "ShacoDeceiveLocations.h"

float deceiveHelperQCastCD{ 0 };
float blockQCastToQuick{ 0 };

void ShacoDeceiveHelper::PerformLogic() {
	if (Player.IsAlive() && Player.IsValid() && Player.IsVisible() && !Player.IsZombie()) {
		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_DECEIVE_ASSISTANT, true)) {
			bool active = EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_ASSISTANT_KEY)).Active;

			if (Player.IsAlive() && !Player.IsZombie() && Player.IsValid() && Player.IsVisible()) {
				SDK_SPELL Q = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
				Spell::Targeted QCast = { SpellSlot::Q, Q.CastRange };

				if (blockQCastToQuick < Game::Time() && deceiveHelperQCastCD < Game::Time()) {
					std::pair<Vector3, Vector3> closestJump{};
					float closestDistance{ FLT_MAX };
					if (Game::IsAvailable() && active) {
						for (auto l : DECEIVE_LOCATIONS) {
							float distance{ Player.Distance(l.first) };
							if (distance < closestDistance) {
								closestJump = l;
								closestDistance = distance;
							}
						}

						pSDK->Control->Move(&closestJump.first);

						if (Player.Distance(closestJump.first) < 10 && QCast.IsReady() && deceiveHelperQCastCD < Game::Time()) {
							if (QCast.Cast(&closestJump.second)) {
								deceiveHelperQCastCD = Game::Time() + 0.25f;
								blockQCastToQuick = Game::Time() + 1.0f;
							}
						}
					}
				}
			}
		}
	}
}

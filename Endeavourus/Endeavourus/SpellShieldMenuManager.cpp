#include "stdafx.h"
#include "SpellShieldMenuManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "SpellShieldConstants.h"
#include "SpellShieldManager.h"
#include "EndeavourusMenu.h"

void SpellShieldMenuManager::DrawSpellShieldSettings()
{
	EndeavourusMenu::Checkbox("Enable SpellShieldManager", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_SPELL_SHIELD), true);
	EndeavourusMenu::Tree("SpellShield Spell Options", EndeavourusMiscUtils::GetUniqueSettingName(SPELL_SHIELD_GROUP), false, DrawSpecificSpellOptions);
}

void SpellShieldMenuManager::DrawSpecificSpellOptions()
{
	for (auto option : SpellShieldManager::SPELL_SHIELD_MENU_OPTIONS) {
		EndeavourusMenu::Checkbox(("Enable: " + option).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(SPELL_SHIELD_SPELL + "_" + option), true);
	}
}
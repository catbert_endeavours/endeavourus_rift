#include "stdafx.h"
#include "EndeavourusPathTracker.h"
#include "EndeavourusPrediction.h"
#include "SDK Extensions.h"

std::map<unsigned int, EnemyData> EndeavourusPathTracker::EnemyInfo;

#pragma region PathTracker
void EndeavourusPathTracker::Init() {
	pSDK->EventHandler->RegisterCallback(CallbackEnum::NewPath, EndeavourusPathTracker::OnNewPath);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Attack, EndeavourusPathTracker::OnAttack);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastEnd, EndeavourusPathTracker::OnCastEnd);
}
EnemyData& EndeavourusPathTracker::GetData(AIHeroClient * Unit) {
	auto nID{ Unit->GetNetworkID() };
	if (EnemyInfo.count(nID) == 0) {
		auto wp{ Unit->GetWaypoints2D() };
		EnemyInfo[nID] = EnemyData(wp);
	}
	return EnemyInfo[nID];
}

unsigned int EndeavourusPathTracker::MovImmobileTime(AIBaseClient * Unit) {
	if (Unit == NULL || !Unit->IsHero()) { return 0; }
	return EnemyInfo[Unit->GetNetworkID()].IsStopped ? GetTickCount() - EnemyInfo[Unit->GetNetworkID()].StopTick : 0;
}
unsigned int EndeavourusPathTracker::LastMovChangeTime(AIBaseClient * Unit) {
	if (Unit == NULL || !Unit->IsHero()) { return 0; }
	return GetTickCount() - EnemyInfo[Unit->GetNetworkID()].LastWaypointTick;
}

float EndeavourusPathTracker::AvgReactionTime(AIBaseClient * Unit) {
	return AvgMovChangeTime(Unit);
}

float EndeavourusPathTracker::AvgMovChangeTime(AIBaseClient * Unit) {
	if (Unit == NULL || !Unit->IsHero()) { return 0.0f; }
	return EnemyInfo[Unit->GetNetworkID()].AvgTick + EndeavourusPrediction::DefaultValues.ReactionDelay;
}
float EndeavourusPathTracker::AvgPathLenght(AIBaseClient * Unit) {
	if (Unit == NULL || !Unit->IsHero()) { return 0.0f; }
	return EnemyInfo[Unit->GetNetworkID()].AvgPathLenght;
}
float EndeavourusPathTracker::LastAngleDiff(AIBaseClient * Unit) {
	if (Unit == NULL || !Unit->IsHero()) { return 0.0f; }
	return EnemyInfo[Unit->GetNetworkID()].LastAngleDiff;
}

std::vector<Vector2> EndeavourusPathTracker::CutPath(std::vector<Vector2>& path, float distance) {
	auto result = std::vector<Vector2>();
	if (distance < 0.0f) {
		path[0] = path[0] + distance * (path[1] - path[0]).Normalized();
		return path;
	}

	for (size_t i = 1; i < path.size(); i++) {
		auto dist = path[i - 1].Distance(path[i]);

		if (dist > distance) {
			result.push_back(path[i - 1] + distance * (path[i] - path[i - 1]).Normalized());

			for (size_t j = i + 1; j < path.size(); j++) {
				result.push_back(path[j]);
			}

			break;
		}

		distance -= dist;
	}

	return result.size() > 0 ? result : std::vector<Vector2>{ path.back() };
}
float  EndeavourusPathTracker::GetPathLength(std::vector<Vector2>& Path) {
	float distance = 0.0f;
	for (size_t i = 1; i < Path.size(); i++) {
		distance += Path[i - 1].Distance(Path[i]);
	}

	return distance;
}

void __cdecl EndeavourusPathTracker::OnCastEnd(void * AI, PSDK_SPELL_CAST SpellCast, void * UserData) {
	UNREFERENCED_PARAMETER(UserData);
	if (SpellCast->IsAutoAttack && SDKSTATUS_SUCCESS(SdkIsObjectHero(AI)) && SpellCast && SpellCast->TargetObject) {
		auto Unit{ pSDK->EntityManager->GetObjectFromPTR(AI) };
		if (Unit == NULL || (Unit->GetNetworkID() != Player.GetNetworkID() && Unit->IsAlly())) { return; }

		EnemyInfo[Unit->GetNetworkID()].LastAATick = GetTickCount();
	}
}
void __cdecl EndeavourusPathTracker::OnAttack(void * AI, void * TargetObject, bool StartAttack, bool StopAttack, void * UserData) {
	UNREFERENCED_PARAMETER(TargetObject);
	UNREFERENCED_PARAMETER(StopAttack);
	UNREFERENCED_PARAMETER(UserData);

	if (StartAttack && SDKSTATUS_SUCCESS(SdkIsObjectHero(AI))) {
		auto Unit{ pSDK->EntityManager->GetObjectFromPTR(AI) };
		if (Unit == NULL || (Unit->GetNetworkID() != Player.GetNetworkID() && Unit->IsAlly())) { return; }

		auto nID{ Unit->GetNetworkID() };
		EnemyInfo[nID].LastWindupTick = GetTickCount();
		EnemyInfo[nID].IsWindupChecked = false;
	}
}
void EndeavourusPathTracker::OnNewPath(void* AI, bool Move, bool Stop, void* UserData) {
	UNREFERENCED_PARAMETER(Move);
	UNREFERENCED_PARAMETER(Stop);
	UNREFERENCED_PARAMETER(UserData);
	if (!SDKSTATUS_SUCCESS(SdkIsObjectHero(AI)))
		return;

	AIHeroClient* Unit{ (AIHeroClient*)(pSDK->EntityManager->GetObjectFromPTR(AI)) };
	if (Unit == NULL || (Unit->GetNetworkID() != Player.GetNetworkID() && Unit->IsAlly())) { return; }

	pSDK->EventHandler->DelayedAction([Unit]() { AnalyzePath(Unit); }, 20);
}

void EndeavourusPathTracker::AnalyzePath(AIHeroClient* Unit) {
	unsigned int nID = Unit->GetNetworkID();

	if (EnemyInfo.count(nID) == 0) {
		std::vector<Vector2> wp{ Unit->GetWaypoints2D() };
		EnemyInfo[nID] = EnemyData(wp);
		wp.insert(wp.begin(), Unit->GetPosition().To2D());
	}

	auto nav{ Unit->NavInfo() };
	auto &enemy{ EnemyInfo[nID] };


	//SdkUiConsoleClear();
	//SdkUiConsoleWrite("Number of waypoints %d", nav.NumberOfWaypoints);
	//SdkUiConsoleWrite("Avg Time Between Move Orders %f", enemy.AvgTick);
	//SdkUiConsoleWrite("Avg Time Standing Still After AA Order %f", enemy.AvgOrbwalkTime);
	//SdkUiConsoleWrite("Avg Path Lenght %f", enemy.AvgPathLenght);	

	if (nav.NumberOfWaypoints < 2) {
		if (!enemy.IsStopped) {
			enemy.StopTick = GetTickCount();
			enemy.LastWaypointTick = GetTickCount();
			enemy.IsStopped = true;
			enemy.LastAngleDiff = 360;
		}
	}
	else {
		std::vector<Vector2> wp{ Unit->GetWaypoints2D() };
		wp.insert(wp.begin(), Unit->GetPosition().To2D());

		std::vector<Vector2> sample1;
		for (size_t i = 1; i < wp.size(); ++i) {
			sample1.emplace_back((wp[i] - wp[i - 1]).Normalized());
		}

		std::vector<Vector2> sample2;
		for (size_t i = 1; i < enemy.LastWaypoints.size(); ++i) {
			sample2.emplace_back((enemy.LastWaypoints[i] - enemy.LastWaypoints[i - 1]).Normalized());
		}

		if (!sample1.empty() && !sample2.empty()) {
			Vector2 vec0(0.0f, 0.0f);

			float sample1_avg;
			for_each(sample1.begin(), sample1.end(), [&sample1_avg, &vec0](Vector2& p) {sample1_avg += p.AngleBetween(vec0); });
			sample1_avg /= (float)sample1.size();

			float sample2_avg;
			for_each(sample2.begin(), sample2.end(), [&sample2_avg, &vec0](Vector2& p) {sample2_avg += p.AngleBetween(vec0); });
			sample2_avg /= (float)sample2.size();

			enemy.LastAngleDiff = std::abs(sample2_avg - sample1_avg);
		}

		if (enemy.LastWaypoints.empty() || wp.empty() || enemy.LastWaypoints.back() != wp.back()) {
			if (!enemy.IsStopped) {
				if (enemy.Count > 100) {
					enemy.AvgTick = 0.0f;
					enemy.AvgPathLenght = 0.0f;
					enemy.Count = 0;
				}
				++enemy.Count;
				enemy.AvgTick = ((enemy.Count - 1) * enemy.AvgTick + (GetTickCount() - enemy.LastWaypointTick) / 1000.0f) / enemy.Count;
				enemy.AvgPathLenght = ((enemy.Count - 1) * enemy.AvgPathLenght + GetPathLength(wp)) / enemy.Count;
			}
			enemy.LastWaypointTick = (unsigned int)GetTickCount();
			enemy.IsStopped = false;
			enemy.LastWaypoints = wp;
			enemy.PathLength = GetPathLength(wp);

			if (!enemy.IsWindupChecked) {
				enemy.IsWindupChecked = true;
				if (GetTickCount() - enemy.LastAATick < 600) {
					if (enemy.OrbwalkCount > 10) {
						enemy.AvgOrbwalkTime = 0.0f;
						enemy.OrbwalkCount = 0;
					}
					++enemy.OrbwalkCount;
					enemy.AvgOrbwalkTime = (enemy.AvgOrbwalkTime * (enemy.OrbwalkCount - 1) + (enemy.LastWaypointTick - enemy.LastWindupTick) / 1000.0f) / enemy.OrbwalkCount;
				}
			}
		}
	}
}
#pragma endregion

#pragma region EnemyData 
unsigned int EnemyData::MovImmobileTime() {
	return this->IsStopped ? GetTickCount() - this->StopTick : 0;
}
unsigned int EnemyData::LastMovChangeTime() {
	return GetTickCount() - this->LastWaypointTick;
}
float EnemyData::AvgReactionTime() {
	return this->AvgTick + EndeavourusPrediction::DefaultValues.ReactionDelay;
}
float EnemyData::AvgMovChangeTime() {
	return this->AvgTick;
}
#pragma endregion
#pragma once
#include "SDK Extensions.h"
#include "BaseLogicManager.h"

class TeemoLogicManager
{
public:
	static void Init();
	static void PerformLogic();

	static Spell::Targeted QCast;
	static Spell::Active WCast;
	static Spell::Skillshot RCast;
};

#pragma once
#include "LogicConstants.h"

class Resource
{
public:
	enum ResourceType { Mana, Energy, Health };

	Resource(bool resourceEnabled = false, bool qEnabled = false, bool qDefaultVal = false, bool qBlockerEnabled = false, bool qPredictionEnabled = false, EndeavourusHitChance qPredictionMode = EndeavourusHitChance::Medium,
		bool wEnabled = false, bool wDefaultVal = false, bool wBlockerEnabled = false, bool wPredictionEnabled = false, EndeavourusHitChance wPredictionMode = EndeavourusHitChance::Medium, bool eEnabled = false,
		bool eDefaultVal = false, bool eBlockerEnabled = false, bool ePredictionEnabled = false, EndeavourusHitChance ePredictionMode = EndeavourusHitChance::Medium, bool rEnabled = false, bool rDefaultVal = false,
		bool rBlockerEnabled = false, bool rPredictionEnabled = false, EndeavourusHitChance rPredictionMode = EndeavourusHitChance::Medium, int defaultQVal = 0,
		int defaultWVal = 0, int defaultEVal = 0, int defaultRVal = 0, ResourceType resourceType = ResourceType::Mana);

	bool isResourceEnabled()
	{
		return resourceEnabled;
	}

	bool isQEnabled()
	{
		return qEnabled;
	}

	bool isWEnabled()
	{
		return wEnabled;
	}

	bool isEEnabled()
	{
		return eEnabled;
	}

	bool isREnabled()
	{
		return rEnabled;
	}

	bool isQDefaultValEnabled()
	{
		return qDefaultVal;
	}

	bool isWDefaultValEnabled()
	{
		return wDefaultVal;
	}

	bool isEDefaultValEnabled()
	{
		return eDefaultVal;
	}

	bool isRDefaultValEnabled()
	{
		return rDefaultVal;
	}

	bool isQPredictionEnabled()
	{
		return qPredictionEnabled;
	}

	bool isWPredictionEnabled()
	{
		return wPredictionEnabled;
	}

	bool isEPredictionEnabled()
	{
		return ePredictionEnabled;
	}

	bool isRPredictionEnabled()
	{
		return rPredictionEnabled;
	}

	ResourceType getResourceType()
	{
		return resourceType;
	}

	EndeavourusHitChance getQPredictionMode()
	{
		return qPredictionMode;
	}

	EndeavourusHitChance getWPredictionMode()
	{
		return wPredictionMode;
	}

	EndeavourusHitChance getEPredictionMode()
	{
		return ePredictionMode;
	}

	EndeavourusHitChance getRPredictionMode()
	{
		return rPredictionMode;
	}

	bool isQBlockerEnabled()
	{
		return qBlockerEnabled;
	}

	bool isWBlockerEnabled()
	{
		return wBlockerEnabled;
	}

	bool isEBlockerEnabled()
	{
		return eBlockerEnabled;
	}

	bool isRBlockerEnabled()
	{
		return rBlockerEnabled;
	}

	int getDefaultQMinResourceValue()
	{
		return defaultQMinResource;
	}

	int getDefaultWMinResourceValue()
	{
		return defaultWMinResource;
	}

	int getDefaultEMinResourceValue()
	{
		return defaultEMinResource;
	}

	int getDefaultRMinResourceValue()
	{
		return defaultRMinResource;
	}

protected:
	bool resourceEnabled;
	bool qEnabled;
	bool qDefaultVal;
	bool qBlockerEnabled;
	bool wEnabled;
	bool wDefaultVal;
	bool wBlockerEnabled;
	bool eEnabled;
	bool eDefaultVal;
	bool eBlockerEnabled;
	bool rEnabled;
	bool rDefaultVal;
	bool rBlockerEnabled;

	int defaultQMinResource;
	int defaultWMinResource;
	int defaultEMinResource;
	int defaultRMinResource;

	bool qPredictionEnabled;
	EndeavourusHitChance qPredictionMode;
	bool wPredictionEnabled;
	EndeavourusHitChance wPredictionMode;
	bool ePredictionEnabled;
	EndeavourusHitChance ePredictionMode;
	bool rPredictionEnabled;
	EndeavourusHitChance rPredictionMode;

	ResourceType resourceType;
};


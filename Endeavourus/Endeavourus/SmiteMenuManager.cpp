#include "stdafx.h"
#include "SmiteMenuManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "JungleConstants.h"
#include "EndeavourusMenu.h"

void SmiteMenuManager::DrawSmiteSettings()
{
	EndeavourusMenu::HotkeyToggle("Smite Logic Enabled", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_SMITE), KeyCode::K, true);

	std::vector<std::string> smiteModes{ "Always", "Save 1 stack for Baron/Drake unless assist key", "Smite only when assist key is held" };

	EndeavourusMenu::DropList("Smite Mode", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MODE), smiteModes, 0);
	EndeavourusMenu::EndeavourusHotkey("Smite Assist", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_ASSIST_KEY), KeyCode::ControlKey);

	EndeavourusMenu::Tree("Smite Monster Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_ALL_MONSTER_GROUP), false, DrawSmiteMonsterSettings);
	EndeavourusMenu::Tree("Smite Player Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_ALL_MONSTER_GROUP), false, DrawSmitePlayerSettings);
	EndeavourusMenu::Tree("Drawings", EndeavourusMiscUtils::GetUniqueSettingName(DRAW_SMITE_GROUP), false, DrawSmiteDrawingSettings);
}

void SmiteMenuManager::DrawSmiteMonsterSettings()
{
	EndeavourusMenu::Tree("Epic Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_GROUP), false, DrawSmiteEpicSettings);
	EndeavourusMenu::Tree("Buff Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_BUFF_MONSTER_GROUP), false, DrawSmiteBuffSettings);
	EndeavourusMenu::Tree("Creep Settings", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_CREEP_MONSTER_GROUP), false, DrawSmiteCreepSettings);
}

void SmiteMenuManager::DrawSmiteEpicSettings()
{
	EndeavourusMenu::Checkbox("Smite Baron", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_BARON), true);
	EndeavourusMenu::Checkbox("Smite Rift", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_RIFT), true);

	EndeavourusMenu::Checkbox("Smite Elder", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_ELDER), true);
	EndeavourusMenu::Checkbox("Smite Infernal", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_INFERNAL), true);
	EndeavourusMenu::Checkbox("Smite Cloud", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_CLOUD), true);
	EndeavourusMenu::Checkbox("Smite Mountain", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_MOUNTAIN), true);
	EndeavourusMenu::Checkbox("Smite Ocean", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_EPIC_MONSTER_OCEAN), true);
}

void SmiteMenuManager::DrawSmiteBuffSettings()
{
	EndeavourusMenu::Checkbox("Smite Blue", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_BUFF_MONSTER_BLUE), true);
	EndeavourusMenu::Checkbox("Smite Red", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_BUFF_MONSTER_RED), true);
}

void SmiteMenuManager::DrawSmiteCreepSettings()
{
	EndeavourusMenu::Checkbox("Smite Wolf", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MONSTER_WOLF), true);
	EndeavourusMenu::Checkbox("Smite Raptor", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MONSTER_RAPTOR), true);
	EndeavourusMenu::Checkbox("Smite Krug", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MONSTER_KRUG), true);
	EndeavourusMenu::Checkbox("Smite Gromp", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MONSTER_GROMP), true);
	EndeavourusMenu::Checkbox("Smite Crab", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_MONSTER_CRAB), true);
}

void SmiteMenuManager::DrawSmitePlayerSettings()
{
	EndeavourusMenu::Checkbox("Auto Smite KS Blue", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_PLAYER_KS_BLUE), true);
	EndeavourusMenu::Checkbox("Use Blue Smite in Combo", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_PLAYER_BLUE_ON_COMBO), true);
	EndeavourusMenu::Checkbox("Use Red Smite in Combo", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_PLAYER_RED_ON_COMBO), true);
}

void SmiteMenuManager::DrawSmiteDrawingSettings()
{
	EndeavourusMenu::Checkbox("Draw Smite Text", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_DRAW_TEXT_ENABLED), true);
	EndeavourusMenu::SliderFloat("Smite Text X Offset", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_DRAW_TEXT_X_OFFSET), 80, -10000, 10000);
	EndeavourusMenu::SliderFloat("Smite Text Y Offset", EndeavourusMiscUtils::GetUniqueSettingName(SMITE_DRAW_TEXT_Y_OFFSET), 75, -10000, 10000);
	EndeavourusMenu::Checkbox("Draw Smite Range", EndeavourusMiscUtils::GetUniqueSettingName(DRAW_SMITE_RANGE_ENABLED), true);
	SDKVECTOR defaultColour{ 66, 244, 116 };
	EndeavourusMenu::ColorPicker("Smite Range Colour", EndeavourusMiscUtils::GetUniqueSettingName(DRAW_SMITE_RANGE_COLOUR), defaultColour);
}
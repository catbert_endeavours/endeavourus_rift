#include "stdafx.h"
#include "TeemoSettings.h"
#include "TeemoConstants.h"
#include "EndeavourusMiscUtils.h"
#include "EndeavourusMenu.h"

bool TeemoSettings::isEnableShroomDraw()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_SHROOM_DRAWINGS, true);
}

float TeemoSettings::getEnableShroomDrawDistance()
{
	return EndeavourusMiscUtils::GetFloatSetting(SHROOM_DISTANCE_DRAWINGS, 3000.0f);
}

bool TeemoSettings::isAutoRImmobileChampion()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_AUTO_R_IMMOBILE, true);
}

bool TeemoSettings::isAutoRShroomPlacement()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_AUTO_R_SHROOM_SPOTS, true);
}

SDKCOLOR TeemoSettings::getShroomPlacementColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(R_PLACEMENT_CIRCLE_COLOUR));
}

bool TeemoSettings::isSnapRKeyPressed()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(TEEMO_R_SNAP_KEY_ENABLED)).Active;
}

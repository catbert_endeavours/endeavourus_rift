#pragma once
#include "SDK Extensions.h"
#include "BaseLogicManager.h"

class TeemoLogicManager
{
private:
	static void RefreshSpells();
	static void RefreshQSpell();
	static void RefreshRSpell();
	static float GetQDamage(AIBaseClient* enemy);

	static void PerformCombo(OrbwalkingMode orbwalkingMode);
	static void PerformLastHit(OrbwalkingMode orbwalkingMode);
	static void PerformEscape(OrbwalkingMode orbwalkingMode);
	static void PerformHarass(OrbwalkingMode orbwalkingMode);
	static void PerformClear(OrbwalkingMode orbwalkingMode);

	static void CastW(OrbwalkingMode orbwalkingMode);
	static void CastR(OrbwalkingMode orbwalkingMode);

	static void KillSteal();

public:
	static void Init();
	static void PerformLogic();
	static Spell::Targeted QCast;
	static Spell::Active WCast;
	static Spell::Skillshot RCast;

	static void CastQ(OrbwalkingMode orbwalkingMode);
	static void AutoRImmobile();

	static float R_COOLDOWN;
};

#pragma once
#include "BaseDrawingManager.h"
#include "SDK Extensions.h"
class ShacoDrawingManager : public BaseDrawingManager
{
public:
	static void Draw();
	static void DrawComboMode();
	static void DrawAutoJungleStatus();
	static void DrawDeceivePositions();
	static SDKCOLOR getDeceiveDrawingColour();
	static SDKCOLOR getDeceiveLocationDrawingColour();
	static SDKCOLOR getCloneModeTextColour();
	static SDKCOLOR getComboModeTextColour();
};


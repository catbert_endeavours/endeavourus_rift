#include "stdafx.h"
#include "BaseDrawingManager.h"
#include "BaseSettings.h"
#include "BaseLogicManager.h"
#include "EndeavourusLogicUtils.h"
#include "EndeavourusMiscUtils.h"
#include "GeneralConstants.h"

void BaseDrawingManager::DrawSpellRanges()
{
	if (BaseSettings::isDrawQRangeEnabled())
	{
		PSDKCOLOR psdkcolor{ &BaseSettings::getQRangeColour() };
		SDKVECTOR playerPosition{ Player.GetPosition() };

		Draw::Circle(&playerPosition, BaseLogicManager::Q_Spell.CastRange, psdkcolor);
	}

	if (BaseSettings::isDrawWRangeEnabled())
	{
		PSDKCOLOR psdkcolor{ &BaseSettings::getWRangeColour() };
		SDKVECTOR playerPosition{ Player.GetPosition() };

		Draw::Circle(&playerPosition, BaseLogicManager::W_Spell.CastRange, psdkcolor);
	}

	if (BaseSettings::isDrawERangeEnabled())
	{
		PSDKCOLOR psdkcolor{ &BaseSettings::getERangeColour() };
		SDKVECTOR playerPosition{ Player.GetPosition() };

		Draw::Circle(&playerPosition, BaseLogicManager::E_Spell.CastRange, psdkcolor);
	}

	if (BaseSettings::isDrawRRangeEnabled())
	{
		PSDKCOLOR psdkcolor{ &BaseSettings::getRRangeColour() };
		SDKVECTOR playerPosition{ Player.GetPosition() };

		Draw::Circle(&playerPosition, BaseLogicManager::R_Spell.CastRange, psdkcolor);
	}
}

void BaseDrawingManager::DrawSelectedTarget()
{
	if (EndeavourusMiscUtils::GetBoolSetting(SELECTED_CHAMPION_LEFT_CLICK, true)) {
		if (EndeavourusLogicUtils::SELECTED_ENEMY != nullptr) {
			PSDKCOLOR psdkcolor{ &BaseSettings::getSelectedChampColour() };
			SDKVECTOR enemyPosition{ EndeavourusLogicUtils::SELECTED_ENEMY->GetPosition() };

			Draw::Circle(&enemyPosition, 200, psdkcolor);
		}
	}
}

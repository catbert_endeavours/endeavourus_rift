#include "stdafx.h"
#include "EndeavourusShaco.h"
#include "SmiteManager.h"
#include "SpellShieldManager.h"
#include "BaseSettings.h"
#include "SmiteDrawingManager.h"
#include "ShacoDrawingManager.h"
#include "ShacoMenuManager.h"
#include "ShacoLogicManager.h"
#include "EndeavourusActivator.h"
#include "ShacoConstants.h"
#include "ShacoAutoJungleLogicManager.h"
#include "EndeavourusMenu.h"
#include "EndeavourusLogicUtils.h"
#include "ShacoDeceiveHelper.h"
#include "SDK Extensions.h"
#include "Geometry.hpp"
#include "ShacoDodgeSpellLogic.h"
#include "ShacoDeceiveBackStabLogicManager.h"
#include "ShacoCloneLogicManager.h"
#include "ShacoDangerousSpells.h"

bool junglePathButtonPressed{ false };
void EndeavourusShaco::Init()
{
	SmiteManager::Init();
	ShacoMenuManager::Init();
	ShacoLogicManager::Init();
	EndeavourusActivator::Init();
	ShacoAutoJungleLogicManager::Init();
	ShacoDodgeSpellLogic::Init();
	ShacoDeceiveBackStabLogicManager::Init();
	PreCacheRSkillShotValues();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Attack, ShacoCloneLogicManager::OnAttack);

	//pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, OnCastStart);
}


void __cdecl EndeavourusShaco::OnUpdate(void * UserData)
{
	EndeavourusLogicUtils::UpdateSelectedChamp();
	if (BaseSettings::isChampEnabled()) {
		ShacoAutoJungleLogicManager::PerformLogic();
	}
	SmiteManager::PerformSmiteLogic();

	if (!Game::IsAvailable())
	{
		return;
	}

	if (BaseSettings::isChampEnabled()) {
		ShacoCloneLogicManager::PerformLogic();
		ShacoLogicManager::PerformLogic();
		ShacoDeceiveHelper::PerformLogic();
		ShacoDodgeSpellLogic::PerformLogic();
		AutoJunglePathSelector();
		ShacoCloneLogicManager::ShacoCloneModeOptionSelection();
		ShacoCloneLogicManager::ShacoComboModeOptionSelection();
	}

	EndeavourusShaco::DebugHelper();
}


void __cdecl EndeavourusShaco::OnDraw(void * UserData)
{
	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();
	ShacoDrawingManager::Draw();

	/**
	for (auto a : pSDK->EntityManager->GetEnemyHeroes()) {
		SDKVECTOR playerPosition{ a.second->GetDirection() };
		SDKVECTOR playerPosition1{ a.second->GetPosition() };
		SDKVECTOR vec1 = playerPosition + playerPosition1;
		SDKVECTOR vec2 = playerPosition1 + playerPosition;
		SDKVECTOR vec3 = { vec1 };

		Draw::Text(&playerPosition, nullptr, "TTTTTTTTTTTTT", "Arial", &Color::Red, 24, 8, 2);
		Draw::Text(&vec1, nullptr, "VEC1", "Arial", &Color::Red, 24, 8, 2);
		Draw::Text(&vec2, nullptr, "VEC2", "Arial", &Color::Red, 24, 8, 2);

		Vector3 t{ Player.GetPosition().Extended(vec1, 1000) };
		Vector3 t1{ a.second->GetPosition().Extended(vec3, -100) };

		Draw::Text(&t, nullptr, "Test", "Arial", &Color::Red, 24, 8, 2);
		Draw::Text(&t1, nullptr, "Wont Work", "Arial", &Color::Red, 24, 8, 2);
	}
	*/
}

void __cdecl EndeavourusShaco::OnDrawMenu(void * UserData)
{
	ShacoMenuManager::DrawMenu();
}

void __cdecl EndeavourusShaco::OnCastStart(void * heroCasting, PSDK_SPELL_CAST SpellCast, void * UserData)
{
	/**
	float startTime{ SpellCast->StartTime };
	Vector3 playerPos{ Player.GetPosition() };
	float distance{ Player.Distance(SpellCast->StartPosition) };

	float timeToReachPlayer = distance / SpellCast->Spell.MissileSpeed;
	float gameTimeToReach = Game::Time() + timeToReachPlayer;
	Game::PrintChat(std::to_string(Game::Time()) + "t");
	Game::PrintChat(std::to_string(timeToReachPlayer));
	Game::PrintChat(std::to_string(gameTimeToReach));
	Game::PrintChat(SpellCast->Spell.Name);
	auto spellRec = Geometry::Rectangle( SpellCast->StartPosition, SpellCast->EndPosition, SpellCast->Spell.LineWidth );
	spellRec.Draw(1, &Color::Red);
	if (spellRec.IsInside(Player.GetServerPosition()))
	{
		Game::PrintChat("True");
	}
	else {
		Game::PrintChat("False");
	}
	*/
}

void EndeavourusShaco::DebugHelper() {
	float x = Player.GetPosition().x;
	float y = Player.GetPosition().y;
	float z = Player.GetPosition().z;

	//Game::PrintChat("x" + std::to_string(x));
	//Game::PrintChat("y" + std::to_string(y));
	//Game::PrintChat("z" + std::to_string(z));

	for (auto a : pSDK->EntityManager->GetAllyMinions(500)) {
		if (std::string(a.second->GetName()).find("Jack In The Box") != std::string::npos)
		{
			Game::PrintChat(a.second->GetName());

			float x = a.second->GetPosition().x;
			float y = a.second->GetPosition().y;
			float z = a.second->GetPosition().z;

			Game::PrintChat("x" + std::to_string(x));
			Game::PrintChat("y" + std::to_string(y));
			Game::PrintChat("z" + std::to_string(z));
		}
	}
}

void EndeavourusShaco::AutoJunglePathSelector() {
	if (Game::IsAvailable()) {
		if (isPathChangeKeyEnabled() && !junglePathButtonPressed) {
			junglePathButtonPressed = true;

			ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX++;
			if (ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX > 2) {
				ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX = 0;
			}
		}
		else if (!isPathChangeKeyEnabled()) {
			junglePathButtonPressed = false;
		}
	}
}


bool EndeavourusShaco::isPathChangeKeyEnabled()
{
	return EndeavourusMenu::Get<EndeavourusHotkey>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_PATH_HOTKEY)).Active;
}

void EndeavourusShaco::PreCacheRSkillShotValues()
{
	EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT_BLOCKER, true);
	EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_CIRCLE_BLOCKER, true);
	EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_TARGETED_BLOCKER, true);
	EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_W_SPELL_BLOCKER, true);

	for (auto option : CIRCLE_MAP_SHACO) {
		EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_CIRCLE + "_" + option.second.displayName, true);
	}

	for (auto option : TARGETED_MAP_SHACO) {
		EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_TARGETED + "_" + option.second.displayName, true);
	}

	for (auto option : SKILLSHOT_MAP_SHACO) {
		EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT + "_" + option.second.displayName, true);
	}

	for (auto option : SKILLSHOT_MAP_W) {
		EndeavourusMiscUtils::GetBoolSetting(SHACO_SPELL_W_BLOCKER + "_" + option.second.displayName, true);
	}
}
#include "stdafx.h"
#include "BaseMenuManager.h"
#include "EndeavourusMiscUtils.h"
#include "JungleConstants.h"
#include "GeneralConstants.h"
#include "SmiteMenuManager.h"
#include "DrawConstants.h"
#include "BaseSettings.h"
#include "EndeavourusMenu.h"
#include "ActivatorMenuManager.h"

bool BaseMenuManager::unsupportedChampionLoaded{ false };
void* BaseMenuManager::specificChampLogic = nullptr;

void BaseMenuManager::DrawMenu()
{
	std::string champName = Player.GetCharName();
	champName += " Settings";

	std::string keySettings = Player.GetCharName();
	keySettings += " Key Settings";
	if (!GetUnsupportedChampionLoaded()) {
		EndeavourusMenu::Checkbox((std::string{ Player.GetCharName() } +" Enabled").c_str(), EndeavourusMiscUtils::GetUniqueSettingName(CHAMP_ENABLED), true);
	}
	
	if (BaseSettings::isChampEnabled() && !GetUnsupportedChampionLoaded()) {
		EndeavourusMenu::Tree(champName.c_str(), EndeavourusMiscUtils::GetUniqueSettingName(CHAMP_SETTINGS), false, DrawLogicSettings);
	}
	EndeavourusMenu::Tree(keySettings.c_str(), EndeavourusMiscUtils::GetUniqueSettingName(KEY_SETTINGS), false, DrawKeySettings);
	ActivatorMenuManager::DrawActivatorSettings();
}

void BaseMenuManager::DrawKeySettings()
{
	EndeavourusMenu::EndeavourusHotkey("Combo Key", EndeavourusMiscUtils::GetUniqueSettingName(COMBO_KEY), KeyCode::Space);
	EndeavourusMenu::EndeavourusHotkey("Mixed Key", EndeavourusMiscUtils::GetUniqueSettingName(Mixed_KEY), KeyCode::C);
	EndeavourusMenu::EndeavourusHotkey("Clear Key", EndeavourusMiscUtils::GetUniqueSettingName(Clear_KEY), KeyCode::V);
	EndeavourusMenu::EndeavourusHotkey("Lasthit Key", EndeavourusMiscUtils::GetUniqueSettingName(Lasthit_KEY), KeyCode::X);
	EndeavourusMenu::EndeavourusHotkey("Escape Key", EndeavourusMiscUtils::GetUniqueSettingName(Escape_KEY), KeyCode::A);
}

Resource BaseMenuManager::GetComboResourceManagement()
{
	return comboResourceSettings;
}

Resource BaseMenuManager::GetHarassResourceManagement()
{
	return harassResourceSettings;
}

Resource BaseMenuManager::GetClearResourceManagement()
{
	return clearResourceSettings;
}

Resource BaseMenuManager::GetLastHitResourceManagement()
{
	return lastHitResourceSettings;
}

Resource BaseMenuManager::GetEscapeResourceManagement()
{
	return escapeResourceSettings;
}

Resource BaseMenuManager::GetKsResourceManagement()
{
	return ksResourceSettings;
}

bool BaseMenuManager::GetUnsupportedChampionLoaded()
{
	return unsupportedChampionLoaded;
}

void BaseMenuManager::SetUnsupportedChampionLoaded(const bool unsupportedChampionLoaded)
{
	BaseMenuManager::unsupportedChampionLoaded = unsupportedChampionLoaded;
}

void DrawQSetting()
{
	EndeavourusMenu::Checkbox("Draw Q Range", EndeavourusMiscUtils::GetUniqueSettingName(Q_RANGE_DRAWING_ENABLED), true);
	SDKVECTOR defaultColour{ 244, 80, 66 };
	EndeavourusMenu::ColorPicker("Draw Q Colour", EndeavourusMiscUtils::GetUniqueSettingName(Q_RANGE_DRAWING_COLOUR), defaultColour);
}

void DrawWSetting()
{
	EndeavourusMenu::Checkbox("Draw W Range", EndeavourusMiscUtils::GetUniqueSettingName(W_RANGE_DRAWING_ENABLED), true);
	SDKVECTOR defaultColour{ 107, 244, 66 };
	EndeavourusMenu::ColorPicker("Draw W Colour", EndeavourusMiscUtils::GetUniqueSettingName(W_RANGE_DRAWING_COLOUR), defaultColour);
}

void DrawESetting()
{
	EndeavourusMenu::Checkbox("Draw E Range", EndeavourusMiscUtils::GetUniqueSettingName(E_RANGE_DRAWING_ENABLED), true);
	SDKVECTOR defaultColour{ 98, 66, 244 };
	EndeavourusMenu::ColorPicker("Draw E Colour", EndeavourusMiscUtils::GetUniqueSettingName(E_RANGE_DRAWING_COLOUR), defaultColour);
}

void DrawRSetting()
{
	EndeavourusMenu::Checkbox("Draw R Range", EndeavourusMiscUtils::GetUniqueSettingName(R_RANGE_DRAWING_ENABLED), true);
	SDKVECTOR defaultColour{ 244, 223, 66 };
	EndeavourusMenu::ColorPicker("Draw R Colour", EndeavourusMiscUtils::GetUniqueSettingName(R_RANGE_DRAWING_COLOUR), defaultColour);
}

void BaseMenuManager::DrawQRange()
{
	EndeavourusMenu::Tree("Q Range Drawing", EndeavourusMiscUtils::GetUniqueSettingName("Q_Setting_Group"), false, DrawQSetting);
}

void BaseMenuManager::DrawWRange()
{
	EndeavourusMenu::Tree("W Range Drawing", EndeavourusMiscUtils::GetUniqueSettingName("W_Setting_Group"), false, DrawWSetting);
}

void BaseMenuManager::DrawERange()
{
	EndeavourusMenu::Tree("E Range Drawing", EndeavourusMiscUtils::GetUniqueSettingName("E_Setting_Group"), false, DrawESetting);
}

void BaseMenuManager::DrawRRange()
{
	EndeavourusMenu::Tree("R Range Drawing", EndeavourusMiscUtils::GetUniqueSettingName("R_Setting_Group"), false, DrawRSetting);
}

void BaseMenuManager::DrawSelectedTarget()
{
	EndeavourusMenu::Tree("Selected Target Drawing", EndeavourusMiscUtils::GetUniqueSettingName("SELECTED_TARGET_GROUP"), false, SelectedTarget);
}

void BaseMenuManager::SelectedTarget()
{
	EndeavourusMenu::Checkbox("Draw Endeavourus Selected Target", EndeavourusMiscUtils::GetUniqueSettingName(SELECTED_CHAMPION_LEFT_CLICK), true);
	SDKVECTOR defaultColour{ 188, 191, 196 };
	EndeavourusMenu::ColorPicker("Draw Endeavourus Selected Target Colour", EndeavourusMiscUtils::GetUniqueSettingName(SELECTED_CHAMP_DRAWING_COLOUR), defaultColour);
}
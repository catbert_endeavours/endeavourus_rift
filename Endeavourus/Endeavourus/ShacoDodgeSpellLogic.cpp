#include "stdafx.h"
#include "ShacoDodgeSpellLogic.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "ShacoConstants.h"
#include "ShacoDangerousSpells.h"
#include "Geometry.hpp"
#include "ShacoLogicManager.h"
#include "ShacoCloneLogicManager.h"

float blitzDodgeQCastCD{ 0 };
float ShacoDodgeSpellLogic::shacoWCastOffset{ 0.0f };

struct shacoWSpellStruct {
	float playerHitTime{};
	SDKVECTOR startPos{};
};

std::map<std::string, shacoWSpellStruct> missilesToBlock{};
std::map<std::string, std::pair<Vector3, float>> qToCounter{};

void ShacoDodgeSpellLogic::Init()
{
	pSDK->EventHandler->RegisterCallback(CallbackEnum::SpellCastStart, OnCastStart);
}

void ShacoDodgeSpellLogic::PerformAntiBlitz() {
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ANTI_BLITZCRANK_ENABLED, true)) {
		SDK_SPELL Q = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
		Spell::Targeted QCast = { SpellSlot::Q, Q.CastRange };

		auto result = qToCounter.find("Blitzcrank");
		Vector3 qCastPos{};
		if (result != qToCounter.end())
		{
			if (Game::Time() < result->second.second) {
				if (QCast.IsReady() && Game::Time() > blitzDodgeQCastCD) {
					if (QCast.Cast(&result->second.first)) {
						blitzDodgeQCastCD = Game::Time() + 0.25f;
					}
				}
			}
		}

		for (auto b : Player.GetBuffs()) {
			GameObject* caster = pSDK->EntityManager->GetObjectFromPTR(b.Caster);
			if (caster != nullptr && caster->IsValid() && caster->IsHero() && caster->IsEnemy()) {
				std::string name{ caster->AsAIHeroClient()->GetCharName() };
				std::string buffName{ b.Name };
				if (name.find("Blitzcrank") != std::string::npos) {
					if (buffName.find("Stun") != std::string::npos || buffName.find("rocketgrab2") != std::string::npos) {
						auto result = qToCounter.find(name);
						Vector3 qCastPos{};
						if (result != qToCounter.end())
						{
							if (Game::Time() > result->second.second) {
								Vector3 pos{ Player.GetPosition() };
								Vector3 extended{ Player.GetPosition().Extended(pos, QCast.Range) };
								std::pair<Vector3, float> qSpell{ extended, Game::Time() + 2 };
								result->second = qSpell;
							}
							else {
								qCastPos = result->second.first;
							}
						}
						else
						{
							Vector3 pos{ Player.GetPosition() };
							Vector3 extended{ Player.GetPosition().Extended(pos, QCast.Range) };

							std::pair<Vector3, float> qSpell{ extended, Game::Time() + 2 };
							qToCounter.emplace(name, qSpell);
							qCastPos = qSpell.first;
						}
					}
				}
			}
		}
	}
}

void ShacoDodgeSpellLogic::PerformLogic() {
	if (Player.IsAlive() && Player.IsValid() && !Player.IsZombie() && Player.IsVisible()) {
		PerformAntiBlitz();
		PerformSkillshotLogic();
	}
}

void ShacoDodgeSpellLogic::PerformSkillshotLogic() {
	bool wCasted{ false };
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT_BLOCKER, true) || EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_W_SPELL_BLOCKER, true)) {
		for (auto m : pSDK->EntityManager->GetEnemyMissiles(1500.0f)) {

			GameObject* owner = m.second->GetOwner();
			if (owner != nullptr && owner->IsHero() && owner->IsEnemy()) {

				if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_W_SPELL_BLOCKER, true)) {
					SDK_SPELL W = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
					Spell::Targeted WCast = { SpellSlot::W, W.CastRange };

					if (WCast.IsReady()) {
						for (auto m : pSDK->EntityManager->GetEnemyMissiles(1500.0f)) {
							auto result = SKILLSHOT_MAP_W.find(owner->AsAIHeroClient()->GetCharName());
							if (result != SKILLSHOT_MAP_W.end()) {

								bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_SPELL_W_BLOCKER + "_" + result->second.displayName, true);
								std::string missileName{ m.second->GetName() };
								if (missileName.find(result->second.missileName) != std::string::npos) {
									float playerDistance{ Player.Distance(&m.second->GetPosition()) };
									float timeToReachPlayer = playerDistance / m.second->GetMissileSpeed();
									float gameTimeToReach = Game::Time() + timeToReachPlayer;
									if (gameTimeToReach > Game::Time() && timeToReachPlayer < 0.70f && timeToReachPlayer >= 0.35) {

										Vector3 startPos{ m.second->GetPosition() };
										Vector3 endPos{ m.second->GetEndPos() };

										std::shared_ptr<ICollision::Output> collision{};
										if (result->second.minionCollision) {
											collision = pSDK->Collision->GetCollisions(startPos, endPos, m.second->GetWidth(), 0, m.second->GetMissileSpeed(), true, (CollisionFlags::Minions | CollisionFlags::Heroes | CollisionFlags::YasuoWall));
										}
										else {
											collision = pSDK->Collision->GetCollisions(startPos, endPos, m.second->GetWidth(), 0, m.second->GetMissileSpeed(), true, (CollisionFlags::Heroes | CollisionFlags::YasuoWall));
										}

										bool collided{ false };
										float distance{ FLT_MAX };
										for (auto unit : collision->Units) {
											if (collision->Units.size() == 1 && unit->GetNetworkID() == Player.GetNetworkID()) {
												// Going to hit player
												collided = true;
												break;
											}

											if (unit->GetNetworkID() == Player.GetNetworkID()) {
												collided = true;
											}
											else {
												float sPos{ unit->GetServerPosition().Distance(startPos) };
												if (sPos < distance) {
													distance = sPos;
												}
											}
										}

										if ((!result->second.heroCollision && !result->second.minionCollision && collided) || (collided && distance == FLT_MAX) || (collided && playerDistance < distance)) {
											const std::string charName{ Player.GetCharName() };
											if (Player.IsAlive() && Player.IsValid()) {
												if (WCast.IsReady() && Game::Time() > shacoWCastOffset) {
													Vector3 castPos{ Player.GetPosition().Extended(startPos, 150.0f) };
													if (WCast.Cast(&castPos)) {
														shacoWCastOffset = Game::Time() + 0.25f;
														wCasted = true;
													}
												}
											}
										}
									}
								}
							}
						}

					}
				}

				if (wCasted) {
					return; // Dont need to block with R
				}

				SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
				Spell::Active RCast = { SpellSlot::R };

				if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {

					auto result = SKILLSHOT_MAP_SHACO.find(owner->AsAIHeroClient()->GetCharName());
					if (result != SKILLSHOT_MAP_SHACO.end())
					{
						bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT + "_" + result->second.displayName, true);
						std::string missileName{ m.second->GetName() };
						if (missileName.find(result->second.missileName) != std::string::npos) {

							if (enabled) {
								float playerDistance{ Player.Distance(&m.second->GetPosition()) };
								float timeToReachPlayer = playerDistance / m.second->GetMissileSpeed();
								float gameTimeToReach = Game::Time() + timeToReachPlayer;
								if (gameTimeToReach > Game::Time() && timeToReachPlayer < 0.70f && timeToReachPlayer >= 0.35) {

									Vector3 startPos{ m.second->GetPosition() };
									Vector3 endPos{ m.second->GetEndPos() };

									std::shared_ptr<ICollision::Output> collision{};
									if (result->second.minionCollision) {
										collision = pSDK->Collision->GetCollisions(startPos, endPos, m.second->GetWidth(), 0, m.second->GetMissileSpeed(), true, (CollisionFlags::Minions | CollisionFlags::Heroes | CollisionFlags::YasuoWall));
									}
									else {
										collision = pSDK->Collision->GetCollisions(startPos, endPos, m.second->GetWidth(), 0, m.second->GetMissileSpeed(), true, (CollisionFlags::Heroes | CollisionFlags::YasuoWall));
									}

									bool collided{ false };
									float distance{ FLT_MAX };
									for (auto unit : collision->Units) {
										if (collision->Units.size() == 1 && unit->GetNetworkID() == Player.GetNetworkID()) {
											// Going to hit player
											collided = true;
											break;
										}

										if (unit->GetNetworkID() == Player.GetNetworkID()) {
											collided = true;
										}
										else {
											float sPos{ unit->GetServerPosition().Distance(startPos) };
											if (sPos < distance) {
												distance = sPos;
											}
										}
									}

									if ((!result->second.heroCollision && !result->second.minionCollision && collided) || (collided && distance == FLT_MAX) || (collided && playerDistance < distance)) {
										const std::string charName{ Player.GetCharName() };
										if (Player.IsAlive() && Player.IsValid()) {
											if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
												RCast.Cast();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void __cdecl ShacoDodgeSpellLogic::OnCastStart(void * heroCasting, PSDK_SPELL_CAST SpellCast, void * UserData)
{
	OnCastStartCheckTargeted(heroCasting, SpellCast);
	OnCastStartCheckSkillshot(heroCasting, SpellCast);
	//OnCastStartCheckCircle(heroCasting, SpellCast);
}

void ShacoDodgeSpellLogic::OnCastStartCheckSkillshot(void* heroCasting, PSDK_SPELL_CAST SpellCast) {
	bool wCasted{ false };
	GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(heroCasting);
	if (hero != nullptr && hero->IsHero() && hero->IsEnemy()) {

		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_W_SPELL_BLOCKER, true)) {
			SDK_SPELL W = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
			Spell::Targeted WCast = { SpellSlot::W, W.CastRange };

			if (WCast.IsReady()) {
				auto result = SKILLSHOT_MAP_W.find(hero->AsAIHeroClient()->GetCharName());
				if (result != SKILLSHOT_MAP_W.end())
				{
					float playerDistance{ Player.Distance(SpellCast->StartPosition) };
					float timeToReachPlayer = playerDistance / SpellCast->Spell.MissileSpeed;
					float gameTimeToReach = Game::Time() + timeToReachPlayer;

					bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_SPELL_W_BLOCKER + "_" + result->second.displayName, true);

					if (enabled) {
						auto spellRec = Geometry::Rectangle(SpellCast->StartPosition, SpellCast->EndPosition, SpellCast->Spell.LineWidth);

						if (spellRec.IsInside(Player.GetPosition())) {

							if (WCast.IsReady() && Game::Time() > shacoWCastOffset) {
								Vector3 castPos{ Player.GetPosition().Extended(SpellCast->StartPosition, 150.0f) };
								if (WCast.Cast(&castPos)) {
									wCasted = true;
									shacoWCastOffset = Game::Time() + 0.25f;
								}
							}
						}
					}
				}
			}
		}

		if (wCasted) {
			return;
		}

		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT_BLOCKER, true)) {

			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Active RCast = { SpellSlot::R };

			if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
				auto result = SKILLSHOT_MAP_SHACO.find(hero->AsAIHeroClient()->GetCharName());
				if (result != SKILLSHOT_MAP_SHACO.end())
				{
					float playerDistance{ Player.Distance(SpellCast->StartPosition) };
					float timeToReachPlayer = playerDistance / SpellCast->Spell.MissileSpeed;
					float gameTimeToReach = Game::Time() + timeToReachPlayer;
					if (gameTimeToReach > Game::Time() && timeToReachPlayer < 0.70f && timeToReachPlayer >= 0.35) {

						std::string spellName{ SpellCast->Spell.Name };
						if (spellName.find(result->second.spellName) != std::string::npos) {

							bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_SKILLSHOT + "_" + result->second.displayName, true);

							if (enabled) {
								if (spellName.find("GnarR") != std::string::npos || spellName.find("CassiopeiaR") != std::string::npos) {
									{
										if (hero->AsAIHeroClient()->IsFacing(&Player)) {
											if (Player.Distance(hero) < 400.0f) {
												if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
													RCast.Cast();
													ShacoLogicManager::SHACO_SHOULD_CAST_CLONE = true;
												}
											}
										}
									}
								}
								else {
									auto spellRec = Geometry::Rectangle(SpellCast->StartPosition, SpellCast->EndPosition, SpellCast->Spell.LineWidth);

									if (spellRec.IsInside(Player.GetPosition())) {
										if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
											RCast.Cast();
											ShacoLogicManager::SHACO_SHOULD_CAST_CLONE = true;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void ShacoDodgeSpellLogic::OnCastStartCheckCircle(void* heroCasting, PSDK_SPELL_CAST SpellCast) {
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_CIRCLE_BLOCKER, true)) {

		GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(heroCasting);
		if (hero != nullptr && hero->IsHero() && hero->IsEnemy()) {
			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Active RCast = { SpellSlot::R };

			if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
				auto result = CIRCLE_MAP_SHACO.find(hero->AsAIHeroClient()->GetCharName());
				if (result != CIRCLE_MAP_SHACO.end())
				{
					std::string spellName{ SpellCast->Spell.Name };
					if (spellName.find(result->second.spellName) != std::string::npos) {
						bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_CIRCLE + "_" + result->second.displayName, true);

						if (enabled) {
							if (Player.Distance(&SpellCast->EndPosition) < SpellCast->Spell.PrimaryCastRadius) {
								if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
									RCast.Cast();
									ShacoLogicManager::SHACO_SHOULD_CAST_CLONE = true;
								}
							}
						}
					}
				}
			}
		}
	}
}

void ShacoDodgeSpellLogic::OnCastStartCheckTargeted(void* heroCasting, PSDK_SPELL_CAST SpellCast) {
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_TARGETED_BLOCKER, true)) {
		GameObject* target = pSDK->EntityManager->GetObjectFromPTR(SpellCast->TargetObject);
		GameObject* hero = pSDK->EntityManager->GetObjectFromPTR(heroCasting);
		if (hero != nullptr && hero->IsHero() && hero->IsEnemy() && target != nullptr && target->IsHero() && target->GetName() == Player.GetName()) {
			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Active RCast = { SpellSlot::R };

			if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
				auto result = TARGETED_MAP_SHACO.find(hero->AsAIHeroClient()->GetCharName());
				if (result != TARGETED_MAP_SHACO.end())
				{
					std::string spellName{ SpellCast->Spell.Name };
					if (spellName.find(result->second.spellName) != std::string::npos) {
						bool enabled = EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_R_TARGETED + "_" + result->second.displayName, true);
						if (enabled) {
							if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED) {
								RCast.Cast();
								ShacoLogicManager::SHACO_SHOULD_CAST_CLONE = true;
							}
						}
					}
				}
			}
		}
	}
}
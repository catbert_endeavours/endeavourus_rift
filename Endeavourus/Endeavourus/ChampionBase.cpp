#include "stdafx.h"
#include "ChampionBase.h"

bool ChampionBase::forceSettingRefresh{ false };

bool ChampionBase::GetForceSettingRefresh()
{
	return forceSettingRefresh;
}

void ChampionBase::SetForceSettingsRefresh(const bool forceMenuRefresh)
{
	forceSettingRefresh = forceMenuRefresh;
}

bool ChampionBase::NeedToRefreshSettings()
{
	if (GetForceSettingRefresh())
	{
		SetForceSettingsRefresh(false);
		return true;
	}
	return false;
}
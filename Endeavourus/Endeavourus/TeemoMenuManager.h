#pragma once
#include "BaseMenuManager.h"

class TeemoMenuManager :
	public BaseMenuManager
{
private:
	static void ColourOptionShroomLocationSetting();
	static void ShroomLocationCircleOption();
	static void SpecificChampMenuLogic();
	static void TeemoDrawingLogic();
	static void TeemoSpecificSpellLogic();

public:
	static void Init();
	static void DrawMenu();
};


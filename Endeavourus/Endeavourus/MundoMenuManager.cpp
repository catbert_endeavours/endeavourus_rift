#include "stdafx.h"
#include "MundoMenuManager.h"
#include "MundoConstants.h"
#include "EndeavourusMiscUtils.h"
#include "DrawConstants.h"
#include "EndeavourusMenu.h"

void MundoMenuManager::Init()
{
	specificChampLogic = SpecificChampMenuLogic;

	comboResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::High, true, true, false, false,
		EndeavourusHitChance::Medium, true, true, false, false, EndeavourusHitChance::Medium, true,
		true, false, false, EndeavourusHitChance::Medium);
	harassResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::Medium, true, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	clearResourceSettings = *new Resource(true, true, true, false, false, EndeavourusHitChance::Medium, true, true, false, false,
		EndeavourusHitChance::Medium, true, true, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 20, 20, 20, 20);
	lastHitResourceSettings = *new Resource(true, true, true, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	escapeResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	ksResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::High, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium);
}

void MundoMenuManager::SpecificChampMenuLogic()
{
	EndeavourusMenu::Tree("Mundo Specific Spell Settings", EndeavourusMiscUtils::GetUniqueSettingName(MUNDO_LOGIC_SETTING_GROUP), false, MundoSpecificSpellLogic);
	EndeavourusMenu::Tree("Drawings", EndeavourusMiscUtils::GetUniqueSettingName(DRAWING_GROUP), false, MundoDrawingLogic);
}

void MundoMenuManager::MundoSpecificSpellLogic()
{
	EndeavourusMenu::SliderInt("Health to Cast R on in Combo", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_MUNDO_ULT_WHEN_HEALTH), 25, 0, 100);
	EndeavourusMenu::Checkbox("Manually turn off Mundo W (Disables W resouce manager)", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_MANUAL_MUNDO_W), true);
}

void MundoMenuManager::MundoDrawingLogic()
{
	DrawQRange();
	DrawWRange();
}

void MundoMenuManager::DrawMenu()
{
	BaseMenuManager::DrawMenu();
}

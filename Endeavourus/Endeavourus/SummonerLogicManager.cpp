#include "stdafx.h"
#include "SummonerLogicManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "ActivatorConstants.h"
#include <string>

const std::vector<int> IGNITE_P_DAMAGE = { 70, 90, 110, 130, 150, 170, 190, 210, 230, 250, 270, 290, 310, 330, 350, 370, 390, 410 };

void SummonerLogicManager::PerformSummoners() {
	if (EndeavourusMiscUtils::GetOrbwalkingMode() == OrbwalkingMode::Combo) {
		PerformIgniteLogic();
		PerformExhaustLogic();
		PerformHealLogic();
		PerformBarrierLogic();
		PerformCleanseLogic();
	}
}

void SummonerLogicManager::PerformIgniteLogic()
{
	if (EndeavourusMiscUtils::GetBoolSetting(IGNITE_ENABLED, true)) {
		SDK_SPELL igniteSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find("SummonerDot") != std::string::npos)
		{
			igniteSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find("SummonerDot") != std::string::npos)
		{
			igniteSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (igniteSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Targeted ignite = Spell::Targeted{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2, igniteSpell.CastRange, DamageType::True };

			for (auto e : pSDK->EntityManager->GetEnemyHeroes(igniteSpell.CastRange)) {
				if (!IsSpellBlockedOnChampion(IGNITE_BLACKLIST_ENABLED, e.second->GetCharName())) {
					if (ignite.IsReady() && e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
						int damage = IGNITE_P_DAMAGE[Player.GetLevel() - 1];

						if (e.second->GetHealth().Current <= damage) {
							ignite.Cast(e.second);
							break;
						}
					}
				}
			}
		}
	}
}

void SummonerLogicManager::PerformExhaustLogic()
{
	if (EndeavourusMiscUtils::GetBoolSetting(EXHAUST_ENABLED, true)) {
		SDK_SPELL exhaustSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find("SummonerExhaust") != std::string::npos)
		{
			exhaustSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find("SummonerExhaust") != std::string::npos)
		{
			exhaustSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (exhaustSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Targeted exhaust = Spell::Targeted{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2, exhaustSpell.CastRange, DamageType::True };

			for (auto e : pSDK->EntityManager->GetEnemyHeroes(exhaustSpell.CastRange)) {
				if (!IsSpellBlockedOnChampion(EXHAUST_BLACKLIST_ENABLED, e.second->GetCharName())) {
					if (exhaust.IsReady() && e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
						for (auto a : pSDK->EntityManager->GetAllyHeroes(exhaustSpell.CastRange)) {
							if (a.second->IsValid() && !a.second->IsZombie() && a.second->IsAlive() && a.second->IsVisible()) {
								if (EndeavourusMiscUtils::GetFloatSetting(EXHAUST_PLAYER_ALLY_HEALTH, 25.0f) > a.second->GetHealthPercent()) {
									if (EndeavourusMiscUtils::GetBoolSetting(EXHAUST_ICOMING_DAMAGE_ENABLED, true)) {
										float healthPred = pSDK->HealthPred->GetHealthPrediction(a.second->AsAIBaseClient(), 2000);
										if (a.second->GetHealth().Current - healthPred > 1) {
											exhaust.Cast(e.second);
											break;
										}
									}
									else {
										exhaust.Cast(e.second);
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void SummonerLogicManager::PerformHealLogic()
{
	if (EndeavourusMiscUtils::GetBoolSetting(HEAL_ENABLED, true)) {
		SDK_SPELL healSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find("SummonerHeal") != std::string::npos)
		{
			healSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find("SummonerHeal") != std::string::npos)
		{
			healSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (healSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Active heal = Spell::Active{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2, healSpell.CastRange };

			for (auto a : pSDK->EntityManager->GetAllyHeroes(healSpell.CastRange)) {
				if (!IsSpellBlockedOnChampion(Heal_BLACKLIST_ENABLED, a.second->GetCharName())) {
					if (heal.IsReady() && a.second->IsValid() && a.second->IsAlive() && !a.second->IsZombie() && a.second->IsVisible()) {

						for (auto e : pSDK->EntityManager->GetEnemyHeroes(EndeavourusMiscUtils::GetFloatSetting(HEAL_ENEMY_RANGE, 750.0f), &a.second->GetPosition())) {
							if (e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
								if (EndeavourusMiscUtils::GetFloatSetting(HEAL_PERCENT, 25.0f) > a.second->GetHealthPercent()) {
									if (EndeavourusMiscUtils::GetBoolSetting(HEAL_ICOMING_DAMAGE_ENABLED, true)) {
										float healthPred = pSDK->HealthPred->GetHealthPrediction(a.second->AsAIBaseClient(), 2000);
										if (a.second->GetHealth().Current - healthPred > 1) {
											heal.Cast();
											break;
										}
									}
									else {
										heal.Cast();
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void SummonerLogicManager::PerformBarrierLogic()
{
	if (EndeavourusMiscUtils::GetBoolSetting(BARRIER_ENABLED, true)) {
		SDK_SPELL barrierSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find("SummonerBarrier") != std::string::npos)
		{
			barrierSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find("SummonerBarrier") != std::string::npos)
		{
			barrierSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (barrierSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Active barrier = Spell::Active{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2, barrierSpell.CastRange };

			if (barrier.IsReady() && Player.IsValid() && Player.IsAlive() && !Player.IsZombie() && Player.IsVisible()) {
				for (auto e : pSDK->EntityManager->GetEnemyHeroes(EndeavourusMiscUtils::GetFloatSetting(BARRIER_ENEMY_RANGE, 750.0f))) {
					if (e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
						if (EndeavourusMiscUtils::GetFloatSetting(BARRIER_PERCENT, 25.0f) > Player.GetHealthPercent()) {
							if (EndeavourusMiscUtils::GetBoolSetting(BARRIER_ICOMING_DAMAGE_ENABLED, true)) {
								float healthPred = pSDK->HealthPred->GetHealthPrediction(Player.AsAIBaseClient(), 2000);
								if (Player.GetHealth().Current - healthPred > 1) {
									barrier.Cast();
									break;
								}
							}
							else {
								barrier.Cast();
								break;
							}
						}
					}
				}
			}
		}
	}
}

void SummonerLogicManager::PerformCleanseLogic()
{
	if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_ENABLED, true)) {
		SDK_SPELL cleanseSpell{};
		bool summoner1{ false };

		if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1)).Name).find("SummonerBoost") != std::string::npos)
		{
			cleanseSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner1));
			summoner1 = true;
		}
		else if (std::string(Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2)).Name).find("SummonerBoost") != std::string::npos)
		{
			cleanseSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Summoner2));
		}

		if (cleanseSpell.Name != nullptr && Player.IsValid() && !Player.IsZombie() && Player.IsAlive())
		{
			Spell::Active cleanse = Spell::Active{ summoner1 ? SpellSlot::Summoner1 : SpellSlot::Summoner2 };

			if (cleanse.IsReady()) {
				if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_CHARM, true) && Player.HasBuffType(BUFF_TYPE_CHARM)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_BLIND, true) && Player.HasBuffType(BUFF_TYPE_BLIND)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_DISARM, true) && Player.HasBuffType(BUFF_TYPE_DISARM)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_FEAR, true) && Player.HasBuffType(BUFF_TYPE_FEAR)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_FLEE, true) && Player.HasBuffType(BUFF_TYPE_FLEE)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_POLY, true) && Player.HasBuffType(BUFF_TYPE_POLYMORPH)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_SNARE, true) && Player.HasBuffType(BUFF_TYPE_SNARE)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_TAUNT, true) && Player.HasBuffType(BUFF_TYPE_TAUNT)) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_EXHAUST, true) && Player.HasBuff("SummonerExhaust")) {
					cleanse.Cast();
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(CleanseBUFF_TYPE_STUN, true) && Player.HasBuffType(BUFF_TYPE_STUN)) {
					cleanse.Cast();
				}
			}
		}
	}
}

bool SummonerLogicManager::IsSpellBlockedOnChampion(std::string spellName, std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(spellName + "_" + enemyName, false);
}
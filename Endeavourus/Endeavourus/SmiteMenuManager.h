#pragma once
class SmiteMenuManager
{
private:
	static void DrawSmiteMonsterSettings();
	static void DrawSmiteCreepSettings();
	static void DrawSmiteEpicSettings();
	static void DrawSmiteBuffSettings();
	static void DrawSmitePlayerSettings();
	static void DrawSmiteDrawingSettings();

public:
	static void DrawSmiteSettings();
};


#pragma once
#include <string>

const std::string ACTIVATOR_TREE = "ACTIVATOR_TREE";
const std::string ACTIVATOR_ENABLED = "ACTIVATOR_ENABLED";

const std::string SUMMONER_SPELLS_TREE = "SUMMONER_SPELLS_TREE";

const std::string IGNITE_TREE = "IGNITE_TREE";
const std::string IGNITE_ENABLED = "IGNITE_ENABLED";
const std::string IGNITE_BLACKLIST = "IGNITE_BLACKLIST";
const std::string IGNITE_BLACKLIST_ENABLED = "IGNITE_BLACKLIST_ENABLED";

const std::string CLEANSE_TREE = "CLEANSE_TREE";
const std::string CleanseBUFF_ENABLED = "CleanseBUFF_ENABLED";
const std::string CleanseDelayBUFF = "CleanseDelayBUFF";
const std::string CleanseBUFF_TYPE_CHARM = "CleanseBUFF_TYPE_CHARM";
const std::string CleanseBUFF_TYPE_BLIND = "CleanseBUFF_TYPE_BLIND";
const std::string CleanseBUFF_TYPE_DISARM = "CleanseBUFF_TYPE_DISARM";
const std::string CleanseBUFF_TYPE_FEAR = "CleanseBUFF_TYPE_FEAR";
const std::string CleanseBUFF_TYPE_FLEE = "CleanseBUFF_TYPE_FLEE";
const std::string CleanseBUFF_TYPE_POLY = "CleanseBUFF_TYPE_POLY";
const std::string CleanseBUFF_TYPE_SNARE = "CleanseBUFF_TYPE_SNARE";
const std::string CleanseBUFF_TYPE_TAUNT = "CleanseBUFF_TYPE_TAUNT";
const std::string CleanseBUFF_TYPE_EXHAUST = "CleanseBUFF_TYPE_EXHAUST";
const std::string CleanseBUFF_TYPE_STUN = "CleanseBUFF_TYPE_STUN";
const std::string CleanseBUFF_TYPE_SUPRESSION = "CleanseBUFF_TYPE_SUPRESSION";

const std::string EXHAUST_TREE = "EXHAUST_TREE";
const std::string EXHAUST_ENABLED = "EXHAUST_ENABLED";
const std::string EXHAUST_ICOMING_DAMAGE_ENABLED = "EXHAUST_ICOMING_DAMAGE_ENABLED";
const std::string EXHAUST_ENEMIES_WITHIN_RANGE = "EXHAUST_ENEMIES_WITHIN_RANGE";
const std::string EXHAUST_PLAYER_ALLY_HEALTH = "EXHAUST_PLAYER_ALLY_HEALTH";
const std::string EXHAUST_BLACKLIST = "EXHAUST_BLACKLIST";
const std::string EXHAUST_BLACKLIST_ENABLED = "EXHAUST_BLACKLIST_ENABLED";

const std::string HEAL_TREE = "HEAL_TREE";
const std::string HEAL_ENABLED = "HEAL_ENABLED";
const std::string HEAL_ICOMING_DAMAGE_ENABLED = "HEAL_ICOMING_DAMAGE_ENABLED";
const std::string HEAL_ENEMY_RANGE = "HEAL_ENEMY_RANGE";
const std::string HEAL_PERCENT = "HEAL_PERCENT";
const std::string HEAL_ENEMY_RADIUS = "HEAL_ENEMY_RADIUS";
const std::string Heal_BLACKLIST = "Heal_BLACKLIST";
const std::string Heal_BLACKLIST_ENABLED = "Heal_BLACKLIST_ENABLED";

const std::string BARRIER_TREE = "BARRIER_TREE";
const std::string BARRIER_ENABLED = "BARRIER_ENABLED";
const std::string BARRIER_ICOMING_DAMAGE_ENABLED = "BARRIER_ICOMING_DAMAGE_ENABLED";
const std::string BARRIER_ENEMY_RANGE = "BARRIER_ENEMY_RANGE";
const std::string BARRIER_PERCENT = "BARRIER_PERCENT";
const std::string BARRIER_ENEMY_RADIUS = "BARRIER_ENEMY_RADIUS";

const std::string ITEMS_TREE = "ITEMS_TREE";

const std::string OFFENSIVE_ITEMS_TREE = "OFFENSIVE_ITEMS_TREE";
const std::string DEFENSIVE_ITEMS_TREE = "DEFENSIVE_ITEMS_TREE";

const std::string HextechProtobeltGLP800TREE = "HextechProtobeltGLP800_TREE";
const std::string HextechProtobelt_ENABLED = "HextechProtobelt_ENABLED";
const std::string HEXTECH_PRO_GLP_MODE = "HEXTECH_PRO_GLP_MODE";
const std::string HEXTECH_PRO_GLP_HEALTH = "HEXTECH_PRO_GLP_HEALTH";
const std::string HEXTECH_PRO_GLP_BLACKLIST = "HEXTECH_PRO_GLP_BLACKLIST";
const std::string HEXTECH_PRO_GLP_BLACKLIST_ENABLE = "HEXTECH_PRO_GLP_BLACKLIST_ENABLE";

const std::string HextechGunbladeTREE = "Hextech_Gunblade_TREE";
const std::string HextechGunblade_ENABLED = "HextechGunblade_ENABLED";
const std::string HextechGunblade_MODE = "HextechGunblade_MODE";
const std::string HextechGunblade_HEALTH = "HextechGunblade_HEALTH";
const std::string HextechGunblade_BLACKLIST = "HextechGunblade_BLACKLIST";
const std::string HextechGunblade_BLACKLIST_ENABLED = "HextechGunblade_BLACKLIST_ENABLED";

const std::string SpellbinderTREE = "Spellbinder_TREE";
const std::string Spellbinder_ENABLED = "Spellbinder_ENABLED";
const std::string Spellbinder_MODE = "Spellbinder_MODE";
const std::string Spellbinder_HEALTH = "Spellbinder_HEALTH";
const std::string Spellbinder_ENEMY_RANGE = "Spellbinder_ENEMY_RANGE";
const std::string Spellbinder_BLACKLIST = "Spellbinder_BLACKLIST";
const std::string Spellbinder_BLACKLIST_ENABLED = "Spellbinder_BLACKLIST_ENABLED";

const std::string TwinShadowsTREE = "TwinShadows_TREE";
const std::string TwinShadows_ENABLED = "TwinShadows_ENABLED";
const std::string TwinShadows_CLOSEST_TARGET = "TwinShadows_CLOSEST_TARGET";

const std::string EDGE_OF_NIGH_TREE = "EDGE_OF_NIGH_TREE";
const std::string EDGE_OF_NIGHT_ENABLED = "EDGE_OF_NIGHT_ENABLED";
const std::string EDGE_OF_NIGHT_CLOSEST_TARGET = "EDGE_OF_NIGHT_CLOSEST_TARGET";

const std::string TiamatTREE = "Tiamat/TitanicHydra/RavenousHydra_TREE";
const std::string TiamatENABLED = "Tiamat/TitanicHydra/RavenousHydra_ENABLED";
const std::string TiamatENABLED_COMBO = "Tiamat/TitanicHydra/RavenousHydra_ENABLED_COMBO";
const std::string TiamatENABLED_CLEAR = "Tiamat/TitanicHydra/RavenousHydra_ENABLED_CLEAR";

const std::string BilgewaterCutlassBOTRKTREE = "BilgewaterCutlass/BOTRK_TREE";
const std::string BilgewaterCutlassBOTRK_ENABLED = "BilgewaterCutlass/BOTRK_ENABLED";
const std::string BilgewaterCutlassBOTRKMODE = "BilgewaterCutlass/BOTRK_MODE";
const std::string BilgewaterCutlassBOTRKHEALTH = "BilgewaterCutlass/BOTRK_HEALTH";
const std::string BilgewaterCutlassBOTRKBLACKLIST = "BilgewaterCutlassBOTRKBLACKLIST";
const std::string BilgewaterCutlassBOTRKBLACKLIST_ENABLED = "BilgewaterCutlassBOTRKBLACKLIST_ENABLED";

const std::string YoumuusGhostbladeTREE = "Youmuus_Ghostblade_TREE";
const std::string YoumuusGhostbladeENABLED = "Youmuus_Ghostblade_ENABLED";
const std::string YoumuusGhostbladeECLOSEST_TARGET = "YoumuusGhostblade_CLOSEST_TARGET";
const std::string YoumuusGhostbladeBLACKLIST_ENABLED = "YoumuusGhostbladeBLACKLIST_ENABLED";
const std::string YoumuusGhostbladeBLACKLIST = "YoumuusGhostbladeBLACKLIST";

const std::string ArchangelsStaffTREE = "ArchangelsStaff_TREE";
const std::string ArchangelsStaffENABLED = "ArchangelsStaff_ENABLED";
const std::string ArchangelsStaff_PERCENT = "ArchangelsStaff_PERCENT";
const std::string ArchangelsStaff_ICOMING_DAMAGE_ENABLED = "ArchangelsStaff_ICOMING_DAMAGE_ENABLED";
const std::string ArchangelsStaff_ENEMY_DISTANCE = "ArchangelsStaff_ENABLED_DISTANCE";

const std::string ZhonyasHourglassTREE = "ZhonyasHourglass_TREE";
const std::string ZhonyasHourglassENABLED = "ZhonyasHourglassENABLED";
const std::string ZhonyasHourglassPERCENT = "ZhonyasHourglassPERCENT";
const std::string ZhonyasHourglass_ICOMING_DAMAGE_ENABLED = "ZhonyasHourglass_ICOMING_DAMAGE_ENABLED";
const std::string ZhonyasHourglass_ENEMY_DISTANCE = "ZhonyasHourglass_ENEMY_DISTANCE";

const std::string ShurelyasReverieTREE = "ShurelyasReverie_TREE";
const std::string ShurelyasReverieENABLED = "ShurelyasReverie_ENABLED";
const std::string ShurelyasReverie_MIN_ENEMIES = "ShurelyasReverie_MIN_ENEMIES";
const std::string ShurelyasReverie_ENEMIES_DISTANCE = "ShurelyasReverie_ENEMIES_DISTANCE";

const std::string QuicksilverSashMercurialScimitarTREE = "QuicksilverSashMercurialScimitar_TREE";
const std::string QuicksilverSashMercurialScimitarBUFF_DELAY = "QuicksilverSashMercurialScimitarBUFF_DELAY";
const std::string QuicksilverSashMercurialScimitarBUFF_ENABLED = "QuicksilverSashMercurialScimitarBUFF_ENABLED";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_CHARM = "QuicksilverSashMercurialScimitarBUFF_TYPE_CHARM";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_BLIND = "QuicksilverSashMercurialScimitarBUFF_TYPE_BLIND";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_DISARM = "QuicksilverSashMercurialScimitarBUFF_TYPE_DISARM";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_FEAR = "QuicksilverSashMercurialScimitarBUFF_TYPE_FEAR";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_FLEE = "QuicksilverSashMercurialScimitarBUFF_TYPE_FLEE";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_POLY = "QuicksilverSashMercurialScimitarBUFF_TYPE_POLY";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_SNARE = "QuicksilverSashMercurialScimitarBUFF_TYPE_SNARE";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_TAUNT = "QuicksilverSashMercurialScimitarBUFF_TYPE_TAUNT";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_EXHAUST = "QuicksilverSashMercurialScimitarBUFF_TYPE_EXHAUST";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_STUN = "QuicksilverSashMercurialScimitarBUFF_TYPE_STUN";
const std::string QuicksilverSashMercurialScimitarBUFF_TYPE_SUPRESSION = "QuicksilverSashMercurialScimitarBUFF_TYPE_SUPRESSION";


const std::string MikaelsCrucibleTREE = "MikaelsCrucible_TREE";
const std::string MikaelsCrucibleBUFF_ENABLED = "MikaelsCrucibleBUFF_ENABLED";
const std::string MikaelsCrucibleBUFF_DELAY = "MikaelsCrucibleBUFF_DELAY";
const std::string MikaelsCrucibleBUFF_TYPE_CHARM = "MikaelsCrucibleBUFF_TYPE_CHARM";
const std::string MikaelsCrucibleBUFF_TYPE_BLIND = "MikaelsCrucibleBUFF_TYPE_BLIND";
const std::string MikaelsCrucibleBUFF_TYPE_DISARM = "MikaelsCrucibleBUFF_TYPE_DISARM";
const std::string MikaelsCrucibleBUFF_TYPE_FEAR = "MikaelsCrucibleBUFF_TYPE_FEAR";
const std::string MikaelsCrucibleBUFF_TYPE_FLEE = "MikaelsCrucibleBUFF_TYPE_FLEE";
const std::string MikaelsCrucibleBUFF_TYPE_POLY = "MikaelsCrucibleBUFF_TYPE_POLY";
const std::string MikaelsCrucibleBUFF_TYPE_SNARE = "MikaelsCrucibleBUFF_TYPE_SNARE";
const std::string MikaelsCrucibleBUFF_TYPE_TAUNT = "MikaelsCrucibleBUFF_TYPE_TAUNT";
const std::string MikaelsCrucibleBUFF_TYPE_EXHAUST = "MikaelsCrucibleBUFF_TYPE_EXHAUST";
const std::string MikaelsCrucibleBUFF_TYPE_STUN = "MikaelsCrucibleBUFF_TYPE_STUN";
const std::string MikaelsCrucibleBUFF_TYPE_SUPRESSION = "MikaelsCrucibleBUFF_TYPE_SUPRESSION";
const std::string MikaelsCrucibleBLACKLIST = "MikaelsCrucibleBUFF_BLACKLIST";
const std::string MikaelsCrucibleBLACKLIST_ENABLED = "MikaelsCrucibleBUFF_BLACKLIST_ENABLED";

const std::string LocketoftheIronSolariTREE = "LocketoftheIronSolari_TREE";
const std::string LocketoftheIronSolari_ENABLED = "LocketoftheIronSolari_ENABLED";
const std::string LocketoftheIronSolari_ICOMING_DAMAGE_ENABLED = "LocketoftheIronSolari_ICOMING_DAMAGE_ENABLED";
const std::string LocketoftheIronSolari_ALLY_HEALTH = "LocketoftheIronSolari_ALLY_HEALTH";
const std::string LocketoftheIronSolari_ENEMIES_WITHIN_RANGE = "LocketoftheIronSolari_ENEMIES_WITHIN_RANGE";

const std::string GargoyleStoneplateTREE = "GargoyleStoneplateTREE";
const std::string GargoyleStoneplateeENABLED = "GargoyleStoneplateENABLED";
const std::string GargoyleStoneplatee_HEALTH = "GargoyleStoneplatee_HEALTH";
const std::string GargoyleStoneplatee_ICOMING_DAMAGE_ENABLED = "GargoyleStoneplatee_ICOMING_DAMAGE_ENABLED";
const std::string GargoyleStoneplatee_ENEMIES_WITHIN_RANGE = "GargoyleStoneplatee_ENEMIES_WITHIN_RANGE";

const std::string RedemptionTREE = "Redemption_TREE";
const std::string REDEMTION_ALLY_ENABLED = "REDEMTION_ALLY_ENABLED";
const std::string REDEMTION_ICOMING_DAMAGE_ENABLED = "REDEMTION_ICOMING_DAMAGE_ENABLED";
const std::string REDEMTION_ENEMY_RANGE = "REDEMTION_ENEMY_RANGE";
const std::string REDEMTION_ENEMY_ENABLED = "REDEMTION_ENEMY_ENABLED";
const std::string REDEMTION_ALLY_PERCENT = "REDEMTION_ALLY_PERCENT";
const std::string REDEMTION_ENEMY_PERCENT = "REDEMTION_ENEMY_PERCENT";

const std::string OhmwreckerTREE = "Ohmwrecker_TREE";
const std::string OhmwreckerENABLED = "Ohmwrecker_ENABLED";

const std::string RighteousGloryTREE = "RighteousGlory_TREE";
const std::string RighteousGloryENABLED = "RighteousGlory_ENABLED";
const std::string RighteousGlory_MIN_ENEMIES = "RighteousGlory_MIN_ENEMIES";
const std::string RighteousGlory_ENEMIES_DISTANCE = "RighteousGlory_ENEMIES_DISTANCE";



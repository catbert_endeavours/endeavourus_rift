#include "stdafx.h"
#include "ShacoMenuManager.h"
#include "LogicConstants.h"
#include "Resource.h"
#include "DrawConstants.h"
#include "EndeavourusMiscUtils.h"
#include "ShacoConstants.h"
#include "EndeavourusMenu.h"
#include "GeneralConstants.h"
#include "ShacoDangerousSpells.h"

int ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX{ 0 };

void ShacoMenuManager::Init()
{
	specificChampLogic = SpecificChampMenuLogic;

	comboResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::High, true, true, false, false,
		EndeavourusHitChance::Medium, true, true, true, false, EndeavourusHitChance::Medium, true,
		true, false, false, EndeavourusHitChance::Medium);
	harassResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, true, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	clearResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::Medium, true, true, false, false,
		EndeavourusHitChance::Medium, true, true, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	lastHitResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	escapeResourceSettings = *new Resource(true, true, true, false, false, EndeavourusHitChance::Medium, true, true, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium);
	ksResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::High, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, true, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium);
}

void ShacoMenuManager::SpecificChampMenuLogic()
{
	EndeavourusMenu::Tree("Shaco Specific Spell Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_SETTING_GROUP), false, ShacoSpecificSpellLogic);
	EndeavourusMenu::Tree("Drawings", EndeavourusMiscUtils::GetUniqueSettingName(DRAWING_GROUP), false, ShacoDrawingLogic);
}

void ShacoMenuManager::DrawMenu()
{
	BaseMenuManager::DrawMenu();
}

void ShacoMenuManager::ShacoAutoJungleSettings()
{
	SdkUiText("Only Works if enabled before the 40 second mark of the game");
	SdkUiText("and the player has smite, and has the first box placed pre 40");
	EndeavourusMenu::Checkbox("Enable Perfect Jungle Start", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_AUTO_JUNGLE), false);
	std::vector<std::string> itemList{ "Disable", "Hunters Machete & Refillable", "Hunters Machete & Rejuvenation Bead" };
	EndeavourusMenu::DropList("Starting Items (Auto Buy)", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_AUTOBUY_OPTIONS), itemList, 1);
	EndeavourusMenu::EndeavourusHotkey("Perfect Jungle Start Path Change HotKey", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_PATH_HOTKEY), KeyCode::J);
	//std::vector<std::string> topBottomDropdown{ "None", "Bottom", "Top" };
}

void ShacoMenuManager::ShacoComboSettings()
{
	EndeavourusMenu::EndeavourusHotkey("Combo Cycle Key", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CYCLE_COMBO_MODE_KEY), KeyCode::CapsLock);
	SdkUiText("The below setting enables casting E if the player has not");
	SdkUiText("had a target in AA range for > 2 Seconds.");
	SdkUiText("(Will not break stealth for it)");
	EndeavourusMenu::Checkbox("Enable E out of combo (Read above)", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_E_OUT_OF_COMBO), false);
	EndeavourusMenu::Tree("Shaco AP Combo Mode Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_AP_COMBO_MODE_GROUP), false, ShacoAPComboSettings);
}

void ShacoMenuManager::ShacoAPComboSettings()
{
	//EndeavourusMenu::Checkbox("Q towards selected enemy (If out of AA range)", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_Q_SELECTED_TARGET_ENABLED), true);
	//EndeavourusMenu::SliderFloat("Enemy must be within this distance to Q", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_Q_DISTANCE), 800, 0, 2000);

	std::vector<std::string> wMode{ "W ontop of Enemy", "Always W Behind" };
	EndeavourusMenu::DropList("W Mode", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_W_OPTIONS), wMode, 0);
	EndeavourusMenu::Checkbox("Only Cast W if target is not moving", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_W_BLOCK_CAST_MOVING_TARGET), true);

	EndeavourusMenu::Checkbox("Only Cast E if target is not in AA Range", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_E_AA_RANGE_CHECK), false);
}

void ShacoMenuManager::ShacoRSettings()
{
	EndeavourusMenu::Checkbox("Enable Clone Logic", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_LOGIN_ENABLED), true);
	EndeavourusMenu::EndeavourusHotkey("Clone Mode Change HotKey", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CYCLE_CLONSE_MODE_KEY), KeyCode::LShiftKey);
	//EndeavourusMenu::EndeavourusHotkey("Freeze Player Move/Attack HotKey", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_FREEZE_ATTACK_MOVE_KEY), KeyCode::LShiftKey);
}

void ShacoMenuManager::ShacoWBlockSettings()
{

}

void ShacoMenuManager::ShacoClearAndJungleSettings()
{
	SdkUiText("---------- E Settings ----------");
	SdkUiText("Will only lasthit minions with E");
	EndeavourusMenu::Checkbox("Cast E at Jungle Target", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_CAST_E_JUNGLE), true);
	EndeavourusMenu::Checkbox("Cast E at Minion Target", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_CAST_E_MINION), true);
	EndeavourusMenu::Checkbox("Block E Cast if target health < 25% unless killable (Jungle)", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_BLOCK_E_LOW_HEALTH), true);
	SdkUiText("---------- W Settings ----------");
	SdkUiText("(Wont auto cast at pesky Crab)");
	EndeavourusMenu::Checkbox("Cast W behind Jungle Target", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_CAST_W_JUNGLE), true);
	EndeavourusMenu::Checkbox("Cast W behind Minion Target", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_CAST_W_MINION), false);
	EndeavourusMenu::Checkbox("Block W Cast if target health < 25%", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_BLOCK_W_LOW_HEALTH), true);
}

void ShacoMenuManager::ShacoSpecificSpellLogic()
{
	EndeavourusMenu::Tree("Shaco Perfect Jungle Start Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_AUTO_JUNGLE_GROUP), false, ShacoAutoJungleSettings);
	EndeavourusMenu::Tree("Shaco Combo Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_COMBO_GROUP), false, ShacoComboSettings);
	//EndeavourusMenu::Tree("Shaco W Spell Block Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_W_SPELL_BLOCK_GROUP), false, ShacoWBlockSettings);
	EndeavourusMenu::Tree("Shaco Jungle/Clear Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_JUNGLE_AND_CLEAR_GROUP), false, ShacoClearAndJungleSettings);
	EndeavourusMenu::Tree("Shaco Clone Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_CLONE_GROUP), false, ShacoRSettings);
	EndeavourusMenu::Tree("Shaco Deceive Assistant Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_LOGIC_DECEIVE_GROUP), false, ShacoDeceiveAssistant);
	EndeavourusMenu::Tree("Shaco Spell Dodge Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_SPELL_DODGE_GROUP), false, ShacoSpellDodgeSettings);
	EndeavourusMenu::Tree("Shaco Misc Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_DECEIVE_SETTINGS_GROUP), false, ShacoDeceiveSettings);
}

void ShacoMenuManager::ShacoDeceiveSettings()
{
	EndeavourusMenu::Checkbox("Auto W Immobile", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_W_CAST_IMMOBILE), true);
	EndeavourusMenu::Checkbox("Block AA in Deceive unless Backstab", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_DECEIVE_BLOCK_AA_IN_DECEIVE_UNLESS_BACKSTAB), false);
}

void ShacoMenuManager::ShacoDeceiveAssistant()
{
	EndeavourusMenu::Checkbox("Enable Deceive Assistant", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_ASSISTANT), true);
	EndeavourusMenu::EndeavourusHotkey("Deceive Assistant Key", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_ASSISTANT_KEY), KeyCode::T);
}

void ShacoMenuManager::DrawQText()
{
	EndeavourusMenu::Checkbox("Enable Deceive Text Timer", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_Q_TIMER), true);
	SDKVECTOR defaultColour{ 224, 11, 191 };
	EndeavourusMenu::ColorPicker("Draw Deceive Text Colour", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_Q_TIMER_COLOUR), defaultColour);
	EndeavourusMenu::SliderFloat("Deceive Text X Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_Q_TIMER_X_OFFSET), 80, -10000, 10000);
	EndeavourusMenu::SliderFloat("Deceive Text Y Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_Q_TIMER_Y_OFFSET), 185, -10000, 10000);
}

void ShacoMenuManager::DrawCloneModeText()
{
	EndeavourusMenu::Checkbox("Enable Clone Mode Text", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_CLONE_MODE_TEXT), true);
	SDKVECTOR defaultColour{ 244, 206, 66 };
	EndeavourusMenu::ColorPicker("Draw Clone Mode Text Colour", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_MODE_TEXT_COLOUR), defaultColour);
	EndeavourusMenu::SliderFloat("Clone Mode Text X Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_MODE_TEXT_X_OFFSET), 75, -10000, 10000);
	EndeavourusMenu::SliderFloat("Clone Mode Text Y Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_MODE_TEXT_Y_OFFSET), 500, -10000, 10000);
}

void ShacoMenuManager::DrawComboModeText()
{
	EndeavourusMenu::Checkbox("Enable Combo Mode Text", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_COMBO_MODE_TEXT), true);
	SDKVECTOR defaultColour{ 244, 206, 66 };
	EndeavourusMenu::ColorPicker("Draw Combo Mode Text Colour", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_COMBO_MODE_TEXT_COLOUR), defaultColour);
	EndeavourusMenu::SliderFloat("Combo Mode Text X Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_COMBO_MODE_TEXT_X_OFFSET), 75, -10000, 10000);
	EndeavourusMenu::SliderFloat("Combo Mode Text Y Offset", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_COMBO_MODE_TEXT_Y_OFFSET), 300, -10000, 10000);
}


void ShacoMenuManager::DrawQTimer()
{
	EndeavourusMenu::Tree("Deceive Timer Draw Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_DECEIVE_TIMER_GROUP), false, DrawQText);
}

void ShacoMenuManager::DrawCloneText()
{
	EndeavourusMenu::Tree("Clone Mode Draw Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_MODE_TEXT_GROUP), false, DrawCloneModeText);
}

void ShacoMenuManager::DrawComboText()
{
	EndeavourusMenu::Tree("Combo Mode Draw Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_COMBO_MODE_TEXT_GROUP), false, DrawComboModeText);
}

void ShacoMenuManager::DeceiveSpots()
{
	EndeavourusMenu::Checkbox("Enable Deceive Position Drawings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_DRAWINGS), true);
	EndeavourusMenu::Checkbox("Enable Distance Limitation to draw spots", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_DRAWINGS_DISTANCE_LIMIT), false);
	EndeavourusMenu::SliderFloat("Distance to draw Deceive Locations", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_DRAWINGS_DISTANCE), 5000, 0, 10000);
	SDKVECTOR defaultColour{ 253, 159, 239 };
	EndeavourusMenu::ColorPicker("Draw Deceive Position Colour", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_DRAWINGS_COLOUR), defaultColour);
}

void ShacoMenuManager::DrawDeceiveSpots()
{
	EndeavourusMenu::Tree("Deceive Positions Draw Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_DECEIVE_POSITIONS_GROUP), false, DeceiveSpots);
}

void ShacoMenuManager::AntiBlitzcrank()
{
	EndeavourusMenu::Checkbox("Enable Q Anti-Blitzcrank", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ANTI_BLITZCRANK_ENABLED), true);
}

void ShacoMenuManager::DrawAntiBlitzcrank()
{
	EndeavourusMenu::Tree("Shaco Anti-Blitzcrank Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ANTI_BLITZCRANK_GROUP), false, AntiBlitzcrank);
}

void ShacoMenuManager::ShacoWSkillshot()
{
	SdkUiText("Calculates if you have time to cast R before");
	SdkUiText("getting hit with the skillshot");
	EndeavourusMenu::Checkbox("Enable W Skillshot Blocking", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_W_SPELL_BLOCKER), true);

	for (auto option : SKILLSHOT_MAP_W) {
		EndeavourusMenu::Checkbox(("Enable: " + option.second.displayName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(SHACO_SPELL_W_BLOCKER + "_" + option.second.displayName), true);
	}
}


void ShacoMenuManager::ShacoRSkillshot()
{
	SdkUiText("Calculates if you have time to cast R before");
	SdkUiText("getting hit with the skillshot");
	EndeavourusMenu::Checkbox("Enable R Skillshot Blocking", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_SKILLSHOT_BLOCKER), true);

	for (auto option : SKILLSHOT_MAP_SHACO) {
		EndeavourusMenu::Checkbox(("Enable: " + option.second.displayName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_SKILLSHOT + "_" + option.second.displayName), true);
	}
}

void ShacoMenuManager::ShacoRCircle()
{
	EndeavourusMenu::Checkbox("Enable R Circle Blocking", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_CIRCLE_BLOCKER), true);

	for (auto option : CIRCLE_MAP_SHACO) {
		EndeavourusMenu::Checkbox(("Enable: " + option.second.displayName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_CIRCLE + "_" + option.second.displayName), true);
	}
}

void ShacoMenuManager::ShacoRTargeted()
{
	EndeavourusMenu::Checkbox("Enable R Targeted Blocking", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_TARGETED), true);

	for (auto option : TARGETED_MAP_SHACO) {
		EndeavourusMenu::Checkbox(("Enable: " + option.second.displayName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_R_TARGETED_BLOCKER + "_" + option.second.displayName), true);
	}
}

void ShacoMenuManager::ShacoRDodgeSettings()
{
	EndeavourusMenu::Tree("Shaco R Dangerous Skillshot Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_R_SKILLSHOT_GROUP), false, ShacoRSkillshot);
	//EndeavourusMenu::Tree("Shaco R Dangerous Circle Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_R_CIRCLE_GROUP), false, ShacoRCircle);
	EndeavourusMenu::Tree("Shaco R Dangerous Targeted Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_R_TARGETED_GROUP), false, ShacoRTargeted);
	EndeavourusMenu::Tree("Shaco W Dangerous Skillshot Settings", EndeavourusMiscUtils::GetUniqueSettingName(SHACO_W_SPELL_DODGE_GROUP), false, ShacoWSkillshot);
}

void ShacoMenuManager::ShacoSpellDodgeSettings()
{
	ShacoRDodgeSettings();
	DrawAntiBlitzcrank();
}

void ShacoMenuManager::ShacoDrawingLogic()
{
	DrawQRange();
	DrawERange();
	DrawWRange();
	DrawSelectedTarget();
	DrawQTimer();
	DrawDeceiveSpots();
	DrawComboText();
	DrawCloneText();
}
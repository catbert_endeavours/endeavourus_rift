#pragma once
#include <string>
#include "SDK Extensions.h"
enum class SpellSlot;
enum class OrbwalkingMode;

class EndeavourusLogicUtils
{
private:
	static bool isQSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck);
	static bool isWSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck);
	static bool isESpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck);
	static bool isRSpellBlockedOnChampion(OrbwalkingMode orbwalkingMode, std::string enemyName, bool ksCheck);
public:
	static bool CanCastSpellOnChampion(OrbwalkingMode orbwalkingMode, SpellSlot spellSlot, std::string enemyName, bool ksCheck = false);
	static AIBaseClient* SELECTED_ENEMY;
	static void UpdateSelectedChamp();

};


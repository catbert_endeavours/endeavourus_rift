#include "stdafx.h"
#include "ItemLogicManager.h"
#include "SDK Extensions.h"
#include "EndeavourusUtils.h"
#include "ActivatorConstants.h"
#include <string>
#include "BaseSettings.h"
#include "ShacoLogicManager.h"
#include "ShacoCloneLogicManager.h"

float eonCooldown{ 0 };
float tiamatOffset{ 0 };

SpellSlot getItemSpellSlotId(int slot) {
	if (slot == 0) {
		return SpellSlot::Item1;
	}
	else if (slot == 1) {
		return SpellSlot::Item2;
	}
	else if (slot == 2) {
		return SpellSlot::Item3;
	}
	else if (slot == 3) {
		return SpellSlot::Item4;
	}
	else if (slot == 4) {
		return SpellSlot::Item5;
	}
	else {
		return SpellSlot::Item6;
	}
}

void ItemLogicManager::PerformItemLogic()
{
	if (EndeavourusMiscUtils::GetOrbwalkingMode() == OrbwalkingMode::Combo) {
		if (Player.IsValid() && Player.IsAlive() && !Player.IsZombie()) {
			std::map<int, SDK_ITEM> items = Player.GetItems();
			for (auto i : items) {
				if (static_cast<int>(ItemID::HextechProtobelt01) == i.first || static_cast<int>(ItemID::HextechGLP800) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(HextechProtobelt_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };

						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Skillshot s{ spellSlot, 800.0f, SkillshotType::Cone, 0.0f, 1600.0f, 0.0f, DamageType::Magical, true, (CollisionFlags::YasuoWall | CollisionFlags::BraumWall | CollisionFlags::Wall | CollisionFlags::Heroes | CollisionFlags::Minions) };

						if (s.IsReady()) {
							AIHeroClient* target{ GetValidTarget(800.0f) };
							if (target != nullptr) {
								if (EndeavourusMiscUtils::GetDropDownSetting(HEXTECH_PRO_GLP_MODE) == 0) {
									if (EndeavourusMiscUtils::GetFloatSetting(HEXTECH_PRO_GLP_HEALTH, 75.0f) > target->GetHealthPercent()) {
										s.Cast(target);
									}
								}
								else {
									s.Cast(target);
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::HextechGunblade) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(HextechGunblade_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };

						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Targeted s{ spellSlot, itemSpell.CastRange };

						if (s.IsReady()) {
							AIHeroClient* target{ GetValidTarget(itemSpell.CastRange) };
							if (target != nullptr) {
								if (EndeavourusMiscUtils::GetDropDownSetting(HextechGunblade_MODE) == 0) {
									if (EndeavourusMiscUtils::GetFloatSetting(HextechGunblade_HEALTH, 75.0f) > target->GetHealthPercent()) {
										s.Cast(target);
									}
								}
								else {
									s.Cast(target);
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::Spellbinder) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(Spellbinder_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };

						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(Spellbinder_ENEMY_RANGE, 750.0f)) };
						if (target != nullptr && s.IsReady()) {
							if (EndeavourusMiscUtils::GetDropDownSetting(Spellbinder_MODE) == 0) {
								if (EndeavourusMiscUtils::GetFloatSetting(Spellbinder_HEALTH, 75.0f) > target->GetHealthPercent()) {
									s.Cast(target);
								}
							}
							else {
								s.Cast(target);
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::TwinShadows) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(TwinShadows_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						AIHeroClient* target{ ItemLogicManager::GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(TwinShadows_CLOSEST_TARGET, 2500.0f)) };

						if (target == nullptr && s.IsReady()) {
							s.Cast();
						}
					}
				}
				else if (static_cast<int>(ItemID::EdgeofNight) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(EDGE_OF_NIGHT_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						AIHeroClient* target{ ItemLogicManager::GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(EDGE_OF_NIGHT_CLOSEST_TARGET, 1000.0f)) };

						if (target == nullptr && s.IsReady() && Game::Time() > eonCooldown) {
							if (s.Cast()) {
								eonCooldown = Game::Time() + 1.0f;
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::BilgewaterCutlass) == i.first || static_cast<int>(ItemID::BladeoftheRuinedKing) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(BilgewaterCutlassBOTRK_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };

						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Targeted s{ spellSlot, itemSpell.CastRange };

						if (s.IsReady()) {
							AIHeroClient* target{ GetValidTarget(itemSpell.CastRange) };
							if (target != nullptr) {
								if (EndeavourusMiscUtils::GetDropDownSetting(BilgewaterCutlassBOTRKMODE) == 0) {
									if (EndeavourusMiscUtils::GetFloatSetting(BilgewaterCutlassBOTRKHEALTH, 75.0f) > target->GetHealthPercent()) {
										s.Cast(target);
									}
								}
								else {
									s.Cast(target);
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::YoumuusGhostblade) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(YoumuusGhostbladeENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };
						AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(YoumuusGhostbladeECLOSEST_TARGET, 700.0f)) };

						if (target != nullptr && s.IsReady()) {
							s.Cast();
						}
					}
				}
				else if (static_cast<int>(ItemID::SeraphsEmbrace) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(ArchangelsStaffENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(ArchangelsStaff_ENEMY_DISTANCE, 700.0f)) };

						if (target != nullptr && s.IsReady()) {
							if (EndeavourusMiscUtils::GetFloatSetting(ArchangelsStaff_PERCENT, 25.0f) > Player.GetHealthPercent()) {
								if (EndeavourusMiscUtils::GetBoolSetting(ArchangelsStaff_ICOMING_DAMAGE_ENABLED, true)) {
									float healthPred = pSDK->HealthPred->GetHealthPrediction(Player.AsAIBaseClient(), 2000);
									if (Player.GetHealth().Current - healthPred > 1) {
										s.Cast();
									}
								}
								else {
									s.Cast();
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::ZhonyasHourglass) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(ZhonyasHourglassENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };
						AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(ZhonyasHourglass_ENEMY_DISTANCE, 700.0f)) };

						if (target != nullptr && s.IsReady()) {
							if (EndeavourusMiscUtils::GetFloatSetting(ZhonyasHourglassPERCENT, 25.0f) > Player.GetHealthPercent()) {
								if (EndeavourusMiscUtils::GetBoolSetting(ZhonyasHourglass_ICOMING_DAMAGE_ENABLED, true)) {
									float healthPred = pSDK->HealthPred->GetHealthPrediction(Player.AsAIBaseClient(), 2000);
									if (Player.GetHealth().Current - healthPred > 1) {
										s.Cast();
									}
								}
								else {
									s.Cast();
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::ShurelyasReverie) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(ShurelyasReverieENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						if (s.IsReady()) {
							int numOfEnemys{ 0 };
							for (auto e : pSDK->EntityManager->GetEnemyHeroes(EndeavourusMiscUtils::GetFloatSetting(ShurelyasReverie_ENEMIES_DISTANCE, 1000.0f))) {
								if (e.second->IsValid() && e.second->IsAlive() && e.second->IsVisible() && !e.second->IsZombie()) {
									numOfEnemys++;
								}
							}

							if (numOfEnemys >= EndeavourusMiscUtils::GetIntSetting(ShurelyasReverie_MIN_ENEMIES, 1)) {
								s.Cast();
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::QuicksilverSash) == i.first || static_cast<int>(ItemID::MercurialScimitar) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };
						if (s.IsReady()) {
							if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_CHARM, true) && Player.HasBuffType(BUFF_TYPE_CHARM)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_BLIND, true) && Player.HasBuffType(BUFF_TYPE_BLIND)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_DISARM, true) && Player.HasBuffType(BUFF_TYPE_DISARM)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_FEAR, true) && Player.HasBuffType(BUFF_TYPE_FEAR)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_FLEE, true) && Player.HasBuffType(BUFF_TYPE_FLEE)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_POLY, true) && Player.HasBuffType(BUFF_TYPE_POLYMORPH)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_SNARE, true) && Player.HasBuffType(BUFF_TYPE_SNARE)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_TAUNT, true) && Player.HasBuffType(BUFF_TYPE_TAUNT)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_EXHAUST, true) && Player.HasBuff("SummonerExhaust")) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_STUN, true) && Player.HasBuffType(BUFF_TYPE_STUN)) {
								s.Cast();
							}
							else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_SUPRESSION, true) && Player.HasBuffType(BUFF_TYPE_SUPPRESSION)) {
								s.Cast();
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::MikaelsCrucible) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Targeted s{ spellSlot, itemSpell.CastRange };

						if (s.IsReady()) {
							AIHeroClient* ally{ GetValidAlly(MikaelsCrucibleBLACKLIST_ENABLED, s.Range) };

							if (ally != nullptr) {
								s.Cast(ally);
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::LocketoftheIronSolari) == i.first) {
					SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
					Spell::Active s{ spellSlot };

					if (s.IsReady()) {
						if (EndeavourusMiscUtils::GetBoolSetting(LocketoftheIronSolari_ENABLED, true)) {
							AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(LocketoftheIronSolari_ENEMIES_WITHIN_RANGE, 750.0f)) };
							if (target != nullptr) {
								for (auto a : pSDK->EntityManager->GetAllyHeroes(s.Range)) {
									if (a.second->IsValid() && a.second->IsAlive() && !a.second->IsZombie() && a.second->IsVisible()) {
										if (EndeavourusMiscUtils::GetFloatSetting(LocketoftheIronSolari_ALLY_HEALTH, 25.0f) > a.second->GetHealthPercent()) {
											if (EndeavourusMiscUtils::GetBoolSetting(LocketoftheIronSolari_ICOMING_DAMAGE_ENABLED, true)) {
												float healthPred = pSDK->HealthPred->GetHealthPrediction(a.second->AsAIBaseClient(), 2000);
												if (a.second->GetHealth().Current - healthPred > 1) {
													s.Cast();
													break;
												}
											}
											else {
												s.Cast();
												break;
											}
										}
									}
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::GargoyleStoneplate) == i.first) {
					SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
					Spell::Active s{ spellSlot };

					if (s.IsReady()) {
						if (EndeavourusMiscUtils::GetBoolSetting(GargoyleStoneplateeENABLED, true)) {
							AIHeroClient* target{ GetValidTarget(EndeavourusMiscUtils::GetFloatSetting(GargoyleStoneplatee_ENEMIES_WITHIN_RANGE, 750.0f)) };

							if (target != nullptr) {
								if (EndeavourusMiscUtils::GetFloatSetting(GargoyleStoneplatee_HEALTH, 25.0f) > Player.GetHealthPercent()) {
									if (EndeavourusMiscUtils::GetBoolSetting(GargoyleStoneplatee_ICOMING_DAMAGE_ENABLED, true)) {
										float healthPred = pSDK->HealthPred->GetHealthPrediction(Player.AsAIBaseClient(), 2000);
										if (Player.GetHealth().Current - healthPred > 1) {
											s.Cast();
										}
									}
									else {
										s.Cast();
									}
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::Redemption) == i.first) {
					SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
					SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
					Spell::Skillshot s{ spellSlot, 5500.0f, SkillshotType::Circle, itemSpell.CastDelay, HUGE_VALF, itemSpell.PrimaryCastRadius, DamageType::Magical, true, CollisionFlags::Nothing };

					if (s.IsReady()) {
						if (EndeavourusMiscUtils::GetBoolSetting(REDEMTION_ALLY_ENABLED, true)) {
							bool castedRedemption{ false };

							for (auto a : pSDK->EntityManager->GetAllyHeroes(5500.0f)) {
								if (a.second->IsValid() && a.second->IsAlive() && !a.second->IsZombie() && a.second->IsVisible()) {
									if (EndeavourusMiscUtils::GetFloatSetting(REDEMTION_ALLY_PERCENT, 25.0f) > a.second->GetHealthPercent()) {
										for (auto e : pSDK->EntityManager->GetEnemyHeroes(EndeavourusMiscUtils::GetFloatSetting(REDEMTION_ENEMY_RANGE, 750.0f), &a.second->GetPosition())) {

											if (e.second->IsValid() && e.second->IsAlive() && !e.second->IsZombie() && e.second->IsVisible()) {
												if (EndeavourusMiscUtils::GetBoolSetting(REDEMTION_ICOMING_DAMAGE_ENABLED, true)) {
													float healthPred = pSDK->HealthPred->GetHealthPrediction(a.second->AsAIBaseClient(), 2000);
													if (a.second->GetHealth().Current - healthPred > 1) {
														s.Cast(a.second);
													}
												}
												else {
													s.Cast(a.second);
													castedRedemption = true;
													break;
												}
											}
										}

										if (castedRedemption) {
											break;
										}
									}
								}
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::Ohmwrecker) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_ENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Targeted s{ spellSlot, itemSpell.CastRange };
						for (auto t : pSDK->EntityManager->GetEnemyMissiles(s.Range)) {
							AttackableUnit* target = t.second->GetTarget();
							if (target != nullptr && s.IsReady() && target->IsAlly() && target->IsValid() && target->IsHero() && target->IsAlive() && !target->IsZombie()) {
								s.Cast(target->AsAIHeroClient());
							}
						}
					}
				}
				else if (static_cast<int>(ItemID::RighteousGlory) == i.first) {
					if (EndeavourusMiscUtils::GetBoolSetting(RighteousGloryENABLED, true)) {
						SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
						SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
						Spell::Active s{ spellSlot };

						if (s.IsReady()) {
							int numOfEnemys{ 0 };
							for (auto e : pSDK->EntityManager->GetEnemyHeroes(EndeavourusMiscUtils::GetFloatSetting(RighteousGlory_ENEMIES_DISTANCE, 1000.0f))) {
								if (e.second->IsValid() && e.second->IsAlive() && e.second->IsVisible() && !e.second->IsZombie()) {
									numOfEnemys++;
								}
							}

							if (numOfEnemys >= EndeavourusMiscUtils::GetIntSetting(RighteousGlory_MIN_ENEMIES, 1)) {
								s.Cast();
							}
						}
					}
				}
			}
		}
	}
}

AIHeroClient* ItemLogicManager::GetValidTarget(float range) {
	AIHeroClient* target = pCore->TS->GetTarget(range);

	if (EndeavourusMiscUtils::IsValidToCastSpell(target)) {
		return target;
	}
	else {
		std::map<unsigned int, AIHeroClient*> targets = pSDK->EntityManager->GetEnemyHeroes(range);

		for (auto t : targets) {
			if (EndeavourusMiscUtils::IsValidToCastSpell(t.second)) {
				return t.second;
			}
		}
	}

	return nullptr;
}

AIHeroClient* ItemLogicManager::GetValidAlly(std::string itemName, float range) {
	std::map<unsigned int, AIHeroClient*> allys = pSDK->EntityManager->GetAllyHeroes(range);

	for (auto a : allys) {
		if (EndeavourusMiscUtils::IsValidToCastSpell(a.second)) {
			if (!ItemLogicManager::IsItemBlockedOnChampion(itemName, a.second->GetCharName())) {
				if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_CHARM, true) && a.second->HasBuffType(BUFF_TYPE_CHARM)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_BLIND, true) && a.second->HasBuffType(BUFF_TYPE_BLIND)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_DISARM, true) && a.second->HasBuffType(BUFF_TYPE_DISARM)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_FEAR, true) && a.second->HasBuffType(BUFF_TYPE_FEAR)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_FLEE, true) && a.second->HasBuffType(BUFF_TYPE_FLEE)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_POLY, true) && a.second->HasBuffType(BUFF_TYPE_POLYMORPH)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_SNARE, true) && a.second->HasBuffType(BUFF_TYPE_SNARE)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_TAUNT, true) && a.second->HasBuffType(BUFF_TYPE_TAUNT)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_EXHAUST, true) && a.second->HasBuff("SummonerExhaust")) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_STUN, true) && a.second->HasBuffType(BUFF_TYPE_STUN)) {
					return a.second;
				}
				else if (EndeavourusMiscUtils::GetBoolSetting(QuicksilverSashMercurialScimitarBUFF_TYPE_SUPRESSION, true) && a.second->HasBuffType(BUFF_TYPE_SUPPRESSION)) {
					return a.second;
				}
			}
		}
	}

	return nullptr;
}

bool ItemLogicManager::IsItemBlockedOnChampion(std::string itemName, std::string enemyName)
{
	return EndeavourusMiscUtils::GetBoolSetting(itemName + "_" + enemyName, false);
}

bool ItemLogicManager::ShouldCastShacoWBeforeAA{ false };
bool ItemLogicManager::ShouldCastShacoEBeforeAA{ false };
bool ItemLogicManager::ShouldCastShacoTiamatBeforeAA{ false };

Spell::Active ItemLogicManager::TIAMAT_SPELL{ SpellSlot::Item1 };

float ItemLogicManager::SHACO_COMBO_OFFSET{ 0.0f };


void ItemLogicManager::PerformTiamatReset() {
	const std::string charName = Player.GetCharName();

	if (charName == "Shaco") {
		OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();

		if (EndeavourusMiscUtils::GetBoolSetting(ACTIVATOR_ENABLED, true)) {

			if (orbwalkingMode == OrbwalkingMode::Combo || BaseSettings::isComboKeyEnabled()) {

				std::map<int, SDK_ITEM> items = Player.GetItems();
				if (EndeavourusMiscUtils::GetBoolSetting(TiamatENABLED_COMBO, true)) {
					for (auto i : items) {
						if (static_cast<int>(ItemID::Tiamat) == i.first || static_cast<int>(ItemID::TitanicHydra) == i.first || static_cast<int>(ItemID::RavenousHydra) == i.first) {
							SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
							SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
							Spell::Active s{ spellSlot };

							if (s.IsReady()) {
								ShouldCastShacoTiamatBeforeAA = true;
								SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
								TIAMAT_SPELL = s;
							}
						}
					}
				}
			}
			else if (orbwalkingMode == OrbwalkingMode::LaneClear || BaseSettings::isClearKeyEnabled()) {
				std::map<int, SDK_ITEM> items = Player.GetItems();

				if (EndeavourusMiscUtils::GetBoolSetting(TiamatENABLED_CLEAR, true)) {
					for (auto i : items) {
						if (static_cast<int>(ItemID::Tiamat) == i.first || static_cast<int>(ItemID::TitanicHydra) == i.first || static_cast<int>(ItemID::RavenousHydra) == i.first) {
							SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
							SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
							Spell::Active s{ spellSlot };

							if (s.IsReady() && Game::Time() > tiamatOffset) {
								if (s.Cast()) {
									tiamatOffset = Game::Time() + 0.25f;
								}
							}
						}
					}
				}
			}
		}

		if (orbwalkingMode == OrbwalkingMode::Combo) {

			if (BaseSettings::isChampEnabled()) {

				if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 0) {
					SDK_SPELL e = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
					Spell::Targeted ECast = { SpellSlot::E, e.CastRange };

					OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
					if (ShacoLogicManager::CanCastE(orbwalkingMode)) {
						ShouldCastShacoEBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
				}
				else if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 1) {
					SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
					Spell::Targeted WCast = { SpellSlot::W, w.CastRange };

					SDK_SPELL e = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
					Spell::Targeted ECast = { SpellSlot::E, e.CastRange };

					OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
					if (ShacoLogicManager::CanCastW(orbwalkingMode)) {
						ShouldCastShacoWBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}

					if (ShacoLogicManager::CanCastE(orbwalkingMode)) {
						ShouldCastShacoEBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
				}
				else if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 2) {
					SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
					Spell::Targeted RCast = { SpellSlot::R, R.CastRange };

					SDK_SPELL e = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
					Spell::Targeted ECast = { SpellSlot::E, e.CastRange };

					/**
					if (RCast.IsReady()) {
						ShouldCastShacoRBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
					*/

					OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
					if (ShacoLogicManager::CanCastE(orbwalkingMode)) {
						ShouldCastShacoEBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
				}
				else if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 3) {
					SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
					Spell::Targeted RCast = { SpellSlot::R, R.CastRange };

					SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
					Spell::Targeted WCast = { SpellSlot::W, w.CastRange };

					SDK_SPELL e = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
					Spell::Targeted ECast = { SpellSlot::E, e.CastRange };
					/*
					if (RCast.IsReady()) {
						ShouldCastShacoRBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
					*/

					OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
					if (ShacoLogicManager::CanCastW(orbwalkingMode)) {
						ShouldCastShacoWBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}

					if (ShacoLogicManager::CanCastE(orbwalkingMode)) {
						ShouldCastShacoEBeforeAA = true;
						SHACO_COMBO_OFFSET = Game::Time() + 1.5f;
					}
				}
			}
		}
	}
	else if (EndeavourusMiscUtils::GetBoolSetting(TiamatENABLED, true)) {
		std::map<int, SDK_ITEM> items = Player.GetItems();
		for (auto i : items) {
			if (static_cast<int>(ItemID::Tiamat) == i.first || static_cast<int>(ItemID::TitanicHydra) == i.first || static_cast<int>(ItemID::RavenousHydra) == i.first) {
				SpellSlot spellSlot{ getItemSpellSlotId(i.second.Slot) };
				SDK_SPELL itemSpell = Player.GetSpell(static_cast<unsigned char>(spellSlot));
				Spell::Active s{ spellSlot };

				if (s.IsReady() && Game::Time() > tiamatOffset) {
					if (BaseSettings::isComboKeyEnabled() && EndeavourusMiscUtils::GetBoolSetting(TiamatENABLED_COMBO, true)) {
						if (s.Cast()) {
							tiamatOffset = Game::Time() + 0.25f;
						}
					}
					else if (BaseSettings::isClearKeyEnabled() && EndeavourusMiscUtils::GetBoolSetting(TiamatENABLED_CLEAR, true)) {
						if (s.Cast()) {
							tiamatOffset = Game::Time() + 0.25f;
						}
					}
				}
			}
		}
	}
}

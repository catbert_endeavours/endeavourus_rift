#include "stdafx.h"
#include "Resource.h"
#include "LogicConstants.h"

Resource::Resource(bool resourceEnabled, bool qEnabled, bool qDefaultVal, bool qBlockerEnabled, bool qPredictionEnabled, EndeavourusHitChance qPredictionMode, bool wEnabled, bool wDefaultVal,
	bool wBlockerEnabled, bool wPredictionEnabled, EndeavourusHitChance wPredictionMode, bool eEnabled, bool eDefaultVal, bool eBlockerEnabled, bool ePredictionEnabled, EndeavourusHitChance ePredictionMode,
	bool rEnabled, bool rDefaultVal, bool rBlockerEnabled, bool rPredictionEnabled, EndeavourusHitChance rPredictionMode, int defaultQVal, int defaultWVal, int defaultEVal, int defaultRVal, ResourceType resourceType)
{
	this->resourceEnabled = resourceEnabled;
	this->qEnabled = qEnabled;
	this->qDefaultVal = qDefaultVal;
	this->qBlockerEnabled = qBlockerEnabled;
	this->qPredictionEnabled = qPredictionEnabled;
	this->qPredictionMode = qPredictionMode;
	this->wEnabled = wEnabled;
	this->wDefaultVal = wDefaultVal;
	this->wBlockerEnabled = wBlockerEnabled;
	this->wPredictionEnabled = wPredictionEnabled;
	this->wPredictionMode = wPredictionMode;
	this->eEnabled = eEnabled;
	this->eDefaultVal = eDefaultVal;
	this->eBlockerEnabled = eBlockerEnabled;
	this->ePredictionEnabled = ePredictionEnabled;
	this->ePredictionMode = ePredictionMode;
	this->rEnabled = rEnabled;
	this->rDefaultVal = rDefaultVal;
	this->rBlockerEnabled = rBlockerEnabled;
	this->rPredictionEnabled = rPredictionEnabled;
	this->rPredictionMode = rPredictionMode;
	this->resourceType = resourceType;
	this->defaultQMinResource = defaultQVal;
	this->defaultWMinResource = defaultWVal;
	this->defaultEMinResource = defaultEVal;
	this->defaultRMinResource = defaultRVal;
}

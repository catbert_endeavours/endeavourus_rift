#pragma once
#include "ChampionBase.h"

class UnsupportedChampion : public ChampionBase
{
public:
	static void Init();
	static void OnDraw(void * UserData);
	static void OnUpdate(void * UserData);
	static void OnDrawMenu(void * UserData);
};
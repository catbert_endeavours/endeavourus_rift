#pragma once
#include "SDK Extensions.h"
#include "TeemoLogicManager.h"
#include "ChampionBase.h"

class EndeavourusTeemo : public ChampionBase
{
private:
	TeemoLogicManager TeemoLogicManager {};

public:
	static void Init();

	static void	__cdecl	OnUpdate(_In_ void* UserData);
	static void	__cdecl	OnDraw(_In_ void* UserData);
	static void __cdecl OnDrawMenu(_In_ void* UserData);
	static void	__cdecl OnPostAttack(_In_ void* UserData);
};


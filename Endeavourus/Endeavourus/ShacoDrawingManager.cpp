#include "stdafx.h"
#include "ShacoDrawingManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "ShacoConstants.h"
#include "ShacoAutoJungleLogicManager.h"
#include "ShacoMenuManager.h"
#include <math.h>
#include <sstream>
#include "EndeavourusMenu.h"
#include "BaseSettings.h"
#include "ShacoDeceiveLocations.h"
#include "ShacoLogicManager.h"
#include "ShacoCloneLogicManager.h"

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6)
{
	std::ostringstream out;
	out.precision(n);
	out << std::fixed << a_value;
	return out.str();
}

void ShacoDrawingManager::Draw()
{
	if (BaseSettings::isChampEnabled()) {
		DrawSpellRanges();
		DrawSelectedTarget();
		DrawAutoJungleStatus();
		DrawDeceivePositions();
		DrawComboMode();

		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_Q_TIMER, true)) {
			if (ShacoLogicManager::DECEIVE_TIMER > 0) {

				SDKVECTOR playerPosition{ Player.GetPosition() };

				playerPosition.y -= EndeavourusMiscUtils::GetFloatSetting(SHACO_Q_TIMER_Y_OFFSET, 185);
				playerPosition.x -= EndeavourusMiscUtils::GetFloatSetting(SHACO_Q_TIMER_X_OFFSET, 80);

				Draw::Text(&playerPosition, nullptr, "Deceive Time: " + to_string_with_precision(ShacoLogicManager::DECEIVE_TIMER, 2), "Arial", &getDeceiveDrawingColour(), 24, 8, 2);
			}
		}

		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CLONE_MODE_TEXT, true)) {
			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Active RCast = { SpellSlot::R };

			SDKVECTOR playerPosition{ Player.GetPosition() };

			playerPosition.x -= EndeavourusMiscUtils::GetFloatSetting(SHACO_CLONE_MODE_TEXT_X_OFFSET, 75);
			playerPosition.y -= EndeavourusMiscUtils::GetFloatSetting(SHACO_CLONE_MODE_TEXT_Y_OFFSET, 500);

			if (RCast.IsLearned()) {
				Draw::Text(&playerPosition, nullptr, "Clone Mode: " + ShacoCloneLogicManager::SHACO_CLONE_OPTIONS[ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX], "Arial", &getCloneModeTextColour(), 24, 8, 2);
			}
		}

		/**
		SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
		Spell::Targeted t = { SpellSlot::W, w.CastRange };

		for (auto e : pSDK->EntityManager->GetEnemyHeroes(t.Range)) {
			Vector3 pos { ShacoLogicManager::GetWPosition(e.second) };

			Draw::Text(&pos, nullptr, "Wont Work", "Arial", &Color::Red, 24, 8, 2);
		}
		*/
	}
}

void ShacoDrawingManager::DrawComboMode() {
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_COMBO_MODE_TEXT, true)) {
		SDKVECTOR playerPosition{ Player.GetPosition() };

		playerPosition.x -= EndeavourusMiscUtils::GetFloatSetting(SHACO_COMBO_MODE_TEXT_X_OFFSET, 75);
		playerPosition.y -= EndeavourusMiscUtils::GetFloatSetting(SHACO_COMBO_MODE_TEXT_Y_OFFSET, 300);

		Draw::Text(&playerPosition, nullptr, "Combo Mode: " + ShacoCloneLogicManager::SHACO_COMBO_OPTIONS[ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX], "Arial", &getComboModeTextColour(), 24, 8, 2);
	}
}

void ShacoDrawingManager::DrawAutoJungleStatus()
{
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_AUTO_JUNGLE, false)) {
		if (ShacoAutoJungleLogicManager::IsEnabled() && Game::Time() <= 90) {
			Resolution res{ Renderer::GetResolution() };
			int middle{ res.Width / 2 - 500 };
			Vector2 screenMiddle{ static_cast<float>(middle), 0.0f };
			PSDKPOINT point{ &screenMiddle };

			int setting = ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX;
			std::string path{};
			if (setting == 0) {
				path = "Bottom";
			}
			else if (setting == 1) {
				path = "Top";
			}
			else if (setting == 2) {
				path = "None";
			}

			Draw::Text(nullptr, point, "Perfect Jungle Start is Enabled, with selected path: " + path, "Arial", &Color::Green, 36, 14, 4);
		}
	}
}

void ShacoDrawingManager::DrawDeceivePositions()
{
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_DECEIVE_DRAWINGS, true)) {
		if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_DECEIVE_DRAWINGS_DISTANCE_LIMIT, false)) {
			float distance{ EndeavourusMiscUtils::GetFloatSetting(SHACO_ENABLE_DECEIVE_DRAWINGS_DISTANCE, 5000.0f) };

			for (auto l : DECEIVE_LOCATIONS) {
				if (Player.Distance(l.first) <= distance) {
					Draw::Circle(&l.first, 100, &getDeceiveLocationDrawingColour());
				}
			}
		}
		else {
			for (auto l : DECEIVE_LOCATIONS) {
				Draw::Circle(&l.first, 100, &getDeceiveLocationDrawingColour());
			}
		}
	}
}

SDKCOLOR ShacoDrawingManager::getDeceiveDrawingColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_Q_TIMER_COLOUR));
}

SDKCOLOR ShacoDrawingManager::getCloneModeTextColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_CLONE_MODE_TEXT_COLOUR));
}

SDKCOLOR ShacoDrawingManager::getComboModeTextColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_COMBO_MODE_TEXT_COLOUR));
}

SDKCOLOR ShacoDrawingManager::getDeceiveLocationDrawingColour()
{
	return EndeavourusMenu::Get<SDKCOLOR>(EndeavourusMiscUtils::GetUniqueSettingName(SHACO_ENABLE_DECEIVE_DRAWINGS_COLOUR));
}
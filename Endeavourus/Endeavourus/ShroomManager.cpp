#include "stdafx.h"
#include "ShroomManager.h"
#include "TeemoLogicManager.h"
#include "TeemoSettings.h"

void ShroomManager::SnapRToClosestShroomLoc()
{
	if (TeemoSettings::isSnapRKeyPressed())
	{
		if (Player.IsAlive() && Player.IsValid() && TeemoLogicManager::RCast.IsReady() && Game::Time() > TeemoLogicManager::R_COOLDOWN) {
			Vector3 mousePos = Renderer::MousePos();

			Vector3 closestMousePos{};
			float closestDistance{ FLT_MAX };
			for (auto s : SHROOM_LOCATIONS)
			{
				bool alreadyPlaced = false;
				for (auto allyWardMap : pSDK->EntityManager->GetAllyWards())
				{
					if (allyWardMap.second != nullptr) {
						if (strncmp(allyWardMap.second->GetName(), TEEMO_TRAP.c_str(), 13) == 0)
						{
							float objDistance = allyWardMap.second->Distance(s);
							if (objDistance < TEEMO_R_PLACEMENT_POS_OFFSET)
							{
								alreadyPlaced = true;
								break;
							}
						}
					}
				}

				if (!alreadyPlaced) {
					float distanceFromMouse = mousePos.Distance(s);
					if (distanceFromMouse < closestDistance)
					{
						closestDistance = distanceFromMouse;
						closestMousePos = s;
					}
				}
			}

			if (closestDistance < 300) {
				if (TeemoLogicManager::RCast.Cast(&closestMousePos))
				{
					TeemoLogicManager::R_COOLDOWN = Game::Time() + TEEMO_R_PLACEMENT_AUTO_SHROOM_TIME_OFFSET;
				}
			}
		}
	}
}

/*
 * Iterates through the optimised shroom locations to find an empty one before attempting
 * to place down a shroom. If it finds a free space it will attempt to plant the shroom
 * but it will add an offset to the cast inorder to make sure the mushroom has been placed
 * at that location before trying to plant another. This is currently down to not being able
 * to track the position when the shroom is moving so I am unable to get the endPos until the
 * shroom has landed. Without this I will currently get multiple shrooms firing until it hits
 * the specified location.
*/
void ShroomManager::PerformShroomAutomation()
{
	
	/**
	 * Catbert - Deprecated due to auto shrooming around map sounds great - but isnt. Changed to snap functionality instead
	 */

	/*
	if (TeemoSettings::isAutoRShroomPlacement()) {
		for (auto s : SHROOM_LOCATIONS)
		{
			if (Player.IsAlive() && Player.IsValid() && TeemoLogicManager::RCast.IsReady() && Game::Time() > TeemoLogicManager::R_COOLDOWN) {
				if (Player.Distance(&s) <= TeemoLogicManager::RCast.Range) {
					bool alreadyPlaced = false;
					for (auto allyWardMap : pSDK->EntityManager->GetAllyWards())
					{
						if (strncmp(allyWardMap.second->GetName(), TEEMO_TRAP.c_str(), 13) == 0)
						{
							float objDistance = allyWardMap.second->GetPosition().Distance(s);
							if (objDistance < TEEMO_R_PLACEMENT_POS_OFFSET)
							{
								alreadyPlaced = true;
								break;
							}
						}
					}

					if (!alreadyPlaced)
					{
						if (TeemoLogicManager::RCast.Cast(&s))
						{
							TeemoLogicManager::R_COOLDOWN = Game::Time() + TEEMO_R_PLACEMENT_AUTO_SHROOM_TIME_OFFSET;
							break;
						}
					}
				}
			}
		}
	}
	*/
}





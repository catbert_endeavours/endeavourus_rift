#pragma once
#include "SDK Extensions.h"
class ShacoDeceiveBackStabLogicManager
{
public:
	static void Init();
	static void __cdecl OnPreAttack(bool * Process, AttackableUnit ** Target);
	static AttackableUnit* PLAYER_TARGET;
	static AIHeroClient* PLAYER_HERO_TARGET;

	static float TARGET_LAST_UPDATE;
};


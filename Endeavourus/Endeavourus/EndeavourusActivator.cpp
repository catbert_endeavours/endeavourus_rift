#include "stdafx.h"
#include "EndeavourusActivator.h"
#include "SDK Extensions.h"
#include "ItemLogicManager.h"
#include "SummonerLogicManager.h"
#include "ActivatorMenuManager.h"
#include "EndeavourusMiscUtils.h"
#include "ActivatorConstants.h"

std::map<std::string, float> ALLY_INCOMING_DAMAGE{ };

void EndeavourusActivator::Init()
{
	ActivatorMenuManager::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::PostAttack, OnPostAttack);

}

void EndeavourusActivator::OnUpdate(_In_ void* UserData) {
	if (EndeavourusMiscUtils::GetBoolSetting(ACTIVATOR_ENABLED, true)) {
		ItemLogicManager::PerformItemLogic();
		SummonerLogicManager::PerformSummoners();
	}
}

void EndeavourusActivator::OnPostAttack(_In_ void* UserData) {
	const std::string charName = Player.GetCharName();

	if (EndeavourusMiscUtils::GetBoolSetting(ACTIVATOR_ENABLED, true) || charName == "Shaco") {
		ItemLogicManager::PerformTiamatReset();
	}
}


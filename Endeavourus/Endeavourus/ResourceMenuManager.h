#pragma once
#include "Resource.h"

class ResourceMenuManager
{
private:
	static void BlacklistSettings();
public:
	static Resource comboResourceSettings;
	static Resource harassResourceSettings;
	static Resource clearResourceSettings;
	static Resource lastHitResourceSettings;
	static Resource escapeResourceSettings;
	static Resource ksResourceSettings;

	static void DrawAllEnabledLogicModeSettings();
	static void DrawLogicSettings();

	static void DrawComboLogicSettings();
	static void DrawHarassLogicSettings();
	static void DrawClearLogicSettings();
	static void DrawLastHitLogicSettings();
	static void DrawEscapeLogicSettings();
	static void DrawKsLogicSettings();

	static void DrawComboSettings();
	static void DrawHarassSettings();
	static void DrawClearSettings();
	static void DrawLastHitSettings();
	static void DrawEscapeSettings();
	static void DrawKsSettings();
};
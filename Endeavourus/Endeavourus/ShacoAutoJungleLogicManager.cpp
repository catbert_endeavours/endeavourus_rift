#include "stdafx.h"
#include "ShacoAutoJungleLogicManager.h"
#include "SDK Extensions.h"
#include "ShacoConstants.h"
#include "EndeavourusMiscUtils.h"
#include "ShacoMenuManager.h"

bool shacoIsSummonersRift{ false };
bool shacoAutoJungleBlueMap{ false };

float shacoWCastOffset{ 0.50f };
float shacoWCooldown{ 0 };

bool shacoSetupComplete{ false };
bool shacoBox1Planted{ false };
bool shacoBox2Planted{ false };
bool shacoBox3Planted{ false };
bool shacoBox4Planted{ false };

void ShacoAutoJungleLogicManager::Init()
{
	shacoAutoJungleBlueMap = EndeavourusMiscUtils::IsBlueTeam();
	shacoIsSummonersRift = EndeavourusMiscUtils::IsSummonersRift();
}

void ShacoAutoJungleLogicManager::PerformLogic() {
	if (shacoIsSummonersRift && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_AUTO_JUNGLE, false) && Game::Time() < 90 && EndeavourusMiscUtils::PlayerHasSmite()) {
		if (Game::Time() <= 40 || Game::Time() > 40 && shacoBox1Planted) {
			if (shacoAutoJungleBlueMap) {
				PerformBlueSideSetup();
			}
			else {
				PerformRedSideSetup();
			}
		}
	}
}


void ShacoAutoJungleLogicManager::PerformBlueSideSetup()
{
	if (ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX == 0) {
		if (!shacoSetupComplete) {
			SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
			Spell::Targeted t = { SpellSlot::W, w.CastRange };
			SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_RED_START_POS1 };
			SDKVECTOR BLUE_SIDE_POS2_PSDK{ BLUE_RED_START_POS2 };

			if (Game::Time() < 40 && Player.Distance(BLUE_SIDE_POS1_PSDK) > 10.0f && !shacoBox1Planted) {
				if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
				}
				else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
				}

				bool leveledUp{ false };
				unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
				SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

				pSDK->Control->Move(&BLUE_SIDE_POS1_PSDK);
			}
			else if (Player.Distance(BLUE_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_RED_START_BOX1 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox1Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted && Player.Distance(BLUE_SIDE_POS2_PSDK) > 10.0f) {
				pSDK->Control->Move(&BLUE_SIDE_POS2_PSDK);
			}
			else if (t.IsReady() && shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted && Player.Distance(BLUE_SIDE_POS2_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_RED_START_BOX2 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox2Planted = true;
					}
				}
			}
			else if (t.IsReady() && shacoBox2Planted && !shacoBox3Planted) {
				if (Player.Distance(BLUE_SIDE_POS2_PSDK) < 10.0f) {
					if (shacoWCooldown < Game::Time()) {
						SDKVECTOR BLUE_SIDE_PSDK{ BLUE_RED_START_BOX3 };
						if (t.Cast(&BLUE_SIDE_PSDK)) {
							shacoWCooldown = Game::Time() + shacoWCastOffset;
							shacoBox3Planted = true;
						}
					}
				}
			}
			else if (shacoBox3Planted && !shacoBox4Planted && Player.Distance(BLUE_SIDE_POS1_PSDK) > 10.0f) {
				pSDK->Control->Move(&BLUE_SIDE_POS1_PSDK);
			}
			else if (t.IsReady() && shacoBox3Planted && !shacoBox4Planted && Player.Distance(BLUE_SIDE_POS1_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_RED_START_BOX4 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox4Planted = true;
						shacoSetupComplete = true;
					}
				}
			}
		}
	}
	else if (ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX == 1) {
		if (!shacoSetupComplete) {
			SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
			Spell::Targeted t = { SpellSlot::W, w.CastRange };
			SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_BLUE_START_POS1 };
			SDKVECTOR BLUE_SIDE_POS2_PSDK{ BLUE_BLUE_START_POS2 };
			SDKVECTOR BLUE_SIDE_POS3_PSDK{ BLUE_BLUE_START_POS3 };

			PSDKPOINT point{};
			SdkGetMouseScreenPosition(point);

			unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);

			if (Game::Time() < 40 && Player.Distance(BLUE_SIDE_POS1_PSDK) > 10.0f && !shacoBox1Planted) {
				if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
				}
				else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
				}

				bool leveledUp{ false };
				unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
				SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

				pSDK->Control->Move(&BLUE_SIDE_POS1_PSDK);
			}
			else if (Player.Distance(BLUE_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_BLUE_START_BOX1 };

					if (t.Cast(&BLUE_SIDE_POS1_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox1Planted = true;
					}
				}
			}
			else if (Player.Distance(BLUE_SIDE_POS1_PSDK) < 10.0f && shacoBox1Planted && !shacoBox2Planted) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_BLUE_START_BOX2 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox2Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && shacoBox2Planted && !shacoBox3Planted && Player.Distance(BLUE_SIDE_POS2_PSDK) > 10.0f) {
				pSDK->Control->Move(&BLUE_SIDE_POS2_PSDK);
			}
			else if (t.IsReady() && shacoBox1Planted && shacoBox2Planted && !shacoBox3Planted && Player.Distance(BLUE_SIDE_POS2_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_BLUE_START_BOX3 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox3Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && shacoBox2Planted && shacoBox3Planted && !shacoBox4Planted && Player.Distance(BLUE_SIDE_POS3_PSDK) > 10.0f) {
				pSDK->Control->Move(&BLUE_SIDE_POS3_PSDK);
			}
			else if (t.IsReady() && shacoBox3Planted && !shacoBox4Planted && Player.Distance(BLUE_SIDE_POS3_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR BLUE_SIDE_PSDK{ BLUE_BLUE_START_BOX4 };
					if (t.Cast(&BLUE_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox4Planted = true;
					}
				}
			}
			else if (shacoBox4Planted && Player.Distance(BLUE_SIDE_POS1_PSDK) > 10.0f) {
				pSDK->Control->Move(&BLUE_SIDE_POS1_PSDK);
			}
			else if (shacoBox4Planted && Player.Distance(BLUE_SIDE_POS1_PSDK) <= 10.0f) {
				shacoSetupComplete = true;
			}
		}
	}
}

/**
if (!shacoSetupComplete) {
	SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	Spell::Targeted t = { SpellSlot::W, w.CastRange };
	SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_SIDE_POS1 };

	if (Game::Time() < 40 && Player.Distance(BLUE_SIDE_POS1_PSDK) > 10.0f){

		if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
			bool pruchased{ false };
			SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
			pruchased = false;
			SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
		}
		else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
			bool pruchased{ false };
			SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
			pruchased = false;
			SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
		}

		bool leveledUp{ false };
		unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
		SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

		pSDK->Control->Move(&BLUE_SIDE_POS1_PSDK);
	}
	else if (Player.Distance(BLUE_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
		if (t.IsReady() && shacoWCooldown < Game::Time()) {
			SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_SIDE_BOX1 };
			if (t.Cast(&BLUE_SIDE_POS1_PSDK)) {
				shacoWCooldown = Game::Time() + shacoWCastOffset;
				shacoBox1Planted = true;
			}
		}
	}
	else if (t.IsReady() && shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted) {

		if (shacoWCooldown < Game::Time()) {
			SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_SIDE_BOX2 };
			if (t.Cast(&BLUE_SIDE_POS1_PSDK)) {
				shacoWCooldown = Game::Time() + shacoWCastOffset;
				shacoBox2Planted = true;
			}
		}
	}
	else if (t.IsReady() && shacoBox2Planted && !shacoBox3Planted) {
		if (shacoWCooldown < Game::Time()) {
			SDKVECTOR BLUE_SIDE_POS1_PSDK{ BLUE_SIDE_BOX3 };
			if (t.Cast(&BLUE_SIDE_POS1_PSDK)) {
				shacoWCooldown = Game::Time() + shacoWCastOffset;
				shacoBox3Planted = true;
			}
		}
	}
	else if (shacoBox3Planted && t.IsOnCooldown()) {
		if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_BLUE_SIDE, 0) == 0) {
			SDKVECTOR BLUE_BOTTOM_POS2_PSDK{ BLUE_BOT_START_POS1 };
			pSDK->Control->Move(&BLUE_BOTTOM_POS2_PSDK);
		}
		else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_BLUE_SIDE, 0) == 1) {
			SDKVECTOR BLUE_TOP_POS2_PSDK{ BLUE_TOP_START_POS1 };
			pSDK->Control->Move(&BLUE_TOP_POS2_PSDK);
		}
	}
	else if (shacoBox3Planted && t.IsReady()) {
		if (shacoWCooldown < Game::Time()) {
			if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_BLUE_SIDE, 0) == 0) {
				SDKVECTOR BLUE_BOTTOM_BOX_PSDK{ BLUE_BOT_START_BOX1 };
				if (t.Cast(&BLUE_BOTTOM_BOX_PSDK)) {
					shacoWCooldown = Game::Time() + shacoWCastOffset;
					shacoSetupComplete = true;
				}
			}
			else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_BLUE_SIDE, 0) == 1) {
				SDKVECTOR BLUE_TOP_BOX_PSDK{ BLUE_TOP_START_BOX1 };
				if (t.Cast(&BLUE_TOP_BOX_PSDK)) {
					shacoWCooldown = Game::Time() + shacoWCastOffset;
					shacoSetupComplete = true;
				}
			}
		}
	}
}
*/

void ShacoAutoJungleLogicManager::PerformRedSideSetup()
{
	if (ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX == 1) {
		if (!shacoSetupComplete) {
			SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
			Spell::Targeted t = { SpellSlot::W, w.CastRange };
			SDKVECTOR RED_SIDE_POS1_PSDK{ RED_RED_START_POS1 };
			SDKVECTOR RED_SIDE_POS2_PSDK{ RED_RED_START_POS2 };

			if (Game::Time() < 40 && Player.Distance(RED_SIDE_POS1_PSDK) > 10.0f && !shacoBox1Planted) {
				if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
				}
				else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
				}

				bool leveledUp{ false };
				unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
				SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

				pSDK->Control->Move(&RED_SIDE_POS1_PSDK);
			}
			else if (Player.Distance(RED_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_RED_START_BOX1 };
					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox1Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted && Player.Distance(RED_SIDE_POS2_PSDK) > 10.0f) {
				pSDK->Control->Move(&RED_SIDE_POS2_PSDK);
			}
			else if (t.IsReady() && shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted && Player.Distance(RED_SIDE_POS2_PSDK) <= 10.0f) {
				if (Player.Distance(RED_SIDE_POS2_PSDK) < 10.0f) {
					if (shacoWCooldown < Game::Time()) {
						SDKVECTOR RED_SIDE_PSDK{ RED_RED_START_BOX2 };
						if (t.Cast(&RED_SIDE_PSDK)) {
							shacoWCooldown = Game::Time() + shacoWCastOffset;
							shacoBox2Planted = true;
						}
					}
				}
			}
			else if (t.IsReady() && shacoBox2Planted && !shacoBox3Planted) {
				if (Player.Distance(RED_SIDE_POS2_PSDK) < 10.0f) {
					if (shacoWCooldown < Game::Time()) {
						SDKVECTOR RED_SIDE_PSDK{ RED_RED_START_BOX3 };
						if (t.Cast(&RED_SIDE_PSDK)) {
							shacoWCooldown = Game::Time() + shacoWCastOffset;
							shacoBox3Planted = true;
						}
					}
				}
			}
			else if (shacoBox3Planted && !shacoBox4Planted && Player.Distance(RED_SIDE_POS1_PSDK) > 10.0f) {
				pSDK->Control->Move(&RED_SIDE_POS1_PSDK);
			}
			else if (t.IsReady() && shacoBox3Planted && !shacoBox4Planted && Player.Distance(RED_SIDE_POS1_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_RED_START_BOX4 };
					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox4Planted = true;
						shacoSetupComplete = true;
					}
				}
			}
		}
	}
	else if (ShacoMenuManager::TOP_BOTTOM_DROPDOWN_INDEX == 0) {
		if (!shacoSetupComplete) {
			SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
			Spell::Targeted t = { SpellSlot::W, w.CastRange };
			SDKVECTOR RED_SIDE_POS1_PSDK{ RED_BLUE_START_POS1 };
			SDKVECTOR RED_SIDE_POS2_PSDK{ RED_BLUE_START_POS2 };
			SDKVECTOR RED_SIDE_POS3_PSDK{ RED_BLUE_START_POS3 };

			PSDKPOINT point{};
			SdkGetMouseScreenPosition(point);

			unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);

			if (Game::Time() < 40 && Player.Distance(RED_SIDE_POS1_PSDK) > 10.0f && !shacoBox1Planted) {
				if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
				}
				else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
					bool pruchased{ false };
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
					pruchased = false;
					SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
				}

				bool leveledUp{ false };
				unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
				SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

				pSDK->Control->Move(&RED_SIDE_POS1_PSDK);
			}
			else if (Player.Distance(RED_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_BLUE_START_BOX1 };

					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox1Planted = true;
					}
				}
			}
			else if (Player.Distance(RED_SIDE_POS1_PSDK) < 10.0f && shacoBox1Planted && !shacoBox2Planted) {
				if (t.IsReady() && shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_BLUE_START_BOX2 };
					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox2Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && shacoBox2Planted && !shacoBox3Planted && Player.Distance(RED_SIDE_POS2_PSDK) > 10.0f) {
				pSDK->Control->Move(&RED_SIDE_POS2_PSDK);
			}
			else if (t.IsReady() && shacoBox1Planted && shacoBox2Planted && !shacoBox3Planted && Player.Distance(RED_SIDE_POS2_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_BLUE_START_BOX3 };
					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox3Planted = true;
					}
				}
			}
			else if (shacoBox1Planted && shacoBox2Planted && shacoBox3Planted && !shacoBox4Planted && Player.Distance(RED_SIDE_POS3_PSDK) > 10.0f) {
				pSDK->Control->Move(&RED_SIDE_POS3_PSDK);
			}
			else if (t.IsReady() && shacoBox3Planted && !shacoBox4Planted && Player.Distance(RED_SIDE_POS3_PSDK) <= 10.0f) {
				if (shacoWCooldown < Game::Time()) {
					SDKVECTOR RED_SIDE_PSDK{ RED_BLUE_START_BOX4 };
					if (t.Cast(&RED_SIDE_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoBox4Planted = true;
					}
				}
			}
			else if (shacoBox4Planted && Player.Distance(RED_SIDE_POS1_PSDK) > 10.0f) {
				pSDK->Control->Move(&RED_SIDE_POS1_PSDK);
			}
			else if (shacoBox4Planted && Player.Distance(RED_SIDE_POS1_PSDK) <= 10.0f) {
				shacoSetupComplete = true;
			}
		}
	}

	/**
	if (!shacoSetupComplete) {
		SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
		Spell::Targeted t = { SpellSlot::W, w.CastRange };
		SDKVECTOR RED_SIDE_POS1_PSDK{ RED_SIDE_POS1 };

		if (Game::Time() < 40 && Player.Distance(RED_SIDE_POS1_PSDK) > 10.0f) {

			if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 1) {
				bool pruchased{ false };
				SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
				pruchased = false;
				SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RefillablePotion), 1, &pruchased);
			}
			else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_AUTOBUY_OPTIONS, 1) == 2) {
				bool pruchased{ false };
				SdkBuyItemLocalPlayer(static_cast<int>(ItemID::HuntersMachete), 0, &pruchased);
				pruchased = false;
				SdkBuyItemLocalPlayer(static_cast<int>(ItemID::RejuvenationBead), 1, &pruchased);
			}

			bool leveledUp{ false };
			unsigned char spellSlot = static_cast<unsigned char>(SpellSlot::W);
			SdkLevelSpellLocalPlayer(spellSlot, &leveledUp);

			pSDK->Control->Move(&RED_SIDE_POS1_PSDK);
		}
		else if (Player.Distance(RED_SIDE_POS1_PSDK) < 10.0f && !shacoBox1Planted && Game::Time() > 30.0f) {
			if (t.IsReady() && shacoWCooldown < Game::Time()) {
				SDKVECTOR RED_SIDE_PSDK{ RED_SIDE_BOX1 };
				if (t.Cast(&RED_SIDE_PSDK)) {
					shacoWCooldown = Game::Time() + shacoWCastOffset;
					shacoBox1Planted = true;
				}
			}
		}
		else if (t.IsReady() && shacoBox1Planted && !shacoBox2Planted && !shacoBox3Planted) {

			if (shacoWCooldown < Game::Time()) {
				SDKVECTOR RED_SIDE_PSDK{ RED_SIDE_BOX2 };
				if (t.Cast(&RED_SIDE_PSDK)) {
					shacoWCooldown = Game::Time() + shacoWCastOffset;
					shacoBox2Planted = true;
				}
			}
		}
		else if (t.IsReady() && shacoBox2Planted && !shacoBox3Planted) {
			if (shacoWCooldown < Game::Time()) {
				SDKVECTOR RED_SIDE_PSDK{ RED_SIDE_BOX3 };
				if (t.Cast(&RED_SIDE_PSDK)) {
					shacoWCooldown = Game::Time() + shacoWCastOffset;
					shacoBox3Planted = true;
				}
			}
		}
		else if (shacoBox3Planted && t.IsOnCooldown()) {
			if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_RED_SIDE, 0) == 0) {
				SDKVECTOR RED_BOTTOM_POS2_PSDK{ RED_BOT_START_POS1 };
				pSDK->Control->Move(&RED_BOTTOM_POS2_PSDK);
			}
			else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_RED_SIDE, 0) == 1) {
				SDKVECTOR RED_TOP_POS2_PSDK{ RED_TOP_START_POS1 };
				pSDK->Control->Move(&RED_TOP_POS2_PSDK);
			}
		}
		else if (shacoBox3Planted && t.IsReady()) {
			if (shacoWCooldown < Game::Time()) {
				if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_RED_SIDE, 0) == 0) {
					SDKVECTOR RED_BOTTOM_BOX_PSDK{ RED_BOT_START_BOX1 };
					if (t.Cast(&RED_BOTTOM_BOX_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoSetupComplete = true;
					}
				}
				else if (EndeavourusMiscUtils::GetDropDownSetting(SHACO_ENABLE_AUTO_JUNGLE_MODE_RED_SIDE, 0) == 1) {
					SDKVECTOR RED_TOP_BOX_PSDK{ RED_TOP_START_BOX1 };
					if (t.Cast(&RED_TOP_BOX_PSDK)) {
						shacoWCooldown = Game::Time() + shacoWCastOffset;
						shacoSetupComplete = true;
					}
				}
			}
		}

	}
			*/
}

bool ShacoAutoJungleLogicManager::IsEnabled() {
	return Game::Time() <= 40 || (Game::Time() > 40 && shacoBox1Planted);
}
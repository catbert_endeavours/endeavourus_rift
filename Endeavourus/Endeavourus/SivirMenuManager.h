#pragma once
#include "BaseMenuManager.h"

class SivirMenuManager : public BaseMenuManager
{
private:
	static void SivirSpecificSpellLogic();
	static void SpecificChampMenuLogic();
	static void SivirDrawingLogic();
public:
	static void Init();
	static void DrawMenu();
};


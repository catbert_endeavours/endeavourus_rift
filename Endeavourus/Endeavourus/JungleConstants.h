#pragma once
#include <string>

// Smite Consts
const std::string SMITE_GROUP = "_SMITE_GROUP";
const std::string ENABLE_SMITE = "_SMITE_ENABLED";
const std::string SMITE_MODE = "_SMITE_MODE";
const std::string DRAW_SMITE_GROUP = "_DRAW_SMITE_GROUP";
const std::string DRAW_SMITE_RANGE_ENABLED = "_DRAW_SMITE_RANGE_ENABLED";
const std::string DRAW_SMITE_RANGE_COLOUR = "_DRAW_SMITE_RANGE_COLOUR";

const std::string SAVE_ONE_STACK_UNLESS_EPIC_OR_ASSIST = "_SMITE_SAVE_STACK";
const std::string SMITE_ASSIST_KEY = "_SMITE_ASSIST_KEY";

const std::string SMITE_ALL_MONSTER_GROUP = "_SMITE_ALL_MONSTER_GROUP";

const std::string SMITE_PLAYER_GROUP = "_SMITE_PLAYER_GROUP";
const std::string SMITE_PLAYER_RED_ON_COMBO = "_SMITE_PLAYER_RED_ON_COMBO";
const std::string SMITE_PLAYER_BLUE_ON_COMBO = "_SMITE_PLAYER_BLUE_ON_COMBO";
const std::string SMITE_PLAYER_KS_BLUE = "_SMITE_PLAYER_BLUE_KS";

const std::string SMITE_CREEP_MONSTER_GROUP = "_SMITE_CREEP_MONSTER_GROUP";
const std::string SMITE_MONSTER_WOLF = "_SMITE_MONSTER_WOLF";
const std::string SMITE_MONSTER_RAPTOR = "_SMITE_MONSTER_RAPTOR";
const std::string SMITE_MONSTER_KRUG = "_SMITE_MONSTER_KRUG";
const std::string SMITE_MONSTER_GROMP = "_SMITE_MONSTER_GROMP";
const std::string SMITE_MONSTER_CRAB = "_SMITE_MONSTER_CRAB";

const std::string SMITE_BUFF_MONSTER_GROUP = "_SMITE_BUFF_MONSTER_GROUP";
const std::string SMITE_BUFF_MONSTER_BLUE = "_SMITE_BUFF_MONSTER_BLUE";
const std::string SMITE_BUFF_MONSTER_RED = "_SMITE_BUFF_MONSTER_RED";

const std::string SMITE_EPIC_MONSTER_GROUP = "_SMITE_EPIC_MONSTER_GROUP";
const std::string SMITE_EPIC_MONSTER_BARON = "_SMITE_EPIC_MONSTER_BARON";
const std::string SMITE_EPIC_MONSTER_RIFT = "_SMITE_EPIC_MONSTER_RIFT";
const std::string SMITE_EPIC_MONSTER_ELDER = "_SMITE_EPIC_MONSTER_ELDER";
const std::string SMITE_EPIC_MONSTER_INFERNAL = "_SMITE_EPIC_MONSTER_INFERNAL";
const std::string SMITE_EPIC_MONSTER_MOUNTAIN = "_SMITE_EPIC_MONSTER_MOUNTAIN";
const std::string SMITE_EPIC_MONSTER_CLOUD = "_SMITE_EPIC_MONSTER_CLOUD";
const std::string SMITE_EPIC_MONSTER_OCEAN = "_SMITE_EPIC_MONSTER_OCEAN";

const std::vector<int> SMITE_M_DAMAGE = { 390, 410, 430, 450, 480, 510, 540, 570, 600, 640, 680, 720, 760, 800, 850, 900, 950, 1000 };
const std::vector<int> SMITE_P_DAMAGE = { 28, 36, 44, 52, 60, 68, 76, 84, 92, 100, 108, 116, 124, 132, 140, 148, 156, 164 };

const std::vector<std::string> JUNGLE_CREEPS{ "Baron", "Dragon", "RiftHerald", "Blue", "Red", "Crab", "Gromp", "Murkwolf", "Razorbeak", "Krug" };

const std::string SMITE = "Smite";

const std::string BARON = "Baron";
const std::string RIFT = "Rift";
const std::string DRAGON = "Dragon";
const std::string DRAGON_INFERNAL = "Fire";
const std::string DRAGON_OCEAN = "Water";
const std::string DRAGON_MOUNTAIN = "Earth";
const std::string DRAGON_CLOUD = "Air";
const std::string DRAGON_ELDER = "Elder";
const std::string RED_BUFF = "Red";
const std::string BLUE_BUFF = "Blue";
const std::string WOLF = "Murkwolf";
const std::string CRAB = "Crab";
const std::string GROMP = "Gromp";
const std::string KRUGS = "Krug";
const std::string RAPTOR = "Razorbeak";

const std::string SMITE_PLAYER_GANK = "Ganker";
const std::string SMITE_PLAYER_DUEL = "Duel";

const std::string SMITE_DRAW_TEXT_ENABLED = "SMITE_DRAW_TEXT_ENABLED";
const std::string SMITE_DRAW_TEXT_X_OFFSET = "SMITE_DRAW_TEXT_X_OFFSET";
const std::string SMITE_DRAW_TEXT_Y_OFFSET = "SMITE_DRAW_TEXT_Y_OFFSET";
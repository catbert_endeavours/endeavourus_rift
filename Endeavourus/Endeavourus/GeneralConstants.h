#pragma once
#include <string>

const std::string MODULE_NAME = "Endeavourus";
const std::string MODULE_WELCOME = "<font color=\"#4286f4\">" + MODULE_NAME + " Loaded.</font>";

const std::string SELECTED_CHAMPION_LEFT_CLICK = "SELECTED_CHAMPION_LEFT_CLICK";

const std::string CHAMP_ENABLED = "_CHAMP_ENABLED";
const std::string CHAMP_SETTINGS = "_CHAMP";
const std::string MISC_SETTINGS =  "_MISC";
const std::string DRAW_SETTINGS = "_DRAW";
const std::string KEY_SETTINGS = "KEY_SETTINGS_TREE";

const std::string COMBO_KEY = "COMBO_KEY";
const std::string Mixed_KEY = "Mixed_KEY";
const std::string Clear_KEY = "Clear_KEY";
const std::string Lasthit_KEY = "Lasthit_KEY";
const std::string Escape_KEY = "Escape_KEY";


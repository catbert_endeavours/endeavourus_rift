#include "stdafx.h"
#include "TeemmoLogicManager.h"
#include "ShroomManager.h"
#include "BaseLogicManager.h"

SDK_SPELL* TeemmoLogicManager::T = nullptr;

void TeemmoLogicManager::Init()
{
	T = &Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));


	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::E_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	Spell::Targeted qCast = Spell::Targeted{SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, DamageType::Magical };
}

void TeemmoLogicManager::PerformLogic()
{
	bool comboActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Combo);
	bool customActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Custom);
	bool fleeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Flee);
	bool freezeActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Freeze);
	bool jungleClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::JungleClear);
	bool lastHitActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LastHit);
	bool laneClearActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::LaneClear);
	bool mixedActive = pCore->Orbwalker->IsModeActive(OrbwalkingMode::Mixed);

	if (comboActive)
	{

	}
	else if (customActive)
	{
	}
	else if (fleeActive)
	{
	}
	else if (freezeActive)
	{
	}
	else if (jungleClearActive)
	{
	}
	else if (lastHitActive)
	{
	}
	else if (laneClearActive)
	{
	}
	else if (mixedActive)
	{
	}
	else
	{
		//None
		ShroomManager::PerformShroomAutomation();
	}
}

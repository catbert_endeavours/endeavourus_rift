// Endeavourus.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Endeavourus.h"
#include "EndeavourusTeemo.h"
#include "UnsupportedChampion.h"
#include "EndeavourusNocturne.h"
#include "EndeavourusSivir.h"
#include "EndeavourusMundo.h"
#include "EndeavourusShaco.h"

std::string VERSION = "(1.2.0): ";
std::string start{ "<font color=\"#F58067\">" };
std::string end{ "</font>" };

/*
 * Endeavourus Entry
*/
void Endeavourus::Init() {
	LoadPlugin();
}

void Endeavourus::LoadPlugin() 
{
	const std::string charName = Player.GetCharName();
	const std::string summonerName = Player.GetSummonerName();

	if (charName == "Teemo")
	{
		Game::PrintChat(start + "Endeavourus " + VERSION + "Teemo Loaded" + end);
		EndeavourusTeemo::Init();
	}
	else if (charName == "Nocturne")
	{
		Game::PrintChat(start + "Endeavourus " + VERSION + "Nocturne Loaded" + end);
		EndeavourusNocturne::Init();
	}
	else if (charName == "Sivir")
	{
		Game::PrintChat(start + "Endeavourus " + VERSION + "Sivir Loaded" + end);
		EndeavourusSivir::Init();
	}
	else if (charName == "DrMundo")
	{
		Game::PrintChat(start + "Endeavourus " + VERSION + "Dr.Mundo Loaded" + end);
		EndeavourusMundo::Init();
	}
	else if (charName == "Shaco")
	{
		Game::PrintChat(start + "Endeavourus " + VERSION + "Shaco Loaded" + end);
		EndeavourusShaco::Init();
	}
	else
	{
		//Unsuported
		Game::PrintChat(start + "Endeavourus " + VERSION + "Unsupported Champion - Loading Utilities Only" + end);
		UnsupportedChampion::Init();
	}

	Game::PrintChat(start + "Please report any issues at: https://forums.rift.lol/index.php?forums/catbert.68/" + end);
}
#include "stdafx.h"
#include "TeemoDrawingManager.h"
#include "TeemoSettings.h"
#include "ShroomLocations.h"
#include "EndeavourusMiscUtils.h"
#include "TeemoConstants.h"

float gameTimeOffset{};
std::map<float, Vector3> distanceAndPositionMap{};

void TeemoDrawingManager::RefreshShroomDistances()
{
	if (Game::Time() > gameTimeOffset) {
		distanceAndPositionMap.clear();
		for (auto s : SHROOM_LOCATIONS)
		{
			float d = Player.GetPosition().Distance(s);
			distanceAndPositionMap.insert({ {d, s} });
		}
		gameTimeOffset = Game::Time() + 1;
	}
}

void TeemoDrawingManager::Draw()
{
	DrawSpellRanges();
	RefreshShroomDistances();
	DrawShroomLocations();
}

void TeemoDrawingManager::DrawShroomLocations()
{
	if (EndeavourusMiscUtils::GetBoolSetting(ENABLE_SHROOM_DRAWINGS_MASTER, true)) {
		float drawDistance = TeemoSettings::getEnableShroomDrawDistance();
		for (auto shroom : distanceAndPositionMap)
		{
			if (shroom.first < drawDistance)
			{
				Draw::Circle(&shroom.second, 100, &TeemoSettings::getShroomPlacementColour());
			}
		}
	}
}
#pragma once
#include <SDK Extensions.h>
#include "ChampionBase.h"

class EndeavourusSivir : public ChampionBase
{
public:
	static void Init();

	static void	__cdecl	OnUpdate(_In_ void* UserData);
	static void	__cdecl	OnDraw(_In_ void* UserData);
	static void __cdecl OnDrawMenu(_In_ void* UserData);
	static void	__cdecl OnPostAttack(_In_ void* UserData);
	static void __cdecl OnCastStart(void* heroCasting, PSDK_SPELL_CAST SpellCast, void* UserData);
};


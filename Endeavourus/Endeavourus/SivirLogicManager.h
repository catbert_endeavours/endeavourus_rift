#pragma once
#include "SDK Extensions.h"
class SivirLogicManager
{
private:
	static Spell::Active RCast;

	static float QCastOffset;

	static void RefreshSpells();

	static void CastQ(OrbwalkingMode orbwalkingMode);
	static void CastR(OrbwalkingMode orbwalkingMode);

	static void KillSteal();

	static float GetQDamage(AIBaseClient* enemy);
	static void GetPrediction(OrbwalkingMode orbwalkingMode);

	static void ShouldPerformOutOfRangeCombo();
	static bool ShouldCastR();

	static int GetREnemySetting();
public:
	static void Init();
	static void PerformLogic();
	static void PerformCombo(OrbwalkingMode orbwalkingMode);
	static void PerformHarass(OrbwalkingMode orbwalkingMode);
	static void PerformClear(OrbwalkingMode orbwalkingMode);
	static void PerformLastHit(OrbwalkingMode orbwalkingMode);
	static void PerformEscape(OrbwalkingMode orbwalkingMode);
	static void PerformMultiSpellCombo(OrbwalkingMode orbwalkingMode);

	static void CastE(OrbwalkingMode orbwalkingMode);
	static void CastW(OrbwalkingMode orbwalkingMode);

	static Spell::Skillshot QCast;
	static Spell::Active WCast;
	static Spell::Active ECast;

	static std::vector<int> QDamage;
	static std::vector<float> QRatioAD;
	static float QRatioAP;

	static bool SIVIR_SHOULD_CAST_SHIELD;
};


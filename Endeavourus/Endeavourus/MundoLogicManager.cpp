#include "stdafx.h"
#include "MundoLogicManager.h"
#include "EndeavourusMiscUtils.h"
#include "BaseLogicManager.h"
#include "NocturneLogicManager.h"
#include "EndeavourusLogicUtils.h"
#include "MundoConstants.h"
#include "BaseSettings.h"
#include "BaseMenuManager.h"

Spell::Skillshot MundoLogicManager::QCast{ SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Magical, true };
Spell::Active MundoLogicManager::WCast{ SpellSlot::W };
Spell::Active MundoLogicManager::ECast{ SpellSlot::E };
Spell::Active MundoLogicManager::RCast{ SpellSlot::R };

float MundoLogicManager::QCastOffset{ 0 };

std::vector<float> MundoLogicManager::QDamage{ 80.0f, 130.0f, 180.0f, 230.0f, 280.0f };
std::vector<float> MundoLogicManager::QRatio{ 0.20f, 0.225f, 0.25f, 0.275f, 0.30f };

float MundoWCDOffset{ 0.5 };
float MundoWCD{ 0.5 };

void MundoLogicManager::Init()
{
	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::E_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	QCast = { SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Magical, true };
	WCast = { SpellSlot::W };
	ECast = { SpellSlot::E };
	RCast = { SpellSlot::R };
}

void MundoLogicManager::PerformLogic()
{
	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		PerformCombo(orbwalkingMode);
		break;

	case OrbwalkingMode::Flee:
		PerformEscape(orbwalkingMode);
		break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		PerformClear(orbwalkingMode);
		break;

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		PerformLastHit(orbwalkingMode);
		break;

	case OrbwalkingMode::Mixed:
		PerformHarass(orbwalkingMode);
		break;

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		break;
	default:
		break;
	}

	KillSteal();
}

void MundoLogicManager::PerformCombo(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetComboResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void MundoLogicManager::PerformHarass(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetHarassResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void MundoLogicManager::PerformClear(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetClearResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void MundoLogicManager::PerformLastHit(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetLastHitResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void MundoLogicManager::PerformEscape(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetEscapeResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void MundoLogicManager::CastQ(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode, Resource::ResourceType::Health))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(QCast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady() && Game::Time() > QCastOffset)
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, enemy->GetCharName()))
				{
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
						if (Player.Distance(&enemy->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(enemy))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::LastHit:
	case OrbwalkingMode::Freeze:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode, Resource::ResourceType::Health)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range, true, true, true, &QCast) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() && Game::Time() > QCastOffset &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName())) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					float qDamage = GetQDamage(target->AsAIBaseClient());
					if (qDamage > target->GetHealth().Current)
					{
						if (Player.Distance(&target->AsAIBaseClient()->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode, Resource::ResourceType::Health)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range, false, true, true, &QCast) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName()) && Game::Time() > QCastOffset) {

				float qDamage = GetQDamage(target->AsAIBaseClient());
				bool killable = qDamage > target->GetHealth().Current;
				if ((!killable && target->GetHealthPercent() > 25 && target->IsMonster()) || killable) {
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
						if (Player.Distance(&target->AsAIBaseClient()->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode, Resource::ResourceType::Health))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(QCast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady() && Game::Time() > QCastOffset)
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, enemy->GetCharName()))
				{
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
						if (Player.Distance(&enemy->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(enemy))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}

	default:
		break;
	}
}

bool isMundoWBuffActive()
{
	BuffInstance buff = Player.GetBuff(MUNDO_W_BUFF_NAME);
	return buff.IsValid();
}

void MundoLogicManager::CastW(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode, Resource::ResourceType::Health))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(BaseLogicManager::W_Spell.CastRange);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && !isMundoWBuffActive() && WCast.IsReady() && Game::Time() > MundoWCDOffset)
			{
				if (WCast.Cast())
				{
					MundoWCDOffset = Game::Time() + MundoWCD;
				}
			}
			else if (enemy == nullptr && isMundoWBuffActive() && WCast.IsReady() && Game::Time() > MundoWCDOffset && !EndeavourusMiscUtils::GetBoolSetting(ENABLE_MANUAL_MUNDO_W, true))
			{
				if (WCast.Cast())
				{
					MundoWCDOffset = Game::Time() + MundoWCD;
				}
			}
		}
		else if (isMundoWBuffActive() && WCast.IsReady() && Game::Time() > MundoWCDOffset && !EndeavourusMiscUtils::GetBoolSetting(ENABLE_MANUAL_MUNDO_W, true))
		{
			if (WCast.Cast())
			{
				MundoWCDOffset = Game::Time() + MundoWCD;
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode, Resource::ResourceType::Health)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(BaseLogicManager::W_Spell.CastRange) };
			AttackableUnit* orbTarget = pCore->TS->GetTarget(BaseLogicManager::W_Spell.CastRange);
			bool orbTargetIsValid = orbTarget != nullptr && (orbTarget->IsMinion() || orbTarget->IsMonster()) && Player.IsValid() && Player.IsAlive() &&
				orbTarget->IsValid() && orbTarget->IsAlive() && std::string{ orbTarget->GetName() }.find("Turret") == std::string::npos;

			if ((target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && WCast.IsReady() && !isMundoWBuffActive() && Game::Time() > MundoWCDOffset)
				|| (orbTargetIsValid && WCast.IsReady() && !isMundoWBuffActive() && Game::Time() > MundoWCDOffset)) {
				if (WCast.Cast())
				{
					MundoWCDOffset = Game::Time() + MundoWCD;
				}
			}
			else if (target == nullptr && isMundoWBuffActive() && orbTarget == nullptr && WCast.IsReady() && Game::Time() > MundoWCDOffset && !EndeavourusMiscUtils::GetBoolSetting(ENABLE_MANUAL_MUNDO_W, true))
			{
				if (WCast.Cast())
				{
					MundoWCDOffset = Game::Time() + MundoWCD;
				}
			}
		}
		else if (isMundoWBuffActive() && WCast.IsReady() && Game::Time() > MundoWCDOffset && !EndeavourusMiscUtils::GetBoolSetting(ENABLE_MANUAL_MUNDO_W, true))
		{
			if (WCast.Cast())
			{
				MundoWCDOffset = Game::Time() + MundoWCD;
			}
		}
	}
	break;

	default:
		break;
	}
}

void MundoLogicManager::CastE(OrbwalkingMode orbwalkingMode)
{
	if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode, Resource::ResourceType::Health) && Player.IsAlive() && Player.IsValid() && ECast.IsReady()) {
		ECast.Cast();
	}
}

void MundoLogicManager::CastR(OrbwalkingMode orbwalkingMode)
{
	if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, orbwalkingMode, Resource::ResourceType::Health) && ShouldCastR()
		&& orbwalkingMode == OrbwalkingMode::Combo && Player.IsAlive() && Player.IsValid() && RCast.IsReady())
	{
		std::map<unsigned int, AIHeroClient*> enemies = pSDK->EntityManager->GetEnemyHeroes(QCast.Range);
		for (auto e : enemies)
		{
			if (e.second != nullptr && e.second->IsAlive() && e.second->IsValid())
			{
				RCast.Cast();
				return;
			}
		}
	}
}

bool MundoLogicManager::ShouldCastR()
{
	return Player.IsValid() && Player.IsAlive() && Player.GetHealthPercent() <= GetRHealthSetting();
}

int MundoLogicManager::GetRHealthSetting()
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_MUNDO_ULT_WHEN_HEALTH, 25);
}

void MundoLogicManager::KillSteal()
{
	if (BaseSettings::isQEnabledKs()) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(QCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && QCast.IsReady() && Game::Time() > QCastOffset) {
				{
					float qDamage = GetQDamage(e.second->AsAIBaseClient());
					if (qDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::Q, e.second->GetCharName(), true)) {
							GetPrediction(OrbwalkingMode::Custom);
							if (Player.Distance(&e.second->GetPosition()) <= QCast.Range) {
								if (QCast.Cast(e.second))
								{
									QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
								}
							}
						}
					}
				}
			}
		}
	}
}

float MundoLogicManager::GetQDamage(AIBaseClient* enemy)
{
	int qLevel = QCast.Level();
	if (enemy != nullptr && qLevel != 0)
	{
		float targetHealth = enemy->GetHealth().Current;
		float dmg{};
		if (!enemy->IsMinion()) {
			float mundoHealthDmg = QRatio[qLevel - 1] * targetHealth;
			dmg = pSDK->DamageLib->CalculateMagicalDamage(Player.AsAIHeroClient(), enemy, QDamage[qLevel - 1] + mundoHealthDmg);
		}
		else
		{
			dmg = QDamage[qLevel - 1];
		}
		return dmg;
	}

	return 0.0f;
}

void MundoLogicManager::GetPrediction(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionCombo()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionCombo();
		}
	}
	break;

	case OrbwalkingMode::Mixed:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionHarass()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionHarass();
		}
	}
	break;

	case OrbwalkingMode::Custom:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionKs()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionKs();
		}
	}
	break;

	case OrbwalkingMode::Flee:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionEscape()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionEscape();
		}
	}
	break;

	default:
		break;
	}
}
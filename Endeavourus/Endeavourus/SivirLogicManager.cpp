#include "stdafx.h"
#include "SivirLogicManager.h"
#include "BaseMenuManager.h"
#include "SpellShieldManager.h"
#include "BaseLogicManager.h"
#include "EndeavourusMiscUtils.h"
#include "EndeavourusLogicUtils.h"
#include "SivirConstants.h"
#include "BaseSettings.h"

Spell::Skillshot SivirLogicManager::QCast{ SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Physical, false };
Spell::Active SivirLogicManager::WCast{ SpellSlot::W };
Spell::Active SivirLogicManager::ECast{ SpellSlot::E };
Spell::Active SivirLogicManager::RCast{ SpellSlot::R };

float SivirLogicManager::QCastOffset{ 0 };

std::vector<int> SivirLogicManager::QDamage{ 35, 55, 75, 95, 115 };
std::vector<float> SivirLogicManager::QRatioAD{ 0.70f, 0.80f, 0.90f, 1.00f, 1.10f };
float SivirLogicManager::QRatioAP{ 0.50f };

bool SivirLogicManager::SIVIR_SHOULD_CAST_SHIELD{ false };

void SivirLogicManager::Init()
{
	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::E_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	QCast = { SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Physical, false };
	WCast = { SpellSlot::W };
	ECast = { SpellSlot::E };
	RCast = { SpellSlot::R };
}

void SivirLogicManager::ShouldPerformOutOfRangeCombo()
{
	AttackableUnit* attackableUnit = pCore->TS->GetTarget(QCast.Range);

	if (attackableUnit != nullptr) {
		float dist = Player.Distance(&attackableUnit->GetServerPosition());
		float pDist = Player.GetAttackRange();
		if (QCast.IsReady() && attackableUnit != nullptr && attackableUnit->IsValid() && attackableUnit->IsAlive() && attackableUnit->IsHero() && (dist > pDist))
		{
			// We only want to do this if enemys are out of AA range - else we let AA reset deal with combo
			OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
			PerformCombo(orbwalkingMode);
		}
	}
}

void SivirLogicManager::PerformLogic()
{
	CastE(OrbwalkingMode::Custom);

	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		ShouldPerformOutOfRangeCombo();
		break;

	case OrbwalkingMode::Flee:
		PerformEscape(orbwalkingMode);
		break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		PerformClear(orbwalkingMode);
		break;

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		PerformLastHit(orbwalkingMode);
		break;

	case OrbwalkingMode::Mixed:
		PerformHarass(orbwalkingMode);
		break;

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		break;
	default:
		break;
	}
	KillSteal();
}

void SivirLogicManager::CastQ(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(QCast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady() && Game::Time() > QCastOffset)
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, enemy->GetCharName()))
				{
					//if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
						if (Player.Distance(&enemy->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(enemy))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					//}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::LastHit:
	case OrbwalkingMode::Freeze:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range, true) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName()) && Game::Time() > QCastOffset) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					float qDamage = GetQDamage(target->AsAIBaseClient());
					if (qDamage > target->GetHealth().Current)
					{
						if (Player.Distance(&target->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName()) && Game::Time() > QCastOffset) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					if (Player.Distance(&target->GetPosition()) <= QCast.Range) {
						if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
						{
							QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
						}
					}
				}
			}
		}
	}
	break;

	default:
		break;
	}
}

void SivirLogicManager::CastW(OrbwalkingMode orbwalkingMode)
{
	if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode) && Player.IsAlive() && Player.IsValid() && WCast.IsReady()) {
		WCast.Cast();
	}
}

void SivirLogicManager::CastE(OrbwalkingMode orbwalkingMode)
{
	if (SIVIR_SHOULD_CAST_SHIELD && Player.IsAlive() && Player.IsValid() && ECast.IsReady())
	{
		ECast.Cast();
	}
	else {
		SIVIR_SHOULD_CAST_SHIELD = false;
	}
}

void SivirLogicManager::CastR(OrbwalkingMode orbwalkingMode)
{
	if (ShouldCastR() && orbwalkingMode == OrbwalkingMode::Combo && Player.IsAlive() && Player.IsValid() && RCast.IsReady())
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, orbwalkingMode)) {
			RCast.Cast();
		}
	}
	else if (orbwalkingMode == OrbwalkingMode::Flee && Player.IsAlive() && Player.IsValid() && RCast.IsReady() && BaseSettings::isREnabledEscape())
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, orbwalkingMode)) {
			RCast.Cast();
		}
	}
}

bool SivirLogicManager::ShouldCastR()
{
	std::map<unsigned int, AIHeroClient*> enemies = pSDK->EntityManager->GetEnemyHeroes(Player.GetAttackRange());
	return !enemies.empty() && static_cast<int>(enemies.size()) >= GetREnemySetting();
}

int SivirLogicManager::GetREnemySetting()
{
	return EndeavourusMiscUtils::GetIntSetting(ENABLE_SIVIR_ULT_WHEN_ENEMIES_CLOSE, 3);
}

void SivirLogicManager::PerformCombo(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetComboResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void SivirLogicManager::PerformMultiSpellCombo(OrbwalkingMode orbwalkingMode)
{
	if (orbwalkingMode == OrbwalkingMode::Combo) {
		if (BaseMenuManager::GetComboResourceManagement().isWEnabled()) {
			CastW(orbwalkingMode);
		}

		GetPrediction(orbwalkingMode);
		if (BaseMenuManager::GetComboResourceManagement().isQEnabled())
		{
			CastQ(orbwalkingMode);
		}

		if (BaseMenuManager::GetComboResourceManagement().isREnabled())
		{
			CastR(orbwalkingMode);
		}
	}
}

void SivirLogicManager::PerformHarass(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetHarassResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void SivirLogicManager::PerformClear(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetClearResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void SivirLogicManager::PerformLastHit(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetLastHitResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void SivirLogicManager::PerformEscape(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetEscapeResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void SivirLogicManager::KillSteal()
{
	if (BaseSettings::isQEnabledKs() && EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, OrbwalkingMode::Custom)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(QCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && QCast.IsReady() && Game::Time() > QCastOffset) {
				{
					float qDamage = GetQDamage(e.second->AsAIBaseClient());
					if (qDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::Q, e.second->GetCharName(), true)) {
							GetPrediction(OrbwalkingMode::Custom);
							if (Player.Distance(&e.second->GetPosition()) <= QCast.Range) {
								if (QCast.Cast(e.second))
								{
									QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
								}
							}
						}
					}
				}
			}
		}
	}
}

float SivirLogicManager::GetQDamage(AIBaseClient * enemy)
{
	int qLevel = QCast.Level();
	if (qLevel != 0) {
		return pSDK->DamageLib->CalculatePhysicalDamage(Player.AsAIHeroClient(), enemy, (QDamage[qLevel - 1] + (Player.GetAttackDamage() * QRatioAD[qLevel - 1])) + (Player.GetAbilityPower() * QRatioAP));
	}

	return 0;
}

void SivirLogicManager::GetPrediction(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionCombo()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionCombo();
		}
	}
	break;

	case OrbwalkingMode::Mixed:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionHarass()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionHarass();
		}
	}
	break;

	case OrbwalkingMode::Custom:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionKs()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionKs();
		}
	}
	break;

	default:
		break;
	}
}
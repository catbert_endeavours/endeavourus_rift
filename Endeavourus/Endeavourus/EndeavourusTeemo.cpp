#include "stdafx.h"
#include "EndeavourusTeemo.h"
#include "TeemoMenuManager.h"
#include "EndeavourusMiscUtils.h"
#include "SmiteManager.h"
#include "SmiteDrawingManager.h"
#include "BaseSettings.h"
#include "TeemoDrawingManager.h"
#include "ShroomManager.h"
#include "EndeavourusActivator.h"

void EndeavourusTeemo::Init()
{
	SmiteManager::Init();
	TeemoMenuManager::Init();
	TeemoLogicManager::Init();
	EndeavourusActivator::Init();

	pSDK->EventHandler->RegisterCallback(CallbackEnum::Tick, OnUpdate);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Overlay, OnDrawMenu);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnDraw);
	pSDK->EventHandler->RegisterCallback(CallbackEnum::PostAttack, OnPostAttack);
}

/*
 * This gets called 30 times per second -> Logic goes here
*/
void __cdecl EndeavourusTeemo::OnUpdate(void * UserData)
{
	if (!Game::IsAvailable())
	{
		return;
	}


	ShroomManager::SnapRToClosestShroomLoc();
	SmiteManager::PerformSmiteLogic();

	if (BaseSettings::isChampEnabled()) {
		TeemoLogicManager::PerformLogic();
	}
}

/*
 * This gets called X times per second, where X is your league fps. -> Important stuff only
*/
void __cdecl EndeavourusTeemo::OnDraw(void * UserData)
{

	if (NeedToRefreshSettings())
	{
		OnDrawMenu(UserData);
	}
	SmiteDrawingManager::DrawSmite();

	if (BaseSettings::isChampEnabled()) {
		TeemoDrawingManager::Draw();
	}
}

/*
 * Menu gets drawn here
*/
void __cdecl EndeavourusTeemo::OnDrawMenu(void * UserData)
{
	TeemoMenuManager::DrawMenu();
}

/*`
 * This gets called when the player has just finished an attack -> Resets etc.
*/
void __cdecl EndeavourusTeemo::OnPostAttack(void * UserData)
{
	if (BaseSettings::isChampEnabled()) {
		OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
		TeemoLogicManager::CastQ(orbwalkingMode);
	}
}
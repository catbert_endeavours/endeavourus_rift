#pragma once
#include <vector>
#include <string>
#include <map>

struct SHACO_SKILLSHOT_DATA {
	std::string spellName{ };
	std::string displayName{ };
	std::string missileName{ };
	bool heroCollision{ };
	bool minionCollision{ };

	SHACO_SKILLSHOT_DATA(std::string displayName, std::string spellName, std::string missileName, bool heroCollision, bool minionCollision = false) {
		this->displayName = displayName;
		this->spellName = spellName;
		this->missileName = missileName;
		this->heroCollision = heroCollision;
		this->minionCollision = minionCollision;
	}
};

static std::map<std::string, SHACO_SKILLSHOT_DATA> SKILLSHOT_MAP_SHACO{
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Fizz", SHACO_SKILLSHOT_DATA{ "Fizz R", "FizzR", "FizzRMissile", true }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Ashe", SHACO_SKILLSHOT_DATA{ "Ashe R", "EnchantedCrystalArrow", "EnchantedCrystalArrow", true }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Draven", SHACO_SKILLSHOT_DATA{ "Draven R", "DravenRCast", "DravenR", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Lux", SHACO_SKILLSHOT_DATA{ "Lux R", "LuxMaliceCannon", "LuxRVfxMis", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Graves", SHACO_SKILLSHOT_DATA{"Graves R", "GravesChargeShot", "GravesChargeShotShot", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Hecarim", SHACO_SKILLSHOT_DATA{"Hecarim R", "HecarimUlt", "HecarimUltMissile", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Sejuani", SHACO_SKILLSHOT_DATA{"Sejuani R", "SejuaniR", "SejuaniRMissile", true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Sona", SHACO_SKILLSHOT_DATA{"Sona R", "SonaR", "SonaR", false }}, /////////////////////////////Needs to go into spellstart
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Varus", SHACO_SKILLSHOT_DATA{"Varus R", "VarusR", "VarusR", true }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Gnar", SHACO_SKILLSHOT_DATA{"Gnar R", "GnarR", "GnarR", false }}, /////////////////////////////Needs to go into spellstart
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Ezreal", SHACO_SKILLSHOT_DATA{"Ezreal R", "EzrealR", "EzrealR", false }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Cassiopeia", SHACO_SKILLSHOT_DATA{"Cassiopeia R", "CassiopeiaR", "CassiopeiaR", false }},  /////////////////////////////Needs to go into spellstart
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Ahri", SHACO_SKILLSHOT_DATA{"Ahri Seduce", "AhriSeduce", "AhriSeduce", true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Blitzcrank", SHACO_SKILLSHOT_DATA{"Blitzcrank RocketGrab", "RocketGrab", "RocketGrabMissile", true, true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Morgana", SHACO_SKILLSHOT_DATA{"Morgana Binding", "MorganaQ", "MorganaQ", true, true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Amumu", SHACO_SKILLSHOT_DATA{"Amumu BandageToss", "BandageToss", "SadMummyBandageToss", true, true }} };

static std::map<std::string, SHACO_SKILLSHOT_DATA> SKILLSHOT_MAP_W{
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Blitzcrank", SHACO_SKILLSHOT_DATA{ "Blitzcrank RocketGrab", "RocketGrab", "RocketGrabMissile", true, true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Morgana", SHACO_SKILLSHOT_DATA{ "Morgana Binding", "MorganaQ", "MorganaQ", true, true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Amumu", SHACO_SKILLSHOT_DATA{ "Amumu BandageToss", "BandageToss", "SadMummyBandageToss", true, true }} };

static std::vector<std::string> Skillshots
{
	"FizzMarinerDoom",
		"AhriSeduce",
		"EnchantedCrystalArrow",
		"DravenRCast",
		"LuxMaliceCannon",
		"GravesChargeShot",
		"HecarimUlt",
		"SejuaniGlacialPrisonStart",
		"SonaCrescendo",
		"VarusR",
		"AzirR",
		"GnarR",
		"EzrealTrueshotBarrage",
		"CassiopeiaPetrifyingGaze",
		"RocketGrabMissile",
		"SadMummyBandageToss"
};

static std::vector<std::string> TargetedList
{
	"BrandWildfire",
		"DariusExecute",
		"Terrify",
		"GarenR",
		"LissandraR",
		"AlZaharNetherGrasp",
		"SyndraR",
		"ZedR"
};

static std::map<std::string, SHACO_SKILLSHOT_DATA> TARGETED_MAP_SHACO{
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Brand", SHACO_SKILLSHOT_DATA{ "Brand R", "BrandR", "", true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Darius", SHACO_SKILLSHOT_DATA{ "Darius R", "DariusExecute", "", true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Garen", SHACO_SKILLSHOT_DATA{ "Garen R", "GarenR", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Lissandra", SHACO_SKILLSHOT_DATA{ "Lissandra R", "LissandraR", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Malzahar", SHACO_SKILLSHOT_DATA{"Malzahar R", "AlZaharNetherGrasp", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Syndra", SHACO_SKILLSHOT_DATA{"Syndra R", "SyndraR", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Zed", SHACO_SKILLSHOT_DATA{"Zed R", "ZedR", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"LeeSin", SHACO_SKILLSHOT_DATA{"Lee R", "BlindMonkRKick", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Veigar", SHACO_SKILLSHOT_DATA{"Veigar R", "VeigarR", "", false }},
};

static std::vector<std::string> CircleSkills
{
		"LeonaSolarFlare",
		"InfernalGuardian",
		"GragasR",
		"OrianaDetonateCommand",
		"CurseoftheSadMummy",
};

static std::map<std::string, SHACO_SKILLSHOT_DATA> CIRCLE_MAP_SHACO{
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Leona", SHACO_SKILLSHOT_DATA{ "Leona R", "LeonaSolarFlare", "", true }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Annie", SHACO_SKILLSHOT_DATA{ "Annie R", "AnnieR", "", true }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Gragas", SHACO_SKILLSHOT_DATA{ "Gragas R", "GragasR", "", false }},
std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Oriana", SHACO_SKILLSHOT_DATA{ "Oriana R", "OrianaDetonateCommand", "", false }},
//std::pair<std::string, SHACO_SKILLSHOT_DATA> {"Amumu", SHACO_SKILLSHOT_DATA{"Amumu R", "CurseoftheSadMummy", "", false }}
};

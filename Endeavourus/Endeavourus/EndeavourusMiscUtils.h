#pragma once
#include "SDK Extensions.h"
#include "Resource.h"

class EndeavourusMiscUtils
{
public:
	struct PointProjection
	{
		Vector2 pointSegment;
		Vector2 pointLine{};
		bool isOnSeg{};
	};


	static bool GetBoolSetting(std::string settingName, const bool defaultVal = false);

	static float GetFloatSetting(std::string settingName, const float defaultVal = 0);
	static int GetDropDownSetting(std::string settingName, const int defaultVal = 0);

	static int GetIntSetting(std::string settingName, const int defaultVal = 0);

	const static std::string GetUniqueSettingName(std::string settingName);
	static bool PlayerHasSmite();
	static bool PlayerHasUnsealedSpellbook();

	static OrbwalkingMode GetOrbwalkingMode();

	static bool CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode, Resource::ResourceType resourceType);
	static bool CanCastSpell(SpellSlot spellSlot, OrbwalkingMode orbwalkingMode);
	static bool IsValidToCastSpell(AIBaseClient* enemy);
	static bool CanCastSpellSmoothInCombo(AIBaseClient * enemy);
	static AIBaseClient* GetJungleMinionTarget(float range, bool lastHitMode = false, bool getLowestHealth = false);
	static AIBaseClient* GetJungleMinionTarget(float range, bool lastHitMode, bool getLowestHealth, bool collisionCheck, Spell::Skillshot* skillshot);
	static PointProjection VectorPointProjectionOnLineSegment(Vector2 v1, Vector2 v2, Vector2 v3);

	static void UpdateBoolSetting(std::string settingName, bool value);
	static void UpdateIntSetting(std::string settingName, int value);
	static bool IsBlueTeam();
	static bool IsSummonersRift();
	static void UpdateFloatSetting(std::string settingName, float value);

};


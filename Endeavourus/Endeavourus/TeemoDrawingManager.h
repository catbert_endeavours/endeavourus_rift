#pragma once
#include "BaseDrawingManager.h"

class TeemoDrawingManager : public BaseDrawingManager
{
protected:
	static void DrawShroomLocations();
	static void RefreshShroomDistances();
public:
	static void Draw();
};
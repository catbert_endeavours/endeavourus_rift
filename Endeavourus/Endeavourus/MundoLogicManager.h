#pragma once
#include "SDK Extensions.h"

class MundoLogicManager
{
private:
	static Spell::Skillshot QCast;
	static Spell::Active WCast;
	static Spell::Active ECast;
	static Spell::Active RCast;

	static void CastQ(OrbwalkingMode orbwalkingMode);
	static void CastR(OrbwalkingMode orbwalkingMode);
	static void CastW(OrbwalkingMode orbwalkingMode);

	static void KillSteal();

	static float GetQDamage(AIBaseClient* enemy);

	static void GetPrediction(OrbwalkingMode orbwalkingMode);

	static bool ShouldCastR();
	static int GetRHealthSetting();

	static float QCastOffset;

public:
	static void Init();
	static void PerformLogic();
	static void PerformCombo(OrbwalkingMode orbwalkingMode);
	static void PerformHarass(OrbwalkingMode orbwalkingMode);
	static void PerformClear(OrbwalkingMode orbwalkingMode);
	static void PerformLastHit(OrbwalkingMode orbwalkingMode);
	static void PerformEscape(OrbwalkingMode orbwalkingMode);

	static void CastE(OrbwalkingMode orbwalkingMode);

	static std::vector<float> QDamage;
	static std::vector<float> QRatio;

	static std::vector<int> EDamage;
	static float ERatio;

	static std::vector<int> RDamage;
	static float RRatio;
};


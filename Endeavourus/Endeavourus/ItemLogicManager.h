#pragma once
#include <string>
#include "SDK Extensions.h"
class ItemLogicManager
{
public:
	static void PerformItemLogic();
	static bool IsItemBlockedOnChampion(std::string itemName, std::string enemyName);
	static void PerformTiamatReset();
	static AIHeroClient* GetValidTarget(float range);
	static AIHeroClient* GetValidAlly(std::string itemName, float range);
	static bool ShouldCastShacoWBeforeAA;
	static bool ShouldCastShacoEBeforeAA;

	static bool ShouldCastShacoTiamatBeforeAA;
	static Spell::Active TIAMAT_SPELL;

	static float SHACO_COMBO_OFFSET;

};


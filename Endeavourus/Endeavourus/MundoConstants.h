#pragma once
#include <string>

const std::string MUNDO_W_BUFF_NAME = "BurningAgony";

const std::string MUNDO_LOGIC_SETTING_GROUP = "_MUNDO_LOGIC_SETTING_GROUP";
const std::string ENABLE_MUNDO_ULT_WHEN_HEALTH = "_ENABLE_MUNDO_ULT_WHEN_HEALTH";

const std::string ENABLE_MANUAL_MUNDO_W = "ENABLE_MANUAL_MUNDO_W";

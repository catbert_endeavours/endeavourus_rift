#pragma once
#include "SDK Extensions.h"

const Vector3 topPassTower{ 2424.0f, 54.325500f, 10406.0f };
const Vector3 topPassTowerLand{ 2086.0f, 52.838f, 11651.0f };
const std::pair<Vector3, Vector3> deceiveLocation1{ topPassTower, topPassTowerLand };

const Vector3 botPassTower{ 12472.0f, 51.72f, 4508.0f };
const Vector3 botPassTowerLand{ 12808.0f, 51.367f, 3462.0f };
const std::pair<Vector3, Vector3> deceiveLocation2{ botPassTower, botPassTowerLand };

const Vector3 topBushJump{ 2736.0f, -66.347603f, 11344.0f };
const Vector3 topBushLand{ 1473.0f, 52.838f, 11347.0f };
const std::pair<Vector3, Vector3> deceiveLocation3{ topBushJump, topBushLand };

const Vector3 botBushJump{ 12082.0f, -66.247200f, 3612.0f };
const Vector3 botBushLand{ 12501.0f, -36.934f, 3768.0f };
const std::pair<Vector3, Vector3> deceiveLocation4{ botBushJump, botBushLand };

const Vector3 midBlueLeftSide{ 6024.0f, 51.6533f, 7506.0f };
const Vector3 midBlueLeftSideLand{ 6831.0f, 53.865f, 7360.0f };
const std::pair<Vector3, Vector3> deceiveLocation5{ midBlueLeftSide, midBlueLeftSideLand };

const Vector3 midMidRightSide{ 8572.0f, -71.24f, 6808.0f };
const Vector3 midMidRightLand{ 8451.0f, 53.688f, 7676.0f };
const std::pair<Vector3, Vector3> deceiveLocation6{ midMidRightSide, midMidRightLand };

const Vector3 blueBotTriJump{ 10992.0f, 49.222f, 2532.0f };
const Vector3 blueBotTriLand{ 12847.0f, 51.367f, 2773.0f };
const std::pair<Vector3, Vector3> deceiveLocation7{ blueBotTriJump, blueBotTriLand };

const Vector3 topRedTriJump{ 4134.0f, 55.377f, 12034.0f };
const Vector3 topRedTriLand{ 3022.0f, -6.106f, 11968.0f };
const std::pair<Vector3, Vector3> deceiveLocation8{ topRedTriJump, topRedTriLand };

const Vector3 redSideBlueJump{ 10372.0f, -71.24f, 5908.0f };
const Vector3 redSideBlueLand{ 11684.0f, 51.727f, 7002.0f };
const std::pair<Vector3, Vector3> deceiveLocation9{ redSideBlueJump, redSideBlueLand };

static const std::vector<std::pair<Vector3, Vector3>> DECEIVE_LOCATIONS{ deceiveLocation1, deceiveLocation2, deceiveLocation3, deceiveLocation4, deceiveLocation5,
deceiveLocation6, deceiveLocation7, deceiveLocation8, deceiveLocation9 };
#pragma once
#include <string>
class SummonerLogicManager
{
public:
	static void PerformSummoners();
	static void PerformIgniteLogic();
	static void PerformExhaustLogic();
	static void PerformHealLogic();
	static void PerformBarrierLogic();
	static void PerformCleanseLogic();

	static bool IsSpellBlockedOnChampion(std::string itemName, std::string enemyName);

};


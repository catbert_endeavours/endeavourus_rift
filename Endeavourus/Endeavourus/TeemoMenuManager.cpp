#include "stdafx.h"
#include "TeemoMenuManager.h"
#include "DrawConstants.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "TeemoConstants.h"
#include "EndeavourusMenu.h"

void TeemoMenuManager::Init()
{
	specificChampLogic = SpecificChampMenuLogic;

	comboResourceSettings = *new Resource(true, true, true, true, false, EndeavourusHitChance::Medium, true, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false,
		false, false, EndeavourusHitChance::Medium);
	harassResourceSettings = *new Resource(true, true, true, true, false, EndeavourusHitChance::Medium, true, false, false, false, 
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false,EndeavourusHitChance::Medium, 40, 40, 40, 40);
	clearResourceSettings = *new Resource(true, true, false, false, false, EndeavourusHitChance::Medium, true, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	lastHitResourceSettings = *new Resource(true, true, false, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	escapeResourceSettings = *new Resource(true, false, false, false, false, EndeavourusHitChance::Medium, true, true, false, false,
		EndeavourusHitChance::Medium,false, false, false, false, EndeavourusHitChance::Medium, true, true,
		false, false, EndeavourusHitChance::Medium);
	ksResourceSettings = *new Resource(true, true, true, true, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium);
}

void TeemoMenuManager::ColourOptionShroomLocationSetting()
{
	SDKVECTOR defaultColour{ 16, 2, 219 };
	EndeavourusMenu::ColorPicker("Shroom Location Colour", EndeavourusMiscUtils::GetUniqueSettingName(R_PLACEMENT_CIRCLE_COLOUR), defaultColour);
}

void TeemoMenuManager::ShroomLocationCircleOption()
{
	EndeavourusMenu::Tree("Shroom Locations", EndeavourusMiscUtils::GetUniqueSettingName(SHROOM_LOCATION_COLOUR_GROUP), false, ColourOptionShroomLocationSetting);
}

void TeemoMenuManager::SpecificChampMenuLogic()
{
	EndeavourusMenu::Tree("Teemo Specific Spell Settings", EndeavourusMiscUtils::GetUniqueSettingName(TEEMO_LOGIC_SETTING_GROUP), false, TeemoSpecificSpellLogic);
	EndeavourusMenu::Tree("Drawings",  EndeavourusMiscUtils::GetUniqueSettingName(DRAWING_GROUP), false, TeemoDrawingLogic);
}

void TeemoMenuManager::TeemoDrawingLogic()
{
	DrawQRange();
	DrawRRange();
	SdkUiText("-----------Shroom Placement Settings-----------");
	EndeavourusMenu::Checkbox("Enable Shroom Drawings", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_SHROOM_DRAWINGS_MASTER), true);
	EndeavourusMenu::SliderFloat("Draw Shroom Location if Distance < value", EndeavourusMiscUtils::GetUniqueSettingName(SHROOM_DISTANCE_DRAWINGS), 3000, 0, 10000);
	ShroomLocationCircleOption();
}

void TeemoMenuManager::TeemoSpecificSpellLogic()
{
	EndeavourusMenu::EndeavourusHotkey("R Snap Assist", EndeavourusMiscUtils::GetUniqueSettingName(TEEMO_R_SNAP_KEY_ENABLED), KeyCode::R);
	//EndeavourusMenu::Checkbox("Auto R Predefined Shroom Spots", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_AUTO_R_SHROOM_SPOTS), true);
	EndeavourusMenu::Checkbox("Auto R on Immobile Champion", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_AUTO_R_IMMOBILE), true);
}

void TeemoMenuManager::DrawMenu()
{
	BaseMenuManager::DrawMenu();
}

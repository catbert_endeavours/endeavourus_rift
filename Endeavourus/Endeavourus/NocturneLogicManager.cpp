#include "stdafx.h"
#include "NocturneLogicManager.h"
#include "BaseLogicManager.h"
#include "EndeavourusMiscUtils.h"
#include "BaseMenuManager.h"
#include "EndeavourusLogicUtils.h"
#include "SpellShieldManager.h"
#include "BaseSettings.h"
#include "NocturneConstants.h"

Spell::Skillshot NocturneLogicManager::QCast{ SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Physical, false };
Spell::Active NocturneLogicManager::WCast{ SpellSlot::W };
Spell::Targeted NocturneLogicManager::ECast{ SpellSlot::E, BaseLogicManager::E_Spell.CastRange, DamageType::Magical };
Spell::Targeted NocturneLogicManager::RCast{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, DamageType::Physical, false };

bool NocturneLogicManager::NOC_SHOULD_CAST_SHIELD{ false };

float QCastOffset{ 0 };

std::vector<int> NocturneLogicManager::QDamage{ 65, 110, 155, 200, 245 };
float NocturneLogicManager::QRatio{ 0.75f };

std::vector<int> NocturneLogicManager::EDamage{ 80, 125, 170, 215, 260 };
float NocturneLogicManager::ERatio{ 1.00f };

std::vector<int> NocturneLogicManager::RDamage{ 150, 275, 400 };
float NocturneLogicManager::RRatio{ 1.20f };

void NocturneLogicManager::Init()
{
	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::E_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	QCast = { SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, SkillshotType::Line, BaseLogicManager::Q_Spell.CastDelay, BaseLogicManager::Q_Spell.MissileSpeed, BaseLogicManager::Q_Spell.LineWidth, DamageType::Physical, false };
	WCast = { SpellSlot::W };
	ECast = { SpellSlot::E, BaseLogicManager::E_Spell.CastRange, DamageType::Magical };
	RCast = { SpellSlot::R, BaseLogicManager::R_Spell.CastRange, DamageType::Physical, false };
}

bool isNocRBuffActive()
{
	BuffInstance buff = Player.GetBuff(NOCTURNE_R_BUFF_NAME);
	return buff.IsValid();
}

void NocturneLogicManager::PerformLogic()
{
	RefreshSpells();

	CastW(OrbwalkingMode::Custom);

	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		PerformCombo(orbwalkingMode);
		break;

	case OrbwalkingMode::Flee:
		PerformEscape(orbwalkingMode);
		break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		PerformClear(orbwalkingMode);
		break;

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		PerformLastHit(orbwalkingMode);
		break;

	case OrbwalkingMode::Mixed:
		PerformHarass(orbwalkingMode);
		break;

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		break;
	default:
		break;
	}

	KillSteal();
}


void NocturneLogicManager::PerformCombo(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetComboResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void NocturneLogicManager::PerformHarass(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);
	if (BaseMenuManager::GetHarassResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void NocturneLogicManager::PerformClear(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetClearResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void NocturneLogicManager::PerformLastHit(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetLastHitResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void NocturneLogicManager::PerformEscape(OrbwalkingMode orbwalkingMode)
{
	GetPrediction(orbwalkingMode);

	if (BaseMenuManager::GetEscapeResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void NocturneLogicManager::CastQ(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(QCast.Range);
			if (Player.HasBuff("nocturneparanoiavo"))
			{
				if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady() && Game::Time() > QCastOffset && Player.Distance(&enemy->GetServerPosition()) < 400)
				{
					if (Player.Distance(&enemy->GetPosition()) <= QCast.Range) {

						if (QCast.Cast(&enemy->GetPosition()))
						{
							QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							return;
						}
					}
				}
			}

			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady() && Game::Time() > QCastOffset)
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, enemy->GetCharName()))
				{
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
						if (Player.Distance(&enemy->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(enemy))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::LastHit:
	case OrbwalkingMode::Freeze:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range, true) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() && Game::Time() > QCastOffset &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName())) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					float qDamage = GetQDamage(target->AsAIBaseClient());
					if (qDamage > target->GetHealth().Current)
					{
						if (Player.Distance(&target->GetPosition()) <= QCast.Range) {
							if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
							{
								QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(QCast.Range) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && QCast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, target->GetCharName()) && Game::Time() > QCastOffset) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					if (Player.Distance(&target->GetPosition()) <= QCast.Range) {
						if (QCast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
						{
							QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode) && QCast.IsReady() && Game::Time() > QCastOffset) {
			std::vector<Vector3> waypoints = Player.GetWaypoints();

			if (!waypoints.empty()) {
				Vector3 point = waypoints.back();
				if (QCast.Cast(&point))
				{
					QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
				}
			}
		}
		break;
	}

	default:
		break;
	}
}

void NocturneLogicManager::CastW(OrbwalkingMode orbwalkingMode)
{
	if (NOC_SHOULD_CAST_SHIELD && Player.IsAlive() && Player.IsValid() && WCast.IsReady())
	{
		WCast.Cast();
	}
	else {
		NOC_SHOULD_CAST_SHIELD = false;
	}
}

void NocturneLogicManager::CastE(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(ECast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && ECast.IsReady())
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, enemy->GetCharName()))
				{
					ECast.Cast(enemy);
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(ECast.Range) };
			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && target->IsMonster() && ECast.IsReady()) {
				ECast.Cast(target->AsAIBaseClient());
			}
		}
	}
	break;

	default:
		break;
	}
}

void NocturneLogicManager::CastR(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(RCast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && RCast.IsReady())
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::R, enemy->GetCharName()))
				{
					RCast.Cast(enemy);
				}
			}
		}
	}
	break;

	default:
		break;
	}
}

void NocturneLogicManager::KillSteal()
{
	if (BaseSettings::isQEnabledKs() && EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, OrbwalkingMode::Custom)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(QCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && QCast.IsReady() && Game::Time() > QCastOffset) {
				{
					float qDamage = GetQDamage(e.second->AsAIBaseClient());
					if (qDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::Q, e.second->GetCharName(), true)) {
							GetPrediction(OrbwalkingMode::Custom);
							if (Player.Distance(&e.second->GetPosition()) <= QCast.Range) {
								if (QCast.Cast(e.second))
								{
									QCastOffset = Game::Time() + BaseLogicManager::Q_Spell.CastDelay;
								}
							}
						}
					}
				}
			}
		}
	}

	if (BaseSettings::isREnabledKs() && EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, OrbwalkingMode::Custom)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(RCast.Range))
		{
			if ((EndeavourusMiscUtils::IsValidToCastSpell(e.second)) && RCast.IsReady()) {
				{
					float rDamage = GetRDamage(e.second->AsAIBaseClient());
					if (rDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::R, e.second->GetCharName(), true)) {
							RCast.Cast(e.second);
						}
					}
				}
			}
		}
	}
}

void NocturneLogicManager::RefreshSpells()
{
	SDK_SPELL rSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	if (rSpell.Level != BaseLogicManager::R_Spell.Level)
	{
		RefreshRSpell();
	}
}

void NocturneLogicManager::RefreshRSpell()
{
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
	RCast = Spell::Targeted{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, DamageType::Physical, false };
}

float NocturneLogicManager::GetQDamage(AIBaseClient* enemy)
{
	int qLevel = QCast.Level();
	if (qLevel != 0) {
		return pSDK->DamageLib->CalculatePhysicalDamage(Player.AsAIHeroClient(), enemy, QDamage[qLevel - 1] + (Player.GetAttackDamage() * QRatio));
	}

	return 0;
}

float NocturneLogicManager::GetEDamage(AIBaseClient* enemy)
{
	int eLevel = ECast.Level();
	if (eLevel != 0) {
		return pSDK->DamageLib->CalculateMagicalDamage(Player.AsAIHeroClient(), enemy, EDamage[eLevel - 1] + (Player.GetAbilityPower() * ERatio));
	}

	return 0;
}

float NocturneLogicManager::GetRDamage(AIBaseClient* enemy)
{
	int rLevel = RCast.Level();
	if (rLevel != 0) {
		return pSDK->DamageLib->CalculatePhysicalDamage(Player.AsAIHeroClient(), enemy, RDamage[rLevel - 1] + (Player.GetAttackDamage() * RRatio));
	}

	return 0;
}

void NocturneLogicManager::GetPrediction(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionCombo()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionCombo();
		}
	}
	break;

	case OrbwalkingMode::Mixed:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionHarass()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionHarass();
		}
	}
	break;

	case OrbwalkingMode::Custom:
	{
		if (QCast.MinimumHitChance != BaseSettings::getQPredictionKs()) {
			QCast.MinimumHitChance = BaseSettings::getQPredictionKs();
		}
	}
	break;

	default:
		break;
	}
}

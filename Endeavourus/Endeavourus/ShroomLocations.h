#pragma once
#include "SDK Extensions.h"

const Vector3 shroom1{ 2687.00f, 53.0f, 5390.0f };
const Vector3 shroom2{ 2598.0f, 57.0f, 6456.0f };
const Vector3 shroom3{ 2748.0f, 49.85f, 7284.0f };
const Vector3 shroom4{ 3360.0f, 52.76f, 7760.0f };
const Vector3 shroom5{ 2764.0f, 51.82f, 8174.0f };
const Vector3 shroom6{ 2764.0f, 51.82f, 8174.0f };
const Vector3 shroom7{ 2304.0f, 51.77f, 8936.0f };
const Vector3 shroom8{ 2322.0f, 53.27f, 9720.0f };
const Vector3 shroom9{ 2982.0f, 51.98f, 9654.0f };
const Vector3 shroom10{ 3528.0f, 43.13f, 9174.0f };
const Vector3 shroom11{ 3536.0f, 51.25f, 8380.0f };
const Vector3 shroom12{ 3104.0f, -67.81f, 10804.0f };
const Vector3 shroom13{ 2892.0f, -71.24f, 11338.0f };
const Vector3 shroom14{ 2140.0f, 52.83f, 11750.0f };
const Vector3 shroom15{ 1250.0f, 52.83f, 12442.0f };
const Vector3 shroom16{ 1554.0f, 52.83f, 12904.0f };
const Vector3 shroom17{ 2384.0f, 52.83f, 13516.0f };
const Vector3 shroom18{ 3216.0f, 52.83f, 12802.0f };
const Vector3 shroom19{ 2930.0f, 52.83f, 12374.0f };
const Vector3 shroom20{ 3382.0f, -67.11f, 11446.0f };
const Vector3 shroom21{ 3848.0f, -62.59f, 11330.0 };
const Vector3 shroom22{ 4474.0f, 56.84f, 11762.0f };
const Vector3 shroom23{ 5666.0f, 52.83f, 12688.0f };
const Vector3 shroom24{ 6760.0f, 56.47f, 12822.0f };
const Vector3 shroom25{ 7160.0f, 52.83f, 14104.0f };
const Vector3 shroom26{ 8764.0f, 55.99f, 12846.0f };
const Vector3 shroom27{ 7902.0f, 56.47f, 11884.0f };
const Vector3 shroom28{ 8434.0f, 55.90f, 11786.0f };
const Vector3 shroom29{ 9382.0f, 52.60f, 11352.0f };
const Vector3 shroom30{ 8832.0f, 52.60f, 11352.0f };
const Vector3 shroom31{ 8833.0f, 50.52f, 10688.0f };
const Vector3 shroom32{ 8252.0f, 49.82f, 10242.0f };
const Vector3 shroom33{ 7298.0f, 52.65f, 9898.0f };
const Vector3 shroom34{ 7660.0f, 53.01f, 11054.0f };
const Vector3 shroom35{ 6912.0f, 53.99f, 11454.0f };
const Vector3 shroom36{ 6002.0f, 55.69f, 11048.0f };
const Vector3 shroom37{ 6286.0f, 54.01f, 10156.0f };
const Vector3 shroom38{ 6732.0f, 54.49f, 9554.0f };
const Vector3 shroom39{ 6698.0f, -71.24f, 8434.0f };
const Vector3 shroom40{ 6410.0f, -69.59f, 8222.0f };
const Vector3 shroom41{ 4766.0f, -71.22f, 9954.0f };
const Vector3 shroom42{ 4588.0f, -71.24f, 10126.0f };
const Vector3 shroom43{ 4678.0f, -71.24f, 10636.0f };
const Vector3 shroom44{ 5270.0f, -71.24f, 10682.0f };
const Vector3 shroom45{ 5232.0f, -71.24f, 10214.0f };
const Vector3 shroom46{ 4692.0f, 51.15f, 7958.0f };
const Vector3 shroom47{ 5120.0f, 50.79f, 7654.0f };
const Vector3 shroom48{ 5884.0f, 51.65f, 7324.0f };
const Vector3 shroom49{ 4756.0f, 50.80f, 6928.0f };
const Vector3 shroom50{ 4700.0f, 51.96f, 6040.0f };
const Vector3 shroom51{ 3260.0f, 51.61f, 6838.0f };
const Vector3 shroom52{ 3738.0f, 50.86f, 7218.0f };
const Vector3 shroom53{ 5572.0f, 51.42f, 3546.0f };
const Vector3 shroom54{ 6540.0f, 48.52f, 4686.0f };
const Vector3 shroom55{ 6890.0f, 48.52f, 3990.0f };
const Vector3 shroom56{ 7504.0f, 52.60f, 3344.0f };
const Vector3 shroom57{ 6598.0f, 50.08f, 3106.0f };
const Vector3 shroom58{ 6106.0f, 52.13f, 2382.0f };
const Vector3 shroom59{ 7548.0f, 51.95f, 2406.0f };
const Vector3 shroom60{ 8038.0f, 50.72f, 1946.0f };
const Vector3 shroom61{ 8672.0f, 50.78f, 2340.0f };
const Vector3 shroom62{ 9206.0f, 58.64f, 2140.0f };
const Vector3 shroom63{ 9688.0f, 49.22f, 2542.0f };
const Vector3 shroom64{ 10580.0f, 49.22f, 2478.0f };
const Vector3 shroom65{ 11458.0f, 49.48f, 1874.0f };
const Vector3 shroom66{ 12246.0f, 51.30f, 1316.0f };
const Vector3 shroom67{ 12786.0f, 51.91f, 1800.0f };
const Vector3 shroom68{ 13164.0f, 51.36f, 2184.0f };
const Vector3 shroom69{ 13506.0f, 51.36f, 2788.0f };
const Vector3 shroom70{ 12668.0f, 51.36f, 3164.0f };
const Vector3 shroom71{ 11842.0f, -41.71f, 3158.0f };
const Vector3 shroom72{ 11936.0f, -66.24f, 3704.0f };
const Vector3 shroom73{ 11388.0f, -71.24f, 3716.0f };
const Vector3 shroom74{ 11294.0f, -68.34f, 3194.0f };
const Vector3 shroom75{ 10768.0f, 14.12f, 3424.0f };
const Vector3 shroom76{ 10380.0f, 49.61f, 3038.0f };
const Vector3 shroom77{ 9538.0f, 54.19f, 3316.0f };
const Vector3 shroom78{ 9074.0f, 54.46f, 3536.0f };
const Vector3 shroom79{ 8280.0f, 53.55f, 3568.0f };
const Vector3 shroom80{ 8818.0f, 53.01f, 4050.0f };
const Vector3 shroom81{ 8280.0f, 53.55f, 3568.0f };
const Vector3 shroom82{ 8658.0f, 51.97f, 4592.0f };
const Vector3 shroom83{ 7444.0f, 48.52f, 4988.0f };
const Vector3 shroom84{ 7900.0f, 52.79f, 5642.0f };
const Vector3 shroom85{ 7194.0f, 52.45f, 6204.0f };
const Vector3 shroom86{ 8596.0f, -51.38f, 5562.0f };
const Vector3 shroom87{ 8232.0f, -71.24f, 6336.0f };
const Vector3 shroom88{ 8586.0f, -71.24f, 6620.0f };
const Vector3 shroom89{ 9386.0f, -71.24f, 5562.0f };
const Vector3 shroom90{ 10094.0f, -71.24f, 4862.0f };
const Vector3 shroom91{ 10276.0f, -71.24f, 4750.0f };
const Vector3 shroom92{ 10040.0f, -71.24f, 4666.0f };
const Vector3 shroom93{ 9524.0f, -71.24f, 4622.0f };
const Vector3 shroom94{ 9650.0f, -71.24f, 4158.0f };
const Vector3 shroom95{ 10044.0f, -71.24f, 4092.0f };
const Vector3 shroom96{ 11246.0f, -59.61f, 5362.0f };
const Vector3 shroom97{ 11016.0f, -67.62f, 5494.0f };
const Vector3 shroom98{ 11612.0f, 50.0f, 5806.0f };
const Vector3 shroom99{ 12036.0f, 51.77f, 4940.0f };
const Vector3 shroom100{ 12520.0f, 51.72f, 5194.0f };
const Vector3 shroom101{ 11282.0f, 51.44f, 6342.0f };
const Vector3 shroom102{ 11520.0f, 51.72f, 7090.0f };
const Vector3 shroom103{ 12080.0f, 51.75f, 6682.0f };
const Vector3 shroom104{ 9786.0f, -2.49f, 6402.0f };
const Vector3 shroom105{ 11954.0f, 52.17f, 7464.0f };
const Vector3 shroom106{ 12706.0f, 51.68f, 7244.0f };
const Vector3 shroom107{ 12210.0f, 52.27f, 8614.0f };
//const Vector3 shroom108{ 9964.0, 51.98, 7118.0 }; - Duplicate
const Vector3 shroom109{ 9152.0f, 52.96f, 7344.0f };
const Vector3 shroom110{ 10042.0f, 51.97f, 7256.0f };
const Vector3 shroom111{ 10008.0f, 51.74f, 7836.0f };
const Vector3 shroom112{ 10160.0f, 50.15f, 8800.0f };
const Vector3 shroom113{ 10806.0f, 64.60f, 8910.0f };
const Vector3 shroom114{ 11538.0f, 54.82f, 8208.0f };
const Vector3 shroom115{ 11230.0f, 52.21f, 7648.0f };
const Vector3 shroom116{ 10654.0f, 51.79f, 7600.0f };

static const std::vector<Vector3> SHROOM_LOCATIONS{ shroom1, shroom2, shroom3, shroom4, shroom5, shroom6, shroom7, shroom8, shroom9, shroom10, shroom11, shroom12,
shroom13, shroom14, shroom15, shroom16, shroom17, shroom18, shroom19, shroom20, shroom21, shroom22, shroom23, shroom24, shroom25, shroom26, shroom27,
shroom28, shroom29, shroom30, shroom31, shroom32, shroom33, shroom34, shroom35, shroom36, shroom37, shroom38, shroom39, shroom40, shroom41, shroom42,
shroom43, shroom44, shroom45, shroom46, shroom47, shroom48, shroom49, shroom50, shroom51, shroom52, shroom53, shroom54, shroom55, shroom56, shroom57,
shroom58, shroom59, shroom60, shroom61, shroom62, shroom63, shroom64, shroom65, shroom66, shroom67, shroom68, shroom69, shroom70, shroom71, shroom72,
shroom73, shroom74, shroom75, shroom76, shroom77, shroom78, shroom79, shroom80, shroom81, shroom82, shroom83, shroom84, shroom85, shroom86, shroom87,
shroom88, shroom89, shroom90, shroom91, shroom92, shroom93, shroom94, shroom95, shroom96, shroom97, shroom98, shroom99, shroom100, shroom101, shroom102,
shroom103, shroom104, shroom105, shroom106, shroom107, shroom109, shroom110, shroom111, shroom112, shroom113, shroom114, shroom115, shroom116 };
#include "stdafx.h"
#include "TeemoLogicManager.h"
#include "ShroomManager.h"
#include "BaseLogicManager.h"
#include "EndeavourusMiscUtils.h"
#include "BaseMenuManager.h"
#include "BaseSettings.h"
#include "TeemoSettings.h"
#include "EndeavourusLogicUtils.h"

Spell::Targeted TeemoLogicManager::QCast{ SpellSlot::Q, 0 };
Spell::Active TeemoLogicManager::WCast{ SpellSlot::W };
Spell::Skillshot TeemoLogicManager::RCast{ SpellSlot::R, 0, SkillshotType::Line };

std::vector<int> QDamage{ 80, 125, 170, 215, 260 };
float QRatio{ 0.80f };
float TeemoLogicManager::R_COOLDOWN{ 0 };

void TeemoLogicManager::Init()
{
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));

	WCast = Spell::Active{ SpellSlot::W };
	RefreshQSpell();
	RefreshRSpell();
}

void TeemoLogicManager::PerformLogic()
{
	RefreshSpells();

	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();

	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		PerformCombo(orbwalkingMode);
		break;

	case OrbwalkingMode::Flee:
		PerformEscape(orbwalkingMode);
		break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		PerformClear(orbwalkingMode);
		break;

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		PerformLastHit(orbwalkingMode);
		break;

	case OrbwalkingMode::Mixed:
		PerformHarass(orbwalkingMode);
		break;

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		break;
	default:
		break;
	}

	KillSteal();
	AutoRImmobile();
}

void TeemoLogicManager::RefreshSpells()
{
	SDK_SPELL rSpell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	if (rSpell.Level != BaseLogicManager::R_Spell.Level)
	{
		RefreshRSpell();
	}
}

void TeemoLogicManager::RefreshQSpell()
{
	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	QCast = Spell::Targeted{ SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange, DamageType::Magical };
}

void TeemoLogicManager::RefreshRSpell()
{
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
	RCast = Spell::Skillshot{ SpellSlot::R, BaseLogicManager::R_Spell.CastRange, SkillshotType::Circle, 0.25f, FLT_MAX, BaseLogicManager::R_Spell.LineWidth, DamageType::Magical, false };
	RCast.MinimumHitChance = HitChance::Immobile;
}

float TeemoLogicManager::GetQDamage(AIBaseClient* enemy)
{
	int qLevel = QCast.Level();
	if (qLevel != 0) {
		return pSDK->DamageLib->CalculateMagicalDamage(Player.AsAIHeroClient(), enemy, QDamage[qLevel - 1] + (Player.GetAbilityPower() * QRatio));
	}

	return 0;
}

void TeemoLogicManager::PerformCombo(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetComboResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void TeemoLogicManager::PerformHarass(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetHarassResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void TeemoLogicManager::PerformClear(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetClearResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void TeemoLogicManager::PerformLastHit(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetLastHitResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void TeemoLogicManager::PerformEscape(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetEscapeResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void TeemoLogicManager::CastQ(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		//Only called on post attack
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(QCast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && QCast.IsReady())
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, enemy->GetCharName())) {
					pCore->TS->ForceTarget(enemy);
					QCast.Cast(enemy);
				}
			}
		}
	}
	break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::LastHit:
	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::JungleClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode)) {
			bool castedQ{ false };
			if (QCast.IsReady()) {
				for (auto e : pSDK->EntityManager->GetJungleMonsters(BaseLogicManager::Q_Spell.CastRange)) {
					if (e.second->IsMinion() || e.second->IsMonster()) {

						float qDamage = GetQDamage(e.second->AsAIBaseClient());
						if (qDamage > e.second->GetHealth().Current)
						{
							if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, e.second->GetCharName())) {
								QCast.Cast(e.second->AsAIBaseClient());
								castedQ = true;
							}
						}
					}
				}

				if (!castedQ) {
					for (auto e : pSDK->EntityManager->GetEnemyMinions(BaseLogicManager::Q_Spell.CastRange)) {
						if (e.second->IsMinion() || e.second->IsMonster()) {

							float qDamage = GetQDamage(e.second->AsAIBaseClient());
							if (qDamage > e.second->GetHealth().Current)
							{
								if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::Q, e.second->GetCharName())) {
									QCast.Cast(e.second->AsAIBaseClient());
								}
							}
						}
					}
				}
			}
		}
	}
	break;

	default:
		break;
	}
}

void TeemoLogicManager::CastW(OrbwalkingMode orbwalkingMode)
{
	if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode))
	{
		if (Player.IsAlive() && Player.IsValid() && WCast.IsReady()) {
			WCast.Cast();
		}
	}
}

/**
 * No logic has been good so taken this out and added shroom on immobile and flee
 */
void TeemoLogicManager::CastR(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::R, orbwalkingMode))
		{
			if (Player.IsAlive() && Player.IsValid() && RCast.IsReady() && Game::Time() > R_COOLDOWN)
			{
				if (RCast.Cast(&Player.GetPosition()))
				{
					R_COOLDOWN = Game::Time() + TEEMO_R_PLACEMENT_TIME_OFFSET;
				}
			}
		}
	}
	break;

	default:
		break;
	}
}

void TeemoLogicManager::KillSteal()
{
	if (BaseSettings::isQEnabledKs() && EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, OrbwalkingMode::Custom)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(QCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && QCast.IsReady()) {
				{
					float qDamage = GetQDamage(e.second->AsAIBaseClient());
					if (qDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::Q, e.second->GetCharName(), true)) {
							QCast.Cast(e.second);
						}
					}
				}
			}
		}
	}
}

void TeemoLogicManager::AutoRImmobile()
{
	if (TeemoSettings::isAutoRImmobileChampion()) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(RCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && RCast.IsReady() && Game::Time() > R_COOLDOWN) {
				//pCore->TS->ForceTarget(e.second);
				if (e.second->IsImmobile() || !e.second->CanProcessMove()) {
					if (RCast.Cast(e.second)) {
						R_COOLDOWN = Game::Time() + TEEMO_R_PLACEMENT_TIME_OFFSET;
					}
				}
			}
		}
	}
}
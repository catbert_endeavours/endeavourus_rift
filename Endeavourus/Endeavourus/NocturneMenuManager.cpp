#include "stdafx.h"
#include "NocturneMenuManager.h"
#include "LogicConstants.h"
#include "Resource.h"
#include "DrawConstants.h"
#include "EndeavourusMiscUtils.h"
#include "NocturneConstants.h"
#include "SpellShieldConstants.h"
#include "SpellShieldMenuManager.h"
#include "EndeavourusMenu.h"

void NocturneMenuManager::Init()
{
	specificChampLogic = SpecificChampMenuLogic;

	comboResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::High, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, true, false, EndeavourusHitChance::Medium, true,
		false, true, false, EndeavourusHitChance::Medium);
	harassResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, true, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	clearResourceSettings = *new Resource(true, true, true, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	lastHitResourceSettings = *new Resource(true, true, false, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium, 40, 40, 40, 40);
	escapeResourceSettings = *new Resource(true, true, true, false, false, EndeavourusHitChance::Medium, false, false, false, false,
		EndeavourusHitChance::Medium, true, true, true, false, EndeavourusHitChance::Medium, false, false,
		false, false, EndeavourusHitChance::Medium);
	ksResourceSettings = *new Resource(true, true, true, true, true, EndeavourusHitChance::High, false, false, false, false,
		EndeavourusHitChance::Medium, false, false, false, false, EndeavourusHitChance::Medium, true, false,
		true, false, EndeavourusHitChance::Medium);
}

void NocturneMenuManager::DrawMenu()
{
	BaseMenuManager::DrawMenu();
}

void NocturneMenuManager::NocturneSpecificSpellLogic()
{
	EndeavourusMenu::Tree("Spell Shield Settings", EndeavourusMiscUtils::GetUniqueSettingName(SPELL_SHIELD_PARENT_GROUP), false, SpellShieldMenuManager::DrawSpellShieldSettings);
}

void NocturneMenuManager::SpecificChampMenuLogic()
{
	EndeavourusMenu::Tree("Nocturne Specific Spell Settings", EndeavourusMiscUtils::GetUniqueSettingName(NOCTURNE_LOGIC_SETTING_GROUP), false, NocturneSpecificSpellLogic);
	EndeavourusMenu::Tree("Drawings", EndeavourusMiscUtils::GetUniqueSettingName(DRAWING_GROUP), false, NocturneDrawingLogic);
}

void NocturneMenuManager::NocturneDrawingLogic()
{
	DrawQRange();
	DrawERange();
	DrawRRange();
}
#include "stdafx.h"
#include "ShacoLogicManager.h"
#include "SDK Extensions.h"
#include "BaseLogicManager.h"
#include "EndeavourusMiscUtils.h"
#include "BaseMenuManager.h"
#include "EndeavourusLogicUtils.h"
#include "ShacoConstants.h"
#include "JungleConstants.h"
#include "BaseSettings.h"
#include "ItemLogicManager.h"
#include "ShacoDeceiveBackStabLogicManager.h"
#include "ShacoCloneLogicManager.h"

Spell::Targeted ShacoLogicManager::QCast{ SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange };
Spell::Targeted ShacoLogicManager::WCast{ SpellSlot::W, BaseLogicManager::W_Spell.CastRange, DamageType::Magical };
Spell::Targeted ShacoLogicManager::ECast{ SpellSlot::E, BaseLogicManager::E_Spell.CastRange, DamageType::Magical };
Spell::Active ShacoLogicManager::RCast{ SpellSlot::R };

float ShacoLogicManager::QCastOffset{ 0 };
float ShacoLogicManager::WCastOffset{ 0 };
float ShacoLogicManager::ECastOffset{ 0 };
float ShacoLogicManager::RCastOffset{ 0 };

std::vector<float> ShacoLogicManager::EDamage{ 55.0f, 80.0f, 105.0f, 130.0f, 155.0f };
std::vector<float> ShacoLogicManager::EAdRatio{ 0.60f, 0.75f, 0.90f, 1.05f, 1.20f };
float ShacoLogicManager::EAPRatio{ 0.75f };

float ShacoLogicManager::DECEIVE_TIMER{ 0.0f };
bool ShacoLogicManager::SHACO_SHOULD_CAST_CLONE{};

float ShacoLogicManager::EOutOfRangeTimer{ FLT_MAX };

bool ShacoLogicManager::PERFORM_SMOOTH_R_COMBO{ false };

void ShacoLogicManager::Init()
{
	pSDK->EventHandler->RegisterCallback(CallbackEnum::Update, OnUpdate);

	BaseLogicManager::Q_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::Q));
	BaseLogicManager::W_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
	BaseLogicManager::E_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
	BaseLogicManager::R_Spell = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));

	QCast = { SpellSlot::Q, BaseLogicManager::Q_Spell.CastRange };
	WCast = { SpellSlot::W, BaseLogicManager::W_Spell.CastRange, DamageType::Magical };
	ECast = { SpellSlot::E, BaseLogicManager::E_Spell.CastRange, DamageType::Magical };
	RCast = { SpellSlot::R };
}

void __cdecl ShacoLogicManager::OnUpdate(void * UserData)
{
	PerformSmoothRCombo();
	PerformSmoothMundoCombo();
	CastR(OrbwalkingMode::Custom);
}

void ShacoLogicManager::PerformLogic()
{
	RefreshSpells();
	UpdateDeceiveTimer();

	OrbwalkingMode orbwalkingMode = EndeavourusMiscUtils::GetOrbwalkingMode();
	switch (orbwalkingMode) {
	case OrbwalkingMode::Combo:
		PerformCombo(orbwalkingMode);
		break;

	case OrbwalkingMode::Flee:
		PerformEscape(orbwalkingMode);
		break;

	case OrbwalkingMode::LaneClear:
	case OrbwalkingMode::JungleClear:
		PerformClear(orbwalkingMode);
		break;

	case OrbwalkingMode::Freeze:
	case OrbwalkingMode::LastHit:
		PerformLastHit(orbwalkingMode);
		break;

	case OrbwalkingMode::Mixed:
		PerformHarass(orbwalkingMode);
		break;

	case OrbwalkingMode::Custom:
	case OrbwalkingMode::None:
		break;
	default:
		break;
	}

	KillSteal();
	AutoWImmobile();
}

void ShacoLogicManager::PerformCombo(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetComboResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetComboResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void ShacoLogicManager::PerformHarass(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetHarassResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isEEnabled())
	{
		//CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetHarassResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void ShacoLogicManager::PerformClear(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetClearResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetClearResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void ShacoLogicManager::PerformLastHit(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetLastHitResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isEEnabled())
	{
		//CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetLastHitResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void ShacoLogicManager::PerformEscape(OrbwalkingMode orbwalkingMode)
{
	if (BaseMenuManager::GetEscapeResourceManagement().isQEnabled())
	{
		CastQ(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isWEnabled())
	{
		CastW(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isEEnabled())
	{
		CastE(orbwalkingMode);
	}

	if (BaseMenuManager::GetEscapeResourceManagement().isREnabled())
	{
		CastR(orbwalkingMode);
	}
}

void ShacoLogicManager::RefreshSpells()
{

}

void ShacoLogicManager::CastQ(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::Q, orbwalkingMode))
		{
			if (Player.IsAlive() && Player.IsValid() && QCast.IsReady() && Game::Time() > QCastOffset)
			{
				Vector3 mousePos{ Renderer::MousePos() };
				Vector3 qCastPos{ Player.GetPosition().Extended(mousePos, QCast.Range) };

				if (QCast.Cast(&qCastPos))
				{
					QCastOffset = Game::Time() + QCast.Delay;
				}
			}
		}
	}
	break;

	default:
		break;
	}
}

void ShacoLogicManager::CastW(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 4 || orbwalkingMode == OrbwalkingMode::Mixed) {

			if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode))
			{
				AIHeroClient* enemy = pCore->TS->GetTarget(WCast.Range);
				if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && WCast.IsReady() && Game::Time() > WCastOffset)
				{
					if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::W, enemy->GetCharName()))
					{
						if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy) && Player.CanCast()) {
							if ((EndeavourusMiscUtils::GetBoolSetting(SHACO_W_BLOCK_CAST_MOVING_TARGET, true) && !enemy->IsMoving()) ||
								!EndeavourusMiscUtils::GetBoolSetting(SHACO_W_BLOCK_CAST_MOVING_TARGET, true)) {
								Vector3 castPos{ GetWPosition(enemy) };

								if (castPos.x != 0.0f || castPos.x != 0) {
									if (WCast.Cast(&castPos))
									{
										WCastOffset = Game::Time() + 0.25f;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(WCast.Range) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && WCast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::W, target->GetCharName()) && Game::Time() > WCastOffset) {

				if ((target->IsMonster() && target->GetHealthPercent() > 25 && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_W_JUNGLE, true) && EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_W_LOW_HEALTH, true))
					|| (target->IsMonster() && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_W_JUNGLE, true) && !EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_W_LOW_HEALTH, true))) {
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target) && Player.CanCast()) {
						std::string creepName{ target->GetName() };
						if (creepName.find(CRAB) != std::string::npos)
						{
							return;
						}
						Vector3 p{};
						for (auto e : pSDK->EntityManager->GetJungleMonsters(WCast.Range)) {
							if (e.second->GetNetworkID() == target->GetNetworkID())
							{
								p = GetPositionAroundMinion(e, -100);
							}
						}

						if (Player.Distance(&p) <= WCast.Range) {
							if (WCast.Cast(&p))
							{
								WCastOffset = Game::Time() + 0.25f;
							}
						}
					}
				}
				else if ((target->IsMinion() && target->GetHealthPercent() > 25 && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_W_MINION, false) && EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_W_LOW_HEALTH, true))
					|| (target->IsMinion() && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_W_MINION, false) && !EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_W_LOW_HEALTH, true))) {
					Vector3 castPos{};
					for (auto e : pSDK->EntityManager->GetEnemyMinions(WCast.Range)) {
						if (e.second->GetNetworkID() == target->GetNetworkID())
						{
							castPos = GetPositionAroundMinion(e, -50);
						}
					}

					if (Player.Distance(&castPos) <= WCast.Range && Player.CanCast()) {
						if (WCast.Cast(&castPos))
						{
							WCastOffset = Game::Time() + 0.25f;
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode))
		{
			if (Player.IsAlive() && Player.IsValid() && WCast.IsReady() && Game::Time() > WCastOffset)
			{
				if (WCast.Cast(&Player.GetPosition()))
				{
					WCastOffset = Game::Time() + WCast.Delay;
				}
			}
		}
	}

	default:
		break;
	}
}

void ShacoLogicManager::CastE(OrbwalkingMode orbwalkingMode)
{
	switch (orbwalkingMode)
	{
	case OrbwalkingMode::Combo:
	case OrbwalkingMode::Mixed:
	{
		if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 4 || orbwalkingMode == OrbwalkingMode::Mixed) {

			if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode))
			{
				AIHeroClient* enemy = pCore->TS->GetTarget(ECast.Range);
				if (!Player.HasBuff("Deceive")) {
					if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && ECast.IsReady() && Game::Time() > ECastOffset)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, enemy->GetCharName()))
						{
							if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
								if (Player.Distance(&enemy->GetPosition()) <= ECast.Range && Player.CanCast() && Game::Time() > WCastOffset) {
									if (ECast.Cast(enemy))
									{
										ECastOffset = Game::Time() + 0.25f;
									}


									/**
									if (EndeavourusMiscUtils::GetBoolSetting(SHACO_E_AA_RANGE_CHECK, false)) {
										if (Player.Distance(enemy) > Player.GetAttackRange()) {
											if (ECast.Cast(enemy))
											{
												ECastOffset = Game::Time() + ECast.Delay;
											}
										}
									}
									*/

								}
							}
						}
					}
				}
			}
		}
		else if (ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 0 || ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 1 ||
			ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 2 || ShacoCloneLogicManager::SHACO_COMBO_OPTION_INDEX == 3) {

			if (EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_E_OUT_OF_COMBO, false)) {
				GameObject* enemyHero = pCore->Orbwalker->GetTarget();
				if (enemyHero == nullptr && !Player.HasBuff("Deceive") && Game::Time() < EOutOfRangeTimer) {
					EOutOfRangeTimer = Game::Time() + 2.5f;
				}
				else {
					EOutOfRangeTimer = FLT_MAX;
				}


				if (ECast.IsReady() && EOutOfRangeTimer != FLT_MAX && EOutOfRangeTimer > Game::Time() + 2.0f && !ItemLogicManager::ShouldCastShacoEBeforeAA) {
					if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode))
					{
						AIHeroClient* enemy = pCore->TS->GetTarget(ECast.Range);
						if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && ECast.IsReady() && Game::Time() > ECastOffset)
						{
							if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, enemy->GetCharName()))
							{
								if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
									if (Player.Distance(&enemy->GetPosition()) <= ECast.Range && Player.CanCast()) {
										if (ECast.Cast(enemy))
										{
											ECastOffset = Game::Time() + 0.25f;
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::LastHit:
	case OrbwalkingMode::Freeze:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(ECast.Range, true) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && ECast.IsReady() && Game::Time() > ECastOffset &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, target->GetCharName())) {
				if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
					float eDamage = GetEDamage(target->AsAIBaseClient());
					if (eDamage > target->GetHealth().Current)
					{
						if (Player.Distance(&target->AsAIBaseClient()->GetPosition()) <= ECast.Range) {
							if (ECast.Cast(&target->AsAIBaseClient()->GetServerPosition()) && Player.CanCast())
							{
								ECastOffset = Game::Time() + 0.25f;
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::JungleClear:
	case OrbwalkingMode::LaneClear:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode)) {
			AIBaseClient* target{ EndeavourusMiscUtils::GetJungleMinionTarget(ECast.Range, false, true) };

			if (target != nullptr && EndeavourusMiscUtils::IsValidToCastSpell(target->AsAIBaseClient()) && (target->IsMinion() || target->IsMonster()) && ECast.IsReady() &&
				EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, target->GetCharName()) && Game::Time() > ECastOffset) {

				float eDamage = GetEDamage(target->AsAIBaseClient());
				bool killable = eDamage > target->GetHealth().Current;
				if (target->IsMonster()) {
					if ((EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_E_JUNGLE, true) && !EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_E_LOW_HEALTH, true)) ||
						(EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_E_JUNGLE, true) && EndeavourusMiscUtils::GetBoolSetting(SHACO_BLOCK_E_LOW_HEALTH, true) && target->GetHealthPercent() > 25.0f) ||
						(killable && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_E_JUNGLE, true))) {
						if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target) && Player.CanCast()) {
							if (Player.Distance(&target->AsAIBaseClient()->GetPosition()) <= ECast.Range && Game::Time() > WCastOffset) {
								if (ECast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
								{
									ECastOffset = Game::Time() + 0.25f;
								}
							}
						}
					}
				}
				else if (target->IsMinion()) {
					if (killable && EndeavourusMiscUtils::GetBoolSetting(SHACO_ENABLE_CAST_E_MINION, true) && Player.CanCast()) {
						if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(target)) {
							if (Player.Distance(&target->AsAIBaseClient()->GetPosition()) <= ECast.Range) {
								if (ECast.Cast(&target->AsAIBaseClient()->GetServerPosition()))
								{
									ECastOffset = Game::Time() + 0.25f;
								}
							}
						}
					}
				}
			}
		}
	}
	break;

	case OrbwalkingMode::Flee:
	{
		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode))
		{
			AIHeroClient* enemy = pCore->TS->GetTarget(ECast.Range);
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && ECast.IsReady() && Game::Time() > ECastOffset)
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, enemy->GetCharName()))
				{
					if (EndeavourusMiscUtils::CanCastSpellSmoothInCombo(enemy)) {
						if (Player.Distance(&enemy->GetPosition()) <= ECast.Range) {
							if (ECast.Cast(enemy))
							{
								ECastOffset = Game::Time() + 0.25f;
							}
						}
					}
				}
			}
		}
	}

	default:
		break;
	}
}

void ShacoLogicManager::CastR(OrbwalkingMode orbwalkingMode)
{
	if (SHACO_SHOULD_CAST_CLONE && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED && Player.IsAlive() && Player.IsValid() && RCast.IsReady())
	{
		if (Game::Time() > RCastOffset) {
			if (RCast.Cast()) {
				RCastOffset = Game::Time() + 0.40f;
			}
		}
	}
	else {
		SHACO_SHOULD_CAST_CLONE = false;
	}
}

void ShacoLogicManager::KillSteal()
{
	if (BaseSettings::isEEnabledKs() && EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, OrbwalkingMode::Custom)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(ECast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && ECast.IsReady() && Game::Time() > ECastOffset) {
				{
					float eDamage = GetEDamage(e.second->AsAIBaseClient());
					if (eDamage > e.second->GetHealth().Current)
					{
						if (EndeavourusLogicUtils::CanCastSpellOnChampion(OrbwalkingMode::Custom, SpellSlot::E, e.second->GetCharName(), true)) {
							if (ECast.Cast(e.second)) {
								ECastOffset = Game::Time() + 0.25f;
							}
						}
					}
				}
			}
		}
	}
}

float ShacoLogicManager::GetEDamage(AIBaseClient * enemy)
{
	int eLevel = ECast.Level();
	if (enemy != nullptr && eLevel != 0)
	{
		float dmg{};

		float adRatio = EAdRatio[eLevel - 1];
		float shacoEAdDamage = Player.GetBonusAttackDamage() * adRatio;
		float shacoEApDamage = Player.GetAbilityPower() * EAPRatio;
		float totalDamage = EDamage[eLevel - 1] + shacoEAdDamage + shacoEApDamage;
		dmg = pSDK->DamageLib->CalculatePhysicalDamage(Player.AsAIHeroClient(), enemy, totalDamage);

		return dmg;
	}

	return 0.0f;
}

Vector3 ShacoLogicManager::GetPositionAroundMinion(std::pair<int, AIMinionClient*> enemy, float distance) {
	SDKVECTOR enemyDirection{ enemy.second->GetDirection() };
	SDKVECTOR enemyPosition{ enemy.second->GetPosition() };
	SDKVECTOR enemyFacingPosition = enemyDirection + enemyPosition;

	Vector3 castPosition{ enemy.second->GetPosition().Extended(enemyFacingPosition, distance) };

	return castPosition;
}

Vector3 ShacoLogicManager::GetPositionAroundHero(std::pair<int, AIHeroClient*> enemy, float distance) {
	SDKVECTOR enemyDirection{ enemy.second->GetDirection() };
	SDKVECTOR enemyPosition{ enemy.second->GetPosition() };
	SDKVECTOR enemyFacingPosition = enemyDirection + enemyPosition;

	Vector3 castPosition{ enemy.second->GetPosition().Extended(enemyFacingPosition, distance) };

	return castPosition;
}

void ShacoLogicManager::UpdateDeceiveTimer() {
	if (Player.HasBuff("Deceive")) {
		BuffInstance b = Player.GetBuff("Deceive");
		if (Player.IsValid() && Player.IsAlive() && !Player.IsZombie() && Player.IsVisible()) {
			float timeLeft{ b.EndTime - Game::Time() };
			float v = ((float)((int)(timeLeft * 100))) / 100;

			DECEIVE_TIMER = v;
		}
	}
	else {
		DECEIVE_TIMER = 0.0f;
	}
}

Vector3 ShacoLogicManager::GetWPosition(AIHeroClient* hero) {
	int mode = EndeavourusMiscUtils::GetDropDownSetting(SHACO_W_OPTIONS, 0);
	bool castPositionSet{ false };
	Vector3 castPos{};
	/**
	if (mode == 0) {
		if (hero->IsMoving() && hero->IsFacing(&Player) && !Player.HasBuff("Deceive")) {
			// Moving towards me - Place infront
			std::pair<int, AIHeroClient*> h{};
			for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
				if (e.second->GetNetworkID() == hero->GetNetworkID()) {
					h = e;
					break;
				}
			}
			Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, 125.0f) };
			Vector3 pos2{ ShacoLogicManager::GetPositionAroundHero(h, 100.0f) };
			Vector3 pos3{ ShacoLogicManager::GetPositionAroundHero(h, 50.0f) };
			if (Player.Distance(pos1) < WCast.Range) {
				castPositionSet = true;
				castPos = pos1;
			}
			else if (Player.Distance(pos2) < WCast.Range) {
				castPositionSet = true;
				castPos = pos2;
			}
			else if (Player.Distance(pos3) < WCast.Range) {
				castPositionSet = true;
				castPos = pos3;
			}
		}
		else if (hero->IsMoving() && !hero->IsFacing(&Player) && !Player.HasBuff("Deceive")) {
			std::pair<int, AIHeroClient*> h{};
			for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
				if (e.second->GetNetworkID() == hero->GetNetworkID()) {
					h = e;
					break;
				}
			}

			//Enemy is running away from me so only cast W at max front range
			Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, 125.0f) };
			if (Player.Distance(pos1) < WCast.Range) {
				castPositionSet = true;
				castPos = pos1;
			}
		}
		else if (Player.HasBuff("Deceive") && hero->IsMoving() && hero->IsFacing(&Player)) {
			std::pair<int, AIHeroClient*> h{};
			for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
				if (e.second->GetNetworkID() == hero->GetNetworkID()) {
					h = e;
					break;
				}
			}

			//Wait for good w behind enemy
			Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, -125.0f) };

			if (Player.Distance(pos1) < WCast.Range) {
				castPositionSet = true;
				castPos = pos1;
			}
		}

		else if (Player.HasBuff("Deceive") && hero->IsMoving() && !hero->IsFacing(&Player)) {
			std::pair<int, AIHeroClient*> h{};
			for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
				if (e.second->GetNetworkID() == hero->GetNetworkID()) {
					h = e;
					break;
				}
			}

			//Wait for good w behind enemy
			Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, -125.0f) };

			if (Player.Distance(pos1) < WCast.Range) {
				castPositionSet = true;
				castPos = pos1;
			}
		}
		else {
			std::pair<int, AIHeroClient*> h{};
			for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
				if (e.second->GetNetworkID() == hero->GetNetworkID()) {
					h = e;
					break;
				}
			}

			Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, -125.0f) };
			Vector3 pos2{ ShacoLogicManager::GetPositionAroundHero(h, -100.0f) };
			Vector3 pos3{ ShacoLogicManager::GetPositionAroundHero(h, -50.0f) };

			if (Player.Distance(pos1) < WCast.Range) {
				castPositionSet = true;
				castPos = pos1;
			}
			else if (Player.Distance(pos2) < WCast.Range) {
				castPositionSet = true;
				castPos = pos2;
			}
			else if (Player.Distance(pos3) < WCast.Range) {
				castPositionSet = true;
				castPos = pos3;
			}
		}

		if (!castPositionSet) {
			castPos = Vector3{ hero->GetPosition() };
		}
	}
	*/
	if (mode == 0) {
		castPos = Vector3{ hero->GetPosition() };
	}
	else if (mode == 1) {
		std::pair<int, AIHeroClient*> h{};
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range)) {
			if (e.second->GetNetworkID() == hero->GetNetworkID()) {
				h = e;
				break;
			}
		}

		Vector3 pos1{ ShacoLogicManager::GetPositionAroundHero(h, -125.0f) };
		if (Player.Distance(pos1) < WCast.Range) {
			castPositionSet = true;
			castPos = pos1;
		}
		else {
			castPos.x = 0.0f;
		}
	}

	return castPos;
}

void ShacoLogicManager::AutoWImmobile()
{
	if (EndeavourusMiscUtils::GetBoolSetting(SHACO_W_CAST_IMMOBILE, true)) {
		for (auto e : pSDK->EntityManager->GetEnemyHeroes(WCast.Range))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(e.second) && WCast.IsReady() && Game::Time() > WCastOffset) {
				//pCore->TS->ForceTarget(e.second);
				if (e.second->IsImmobile() || !e.second->CanProcessMove()) {
					if (WCast.Cast(&e.second->GetPosition())) {
						WCastOffset = Game::Time() + WCast.Delay;
					}
				}
			}
		}
	}
}

bool ShacoLogicManager::CanCastW(OrbwalkingMode orbwalkingMode) {
	AIBaseClient* enemy;

	if (ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET != nullptr) {
		enemy = ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET;

		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::W, orbwalkingMode))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && WCast.IsReady())
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::W, enemy->GetCharName()))
				{
					return true;
				}
			}
		}
	}
	return false;
}

bool ShacoLogicManager::CanCastE(OrbwalkingMode orbwalkingMode) {
	AIBaseClient* enemy;

	if (ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET != nullptr) {
		enemy = ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET;

		if (EndeavourusMiscUtils::CanCastSpell(SpellSlot::E, orbwalkingMode))
		{
			if (EndeavourusMiscUtils::IsValidToCastSpell(enemy) && ECast.IsReady())
			{
				if (EndeavourusLogicUtils::CanCastSpellOnChampion(orbwalkingMode, SpellSlot::E, enemy->GetCharName()))
				{
					return true;
				}
			}
		}
	}

	return false;
}


void ShacoLogicManager::PerformSmoothRCombo()
{
	SDK_ABILITY_RESOURCE resource = Player.GetResource();

	SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
	Spell::Active RCast = { SpellSlot::R };

	if (PERFORM_SMOOTH_R_COMBO) {
		if (RCast.IsReady() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED && resource.Current >= RCast.ManaCost()) {
			if (Game::Time() > RCastOffset) {
				if (RCast.Cast()) {
					ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX = 0;
					RCastOffset = Game::Time() + 0.40f;
				}
			}
		}
		else {
			PERFORM_SMOOTH_R_COMBO = false;
		}
	}

	if (ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET == nullptr || !ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET->IsAlive()
		|| ShacoDeceiveBackStabLogicManager::TARGET_LAST_UPDATE < Game::Time()) {
		PERFORM_SMOOTH_R_COMBO = false;
		return;
	}

	/**
	if (PERFORM_SMOOTH_R_COMBO) {
		SDK_ABILITY_RESOURCE resource = Player.GetResource();

		if (ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET == nullptr || !ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET->IsAlive()) {
			PERFORM_SMOOTH_R_COMBO = false;
			return;
		}

		SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
		Spell::Active RCast = { SpellSlot::R };

		if (RCast.IsReady() && resource.Current >= RCast.ManaCost() && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED && ShacoDeceiveBackStabLogicManager::TARGET_LAST_UPDATE > Game::Time()) {
			ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX = 0; // Reset to attack player target
			if (Game::Time() > RCastOffset) {
				if (RCast.Cast()) {
					RCastOffset = Game::Time() + 0.05f;
				}
			}
		}
		else {
			PERFORM_SMOOTH_R_COMBO = false;
		}
	}
	*/
}



void ShacoLogicManager::PerformSmoothMundoCombo()
{
	SDK_ABILITY_RESOURCE resource = Player.GetResource();

	if (!PERFORM_SMOOTH_R_COMBO) {
		if (ItemLogicManager::ShouldCastShacoEBeforeAA || ItemLogicManager::ShouldCastShacoWBeforeAA || ItemLogicManager::ShouldCastShacoTiamatBeforeAA) {

			if (ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET == nullptr || !ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET->IsAlive()) {
				ItemLogicManager::ShouldCastShacoEBeforeAA = false;
				ItemLogicManager::ShouldCastShacoWBeforeAA = false;
				ItemLogicManager::ShouldCastShacoTiamatBeforeAA = false;
				return;
			}

			OrbwalkingMode orb = EndeavourusMiscUtils::GetOrbwalkingMode();

			SDK_SPELL R = Player.GetSpell(static_cast<unsigned char>(SpellSlot::R));
			Spell::Active RCast = { SpellSlot::R };

			/**
			if (ItemLogicManager::ShouldCastShacoRBeforeAA && RCast.IsReady() && resource.Current >= RCast.ManaCost()
				&& Game::Time() < ItemLogicManager::SHACO_COMBO_OFFSET && !ShacoCloneLogicManager::SHACO_CLONE_ENABLED && Game::Time() > RCastOffset) {
				ShacoCloneLogicManager::SHACO_CLONE_OPTION_INDEX = 0; // Reset to attack player target
				if (RCast.Cast()) {
					RCastOffset = Game::Time() + 0.05f;
				}
			}
			else {
				ItemLogicManager::ShouldCastShacoRBeforeAA = false;
			}
			*/

			if (ItemLogicManager::ShouldCastShacoTiamatBeforeAA && ItemLogicManager::TIAMAT_SPELL.IsReady() && Game::Time() < ItemLogicManager::SHACO_COMBO_OFFSET) {
				ItemLogicManager::TIAMAT_SPELL.Cast();
			}
			else {
				ItemLogicManager::ShouldCastShacoTiamatBeforeAA = false;
			}

			if (ItemLogicManager::ShouldCastShacoEBeforeAA && ECast.IsReady() && resource.Current >= ECast.ManaCost() && Game::Time() < ItemLogicManager::SHACO_COMBO_OFFSET) {
				if (!ItemLogicManager::ShouldCastShacoTiamatBeforeAA) {
					SDK_SPELL E = Player.GetSpell(static_cast<unsigned char>(SpellSlot::E));
					Spell::Targeted ECast = { SpellSlot::E, E.CastRange };

					ECast.Cast(ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET);
				}
			}
			else {
				ItemLogicManager::ShouldCastShacoEBeforeAA = false;
			}

			if (ItemLogicManager::ShouldCastShacoWBeforeAA && WCast.IsReady() && resource.Current >= WCast.ManaCost() && Game::Time() < ItemLogicManager::SHACO_COMBO_OFFSET) {
				if (!ItemLogicManager::ShouldCastShacoTiamatBeforeAA && !ItemLogicManager::ShouldCastShacoEBeforeAA) {

					SDK_SPELL w = Player.GetSpell(static_cast<unsigned char>(SpellSlot::W));
					Spell::Targeted t = { SpellSlot::W, w.CastRange };

					Vector3 enemyPos{ ShacoDeceiveBackStabLogicManager::PLAYER_HERO_TARGET->GetPosition() };
					Vector3 extendedFrontPos{ Player.GetPosition().Extended(enemyPos, 75.0f) };

					if (Game::Time() > WCastOffset) {
						if (t.Cast(&extendedFrontPos)) {
							WCastOffset = Game::Time() + 0.10f;
						}
					}
				}
			}
			else {
				ItemLogicManager::ShouldCastShacoWBeforeAA = false;
			}
		}
	}
}


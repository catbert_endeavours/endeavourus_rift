#pragma once
class ChampionBase
{
private:
	static bool forceSettingRefresh;
public:
	static bool NeedToRefreshSettings();
	static bool GetForceSettingRefresh();
	static void SetForceSettingsRefresh(const bool forceMenuRefresh);
};


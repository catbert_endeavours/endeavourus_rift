#include "stdafx.h"
#include "SpellShieldManager.h"
#include <SDK Extensions.h>
#include "EndeavourusMiscUtils.h"
#include "SpellShieldConstants.h"
#include "NocturneLogicManager.h"
#include "SivirLogicManager.h"

std::vector<std::string> SpellShieldManager::SPELL_SHIELD_MENU_OPTIONS{};
std::vector<std::string> SpellShieldManager::TARGETED_SPELLS{
		"Headbutt", "Frostbite", "Disintegrate", "AkaliMota", "AkaliShadowDance", "BrandE", "BrandR", "BraumE", "BraumRWrapper", "CaitlynAceintheHole",
		"CamilleR", "CassiopeiaE", "Feast", "DariusNoxianTacticsONH", "DianaTeleport", "EliseHumanQ", "EvelynnW", "EvelynnE", "FiddlesticksDarkWind",
		"Terrify", "Drain", "FizzQ", "GangplankQWrapper", "GarenQ", "IreliaGatotsu", "IreliaEquilibriumStrike", "SowTheWind", "JarvanIVCataclysm",
		"JaxLeapStrike", "JayceThunderingBlow", "JayceToTheSkies", "KarmaSpiritBind", "JudicatorReckoning", "KaynR", "NullLance", "KatarinaQ",
		"KatarinaEWrapper", "KhazixQ", "KhazixQLong", "LeblancQ", "BlindMonkRKick", "LissandraR", "LucianQ", "LuluE", "Whimsy", "LuluW", "SeismicShard",
		"MalzaharE", "MalzaharR", "MaokaiW", "AlphaStrike", "MissFortuneRicochetShot", "MordekaiserChildrenOfTheGrave", "NamiW", "NasusW",
		"NautilusGrandLine", "NautilusAnchorDrag", "NocturneUnspeakableHorror", "IceBlast", "OlafRecklessStrike", "PantheonQ",
		"PantheonW", "PoppyE", "QuinnE", "RekSaiE", "RekSaiRWrapper", "PuncturingTaunt", "RyzeW", "RyzeE", "TwoShivPoison", "Fling", "SkarnerImpale",
		"SwainTorment", "SyndraR", "TahmKenchW", "TalonQ", "BlindingDart", "TristanaE", "TristanaR", "TrundlePain", "VayneCondemn", "VeigarR", "ViR",
		"ViktorPowerTransfer", "VladimirQ", "VolibearW", "WarwickQ", "WarwickR", "MonkeyKingNimbus", "XinZhaoE", "YasuoDashWrapper", "ZedR", "TimeWarp" };

#define SPELL_CAST_TYPE_INSTANT						0
#define SPELL_CAST_TYPE_MISSILE						1
#define SPELL_CAST_TYPE_CHAIN						2
#define SPELL_CAST_TYPE_ARC							3
#define SPELL_CAST_TYPE_CIRCLE						4

struct SPELLSHIELD_SKILLSHOT {
	std::string charName{};
	int slot{};
	std::string spellType{};
	float delay{};
	int range{};
	int radius{};
	int speed{};
	bool addHitBox{};
	int danger{};
	bool dangerous{};
	std::string proj{};
	float killTime{};
	std::string displayName{};
	bool collision{};
	std::string killName{};

	SPELLSHIELD_SKILLSHOT(std::string charName, int slot, std::string spellType, float delay, int range, int radius, int speed, bool addHitBox, int danger, bool dangerous, std::string proj, float killTime, std::string displayName, bool collision, std::string killName = "") {
		this->charName = charName;
		this->slot = slot;
		this->spellType = spellType;
		this->delay = delay;
		this->range = range;
		this->radius = radius;
		this->speed = speed;
		this->addHitBox = addHitBox;
		this->danger = danger;
		this->dangerous = dangerous;
		this->proj = proj;
		this->killTime = killTime;
		this->displayName = displayName;
		this->collision = collision;
		this->killName = killName;
	}
};

std::map<std::string, std::string> displayNames{};

std::map<std::string, SPELLSHIELD_SKILLSHOT> SPELL_SHIELDS = {
	//{"AatroxQ", SPELLSHIELD_SKILLSHOT {"Aatrox",0,"Circle",0.6f,650,250,2000,true,3,true,"nil",0.225f,"Dark Flight",false }},
	//{"AatroxE", SPELLSHIELD_SKILLSHOT {"Aatrox",2,"Line",0.25f,1075,35,1250,true,3,false,"AatroxEConeMissile",0.0f,"Blade of Torment",false }},
	{"AhriOrbofDeception", SPELLSHIELD_SKILLSHOT {"Ahri",0,"Line",0.25f,1000,100,1700,true,2,false,"AhriOrbMissile",0.0f,"Orb of Deception",false }},
	{"AhriOrbReturn", SPELLSHIELD_SKILLSHOT {"Ahri",0,"Line",0.25f,1000,100,915,true,2,false,"AhriOrbReturn",0.0f,"Orb of Deception2",false }},
	{"AhriSeduce", SPELLSHIELD_SKILLSHOT {"Ahri",2,"Line",0.25f,1000,60,1600,true,3,true,"AhriSeduceMissile",0.0f,"Charm",true }},
	{"BandageToss", SPELLSHIELD_SKILLSHOT {"Amumu",0,"Line",0.25f,1000,90,2000,true,3,true,"SadMummyBandageToss",0.0f,"Bandage Toss",true }},
	//{"CurseoftheSadMummy", SPELLSHIELD_SKILLSHOT {"Amumu",3,"Circle",0.25f,0,550,INT_MAX,false,5,true,"nil",1.2f,"Curse of the Sad Mummy",false }},
	{"FlashFrost", SPELLSHIELD_SKILLSHOT {"Anivia",0,"Line",0.25f,1200,110,850,true,3,true,"FlashFrostSpell",0.0f,"Flash Frost",false }},
	//{"Incinerate", SPELLSHIELD_SKILLSHOT {"Annie",1,"Cone",0.25f,825,80,INT_MAX,false,2,false,"nil",0.0f,"",false }},
	//{"InfernalGuardian", SPELLSHIELD_SKILLSHOT {"Annie",3,"Circle",0.25f,600,251,INT_MAX,true,5,true,"nil",0.3f,"",false }},
	//{"Volley", SPELLSHIELD_SKILLSHOT {"Ashe",1,"Sevenway",0.25f,1200,60,1500,true,2,false,"VolleyAttack",0.0f,"",false }},
	{"EnchantedCrystalArrow", SPELLSHIELD_SKILLSHOT {"Ashe",3,"Line",0.2f,20000,130,1600,true,5,true,"EnchantedCrystalArrow",0.0f,"Enchanted Arrow",false }},
	{"AurelionSolQ", SPELLSHIELD_SKILLSHOT {"AurelionSol",0,"Line",0.25f,1500,180,850,true,2,false,"AurelionSolQMissile",0.0f,"AurelionSolQ",false }},
	{"AurelionSolR", SPELLSHIELD_SKILLSHOT {"AurelionSol",3,"Line",0.3f,1420,120,4500,true,3,true,"AurelionSolRBeamMissile",0.0f,"AurelionSolR",false }},
	{"BardQ", SPELLSHIELD_SKILLSHOT {"Bard",0,"Line",0.25f,850,60,1600,true,3,true,"BardQMissile",0.0f,"BardQ",true }},
	//{"BardR", SPELLSHIELD_SKILLSHOT {"Bard",3,"Circle",0.5f,3400,350,2100,true,2,false,"BardR",1.0f,"BardR",false }},
	{"RocketGrab", SPELLSHIELD_SKILLSHOT {"Blitzcrank",0,"Line",0.25f,1050,70,1800,true,4,true,"RocketGrabMissile",0.0f,"Rocket Grab",true }},
	//{"StaticField", SPELLSHIELD_SKILLSHOT {"Blitzcrank",3,"Circle",0.25f,0,600,INT_MAX,false,2,false,"nil",0.2f,"Static Field",false }},
	{"BrandQ", SPELLSHIELD_SKILLSHOT {"Brand",0,"Line",0.25f,1050,60,1600,true,3,true,"BrandQMissile",0.0f,"Sear",true }},
	{"BraumQ", SPELLSHIELD_SKILLSHOT {"Braum",0,"Line",0.25f,1000,60,1700,true,3,true,"BraumQMissile",0.0f,"Winter's Bite",true }},
	{"BraumRWrapper", SPELLSHIELD_SKILLSHOT {"Braum",3,"Line",0.5f,1250,115,1400,true,4,true,"braumrmissile",0.0f,"Glacial Fissure",false }},
	{"CaitlynPiltoverPeacemaker", SPELLSHIELD_SKILLSHOT {"Caitlyn",0,"Line",0.625f,1300,90,1800,true,2,false,"CaitlynPiltoverPeacemaker",0.0f,"Piltover Peacemaker",false }},
	{"CaitlynEntrapment", SPELLSHIELD_SKILLSHOT {"Caitlyn",2,"Line",0.4f,1000,70,1600,true,1,false,"CaitlynEntrapmentMissile",0.0f,"90 Caliber Net",true }},
	//{"CassiopeiaNoxiousBlast", SPELLSHIELD_SKILLSHOT {"Cassiopeia",0,"Circle",0.75f,850,150,INT_MAX,true,2,false,"CassiopeiaNoxiousBlast",0.2f,"Noxious Blast",false }},
	//{"CassiopeiaPetrifyingGaze", SPELLSHIELD_SKILLSHOT {"Cassiopeia",3,"Cone",0.6f,825,80,INT_MAX,false,5,true,"CassiopeiaPetrifyingGaze",0.0f,"Petrifying Gaze",false }},
	//{"Rupture", SPELLSHIELD_SKILLSHOT {"Chogath",0,"Circle",1.2f,950,250,INT_MAX,true,3,false,"Rupture",0.8f,"Rupture",false }},
	//{"PhosphorusBomb", SPELLSHIELD_SKILLSHOT {"Corki",0,"Circle",0.3f,825,250,1000,true,2,false,"PhosphorusBombMissile",0.35f,"Phosphorus Bomb",false }},
	{"MissileBarrage", SPELLSHIELD_SKILLSHOT {"Corki",3,"Line",0.2f,1300,40,2000,true,2,false,"MissileBarrageMissile",0.0f,"Missile Barrage",true }},
	{"MissileBarrage2", SPELLSHIELD_SKILLSHOT {"Corki",3,"Line",0.2f,1500,40,2000,true,2,false,"MissileBarrageMissile2",0.0f,"Missile Barrage big",true }},
	//{"DariusCleave", SPELLSHIELD_SKILLSHOT {"Darius",0,"Circle",0.75f,0,425 - 50,INT_MAX,true,3,false,"DariusCleave",0.0f,"Cleave",false }},
	//{"DariusAxeGrabCone", SPELLSHIELD_SKILLSHOT {"Darius",2,"Cone",0.25f,550,80,INT_MAX,false,3,true,"DariusAxeGrabCone",0.0f,"Apprehend",false }},
	//{"DianaArc", SPELLSHIELD_SKILLSHOT {"Diana",0,"Circle",0.25f,835,195,1400,true,3,true,"DianaArcArc",0.0f,"",false }},
	//{"DianaArcArc", SPELLSHIELD_SKILLSHOT {"Diana",0,"Arc",0.25f,835,195,1400,true,3,true,"DianaArcArc",0.0f,"",false }},
	{"InfectedCleaverMissileCast", SPELLSHIELD_SKILLSHOT {"DrMundo",0,"Line",0.25f,1050,60,2000,true,3,false,"InfectedCleaverMissile",0.0f,"Infected Cleaver",true }},
	{"DravenDoubleShot", SPELLSHIELD_SKILLSHOT {"Draven",2,"Line",0.25f,1100,130,1400,true,3,true,"DravenDoubleShotMissile",0.0f,"Stand Aside",false }},
	{"DravenRCast", SPELLSHIELD_SKILLSHOT {"Draven",3,"Line",0.5f,25000,160,2000,true,5,true,"DravenR",0.0f,"Whirling Death",false }},
	{"EkkoQ", SPELLSHIELD_SKILLSHOT {"Ekko",0,"Line",0.25f,925,60,1650,true,4,true,"ekkoqmis",0.0f,"Timewinder",false }},
	//{"EkkoW", SPELLSHIELD_SKILLSHOT {"Ekko",1,"Circle",3.75f,1600,375,1650,false,3,false,"EkkoW",1.2f,"Parallel Convergence",false }},
	//{"EkkoR", SPELLSHIELD_SKILLSHOT {"Ekko",3,"Circle",0.25f,1600,375,1650,true,3,false,"EkkoR",0.2f,"Chronobreak",false }},
	{"EliseHumanE", SPELLSHIELD_SKILLSHOT {"Elise",2,"Line",0.25f,925,55,1600,true,4,true,"EliseHumanE",0.0f,"Cocoon",true }},
	//{"EvelynnR", SPELLSHIELD_SKILLSHOT {"Evelynn",3,"Circle",0.25f,650,350,INT_MAX,true,5,true,"EvelynnR",0.2f,"Agony's Embrace",false}},
	{"EzrealMysticShot", SPELLSHIELD_SKILLSHOT {"Ezreal",0,"Line",0.25f,1300,50,1975,true,2,false,"EzrealMysticShotMissile",0.25f,"Mystic Shot",true }},
	{"EzrealEssenceFlux", SPELLSHIELD_SKILLSHOT {"Ezreal",1,"Line",0.25f,1000,80,1500,true,2,false,"EzrealEssenceFluxMissile",0.0f,"Essence Flux",false }},
	{"EzrealTrueshotBarrage", SPELLSHIELD_SKILLSHOT {"Ezreal",3,"Line",1.0f,20000,150,2000,true,3,true,"EzrealTrueshotBarrage",0.0f,"Trueshot Barrage",false }},
	{"FioraW", SPELLSHIELD_SKILLSHOT {"Fiora",1,"Line",0.5f,800,70,3200,true,2,false,"FioraWMissile",0.0f,"Riposte",false }},
	{"FizzMarinerDoom", SPELLSHIELD_SKILLSHOT {"Fizz",3,"Line",0.25f,1150,120,1350,true,5,true,"FizzMarinerDoomMissile",0.0f,"Chum the Waters",false }},
	//{"GalioResoluteSmite", SPELLSHIELD_SKILLSHOT {"Galio",0,"Circle",0.25f,900,200,1300,true,2,false,"GalioResoluteSmite",0.2f,"Resolute Smite",false }},
	{"GalioRighteousGust", SPELLSHIELD_SKILLSHOT {"Galio",2,"Line",0.25f,1000,120,1200,true,2,false,"GalioRighteousGust",0.0f,"Righteous Ghost",false }},
	//{"GalioIdolOfDurand", SPELLSHIELD_SKILLSHOT {"Galio",3,"Circle",0.25f,0,550,INT_MAX,false,5,true,"nil",1.0f,"Idol of Durand",false }},
	{"GnarQ", SPELLSHIELD_SKILLSHOT {"Gnar",0,"Line",0.25f,1200,60,1225,true,2,false,"gnarqmissile",0.0f,"Boomerang Throw",false }},
	{"GnarQReturn", SPELLSHIELD_SKILLSHOT {"Gnar",0,"Line",0.0f,1200,75,1225,true,2,false,"GnarQMissileReturn",0.0f,"Boomerang Throw2",false }},
	{"GnarBigQ", SPELLSHIELD_SKILLSHOT {"Gnar",0,"Line",0.5f,1150,90,2100,true,2,false,"GnarBigQMissile",0.0f,"Boulder Toss",true }},
	{"GnarBigW", SPELLSHIELD_SKILLSHOT {"Gnar",1,"Line",0.6f,600,80,INT_MAX,true,2,false,"GnarBigW",0.0f,"Wallop",false }},
	//{"GnarE", SPELLSHIELD_SKILLSHOT {"Gnar",2,"Circle",0.0f,473,150,903,true,2,false,"GnarE",0.2f,"GnarE",false }},
	//{"GnarBigE", SPELLSHIELD_SKILLSHOT {"Gnar",2,"Circle",0.25f,475,200,1000,true,2,false,"GnarBigE",0.2f,"GnarBigE",false }},
	//{"GnarR", SPELLSHIELD_SKILLSHOT {"Gnar",3,"Circle",0.25f,0,500,INT_MAX,false,5,true,"nil",0.3f,"GnarUlt",false }},
	//{"GragasQ", SPELLSHIELD_SKILLSHOT {"Gragas",0,"Circle",0.25f,1100,275,1300,true,2,false,"GragasQMissile",2.5f,"Barrel Roll",false,"GragasQToggle" }},
	{"GragasE", SPELLSHIELD_SKILLSHOT {"Gragas",2,"Line",0.0f,950,200,1200,true,2,false,"GragasE",0.0f,"Body Slam",true }},
	//{"GragasR", SPELLSHIELD_SKILLSHOT {"Gragas",3,"Circle",0.25f,1050,375,1800,true,5,true,"GragasRBoom",0.3f,"Explosive Cask",false }},
	{"GravesQLineSpell", SPELLSHIELD_SKILLSHOT {"Graves",0,"Line",0.2f,750,40,2000,true,2,false,"GravesQLineMis",0.0f,"Buckshot",false }},
	{"GravesChargeShot", SPELLSHIELD_SKILLSHOT {"Graves",3,"Line",0.2f,1000,100,2100,true,5,true,"GravesChargeShotShot",0.0f,"Collateral Damage",false }},
	{"HeimerdingerW", SPELLSHIELD_SKILLSHOT {"Heimerdinger",1,"Line",0.25f,1500,70,1800,true,2,false,"HeimerdingerWAttack2",0.0f,"HeimerdingerUltW",true }},
	//{"HeimerdingerE", SPELLSHIELD_SKILLSHOT {"Heimerdinger",2,"Circle",0.25f,925,100,1200,true,2,false,"heimerdingerespell",0.3f,"HeimerdingerE",false }},
	{"IllaoiQ", SPELLSHIELD_SKILLSHOT {"Illaoi",0,"Line",0.75,750,160,INT_MAX,true,3,true,"illaoiemis",0.0f,"",false }},
	{"IllaoiE", SPELLSHIELD_SKILLSHOT {"Illaoi",2,"Line",0.25f,1100,50,1900,true,3,true,"illaoiemis",0.0f,"",true }},
	//{"IllaoiR", SPELLSHIELD_SKILLSHOT {"Illaoi",3,"Circle",0.5f,0,450,INT_MAX,false,3,true,"nil",0.2f,"",false }},
	{"IreliaTranscendentBlades", SPELLSHIELD_SKILLSHOT {"Irelia",3,"Line",0.0f,1200,65,1600,true,2,false,"IreliaTranscendentBlades",0.0f,"Transcendent Blades",false }},
	{"HowlingGale", SPELLSHIELD_SKILLSHOT {"Janna",0,"Line",0.25f,1700,120,900,true,2,false,"HowlingGaleSpell",0.0f,"HowlingGale",false }},
	{"JarvanIVDragonStrike", SPELLSHIELD_SKILLSHOT {"JarvanIV",0,"Line",0.6f,770,70,INT_MAX,true,3,false,"nil",0.0f,"DragonStrike",false }},
	{"JarvanIVEQ", SPELLSHIELD_SKILLSHOT {"JarvanIV",0,"Line",0.25f,880,70,1450,true,3,true,"nil",0,"DragonStrike2",false }},
	//{"JarvanIVDemacianStandard", SPELLSHIELD_SKILLSHOT {"JarvanIV",2,"Circle",0.5f,860,175,INT_MAX,true,2,false,"JarvanIVDemacianStandard",1.5f,"Demacian Standard",false }},
	{"jayceshockblast", SPELLSHIELD_SKILLSHOT {"Jayce",0,"Line",0.25f,1300,70,1450,true,2,false,"JayceShockBlastMis",0.0f,"ShockBlast",true }},
	{"JayceQAccel", SPELLSHIELD_SKILLSHOT {"Jayce",0,"Line",0.25f,1300,70,2350,true,2,false,"JayceShockBlastWallMis",0.0f,"ShockBlastCharged",true }},
	{"JhinW", SPELLSHIELD_SKILLSHOT {"Jhin",1,"Line",0.75f,2550,40,5000,true,3,true,"JhinWMissile",0.0f,"",false }},
	{"JhinRShot", SPELLSHIELD_SKILLSHOT {"Jhin",3,"Line",0.25f,3500,80,5000,true,3,true,"JhinRShotMis",0.0f,"JhinR",false }},
	{"JinxW", SPELLSHIELD_SKILLSHOT {"Jinx",1,"Line",0.6f,1400,60,3300,true,3,true,"JinxWMissile",0.0f,"Zap",true }},
	{"JinxR", SPELLSHIELD_SKILLSHOT {"Jinx",3,"Line",0.6f,20000,140,1700,true,5,true,"JinxR",0.0f,"",false }},
	{"KalistaMysticShot", SPELLSHIELD_SKILLSHOT {"Kalista",0,"Line",0.25f,1200,40,1700,true,2,false,"kalistamysticshotmis",0.0f,"MysticShot",true }},
	{"KarmaQ", SPELLSHIELD_SKILLSHOT {"Karma",0,"Line",0.25f,1050,60,1700,true,2,false,"KarmaQMissile",0.0f,"",true }},
	{"KarmaQMantra", SPELLSHIELD_SKILLSHOT {"Karma",0,"Line",0.25f,950,80,1700,true,2,false,"KarmaQMissileMantra",0.0f,"",true }},
	//{"KarthusLayWasteA2", SPELLSHIELD_SKILLSHOT {"Karthus",0,"Circle",0.625f,875,160,INT_MAX,true,2,false,"nil",0.2f,"Lay Waste",false }},
	//{"RiftWalk", SPELLSHIELD_SKILLSHOT {"Kassadin",3,"Circle",0.25f,450,270,INT_MAX,true,2,false,"RiftWalk",0.3f,"",false }},
	{"KennenShurikenHurlMissile1", SPELLSHIELD_SKILLSHOT {"Kennen",0,"Line",0.18f,1050,50,1650,true,2,false,"KennenShurikenHurlMissile1",0.0f,"Thundering Shuriken",true }},
	{"KhazixW", SPELLSHIELD_SKILLSHOT {"Khazix",1,"Line",0.25f,1025,70,1700,true,2,false,"KhazixWMissile",0.0f,"",true }},
	//{"KhazixE", SPELLSHIELD_SKILLSHOT {"Khazix",2,"Circle",0.25f,600,300,1500,true,2,false,"KhazixE",0.2f,"",false }},
	{"KogMawQ", SPELLSHIELD_SKILLSHOT {"Kogmaw",0,"Line",0.25f,975,70,1650,true,2,false,"KogMawQ",0.0f,"",true }},
	{"KogMawVoidOoze", SPELLSHIELD_SKILLSHOT {"Kogmaw",2,"Line",0.25f,1200,120,1400,true,2,false,"KogMawVoidOozeMissile",0.0f,"Void Ooze",false }},
	//{"KogMawLivingArtillery", SPELLSHIELD_SKILLSHOT {"Kogmaw",3,"Circle",1.2f,1800,225,INT_MAX,true,2,false,"KogMawLivingArtillery",0.5f,"LivingArtillery",false }},
	//{"LeblancSlide", SPELLSHIELD_SKILLSHOT {"Leblanc",1,"Circle",0.0f,600,220,1450,true,2,false,"LeblancSlide",0.2f,"Slide",false }},
	//{"LeblancSlideM", SPELLSHIELD_SKILLSHOT {"Leblanc",3,"Circle",0.0f,600,220,1450,true,2,false,"LeblancSlideM",0.2f,"Slide R",false }},
	{"LeblancSoulShackle", SPELLSHIELD_SKILLSHOT {"Leblanc",2,"Line",0.0f,950,70,1750,true,3,true,"LeblancSoulShackle",0.0f,"Ethereal Chains R",true }},
	{"LeblancSoulShackleM", SPELLSHIELD_SKILLSHOT {"Leblanc",3,"Line",0.0f,950,70,1750,true,3,true,"LeblancSoulShackleM",0.0f,"Ethereal Chains",true }},
	{"BlindMonkQOne", SPELLSHIELD_SKILLSHOT {"LeeSin",0,"Line",0.1f,1000,65,1800,true,3,true,"BlindMonkQOne",0.0f,"Sonic Wave",true }},
	{"LeonaZenithBlade", SPELLSHIELD_SKILLSHOT {"Leona",2,"Line",0.25f,875,70,1750,true,3,true,"LeonaZenithBladeMissile",0.0f,"Zenith Blade",false }},
	//{"LeonaSolarFlare", SPELLSHIELD_SKILLSHOT {"Leona",3,"Circle",1.0f,1200,300,INT_MAX,true,5,true,"LeonaSolarFlare",0.5f,"Solar Flare",false }},
	{"LissandraQ", SPELLSHIELD_SKILLSHOT {"Lissandra",0,"Line",0.25f,700,75,2200,true,2,false,"LissandraQMissile",0.0f,"Ice Shard",false }},
	{"LissandraQShards", SPELLSHIELD_SKILLSHOT {"Lissandra",0,"Line",0.25f,700,90,2200,true,2,false,"lissandraqshards",0.0f,"Ice Shard2",false }},
	{"LissandraE", SPELLSHIELD_SKILLSHOT {"Lissandra",2,"Line",0.25f,1025,125,850,true,2,false,"LissandraEMissile",0.0f,"",false }},
	{"LucianQ", SPELLSHIELD_SKILLSHOT {"Lucian",0,"Line",0.5f,800,65,INT_MAX,true,2,false,"LucianQ",0.0f,"",false }},
	{"LucianW", SPELLSHIELD_SKILLSHOT {"Lucian",1,"Line",0.2f,1000,55,1600,true,2,false,"lucianwmissile",0.0f,"",true }},
	{"LucianRMis", SPELLSHIELD_SKILLSHOT {"Lucian",3,"Line",0.5f,1400,110,2800,true,2,false,"lucianrmissileoffhand",0.0f,"LucianR",true }},
	{"LuluQ", SPELLSHIELD_SKILLSHOT {"Lulu",0,"Line",0.25f,950,60,1450,true,2,false,"LuluQMissile",0.0f,"",false }},
	{"LuluQPix", SPELLSHIELD_SKILLSHOT {"Lulu",0,"Line",0.25f,950,60,1450,true,2,false,"LuluQMissileTwo",0.0f,"",false }},
	{"LuxLightBinding", SPELLSHIELD_SKILLSHOT {"Lux",0,"Line",0.225f,1300,70,1200,true,3,true,"LuxLightBindingMis",0.0f,"Light Binding",true }},
	//{"LuxLightStrikeKugel", SPELLSHIELD_SKILLSHOT {"Lux",2,"Circle",0.25f,1100,275,1300,true,2,false,"LuxLightStrikeKugel",5.25f,"LightStrikeKugel",false,"LuxLightstrikeToggle" }},
	{"LuxMaliceCannon", SPELLSHIELD_SKILLSHOT {"Lux",3,"Line",1.0f,3500,190,INT_MAX,true,5,true,"LuxMaliceCannon",0.0f,"Malice Cannon",false }},
	//{"UFSlash", SPELLSHIELD_SKILLSHOT {"Malphite",3,"Circle",0.0f,1000,270,1500,true,5,true,"UFSlash",0.4f,"",false }},
	{"MalzaharQ", SPELLSHIELD_SKILLSHOT {"Malzahar",0,"Line",0.75f,900,85,INT_MAX,true,2,false,"MalzaharQ",0.0f,"",false }},
	//{"DarkBindingMissile", SPELLSHIELD_SKILLSHOT {"Morgana",0,"Line",0.2f,1300,80,1200,true,3,true,"DarkBindingMissile",0.0f,"Dark Binding",true }},
	{ "MorganaQ", SPELLSHIELD_SKILLSHOT {"Morgana",0,"Line",0.2f,1300,70,1200,true,3,true,"MorganaQ",0.0f,"Dark Binding",true } },
	//{"NamiQ", SPELLSHIELD_SKILLSHOT {"Nami",0,"Circle",0.95f,1625,150,INT_MAX,true,3,true,"namiqmissile",0.35f,"",false }},
	{"NamiR", SPELLSHIELD_SKILLSHOT {"Nami",3,"Line",1.0f,2750,260,850,true,2,false,"NamiRMissile",0.0f,"",false }},
	{"NautilusAnchorDrag", SPELLSHIELD_SKILLSHOT {"Nautilus",0,"Line",0.25f,1080,90,2000,true,3,true,"NautilusAnchorDragMissile",0.0f,"Anchor Drag",true }},
	{"NocturneDuskbringer", SPELLSHIELD_SKILLSHOT {"Nocturne",0,"Line",0.25f,1125,60,1400,true,2,false,"NocturneDuskbringer",0.0f,"Duskbringer",false }},
	{"JavelinToss", SPELLSHIELD_SKILLSHOT {"Nidalee",0,"Line",0.25f,1500,40,1300,true,3,true,"JavelinToss",0.0f,"JavelinToss",true }},
	{"OlafAxeThrowCast", SPELLSHIELD_SKILLSHOT {"Olaf",0,"Line",0.25f,1000,105,1600,true,2,false,"olafaxethrow",0.0f,"Axe Throw",false }},
	{"OriannasQ", SPELLSHIELD_SKILLSHOT {"Orianna",0,"Line",0.0f,1500,80,1200,true,2,false,"orianaizuna",0.0f,"",false }},
	//{"OriannaQend", SPELLSHIELD_SKILLSHOT {"Orianna",0,"Circle",0.0f,1500,90,1200,true,2,false,"nil",0.1f,"",false }},
	//{"OrianaDissonanceCommand-", SPELLSHIELD_SKILLSHOT {"Orianna",1,"Circle",0.25f,0,255,INT_MAX,true,2,false,"OrianaDissonanceCommand-",0.3f,"",false }},
	{"OriannasE", SPELLSHIELD_SKILLSHOT {"Orianna",2,"Line",0.0f,1500,85,1850,true,2,false,"orianaredact",0.0f,"",false }},
	//{"OrianaDetonateCommand-", SPELLSHIELD_SKILLSHOT {"Orianna",3,"Circle",0.7f,0,410,INT_MAX,true,5,true,"OrianaDetonateCommand-",0.5f,"",false }},
	{"QuinnQ", SPELLSHIELD_SKILLSHOT {"Quinn",0,"Line",0.0f,1050,60,1550,true,2,false,"QuinnQ",0.0f,"",true }},
	{"PoppyQ", SPELLSHIELD_SKILLSHOT {"Poppy",0,"Line",0.5f,430,100,INT_MAX,true,2,false,"PoppyQ",0.0f,"",false }},
	{"PoppyRSpell", SPELLSHIELD_SKILLSHOT {"Poppy",3,"Line",0.3f,1200,100,1600,true,3,true,"PoppyRMissile",0.0f,"PoppyR",false }},
	{"RengarE", SPELLSHIELD_SKILLSHOT {"Rengar",2,"Line",0.25f,1000,70,1500,true,3,true,"RengarEFinal",0.0f,"",true }},
	{"reksaiqburrowed", SPELLSHIELD_SKILLSHOT {"RekSai",0,"Line",0.5f,1050,60,1550,true,3,false,"RekSaiQBurrowedMis",0.0f,"RekSaiQ",true }},
	{"RivenIzunaBlade", SPELLSHIELD_SKILLSHOT {"Riven",3,"Line",0.25f,1100,125,1600,false,5,true,"RivenLightsaberMissile",0.0f,"WindSlash",false }},
	{"RumbleGrenade", SPELLSHIELD_SKILLSHOT {"Rumble",2,"Line",0.25f,850,60,2000,true,2,false,"RumbleGrenade",0.0f,"Grenade",true }},
	{"RyzeQ", SPELLSHIELD_SKILLSHOT {"Ryze",0,"Line",0.0f,900,50,1700,true,2,false,"RyzeQ",0.0f,"",true }},
	{"ryzerq", SPELLSHIELD_SKILLSHOT {"Ryze",0,"Line",0.0f,900,50,1700,true,2,false,"ryzerq",0.0f,"RyzeQ R",true }},
	{"SejuaniArcticAssault", SPELLSHIELD_SKILLSHOT {"Sejuani",0,"Line",0.0f,900,70,1600,true,3,true,"nil",0.0f,"ArcticAssault",true }},
	{"SejuaniGlacialPrisonStart", SPELLSHIELD_SKILLSHOT {"Sejuani",3,"Line",0.25f,1200,110,1600,true,3,true,"sejuaniglacialprison",0.0f,"GlacialPrisonStart",false }},
	{"SionE", SPELLSHIELD_SKILLSHOT {"Sion",2,"Line",0.25f,800,80,1800,true,3,true,"SionEMissile",0.0f,"",false }},
	{"SionR", SPELLSHIELD_SKILLSHOT {"Sion",3,"Line",0.5f,20000,120,1000,true,3,true,"nil",0.0f,"",false }},
	//{"SorakaQ", SPELLSHIELD_SKILLSHOT {"Soraka",0,"Circle",0.5f,950,300,1750,true,2,false,"nil",0.25f,"",false }},
	//{"SorakaE", SPELLSHIELD_SKILLSHOT {"Soraka",2,"Circle",0.25f,925,275,INT_MAX,true,2,false,"nil",1.0f,"",false }},
	{"ShenE", SPELLSHIELD_SKILLSHOT {"Shen",2,"Line",0.0f,650,50,1600,true,3,true,"ShenE",0.0f,"Shadow Dash",false }},
	{"ShyvanaFireball", SPELLSHIELD_SKILLSHOT {"Shyvana",2,"Line",0.25f,925,60,1700,true,2,false,"ShyvanaFireballMissile",0.0f,"Fireball",false }},
	{"ShyvanaTransformCast", SPELLSHIELD_SKILLSHOT {"Shyvana",3,"Line",0.25f,750,150,1500,true,3,true,"ShyvanaTransformCast",0.0f,"Transform Cast",false }},
	{"shyvanafireballdragon2", SPELLSHIELD_SKILLSHOT {"Shyvana",3,"Line",0.25f,925,70,2000,true,3,false,"ShyvanaFireballDragonFxMissile",0.0f,"Fireball Dragon",false }},
	{"SivirQReturn", SPELLSHIELD_SKILLSHOT {"Sivir",0,"Line",0.0f,1075,100,1350,true,2,false,"SivirQMissileReturn",0.0f,"SivirQ2",false }},
	{"SivirQ", SPELLSHIELD_SKILLSHOT {"Sivir",0,"Line",0.25f,1075,90,1350,true,2,false,"SivirQMissile",0.0f,"SivirQ",false }},
	{"SkarnerFracture", SPELLSHIELD_SKILLSHOT {"Skarner",2,"Line",0.35f,350,70,1500,true,2,false,"SkarnerFractureMissile",0.0f,"Fracture",false }},
	{"SonaR", SPELLSHIELD_SKILLSHOT {"Sona",3,"Line",0.25f,900,140,2400,true,5,true,"SonaR",0.0f,"Crescendo",false }},
	//{"SwainShadowGrasp", SPELLSHIELD_SKILLSHOT {"Swain",1,"Circle",1.1f,900,180,INT_MAX,true,3,true,"SwainShadowGrasp",0.5f,"Shadow Grasp",false }},
	//{"SyndraQ", SPELLSHIELD_SKILLSHOT {"Syndra",0,"Circle",0.6f,800,150,INT_MAX,true,2,false,"SyndraQ",0.2f,"",false }},
	//{"syndrawcast", SPELLSHIELD_SKILLSHOT {"Syndra",1,"Circle",0.25f,950,210,1450,true,2,false,"syndrawcast",0.2f,"SyndraW",false }},
	{"syndrae5", SPELLSHIELD_SKILLSHOT {"Syndra",2,"Line",0.0f,950,100,2000,true,2,false,"syndrae5",0.0f,"SyndraE",false }},
	{"SyndraE", SPELLSHIELD_SKILLSHOT {"Syndra",2,"Line",0.0f,950,100,2000,true,2,false,"SyndraE",0.0f,"SyndraE2",false }},
	//{"TalonRake", SPELLSHIELD_SKILLSHOT {"Talon",1,"Threeway",0.25f,800,80,2300,true,2,true,"talonrakemissileone",0.0f,"Rake",false }},
	//{"TalonRakeReturn", SPELLSHIELD_SKILLSHOT {"Talon",1,"Threeway",0.25f,800,80,1850,true,2,true,"talonrakemissiletwo",0.0f,"Rake2",false }},
	{"TahmKenchQ", SPELLSHIELD_SKILLSHOT {"TahmKench",0,"Line",0.25f,951,90,2800,true,3,true,"tahmkenchqmissile",0,"Tongue Slash",true }},
	{"TaricE", SPELLSHIELD_SKILLSHOT {"Taric",2,"Line",1.0f,750,100,INT_MAX,true,3,true,"TaricE",0.0f,"",false }},
	{"ThreshQ", SPELLSHIELD_SKILLSHOT {"Thresh",0,"Line",0.5f,1050,70,1900,true,3,true,"ThreshQMissile",0.0f,"",true }},
	{"ThreshEFlay", SPELLSHIELD_SKILLSHOT {"Thresh",2,"Line",0.125f,500,110,2000,true,3,true,"ThreshEMissile1",0.0f,"Flay",false }},
	//{"RocketJump", SPELLSHIELD_SKILLSHOT {"Tristana",1,"Circle",0.5f,900,270,1500,true,2,false,"RocketJump",0.3f,"",false }},
	{"slashCast", SPELLSHIELD_SKILLSHOT {"Tryndamere",2,"Line",0.0f,660,93,1300,true,2,false,"slashCast",0.0f,"",false }},
	//{"WildCards", SPELLSHIELD_SKILLSHOT {"TwistedFate",0,"Threeway",0.25f,1450,40,1000,true,2,false,"SealFateMissile",0.0f,"",false }},
	//{"TwitchVenomCask", SPELLSHIELD_SKILLSHOT {"Twitch",1,"Circle",0.25f,900,275,1400,true,2,false,"TwitchVenomCaskMissile",0.3f,"Venom Cask",false }},
	{"UrgotHeatseekingLineMissile", SPELLSHIELD_SKILLSHOT {"Urgot",0,"Line",0.125f,1000,60,1600,true,2,false,"UrgotHeatseekingLineMissile",0.0f,"Heatseeking Line",true }},
	//{"UrgotPlasmaGrenade", SPELLSHIELD_SKILLSHOT {"Urgot",2,"Circle",0.25f,1100,210,1500,true,2,false,"UrgotPlasmaGrenadeBoom",0.3f,"PlasmaGrenade",false }},
	{"VarusQMissile", SPELLSHIELD_SKILLSHOT {"Varus",0,"Line",0.25f,1475,70,1900,true,2,false,"VarusQMissile",0.0f,"VarusQ",false }},
	//{"VarusE", SPELLSHIELD_SKILLSHOT {"Varus",2,"Circle",1.0f,925,235,1500,true,2,false,"VarusE",1.5f,"",false }},
	{"VarusR", SPELLSHIELD_SKILLSHOT {"Varus",3,"Line",0.25f,800,120,1950,true,3,true,"VarusRMissile",0.0f,"",false }},
	{"VeigarBalefulStrike", SPELLSHIELD_SKILLSHOT {"Veigar",0,"Line",0.25f,900,70,2000,true,2,false,"VeigarBalefulStrikeMis",0.0f,"BalefulStrike",false }},
	//{"VeigarDarkMatter", SPELLSHIELD_SKILLSHOT {"Veigar",1,"Circle",1.35f,900,225,INT_MAX,true,2,false,"nil",0.5f,"DarkMatter",false }},
	//{"VeigarEventHorizon", SPELLSHIELD_SKILLSHOT {"Veigar",2,"Ring",0.5f,700,80,INT_MAX,false,3,true,"nil",3.5f,"EventHorizon",false }},
	{"VelkozQ", SPELLSHIELD_SKILLSHOT {"Velkoz",0,"Line",0.25f,1100,50,1300,true,2,false,"VelkozQMissile",0.0f,"",true }},
	{"VelkozQSplit", SPELLSHIELD_SKILLSHOT {"Velkoz",0,"Line",0.25f,1100,55,2100,true,2,false,"VelkozQMissileSplit",0.0f,"",true }},
	{"VelkozW", SPELLSHIELD_SKILLSHOT {"Velkoz",1,"Line",0.25f,1050,88,1700,true,2,false,"VelkozWMissile",0.0f,"",false }},
	//{"VelkozE", SPELLSHIELD_SKILLSHOT {"Velkoz",2,"Circle",0.5f,800,225,1500,false,2,false,"VelkozEMissile",0.5f,"",false }},
	{"Vi-q", SPELLSHIELD_SKILLSHOT {"Vi",0,"Line",0.25f,715,90,1500,true,3,true,"ViQMissile",0.0f,"Vi-Q",false }},
	//{"VladimirR", SPELLSHIELD_SKILLSHOT {"Vladimir",3,"Circle",0.25f,700,175,INT_MAX,true,4,true,"nil",0.0f,"Hemoplague",false }},
	{"Laser", SPELLSHIELD_SKILLSHOT {"Viktor",2,"Line",0.25f,1200,80,1050,true,2,false,"ViktorDeathRayMissile",0.0f,"",false }},
	{"xeratharcanopulse2", SPELLSHIELD_SKILLSHOT {"Xerath",0,"Line",0.6f,1600,95,INT_MAX,true,2,false,"xeratharcanopulse2",0.0f,"Arcanopulse",false }},
	//{"XerathArcaneBarrage2", SPELLSHIELD_SKILLSHOT {"Xerath",1,"Circle",0.7f,1000,200,INT_MAX,true,2,false,"XerathArcaneBarrage2",0.3f,"ArcaneBarrage",false }},
	{"XerathMageSpear", SPELLSHIELD_SKILLSHOT {"Xerath",2,"Line",0.2f,1050,60,1400,true,2,true,"XerathMageSpearMissile",0.0f,"MageSpear",true }},
	//{"xerathrmissilewrapper", SPELLSHIELD_SKILLSHOT {"Xerath",3,"Circle",0.7f,5600,130,INT_MAX,true,3,true,"xerathrmissilewrapper",0.4f,"XerathLocusPulse",false }},
	{"yasuoqw", SPELLSHIELD_SKILLSHOT {"Yasuo",0,"Line",0.4f,550,20,INT_MAX,true,2,true,"yasuoq",0.0f,"Steel Tempest 1",false }},
	{"yasuoq2", SPELLSHIELD_SKILLSHOT {"Yasuo",0,"Line",0.4f,550,20,INT_MAX,true,2,true,"yasuoq2",0.0f,"Steel Tempest 2",false }},
	{"yasuoq3", SPELLSHIELD_SKILLSHOT {"Yasuo",0,"Line",0.5f,1200,90,1500,true,3,true,"yasuoq3w",0.0f,"Steel Tempest 3",false }},
	{"ZacQ", SPELLSHIELD_SKILLSHOT {"Zac",0,"Line",0.5f,550,120,INT_MAX,true,2,false,"ZacQ",0.0f,"",false }},
	{"ZedQ", SPELLSHIELD_SKILLSHOT {"Zed",0,"Line",0.25f,925,50,1700,true,2,false,"ZedQMissile",0.0f,"",false }},
	{"ZiggsQ", SPELLSHIELD_SKILLSHOT {"Ziggs",0,"Line",0.5f,1100,100,1750,true,2,false,"ZiggsQSpell",0.2f,"",true }},
	//{"ZiggsW", SPELLSHIELD_SKILLSHOT {"Ziggs",1,"Circle",0.25f,1000,275,1750,true,2,false,"ZiggsW",3.0f,"",false,"ZiggsWToggle" }},
	//{"ZiggsE", SPELLSHIELD_SKILLSHOT {"Ziggs",2,"Circle",0.5f,900,235,1750,true,2,false,"ZiggsE",3.25f,"",false }},
	//{"ZiggsR", SPELLSHIELD_SKILLSHOT {"Ziggs",3,"Circle",0.0f,5300,500,INT_MAX,true,2,false,"ZiggsR",1.2f,"",false }},
	//{"ZileanQ", SPELLSHIELD_SKILLSHOT {"Zilean",0,"Circle",0.3f,900,210,2000,true,2,false,"ZileanQMissile",1.5f,"",false }},
	//{"ZyraQ", SPELLSHIELD_SKILLSHOT {"Zyra",0,"Rectangle",0.25f,800,140,INT_MAX,true,2,false,"ZyraQ",0.3f,"",false }},
	{"ZyraE", SPELLSHIELD_SKILLSHOT {"Zyra",2,"Line",0.25f,1100,70,1300,true,3,true,"ZyraE",0,"Grasping Roots",false }},
	//{"ZyraRSplash", SPELLSHIELD_SKILLSHOT {"Zyra",3,"Circle",0.7f,700,550,INT_MAX,true,4,false,"ZyraRSplash",1.0f,"Splash",false }}
};
std::map<void*, std::string> missiles{};

void getMissile(void* missile) {
	MissileClient missileClient = pSDK->EntityManager->GetObjectFromPTR(missile);
}

void SpellShieldManager::Init()
{
	for (auto s : SPELL_SHIELDS) {
		displayNames.emplace(s.second.proj, s.first);
	}

	SdkRegisterOnObjectCreate([](void* object, unsigned net_id, void*) -> bool
	{
		std::string missileStr{ "Missile" };
		SDKSTATUS sdkStatus = SdkIsObjectSpellMissile(object);
		if (sdkStatus == SDKSTATUS_NO_ERROR) {

			const char* name{};
			SdkGetObjectName(object, &name);
			std::string missileName{ name };

			SDKVECTOR endPos{};
			SdkGetMissileTarget(object, &endPos, NULL, NULL);

			SDKVECTOR startPos{};
			SdkGetMissileStartPosition(object, &startPos);

			std::string missileN{};
			auto result = displayNames.find(missileName);
			if (result != displayNames.end()) {
				missileN = result->second;
			}

			if (CanShieldSpell(missileN)) {
				auto result = SPELL_SHIELDS.find(missileN);
				if (result != SPELL_SHIELDS.end()) {

					float radius{ static_cast<float>(result->second.radius) };
					float speed{ static_cast<float>(result->second.speed) };

					std::shared_ptr<ICollision::Output> collision = pSDK->Collision->GetCollisions(startPos, endPos, radius, 0.0f, speed, true, (CollisionFlags::Minions | CollisionFlags::Heroes | CollisionFlags::YasuoWall));

					bool collided{ false };
					float playerDistance{ Player.Distance(startPos) };
					float distance{ FLT_MAX };
					for (auto unit : collision->Units) {
						if (collision->Units.size() == 1 && unit->GetNetworkID() == Player.GetNetworkID()) {
							// Going to hit player
							collided = true;
							break;
						}

						if (unit->GetNetworkID() == Player.GetNetworkID()) {
							collided = true;
						}
						else {
							float sPos{ unit->GetServerPosition().Distance(startPos) };
							if (sPos < distance) {
								distance = sPos;
							}
						}
					}

					if ((!result->second.collision && collided) || (collided && distance == FLT_MAX) || (collided && playerDistance < distance)) {
						const std::string charName{ Player.GetCharName() };
						if (Player.IsAlive() && Player.IsValid()) {
							if (charName == "Nocturne")
							{
								if (NocturneLogicManager::WCast.IsReady() && Player.GetManaPercent() >= 5.0f) {
									NocturneLogicManager::NOC_SHOULD_CAST_SHIELD = true;
								}
							}
							else if (charName == "Sivir") {
								if (SivirLogicManager::ECast.IsReady()) {
									SivirLogicManager::SIVIR_SHOULD_CAST_SHIELD = true;
								}
							}
						}
					}
				}
			}
		}

		return true;
	}, nullptr);

	LoadCurrentSupportedInGameSpells();
}

void SpellShieldManager::LoadCurrentSupportedInGameSpells()
{
	if (pSDK != nullptr && pSDK->EntityManager != nullptr) {
		for (auto enemy : pSDK->EntityManager->GetEnemyHeroes())
		{
			if (enemy.second != nullptr) {
				for (auto eSpell : enemy.second->GetSpells())
				{
					for (auto tSpellName : TARGETED_SPELLS)
					{
						if (eSpell.Name != nullptr && std::string{ eSpell.Name }.find(tSpellName) != std::string::npos)
						{
							SPELL_SHIELD_MENU_OPTIONS.push_back(tSpellName);
						}
					}

					for (auto tSpellName : SPELL_SHIELDS)
					{
						if (eSpell.Name != nullptr && std::string{ eSpell.Name }.find(tSpellName.first) != std::string::npos)
						{
							SPELL_SHIELD_MENU_OPTIONS.push_back(tSpellName.first);
						}
					}
				}
			}
		}
	}
}


bool SpellShieldManager::CanShieldSpell(std::string spellName)
{
	if (!spellName.empty() && isSpellShieldEnabled()) {
		for (auto loadedOption : SPELL_SHIELD_MENU_OPTIONS)
		{
			if (loadedOption.find(spellName) != std::string::npos)
			{
				if (isSpellShieldEnabledForSpell(loadedOption))
				{
					return true;
				}
			}
		}
	}

	return false;
}


bool SpellShieldManager::isSpellShieldEnabledForSpell(std::string option)
{
	return EndeavourusMiscUtils::GetBoolSetting(SPELL_SHIELD_SPELL + "_" + option, true);
}

bool SpellShieldManager::isSpellShieldEnabled()
{
	return EndeavourusMiscUtils::GetBoolSetting(ENABLE_SPELL_SHIELD, true);
}
#include "stdafx.h"
#include "SpellShieldManager.h"
#include <SDK Extensions.h>
#include "EndeavourusUtils.h"
#include "SpellShieldConstants.h"

std::vector<std::string> SpellShieldManager::SPELL_SHIELD_MENU_OPTIONS{};
std::vector<std::string> SpellShieldManager::TARGETED_SPELLS{
		"Headbutt", "Frostbite", "Disintegrate", "AkaliMota", "AkaliShadowDance", "BrandE", "BrandR", "BraumE", "BraumRWrapper", "CaitlynAceintheHole",
		"CamilleR", "CassiopeiaE", "Feast", "DariusNoxianTacticsONH", "DianaTeleport", "EliseHumanQ", "EvelynnW", "EvelynnE", "FiddlesticksDarkWind",
		"Terrify", "Drain", "FizzQ", "GangplankQWrapper", "GarenQ", "IreliaGatotsu", "IreliaEquilibriumStrike", "SowTheWind", "JarvanIVCataclysm",
		"JaxLeapStrike", "JayceThunderingBlow", "JayceToTheSkies", "KarmaSpiritBind", "JudicatorReckoning", "KaynR", "NullLance", "KatarinaQ",
		"KatarinaEWrapper", "KhazixQ", "KhazixQLong", "LeblancQ", "BlindMonkRKick", "LissandraR", "LucianQ", "LuluE", "Whimsy", "LuluW", "SeismicShard",
		"MalzaharE", "MalzaharR", "MaokaiW", "AlphaStrike", "MissFortuneRicochetShot", "MordekaiserChildrenOfTheGrave", "NamiW", "NasusW",
		"NautilusGrandLine", "NautilusAnchorDrag", "NocturneUnspeakableHorror", "IceBlast", "OlafRecklessStrike", "PantheonQ",
		"PantheonW", "PoppyE", "QuinnE", "RekSaiE", "RekSaiRWrapper", "PuncturingTaunt", "RyzeW", "RyzeE", "TwoShivPoison", "Fling", "SkarnerImpale",
		"SwainTorment", "SyndraR", "TahmKenchW", "TalonQ", "BlindingDart", "TristanaE", "TristanaR", "TrundlePain", "VayneCondemn", "VeigarR", "ViR",
		"ViktorPowerTransfer", "VladimirQ", "VolibearW", "WarwickQ", "WarwickR", "MonkeyKingNimbus", "XinZhaoE", "YasuoDashWrapper", "ZedR", "TimeWarp" };

bool SpellShieldManager::Init()
{
	LoadCurrentSupportedInGameSpells();
}

void SpellShieldManager::LoadCurrentSupportedInGameSpells()
{
	for (auto enemy : pSDK->EntityManager->GetEnemyHeroes())
	{
		for (auto eSpell : enemy.second->GetSpells())
		{
			for (auto tSpellName : TARGETED_SPELLS)
			{
				if (std::string{ eSpell.Name }.find(tSpellName) != std::string::npos)
				{
					SPELL_SHIELD_MENU_OPTIONS.push_back(tSpellName);
				}
			}
		}
	}
}

bool SpellShieldManager::CanShieldSpell()
{
	if (isSpellShieldEnabled()) {
		for (auto enemy : pSDK->EntityManager->GetEnemyHeroes())
		{
			if (enemy.second != nullptr && enemy.second->IsHero() && enemy.second->GetCurrentTarget() == Player.PTR() && Player.IsAlive() && Player.IsValid())
			{
				std::string activeSpellName = std::string{ enemy.second->GetActiveSpell().SpellCast.Spell.Name };
				Game::PrintChat(activeSpellName);
				for (auto loadedOption : SPELL_SHIELD_MENU_OPTIONS)
				{
					if (loadedOption.find(activeSpellName) != std::string::npos)
					{
						if (isSpellShieldEnabledForSpell(loadedOption))
						{
							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

bool SpellShieldManager::isSpellShieldEnabledForSpell(std::string option)
{
	return EndeavourusUtils::GetBoolSetting(SPELL_SHIELD_SPELL + "_" + option, true);
}

bool SpellShieldManager::isSpellShieldEnabled()
{
	return EndeavourusUtils::GetBoolSetting(ENABLE_SPELL_SHIELD, true);
}
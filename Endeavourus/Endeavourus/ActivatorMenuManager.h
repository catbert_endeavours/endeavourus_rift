#pragma once
#include "SDK Extensions.h" 
#include <vector> 

class ActivatorMenuManager
{
public:
	static void Init();
	static void HextechProtobeltGLP800();
	static void HextechGunblade();
	static void Spellbinder();
	static void EdgeOfNight();
	static void TwinShadows();
	static void Tiamat();
	static void BilgewaterCutlassBOTRK();
	static void YoumuusGhostblade();
	static void ArchangelsStaff();
	static void ZhonyasHourglass();
	static void ShurelyasReverie();
	static void QuicksilverSashMercurialScimitar();
	static void MikaelsCrucible();
	static void LocketoftheIronSolari();
	static void GargoyleStoneplate();
	static void Redemption();
	static void Ohmwrecker();
	static void RighteousGlory();
	static void DefensiveItems();
	static void OffensiveItems();
	static void Items();
	static void Ignite();
	static void Exhaust();
	static void Cleanse();
	static void Heal();
	static void Barrier();
	static void SummonerSpells();
	static void ActivatorSettings();
	static void DrawActivatorSettings();
	static void BlacklistSettings();

	static void AllyBlacklistSettings();

	static std::vector<std::string> ENEMY_NAMES;
	static std::vector<std::string> ALLY_NAMES;
};


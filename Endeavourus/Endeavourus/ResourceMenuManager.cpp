#include "stdafx.h"
#include "ResourceMenuManager.h"
#include "SDK Extensions.h"
#include "EndeavourusMiscUtils.h"
#include "LogicConstants.h"
#include "GeneralConstants.h"
#include "BaseMenuManager.h"
#include "BaseSettings.h"
#include "EndeavourusMenu.h"

Resource ResourceMenuManager::comboResourceSettings{};
Resource ResourceMenuManager::harassResourceSettings{};
Resource ResourceMenuManager::clearResourceSettings{};
Resource ResourceMenuManager::lastHitResourceSettings{};
Resource ResourceMenuManager::escapeResourceSettings{};
Resource ResourceMenuManager::ksResourceSettings{};

std::vector<std::string> predictionDropDownOptions{ "Low", "Medium", "High", "Very High", "Immobile" };

std::vector<std::string> enemyNames{};
std::string blacklistSettingToPopulate{};
SpellSlot spell{};

void ResourceMenuManager::DrawAllEnabledLogicModeSettings()
{
	DrawComboSettings();
	DrawHarassSettings();
	DrawClearSettings();
	DrawLastHitSettings();
	DrawEscapeSettings();
	DrawKsSettings();
}

void ResourceMenuManager::DrawLogicSettings()
{
	EndeavourusMenu::Tree(BaseMenuManager::GetUnsupportedChampionLoaded() ? "Unsupported Champion" : "Spell Management", EndeavourusMiscUtils::GetUniqueSettingName(CHAMP_SETTINGS), false, DrawAllEnabledLogicModeSettings);

	if (!BaseMenuManager::GetUnsupportedChampionLoaded() && enemyNames.size() == 0)
	{
		for (auto enemy : pSDK->EntityManager->GetEnemyHeroes())
		{
			if (enemy.second != nullptr)
			{
				enemyNames.emplace_back(enemy.second->GetCharName());
			}
		}
	}

	if (BaseMenuManager::specificChampLogic != nullptr)
	{
		//((void(*)())BaseMenuManager::specificChampLogic)();
		static_cast<void*(*)()>(BaseMenuManager::specificChampLogic)();
	}
}

void ResourceMenuManager::DrawComboSettings()
{
	if (comboResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("Combo Settings", EndeavourusMiscUtils::GetUniqueSettingName(COMBO_LOGIC_SETTINGS), false, DrawComboLogicSettings);
	}
}

void ResourceMenuManager::DrawHarassSettings()
{
	if (harassResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("Harass Settings", EndeavourusMiscUtils::GetUniqueSettingName(HARASS_LOGIC_SETTINGS), false, DrawHarassLogicSettings);
	}
}

void ResourceMenuManager::DrawClearSettings()
{
	if (clearResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("Clear Settings", EndeavourusMiscUtils::GetUniqueSettingName(CLEAR_LOGIC_SETTINGS), false, DrawClearLogicSettings);
	}
}

void ResourceMenuManager::DrawLastHitSettings()
{
	if (lastHitResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("LastHit Settings", EndeavourusMiscUtils::GetUniqueSettingName(LAST_HIT_LOGIC_SETTINGS), false, DrawLastHitLogicSettings);
	}
}

void ResourceMenuManager::DrawEscapeSettings()
{
	if (escapeResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("Escape Settings", EndeavourusMiscUtils::GetUniqueSettingName(ESCAPE_LOGIC_SETTINGS), false, DrawEscapeLogicSettings);
	}
}

void ResourceMenuManager::DrawKsSettings()
{
	if (ksResourceSettings.isResourceEnabled()) {
		EndeavourusMenu::Tree("KS Settings", EndeavourusMiscUtils::GetUniqueSettingName(KS_LOGIC_SETTINGS), false, DrawKsLogicSettings);
	}
}

void ResourceMenuManager::DrawComboLogicSettings()
{
	if (comboResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Enable Q in Combo", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_COMBO), comboResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_COMBO_RESOURCE), BaseSettings::getComboQResourceLimit(comboResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (comboResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_COMBO_PREDICTION_LEVEL), predictionDropDownOptions, comboResourceSettings.getQPredictionMode());
		}

		if (comboResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_COMBO;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q Combo Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_COMBO_GROUP), false, BlacklistSettings);
		}
	}

	if (comboResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Enable W in Combo", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_COMBO), comboResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_COMBO_RESOURCE), BaseSettings::getComboWResourceLimit(comboResourceSettings.getDefaultWMinResourceValue()), 0, 100);
		if (comboResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_COMBO_PREDICTION_LEVEL), predictionDropDownOptions, comboResourceSettings.getWPredictionMode());
		}

		if (comboResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_COMBO;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W Combo Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_COMBO_GROUP), false, BlacklistSettings);
		}
	}

	if (comboResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Enable E in Combo", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_COMBO), comboResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_COMBO_RESOURCE), BaseSettings::getComboEResourceLimit(comboResourceSettings.getDefaultEMinResourceValue()), 0, 100);

		if (comboResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_COMBO_PREDICTION_LEVEL), predictionDropDownOptions, comboResourceSettings.getEPredictionMode());
		}

		if (comboResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_COMBO;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E Combo Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_COMBO_GROUP), false, BlacklistSettings);
		}
	}

	if (comboResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Enable R in Combo", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_COMBO), comboResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_COMBO_RESOURCE), BaseSettings::getComboRResourceLimit(comboResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (comboResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_COMBO_PREDICTION_LEVEL), predictionDropDownOptions, comboResourceSettings.getRPredictionMode());
		}

		if (comboResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_COMBO;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R Combo Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_COMBO_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::DrawHarassLogicSettings()
{
	if (harassResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Enable Q in Harass", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_HARASS), harassResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_HARASS_RESOURCE), BaseSettings::getHarassQResourceLimit(harassResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (harassResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_HARASS_PREDICTION_LEVEL), predictionDropDownOptions, harassResourceSettings.getQPredictionMode());
		}

		if (harassResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_HARASS;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q Harass Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_HARASS_GROUP), false, BlacklistSettings);
		}
	}

	if (harassResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Enable W in Harass", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_HARASS), harassResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_HARASS_RESOURCE), BaseSettings::getHarassWResourceLimit(harassResourceSettings.getDefaultWMinResourceValue()), 0, 100);

		if (harassResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_HARASS_PREDICTION_LEVEL), predictionDropDownOptions, harassResourceSettings.getWPredictionMode());
		}

		if (harassResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_HARASS;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W Harass Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_HARASS_GROUP), false, BlacklistSettings);
		}
	}

	if (harassResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Enable E in Harass", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_HARASS), harassResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_HARASS_RESOURCE), BaseSettings::getHarassEResourceLimit(harassResourceSettings.getDefaultEMinResourceValue()), 0, 100);

		if (harassResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_HARASS_PREDICTION_LEVEL), predictionDropDownOptions, harassResourceSettings.getEPredictionMode());
		}

		if (harassResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_HARASS;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E Harass Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_HARASS_GROUP), false, BlacklistSettings);
		}
	}

	if (harassResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Enable R in Harass", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_HARASS), harassResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_HARASS_RESOURCE), BaseSettings::getHarassRResourceLimit(harassResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (harassResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_HARASS_PREDICTION_LEVEL), predictionDropDownOptions, harassResourceSettings.getRPredictionMode());
		}

		if (harassResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_HARASS;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R Harass Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_HARASS_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::DrawClearLogicSettings()
{
	if (clearResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Enable Q in LaneClear/Jungle", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_CLEAR), clearResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_CLEAR_RESOURCE), BaseSettings::getClearQResourceLimit(clearResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (clearResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_CLEAR_PREDICTION_LEVEL), predictionDropDownOptions, clearResourceSettings.getQPredictionMode());
		}

		if (clearResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_CLEAR;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q Clear Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_CLEAR_GROUP), false, BlacklistSettings);
		}
	}

	if (clearResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Enable W in LaneClear/Jungle", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_CLEAR), clearResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_CLEAR_RESOURCE), BaseSettings::getClearWResourceLimit(clearResourceSettings.getDefaultWMinResourceValue()), 0, 100);

		if (clearResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_CLEAR_PREDICTION_LEVEL), predictionDropDownOptions, clearResourceSettings.getWPredictionMode());
		}

		if (clearResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_CLEAR;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W Clear Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_CLEAR_GROUP), false, BlacklistSettings);
		}
	}

	if (clearResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Enable E in LaneClear/Jungle", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_CLEAR), clearResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_CLEAR_RESOURCE), BaseSettings::getClearEResourceLimit(clearResourceSettings.getDefaultEMinResourceValue()) , 0, 100);

		if (clearResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_CLEAR_PREDICTION_LEVEL), predictionDropDownOptions, clearResourceSettings.getEPredictionMode());
		}

		if (clearResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_CLEAR;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E Clear Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_CLEAR_GROUP), false, BlacklistSettings);
		}
	}

	if (clearResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Enable R in LaneClear/Jungle", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_CLEAR), clearResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_CLEAR_RESOURCE), BaseSettings::getClearRResourceLimit(clearResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (clearResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_CLEAR_PREDICTION_LEVEL), predictionDropDownOptions, clearResourceSettings.getRPredictionMode());
		}

		if (clearResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_CLEAR;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R Clear Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_CLEAR_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::DrawLastHitLogicSettings()
{
	if (lastHitResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Enable Q in Lasthit", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_LH), lastHitResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_LH_RESOURCE), BaseSettings::getLHQResourceLimit(lastHitResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (lastHitResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_LH_PREDICTION_LEVEL), predictionDropDownOptions, lastHitResourceSettings.getQPredictionMode());
		}

		if (lastHitResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_LH;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q LastHit Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_LH_GROUP), false, BlacklistSettings);
		}
	}

	if (lastHitResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Enable W in Lasthit", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_LH), lastHitResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_LH_RESOURCE), BaseSettings::getLHWResourceLimit(lastHitResourceSettings.getDefaultWMinResourceValue()), 0, 100);

		if (lastHitResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_LH_PREDICTION_LEVEL), predictionDropDownOptions, lastHitResourceSettings.getWPredictionMode());
		}

		if (lastHitResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_LH;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W LastHit Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_LH_GROUP), false, BlacklistSettings);
		}
	}

	if (lastHitResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Enable E in Lasthit", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_LH), lastHitResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_LH_RESOURCE), BaseSettings::getLHEResourceLimit(lastHitResourceSettings.getDefaultEMinResourceValue()), 0, 100);

		if (lastHitResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_LH_PREDICTION_LEVEL), predictionDropDownOptions, lastHitResourceSettings.getEPredictionMode());
		}

		if (lastHitResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_LH;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E LastHit Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_LH_GROUP), false, BlacklistSettings);
		}
	}

	if (lastHitResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Enable R in Lasthit", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_LH), lastHitResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_LH_RESOURCE), BaseSettings::getLHRResourceLimit(lastHitResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (lastHitResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_LH_PREDICTION_LEVEL), predictionDropDownOptions, lastHitResourceSettings.getRPredictionMode());
		}

		if (lastHitResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_LH;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R LastHit Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_LH_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::DrawEscapeLogicSettings()
{
	if (escapeResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Enable Q in Escape", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_ESCAPE), escapeResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_ESCAPE_RESOURCE), BaseSettings::getEscapeQResourceLimit(escapeResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (escapeResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_ESCAPE_PREDICTION_LEVEL), predictionDropDownOptions, escapeResourceSettings.getQPredictionMode());
		}

		if (escapeResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_ESCAPE;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q Escape Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_ESCAPE_GROUP), false, BlacklistSettings);
		}
	}

	if (escapeResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Enable W in Escape", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_ESCAPE), escapeResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_ESCAPE_RESOURCE), BaseSettings::getEscapeWResourceLimit(escapeResourceSettings.getDefaultWMinResourceValue()), 0, 100);

		if (escapeResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_ESCAPE_PREDICTION_LEVEL), predictionDropDownOptions, escapeResourceSettings.getWPredictionMode());
		}

		if (escapeResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_ESCAPE;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W Escape Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_ESCAPE_GROUP), false, BlacklistSettings);
		}
	}

	if (escapeResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Enable E in Escape", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_ESCAPE), escapeResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_ESCAPE_RESOURCE), BaseSettings::getEscapeEResourceLimit(escapeResourceSettings.getDefaultEMinResourceValue()), 0, 100);

		if (escapeResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_ESCAPE_PREDICTION_LEVEL), predictionDropDownOptions, escapeResourceSettings.getEPredictionMode());
		}

		if (escapeResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_ESCAPE;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E Escape Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_ESCAPE_GROUP), false, BlacklistSettings);
		}
	}

	if (escapeResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Enable R in Escape", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_ESCAPE), escapeResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_ESCAPE_RESOURCE), BaseSettings::getEscapeRResourceLimit(escapeResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (escapeResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_ESCAPE_PREDICTION_LEVEL), predictionDropDownOptions, escapeResourceSettings.getRPredictionMode());
		}

		if (escapeResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_ESCAPE;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R Escape Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_ESCAPE_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::DrawKsLogicSettings()
{
	if (ksResourceSettings.isQEnabled())
	{
		EndeavourusMenu::Checkbox("Auto KS with Q", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_KS), ksResourceSettings.isQDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast Q Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_Q_KS_RESOURCE), BaseSettings::getKsQResourceLimit(ksResourceSettings.getDefaultQMinResourceValue()), 0, 100);

		if (ksResourceSettings.isQPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(Q_KS_PREDICTION_LEVEL), predictionDropDownOptions, ksResourceSettings.getQPredictionMode());
		}

		if (ksResourceSettings.isQBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_Q_KS;
			spell = SpellSlot::Q;
			EndeavourusMenu::Tree("Q KillSteal Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_Q_KS_GROUP), false, BlacklistSettings);
		}
	}

	if (ksResourceSettings.isWEnabled())
	{
		EndeavourusMenu::Checkbox("Auto KS with W", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_KS), ksResourceSettings.isWDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast W Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_W_KS_RESOURCE), BaseSettings::getKsWResourceLimit(ksResourceSettings.getDefaultWMinResourceValue()), 0, 100);

		if (ksResourceSettings.isWPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(W_KS_PREDICTION_LEVEL), predictionDropDownOptions, ksResourceSettings.getWPredictionMode());
		}

		if (ksResourceSettings.isWBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_W_KS;
			spell = SpellSlot::W;
			EndeavourusMenu::Tree("W KillSteal Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_W_KS_GROUP), false, BlacklistSettings);
		}
	}

	if (ksResourceSettings.isEEnabled())
	{
		EndeavourusMenu::Checkbox("Auto KS with E", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_KS), ksResourceSettings.isEDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast E Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_E_KS_RESOURCE), BaseSettings::getKsEResourceLimit(ksResourceSettings.getDefaultEMinResourceValue()), 0, 100);

		if (ksResourceSettings.isEPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(E_KS_PREDICTION_LEVEL), predictionDropDownOptions, ksResourceSettings.getEPredictionMode());
		}

		if (ksResourceSettings.isEBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_E_KS;
			spell = SpellSlot::E;
			EndeavourusMenu::Tree("E KillSteal Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_E_KS_GROUP), false, BlacklistSettings);
		}
	}

	if (ksResourceSettings.isREnabled())
	{
		EndeavourusMenu::Checkbox("Auto KS with R", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_KS), ksResourceSettings.isRDefaultValEnabled());
		EndeavourusMenu::SliderInt("Cast R Only if Resource is > %", EndeavourusMiscUtils::GetUniqueSettingName(ENABLE_R_KS_RESOURCE), BaseSettings::getKsRResourceLimit(ksResourceSettings.getDefaultRMinResourceValue()), 0, 100);

		if (ksResourceSettings.isRPredictionEnabled())
		{
			EndeavourusMenu::DropList("Select HitChance", EndeavourusMiscUtils::GetUniqueSettingName(R_KS_PREDICTION_LEVEL), predictionDropDownOptions, ksResourceSettings.getRPredictionMode());
		}

		if (ksResourceSettings.isRBlockerEnabled())
		{
			blacklistSettingToPopulate = ENABLE_CHAMPION_BLOCK_R_KS;
			spell = SpellSlot::R;
			EndeavourusMenu::Tree("R KillSteal Blacklist Settings", EndeavourusMiscUtils::GetUniqueSettingName(CHAMPION_BLOCK_R_KS_GROUP), false, BlacklistSettings);
		}
	}
}

void ResourceMenuManager::BlacklistSettings()
{
	for (auto enemyName : enemyNames)
	{
		switch (spell)
		{
		case SpellSlot::Q:
		{
			EndeavourusMenu::Checkbox(("Block Q on: " + enemyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(blacklistSettingToPopulate + "_" + enemyName), false);
		}
		break;

		case SpellSlot::W:
		{
			EndeavourusMenu::Checkbox(("Block W on: " + enemyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(blacklistSettingToPopulate + "_" + enemyName), false);
		}
		break;

		case SpellSlot::E:
		{
			EndeavourusMenu::Checkbox(("Block E on: " + enemyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(blacklistSettingToPopulate + "_" + enemyName), false);
		}
		break;

		case SpellSlot::R:
		{
			EndeavourusMenu::Checkbox(("Block R on: " + enemyName).c_str(), EndeavourusMiscUtils::GetUniqueSettingName(blacklistSettingToPopulate + "_" + enemyName), false);
		}
		break;

		default:
			break;
		}
	}
}